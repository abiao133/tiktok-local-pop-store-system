package com.qingyun.quartz.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.quartz.domain.SysJob;

/**
 * 调度任务信息 数据层
 *
 * @author ruoyi
 */
public interface SysJobMapper extends BaseMapperPlus<SysJob> {

}
