package com.qingyun.quartz.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.quartz.domain.SysJobLog;

/**
 * 调度任务日志信息 数据层
 *
 * @author ruoyi
 */
public interface SysJobLogMapper extends BaseMapperPlus<SysJobLog> {

}
