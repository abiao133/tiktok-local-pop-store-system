package com.qingyun.quartz.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.quartz.domain.TiktokPushTask;

/**
 * 活动视频发送任务Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokPushTaskMapper extends BaseMapperPlus<TiktokPushTask> {

}
