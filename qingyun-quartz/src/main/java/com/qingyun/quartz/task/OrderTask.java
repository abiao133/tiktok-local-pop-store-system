package com.qingyun.quartz.task;

import com.qingyun.shop.domain.TiktokOrder;
import com.qingyun.shop.service.ITiktokOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Classname OrderTask
 * @Author dyh
 * @Date 2021/10/11 16:37
 */
@Component("orderTask")
public class OrderTask {

	@Autowired
	private ITiktokOrderService orderService;

	/**
	 * 定时查看未支付的订单让其过期
	 */
	@Transactional(rollbackFor = Exception.class)
	public void checkOrder(){
		List<TiktokOrder> orders = orderService.selectInvalidOrder(new Date());
		orders.forEach((order)->{
			order.setStatus(2);
			order.setCloseTime(new Date());
		});
		orderService.updateBatchById(orders);
	}

}
