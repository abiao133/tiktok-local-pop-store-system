package com.qingyun.quartz.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.quartz.domain.TiktokPushTask;
import com.qingyun.quartz.domain.bo.TiktokPushTaskBo;
import com.qingyun.quartz.domain.vo.TiktokPushTaskVo;

import java.util.Collection;
import java.util.List;

/**
 * 活动视频发送任务Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokPushTaskService extends IServicePlus<TiktokPushTask, TiktokPushTaskVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokPushTaskVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokPushTaskVo> queryPageList(TiktokPushTaskBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokPushTaskVo> queryList(TiktokPushTaskBo bo);

	/**
	 * 根据新增业务对象插入活动视频发送任务
	 * @param bo 活动视频发送任务新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokPushTaskBo bo);

	/**
	 * 根据编辑业务对象修改活动视频发送任务
	 * @param bo 活动视频发送任务编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokPushTaskBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
