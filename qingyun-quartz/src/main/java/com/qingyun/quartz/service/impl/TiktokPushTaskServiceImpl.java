package com.qingyun.quartz.service.impl;

import cn.hutool.core.bean.BeanUtil;
    import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.quartz.domain.TiktokPushTask;
import com.qingyun.quartz.domain.bo.TiktokPushTaskBo;
import com.qingyun.quartz.domain.vo.TiktokPushTaskVo;
import com.qingyun.quartz.mapper.TiktokPushTaskMapper;
import com.qingyun.quartz.service.ITiktokPushTaskService;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 活动视频发送任务Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokPushTaskServiceImpl extends ServicePlusImpl<TiktokPushTaskMapper, TiktokPushTask, TiktokPushTaskVo> implements ITiktokPushTaskService {

    @Override
    public TiktokPushTaskVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokPushTaskVo> queryPageList(TiktokPushTaskBo bo) {
        PagePlus<TiktokPushTask, TiktokPushTaskVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokPushTaskVo> queryList(TiktokPushTaskBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokPushTask> buildQueryWrapper(TiktokPushTaskBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokPushTask> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, TiktokPushTask::getUserId, bo.getUserId());
        lqw.eq(bo.getStatus() != null, TiktokPushTask::getStatus, bo.getStatus());
        lqw.eq(bo.getActivityId() != null, TiktokPushTask::getActivityId, bo.getActivityId());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokPushTaskBo bo) {
        TiktokPushTask add = BeanUtil.toBean(bo, TiktokPushTask.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokPushTaskBo bo) {
        TiktokPushTask update = BeanUtil.toBean(bo, TiktokPushTask.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokPushTask entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
