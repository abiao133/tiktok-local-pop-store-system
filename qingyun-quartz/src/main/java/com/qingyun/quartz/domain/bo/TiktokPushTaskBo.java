package com.qingyun.quartz.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 活动视频发送任务业务对象 tiktok_push_task
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("活动视频发送任务业务对象")
public class TiktokPushTaskBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 任务状态 0 待执行 1执行成功 2执行失败
     */
    @ApiModelProperty(value = "任务状态 0 待执行 1执行成功 2执行失败", required = true)
    @NotNull(message = "任务状态 0 待执行 1执行成功 2执行失败不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 活动id
     */
    @ApiModelProperty(value = "活动id", required = true)
    @NotNull(message = "活动id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activityId;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
