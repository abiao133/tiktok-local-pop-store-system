package com.qingyun.quartz.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 活动视频发送任务视图对象 tiktok_push_task
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("活动视频发送任务视图对象")
@ExcelIgnoreUnannotated
public class TiktokPushTaskVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 用户id
     */
	@ExcelProperty(value = "用户id")
	@ApiModelProperty("用户id")
	private Long userId;

    /**
     * 任务状态 0 待执行 1执行成功 2执行失败
     */
	@ExcelProperty(value = "任务状态 0 待执行 1执行成功 2执行失败")
	@ApiModelProperty("任务状态 0 待执行 1执行成功 2执行失败")
	private Integer status;

    /**
     * 活动id
     */
	@ExcelProperty(value = "活动id")
	@ApiModelProperty("活动id")
	private Long activityId;


}
