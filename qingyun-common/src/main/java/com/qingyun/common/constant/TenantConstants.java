package com.qingyun.common.constant;

/**
 * @Classname TenantConstants
 * @Author dyh
 * @Date 2021/5/20 18:34
 */
public class TenantConstants {

    /**
     * 未加入企业
     */
    public final static Integer UNASSIGNED = 0;

    /**
     * 普通用户
     */
    public final static Integer GENERAL = 1;

    /**
     * 企业管理员
     */
    public final static Integer MANAGE = 2;

    /**
     * 通用开启状态
     */
    public final static Integer ENABLE = 0;

    /**
     * 通用关闭状态
     */
    public final static Integer DISABLE = 1;

    /**
     * 男
     */
    public final static Integer MALE = 0;

    /**
     * 女
     */
    public final static Integer GIRL = 1;

    /**
     * 未知
     */
    public final static  Integer UNKNOW = 2 ;

    /**
     * 未认证
     */
    public final static  Integer NOTAUTHO = 0;

    /**
     * 已认证
     */
   public final static  Integer AUTHO = 1;

    /**
     * 通用待审核
     */
   public final static Integer NOTCHECK = 0;

    /**
     * 通用已审核
     */
   public final static Integer ALREADYCHECK= 1 ;

    /**
     * 通用审核驳回
     */
   public final static Integer OVERRULE = 2;

    /**
     * 微信公众
     */
   public final static Integer WXMP = 1;

    /**
     * 微信小程序
     */
   public final static Integer WXMINIAPP = 2;






}
