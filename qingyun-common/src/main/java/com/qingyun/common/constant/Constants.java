package com.qingyun.common.constant;

import io.jsonwebtoken.Claims;
import org.springframework.boot.actuate.autoconfigure.scheduling.ScheduledTasksEndpointAutoConfiguration;

/**
 * 通用常量信息
 *
 * @author qingyun
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

	/**
	 * 管理端令牌key
	 */
	public static final String SYS_USER_KEY="sys_user_key";

	/**
	 * 平台微信用户令牌key
	 */
	public static final String APP_USER_KEY="app_user_key";

	/**
	 * 抖音用户令牌key
	 */
	public static final String TIKTOK_USER_KEY="tiktok_user_key";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

	/**
	 * 第一次登录标识
	 */
	public static final String ISONELOGIN="isOneLogin";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = Claims.SUBJECT;

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

	/**
	 * RMI 远程方法调用
	 */
	public static final String LOOKUP_RMI = "rmi://";

	/**
	 * 系统（平台）用户
	 */
	public static final String SYS_USER = "00";

	/**
	 * 代理用户
	 */
	public static final String PROXY_USER = "01";

	/**
	 * 商家用户
	 */
	public static final String SHOP_USER = "02";

	/**
	 * 代理商
	 */
	public static final Long PROXY=0L;

	/**
	 * 商户
	 */
	public static final Long SHOP=1L;

	/**
	 * 活动参与次数 无限次
	 */
	public static final  Long JOINCOUNTSTATUS=0L;


	/**
	 * 待审核
	 */
	public static final  Long REVIEW=0L;


	/**
	 * 待上线
	 */
	public static final Long WAITONLINE=1L;


	/**
	 * 上线
	 */
	public static final  Long ONLINE=2L;

	/**
	 * 下线
	 */
	public  static  final  Long OFFLINE=3L;


	/**
	 * 资源映射路径 前缀
	 */
	public static final String RESOURCE_PREFIX = "/profile";

	/**
	 * 1直接发送 0领取优惠卷发送
	 */
	public static final String SEND_TYPE_COUPON="0";



   //纵向
	public static final Integer P1080PORTRAIT=0;
   //横向
	public static final Integer P1080TRANSVERSE=1;


}
