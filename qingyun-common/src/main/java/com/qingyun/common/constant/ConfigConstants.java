package com.qingyun.common.constant;

/**
 * 系统参数配置常量
 */
public final class ConfigConstants {
	/*
	订单超时偏移时间
	 */
	public static final String SYS_ORDER_PAY_TIME_OFFSET = "sys.order.payTimeOffset";
}
