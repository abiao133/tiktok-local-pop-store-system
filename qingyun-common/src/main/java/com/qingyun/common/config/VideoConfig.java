package com.qingyun.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname VideoConfig
 * @Author dyh
 * @Date 2021/9/30 14:46
 */
@Data
@Component
@ConfigurationProperties(prefix = "video")
public class VideoConfig {
	public Integer fragTime;
}
