package com.qingyun.common.config;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author ruoyi
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Component
@ConfigurationProperties(prefix = "qingyun")
public class QingYunConfig
{
    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

    /** 实例演示开关 */
    private boolean demoEnabled;

    /** 上传路径 */
    private static String profile;

    /**项目网址*/
    private  static String projectUrl;

    /** 获取地址开关 */
    @Getter
    private static boolean addressEnabled;

    public void setAddressEnabled(boolean addressEnabled)
    {
        QingYunConfig.addressEnabled = addressEnabled;
    }

    public void setProfile(String profile) {QingYunConfig.profile = profile;}

    /**
     * 获取下载路径
     */
    public static String getDownloadPath()
    {
        return getProfile() + "/download/";
    }

    public static String getProfile()
    {
        return profile;
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath()
    {
        return getProfile() + "/upload";
    }

	public static String getProjectUrl() {
		return projectUrl;
	}

	public void setProjectUrl(String projectUrl) {
		QingYunConfig.projectUrl = projectUrl;
	}


}
