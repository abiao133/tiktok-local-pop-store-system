package com.qingyun.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 企业用户对象 pd_entpris_user
 *
 * @author dangyonghang
 * @date 2021-05-13
 */
@Data
@NoArgsConstructor
@TableName("tenant_entpris_user")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TenantEntprisUser implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 身份证号码
     */
    private String idCard;

    /**
     * $column.columnComment
     */
    private String nickName;

    /**
     * 真实名称
     */
    private String name;
    @JsonIgnore
    /** $column.columnComment */
    private String passWord;

    /**
     * 用户类型 0普通用户 1企业超级管理员
     */
    private Integer userType;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别 0 男 1 女 2未知
     */
    private Integer sex;

    /**
     * 用户生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    /**
     * 登陆ip地址
     */
    private String loginIp;

    /**
     * $column.columnComment
     */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime loginDate;

    /**
     * 0启用 1 禁用
     */
    private Integer status;

    /**
     * 逻辑删除 0 未删除 1已删除
     */
    @TableLogic
    private Integer delFlag;

    /**
     * 当前登陆企业id
     */
    @TableField(exist = false)
    private Long entprisId;


    /**
     * 所属部门id
     */
    @TableField(exist = false)
    private Long deptId;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    public boolean isAdmin() {
        return userType.equals(2);
    }

    /**
     * 备注
     */
    private String remark;

    //加入的企业
    @TableField(exist = false)
    private List<TenantEnterprise> enterprise;

    @TableField(exist = false)
    private TenantDeptDto dept;

    @TableField(exist = false)
    private List<TenantRole> roles;

    @TableField(exist = false)
    private String entprisName;

}
