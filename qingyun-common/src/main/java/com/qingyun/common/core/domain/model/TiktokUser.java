package com.qingyun.common.core.domain.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 【请填写功能名称】对象 tiktok_user
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_user")
public class TiktokUser implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 用户id
     */
    @TableId(value = "id")
    private Long id;



    /**
     * 昵称
     */
    private String nickName;

    /**
     * openid
     */
    private String openId;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别（0男 1女 2未知）
     */
    private String gender;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 国家
     */
    private String country;

    /**
     * 通用unionid
     */
    private String unionId;

    /**
     * 会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员
     */
    private Integer memberLevel;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 0正常1禁用
     */
    private Integer state;

}
