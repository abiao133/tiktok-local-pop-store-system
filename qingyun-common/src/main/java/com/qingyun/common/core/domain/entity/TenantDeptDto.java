package com.qingyun.common.core.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Classname PdDeptDto
 * @Author dyh
 * @Date 2021/7/5 17:33
 */
@Data
public class TenantDeptDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 父部门id
     */
    private Long parentId;

    /**
     * 是否是门店 0 不是 1 是
     */
    private Integer shop;

    /**
     * 部门名称
     */
    private String deptName;

}
