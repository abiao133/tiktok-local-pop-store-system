package com.qingyun.common.core.domain.model;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * 代理组织视图对象 tiktok_proxy
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("代理组织视图对象")
@ExcelIgnoreUnannotated
@NoArgsConstructor
public class TiktokProxyVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
     *  代理商id
     */
	@ApiModelProperty("代理商id")
	private Long id;

    /**
     * 父id
     */
	@ExcelProperty(value = "父id")
	@ApiModelProperty("父id")
	private Long parentId;

    /**
     * 祖级列表
     */
	@ExcelProperty(value = "祖级列表")
	@ApiModelProperty("祖级列表")
	private String ancestors;

    /**
     * 代理商名称
     */
	@ExcelProperty(value = "代理商名称")
	@ApiModelProperty("代理商名称")
	private String name;



	@ApiModelProperty("代理商名称")
	private String lable;

    /**
     * 显示顺序
     */
	@ExcelProperty(value = "显示顺序")
	@ApiModelProperty("显示顺序")
	private Integer orderNum;

    /**
     * 负责人
     */
	@ExcelProperty(value = "负责人")
	@ApiModelProperty("负责人")
	private String leader;

    /**
     * 代理商手机号码
     */
	@ExcelProperty(value = "代理商手机号码")
	@ApiModelProperty("代理商手机号码")
	private String phone;

    /**
     * 代理商邮箱
     */
	@ExcelProperty(value = "代理商邮箱")
	@ApiModelProperty("代理商邮箱")
	private String email;

    /**
     * 数量
     */
	@ExcelProperty(value = "数量")
	@ApiModelProperty("数量")
	private Long videoCount;

	@ApiModelProperty(value = "总余额")
	private BigDecimal totalBalance;

	@ApiModelProperty(value = "可提现余额")
	private BigDecimal usableBalance;

	@ApiModelProperty(value = "冻结余额")
	private BigDecimal freezeBalance;

    /**
     * 代理商状态 0启用 1禁用
     */
	@ExcelProperty(value = "代理商状态 0启用 1禁用")
	@ApiModelProperty("代理商状态 0启用 1禁用")
	private Integer status;

	@ApiModelProperty("代理商商户头像")
	private String shopPic;


	@ApiModelProperty("子数据")
	private List<TiktokProxyVo> children=new ArrayList<>();

}
