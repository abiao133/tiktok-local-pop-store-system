package com.qingyun.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 企业 对象 pd_enterprise
 *
 * @author dangyonghang
 * @date 2021-05-13
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tenant_enterprise")
public class TenantEnterprise implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
        private Long id;

    /** 企业名称 */
        private String name;

    /** 企业编码 */
        private String code;

    /** 运营城市 */
        private String operatingCity;

    /** 微信openid */
        private String openid;

    /** 开放平台id */
        private String unionid;

    /** 省 */
        private String province;

    /** 市 */
        private String city;

    /** 区 */
        private String area;

    /** 详情地址 */
        private String address;

    /** 认证状态 0未认证 1认证 */
        private Integer authoState;

    /** 品牌名称 */
        private String brandName;

    /** 企业简介 */
        private String brief;

    /** 企业介绍 */
        @TableField("`desc`")
        private String desc;

    /** 网址 */
        private String website;

    /** 审核状态 0 待审核 1 审核通过 2审核不通过 */
        private Integer status;

    /** 管理员_id */
        private Long adminId;

    /** 管理员名称 */
        private String adminName;

    /** 用户手机号码 */
        private String adminPhone;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT)

            @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT_UPDATE)

            @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT)
        private String createBy;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT_UPDATE)
        private String updateBy;

}
