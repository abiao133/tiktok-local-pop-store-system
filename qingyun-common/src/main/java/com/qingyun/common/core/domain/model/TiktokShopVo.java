package com.qingyun.common.core.domain.model;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 商家视图对象 tiktok_shop
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("商家视图对象")
@ExcelIgnoreUnannotated
@NoArgsConstructor
public class TiktokShopVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
     *  id
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 代理商id -1为平台
     */
	@ExcelProperty(value = "代理商id -1为平台")
	@ApiModelProperty("代理商id -1为平台")
	private Long proxyId;

    /**
     * 代理商名称
     */
	@ExcelProperty(value = "代理商名称")
	@ApiModelProperty("代理商名称")
	private String proxyName;

	/**
	 * 商家头像
	 */
	@ExcelProperty(value = "商家头像")
	@ApiModelProperty("商家头像")
	private String pic;

    /**
     * 祖级列表
     */
	@ExcelProperty(value = "祖级列表")
	@ApiModelProperty("祖级列表")
	private String ancestors;


	@ApiModelProperty(value = "是否是代理商")
	private Integer isProxy;

	/**
     * 商家名称
     */
	@ExcelProperty(value = "商家名称")
	@ApiModelProperty("商家名称")
	private String name;

    /**
     * 营业时间
     */
	@ExcelProperty(value = "营业时间")
	@ApiModelProperty("营业时间")
	private String businessTime;

    /**
     * 热度
     */
	@ExcelProperty(value = "热度")
	@ApiModelProperty("热度")
	private String heat;

    /**
     * 负责人
     */
	@ExcelProperty(value = "负责人")
	@ApiModelProperty("负责人")
	private String leader;

    /**
     * 显示顺序
     */
	@ExcelProperty(value = "显示顺序")
	@ApiModelProperty("显示顺序")
	private Integer orderNum;

    /**
     * 商户手机号码
     */
	@ExcelProperty(value = "商户手机号码")
	@ApiModelProperty("商户手机号码")
	private String phone;

    /**
     * 商户邮箱
     */
	@ExcelProperty(value = "商户邮箱")
	@ApiModelProperty("商户邮箱")
	private String email;

    /**
     * 商户余额
     */
	@ExcelProperty(value = "商户余额")
	@ApiModelProperty("商户余额")
	private Long videoCount;

    /**
     * 商户状态 0启用 1禁用
     */
	@ExcelProperty(value = "商户状态 0启用 1禁用")
	@ApiModelProperty("商户状态 0启用 1禁用")
	private Integer status;

	@ApiModelProperty("创建时间")
	private Date createTime;
}
