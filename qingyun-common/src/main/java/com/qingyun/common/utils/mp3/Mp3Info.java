package com.qingyun.common.utils.mp3;

import lombok.Data;

@Data
public class Mp3Info {
	//音乐名
	private String songName;
	//歌手名
	private String singerName;
	//专辑名
	private String album;
	//时长(秒)
	private long duration;

	public Mp3Info(String songName, String singerName, String album, long duration) {
		this.songName = songName;
		this.singerName = singerName;
		this.album = album;
		this.duration = duration;
	}
}
