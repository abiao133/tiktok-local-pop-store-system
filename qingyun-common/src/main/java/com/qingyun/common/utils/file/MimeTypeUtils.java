package com.qingyun.common.utils.file;

import javax.validation.OverridesAttribute;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 媒体类型工具类
 *
 * @author ruoyi
 */
public class MimeTypeUtils
{
    public static final String IMAGE_PNG = "image/png";

    public static final String IMAGE_JPG = "image/jpg";

    public static final String IMAGE_JPEG = "image/jpeg";

    public static final String IMAGE_BMP = "image/bmp";

    public static final String IMAGE_GIF = "image/gif";

    public static final String[] IMAGE_EXTENSION = { "bmp", "gif", "jpg", "jpeg", "png" };

    public static final String[] FLASH_EXTENSION = { "swf", "flv" };

    public static final String[] MP3 = {"MP3","mp3"};

    public static final String[] MEDIA_EXTENSION = { "swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg",
            "asf", "rm", "rmvb","mp4" };

    public static final String[] DEFAULT_ALLOWED_EXTENSION = {
            // 图片
            "bmp", "gif", "jpg", "jpeg", "png",
            // word excel powerpoint
            "doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt",
            // 压缩文件
            "rar", "zip", "gz", "bz2",
            // pdf
            "pdf" };
    public static Map<String,Map<String,Integer>> RATE;

	static {
		Map<String, Map<String,Integer>> rate = new HashMap<>();
		Map<String, Integer> tar1 = new HashMap<>();
		tar1.put("width",720);
		tar1.put("height",1280);
		rate.put("9:16",tar1);
		Map<String, Integer> tar2 = new HashMap<>();
		tar2.put("width",1280);
		tar2.put("height",720);
		rate.put("16:9",tar2);
		Map<String, Integer> tar3 = new HashMap<>();
		tar3.put("width",720);
		tar3.put("height",1080);
		rate.put("6:10",tar3);
		Map<String, Integer> tar4 = new HashMap<>();
		tar4.put("width",1080);
		tar4.put("height",720);
		rate.put("10:6",tar4);
		RATE=rate;
	}

    public static String getExtension(String prefix)
    {
        switch (prefix)
        {
            case IMAGE_PNG:
                return "png";
            case IMAGE_JPG:
                return "jpg";
            case IMAGE_JPEG:
                return "jpeg";
            case IMAGE_BMP:
                return "bmp";
            case IMAGE_GIF:
                return "gif";
            default:
                return "";
        }
    }
}
