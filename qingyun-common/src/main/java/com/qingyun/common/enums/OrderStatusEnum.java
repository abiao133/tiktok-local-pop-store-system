package com.qingyun.common.enums;

public enum OrderStatusEnum {

	INIT(0, "初始化，未付款"),
	PAY_UP(	1, "已付款，未发货"),
	DELIVERED(2, "交易失败"),
	CONFIRMED(3, "订单超时");


	private final Integer value;
	private final String msg;

	OrderStatusEnum(Integer value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public Integer value() {
		return this.value;
	}

	public String msg() {
		return msg;
	}
}
