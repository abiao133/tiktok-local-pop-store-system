package com.qingyun.common.enums;

/**
 * 操作人类别
 *
 * @author qingyun
 */
public enum OperatorType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
