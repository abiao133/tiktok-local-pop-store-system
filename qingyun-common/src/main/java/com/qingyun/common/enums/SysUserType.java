package com.qingyun.common.enums;

import lombok.Data;

import java.util.Arrays;


public enum SysUserType {

	ADMIN("00"),
	PROXY("01"),
	SHOP("02");

	SysUserType(String type) {
		this.type = type;
	}

	private String type;

	public String getType() {
		return type;
	}

	public static SysUserType selectType(String key){
		SysUserType [] cmdTypes = SysUserType .values();
		SysUserType  result = Arrays.asList(cmdTypes).stream()
			.filter(alarmGrade -> alarmGrade.getType().equals(key))
			.findFirst().orElse(null);
		return result;
	}
}
