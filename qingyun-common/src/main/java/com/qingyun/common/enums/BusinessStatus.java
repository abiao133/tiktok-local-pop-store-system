package com.qingyun.common.enums;

/**
 * 操作状态
 *
 * @author qingyun
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
