
export default  {
  //bgm上传
  musicAction: process.env.VUE_APP_BASE_URL + "/resource/bgm/upload",
  //视频上传
  action: process.env.VUE_APP_BASE_URL + "/common/video/uploads",
  //图片
  oss: process.env.VUE_APP_BASE_URL + "/system/oss/upload",
  //线上api地址
  api:process.env.VUE_APP_BASE_URL

}
