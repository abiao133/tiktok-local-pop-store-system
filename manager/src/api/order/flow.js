import request from '@/utils/request'

// 查询订单流水列表
export function flowList(query) {
  return request({
    url: '/order/settlement/list',
    method: 'get',
    params: query
  })
}
