import request from '@/utils/request'

// 查询订单列表
export function list(query) {
  return request({
    url: '/order/list',
    method: 'get',
    params: query
  })
}
// 商品下单
export function orderAdd(data) {
  return request({
    url: '/order',
    method: 'post',
    data: data
  })
}
// 根据订单编号获取订单详细信息
export function orderInfo(id) {
  return request({
    url: '/order/detail/'+id,
    method: 'get'
  })
}
// 订单支付结果查询
export function getPayStatus(id) {
  return request({
    url: '/order/getPayStatus/'+id,
    method: 'get'
  })
}
