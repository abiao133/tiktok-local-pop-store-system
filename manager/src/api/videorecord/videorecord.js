import request from '@/utils/request'

// 查询视频发送列表
export function videolist(query) {
  return request({
    url: '/shop/video/send/list',
    method: 'get',
    params: query
  })
}
//查询详情数据列表
export function getvideolist(id) {
  return request({
    url: '/shop/video/send/' + id,
    method: 'get',
  })
}

// 抓取一次抖音视频最新数据
export function refreshVideoData(id) {
  return request({
    url: '/shop/video/send/refresh/' + id,
    method: 'get',
  })
}
