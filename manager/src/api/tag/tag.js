import request from '@/utils/request'

// 查询标签列表
export function listTag(query) {
  return request({
    url: '/resource/tag/list',
    method: 'get',
    params: query
  })
}

// 查询标签详细
export function getTag(tid) {
  return request({
    url: '/resource/tag/' + tid,
    method: 'get'
  })
}

// 新增标签
export function addTag(data) {
  return request({
    url: '/resource/tag/add',
    method: 'post',
    data: data
  })
}

// 修改标签
export function updateTag(data) {
  return request({
    url: '/resource/tag/edit',
    method: 'put',
    data: data
  })
}

// 删除标签
export function delTag(tId) {
  return request({
    url: '/resource/tag/' + tId,
    method: 'delete'
  })
}

// 上下架标签
export function nodifyStatus(params) {
  return request({
    url: '/resource/tag/nodifyStatus',
    method: 'post',
    params: params
  })
}

// 音乐标签
export function getSelectTags(query) {
  return request({
    url: '/resource/tag/getSelectTags',
    method: 'get',
    params: query
  })
}
