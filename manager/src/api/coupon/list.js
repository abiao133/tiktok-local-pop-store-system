import request from '@/utils/request'

// 查询优惠券列表
export function list(query) {
  return request({
    url: '/shop/coupon/list',
    method: 'get',
    params: query
  })
}
// 获取优惠券详情
export function getCoupon(id) {
  return request({
    url: '/shop/coupon/'+id,
    method: 'get'
  })
}
// 新增优惠券
export function addCoupon(data) {
  return request({
    url: '/shop/coupon',
    method: 'post',
    data: data
  })
}
// 修改优惠券
export function editCoupon(data) {
  return request({
    url: '/shop/coupon',
    method: 'put',
    data: data
  })
}
// 删除优惠券
export function delCoupon(id) {
  return request({
    url: '/shop/coupon/'+id,
    method: 'delete'
  })
}
// 导出优惠券列表
export function exportCoupon(data) {
  return request({
    url: '/shop/coupon/export',
    method: 'get',
    params: data
  })
}
//查询优惠券领取列表
export function getCouponList(query) {
  return request({
    url: '/system/shop/couponDraw/list',
    method: 'get',
    params: query
  })
}

//上下架优惠券
export function modifiStatus(query) {
  return request({
    url: '/shop/coupon/modifiStatus',
    method: 'post',
    params: query

  })
}
//核销优惠券
export function checkQrcode(data) {
  return request({
    url: '/system/shop/couponDraw/checkQrcode',
    method: 'post',
    params: data
  })
}
