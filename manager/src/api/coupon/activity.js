import request from '@/utils/request'

// 查询优惠券活动列表
export function list(query) {
  return request({
    url: '/shop/activity/list',
    method: 'get',
    params: query
  })
}
// 获取优惠券活动详情
export function getActivity(id) {
  return request({
    url: '/shop/activity/'+id,
    method: 'get'
  })
}
// 新增优惠券活动
export function addActivity(data) {
  return request({
    url: '/shop/activity',
    method: 'post',
    data: data
  })
}
// 修改优惠券活动
export function editActivity(data) {
  return request({
    url: '/shop/activity',
    method: 'put',
    data: data
  })
}
// 删除优惠券活动
export function delActivity(id) {
  return request({
    url: '/shop/activity/'+id,
    method: 'delete'
  })
}
// 导出优惠券活动列表
export function exportActivity(data) {
  return request({
    url: '/shop/activity/export',
    method: 'get',
    params: data
  })
}
// 获取商户优惠卷列表
export function activityShopCoupons(data) {
  return request({
    url: '/shop/activity/activityShopCoupons',
    method: 'get',
    params: data
  })
}
// 查看活动二维码
export function getActivityQrcode(data) {
  return request({
    url: '/shop/activity/getActivityQrcode',
    method: 'get',
    params: data
  })
}

//获取是否有文案
export function getIsWriting(query) {
  return request({
    url: '/shop/activity/isWriting' ,
    method: 'get',
    params: query
  })
}