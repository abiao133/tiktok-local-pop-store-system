import request from '@/utils/request'
// 查询混剪记录列表
export function mixshearList (query) {
    return request({
      url: '/system/video/merge/list',
      method: 'get',
      params: query
    })
  }
  export function delmixshear(id) {
    return request({
      url: '/system/video/merge/' + id,
      method: 'delete'
    })
  }
//添加混剪任务
export function addMixshearTask (query) {
  return request({
    url: '/system/video/merge/add',
    method: 'get',
    params: query
  })
}
