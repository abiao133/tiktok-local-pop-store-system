import request from '@/utils/request'

export function feedbacklist(query){
    return request({
        url: "/system/feedback/list",
        method: 'get',
        params: query
    })
}
export function delfeedbackId(id) {
    return request({
      url: '/system/feedback/' + id,
      method: 'delete'
    })
  }
export function feedbackDetail(id) {
    return request({
        url: '/system/feedback/' + id,
        method: 'get'
    })
}
export function addFeedback(data) {
    return request({
        url: '/system/feedback',
        method: 'post',
        data: data
      })
}