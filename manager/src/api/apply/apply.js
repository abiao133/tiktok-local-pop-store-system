import request from '@/utils/request'

//查询申请列表
export function applyList(query) {
    return request({
        url: '/system/shop/applyProxy/list',
        method: 'get',
        params: query
    })
}

//获取申请详情
export function getapplyList(id) {
    return request({
        url: '/system/shop/applyProxy/' + id,
        method: 'get'
    })
}

//审核状态
export function apply(query) {
    return request({
      url: '/system/shop/applyProxy/check',
      method: 'post',
      params: query 
    })
  }