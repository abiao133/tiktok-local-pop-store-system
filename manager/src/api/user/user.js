
import request from '@/utils/request'

// 查询生成表数据
export function userActingList(query) {
  return request({
    url: '/system/proxy/user/treelist',
    method: 'get',
    params: query
  })
}


