import request from '@/utils/request'
// 查询抖音用户列表
export function tiktokList (query) {
    return request({
      url: '/system/tkUser/list',
      method: 'get',
      params: query
    })
  }
//获取抖音用户详情信息
export function tiktokDetail (id) {
  return request({
    url: '/system/tkUser/' + id,
    method: 'get'
  })
}