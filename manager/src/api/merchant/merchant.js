import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";

// 查询商户列表
export function listUser(query) {
  return request({
    url: '/system/shop/manager/list',
    method: 'get',
    params: query
  })
}

// 查询所有商户列表
export function listAllShop(query) {
  return request({
    url: '/system/shop/manager/listAll',
    method: 'get',
    params: query
  })
}

//查询商户下管理者账号列表
export function getUserList(query) {
  return request({
    url: '/system/shop/manager/getUserList',
    method: 'get',
    params: query
  })
}
// 查询商户详细
export function getUser(id) {
  return request({
    url: '/system/shop/manager/detail/' + praseStrEmpty(id),
    method: 'get'
  })
}

// 新增商户
export function addUser(data) {
  return request({
    url: '/system/shop/manager/insert',
    method: 'post',
    data: data
  })
}

// 修改商户
export function updateUser(data) {
  return request({
    url: '/system/shop/manager/edit',
    method: 'post',
    data: data
  })
}

// // 删除商户
// export function delUser(userId) {
//   return request({
//     url: '/system/shop/manager/' + userId,
//     method: 'delete'
//   })
// }



// 获取商家抖音信息
export function getTiktokShopInfo(id) {
  return request({
    url: '/system/shop/manager/getTiktokShopInfo',
    method: 'get',
    params:{
      shopId:id
    }
  })
}
// 修改商家抖音信息
export function updateTiktokShopInfo(params) {
  return request({
    url: '/system/shop/manager/updateTiktokShopInfo',
    method: 'put',
    data:params
  })
}

// 商家抖音授权
export function shopAuth(params) {
  return request({
    url: '/system/shop/manager/shopAuth',
    method: 'get',
    params:params
  })
}
//抖音授权二维码地址
export function getAuthQrCodeUrl(params) {
  return request({
    url: '/system/shop/manager/getAuthQrCodeUrl',
    method: 'get',
    params:params
  })
}



// 添加商户用户账号
export function addManagerUser(data) {
  return request({
    url: '/system/shop/manager/addManagerUser' ,
    method: 'post',
    data:data
  })
}


// 导出商户列表
export function exportData(data) {
  return request({
    url: '/system/shop/manager/export',
    method: 'get',
    params: data
  })
}
//获取商户用户详情
export function getUserInfo(params) {
  return request({
    url: '/system/shop/manager/getUserInfo',
    method: 'get',
    params:params
  })
}
//修改商户用户信息
export function updateShopUser(params) {
  return request({
    url: '/system/shop/manager/updateShopUser',
    method: 'put',
    data:params
  })
}
//修改商户下面用户密码
export function resetPwd(params) {
  return request({
    url: '/system/shop/manager/resetPwd',
    method: 'put',
    data:params
  })
}
//查询商家poi地址
export function getAddress(query) {
  return request({
    url: "/system/shop/manager/searchPoi",
    method: 'get',
    params: query
  })
}
//删除商家用户
export function deleteShopUser(id) {
  return request({
    url: '/system/shop/manager/deleteShopUser/' + praseStrEmpty(id),
    method: 'DELETE'
  })
}
//查询代理商下拉树
export function getTreesList(query){
  return request({
    url: '/system/proxy/user/treelist',
    method: 'get',
    params: query
  })
}
//查询推广列表
export function pushList(query){
  return request({
    url: '/system/shop/manager/pushList',
    method: 'get',
    params: query
  })
}
//查询推广总人数
export function getPushCount(query){
  return request({
    url: '/system/shop/manager/getPushCount',
    method: 'get',
    params: query
  })
}
