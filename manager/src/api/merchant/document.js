import request from '@/utils/request'

// 查询商户文案列表
export function listDocument(query) {
  return request({
    url: '/resource/document/list',
    method: 'get',
    params: query
  })
}

// 查询商户文案详细
export function getDocument(id) {
  return request({
    url: '/resource/document/' + id,
    method: 'get'
  })
}

// 新增商户文案
export function addDocument(data) {
  return request({
    url: '/resource/document',
    method: 'post',
    data: data
  })
}

// 修改商户文案
export function updateDocument(data) {
  return request({
    url: '/resource/document',
    method: 'put',
    data: data
  })
}

// 删除商户文案
export function delDocument(id) {
  return request({
    url: '/resource/document/' + id,
    method: 'delete'
  })
}
