import request from '@/utils/request'

// 查询bgm资源列表
export function list(query) {
  return request({
    url: '/resource/bgm/list',
    method: 'get',
    params: query
  })
}
// 新增bgm资源
export function addMusic(query) {
  return request({
    url: '/resource/bgm',
    method: 'post',
    data: query
  })
}
// 修改bgm资源
export function editMusic(query) {
  return request({
    url: '/resource/bgm',
    method: 'put',
    data: query
  })
}
// 获取bgm资源详细信息
export function getMusic(id) {
  return request({
    url: '/resource/bgm/'+id,
    method: 'get'
  })
}
// 上传音乐文件 mp3格式
export function uploadMusic(query) {
  return request({
    url: '/resource/bgm/upload',
    method: 'post',
    data: query
  })
}
// 上传音乐文件 mp3格式
export function delMusic(id) {
  return request({
    url: '/resource/bgm/'+id,
    method: 'delete'
  })
}
