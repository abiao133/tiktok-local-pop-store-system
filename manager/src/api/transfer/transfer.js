import request from '@/utils/request'

// 查询转账记录列表
export function transferlist(query) {
  return request({
    url: '/system/shop/transfer/list',
    method: 'get',
    params: query
  })
}