import request from '@/utils/request'

// 查询商户资源列表
export function list(query) {
  return request({
    url: '/resource/list',
    method: 'get',
    params: query
  })
}
// 新增商户资源
export function resourceAdd(data) {
  return request({
    url: '/resource/add',
    method: 'post',
    data: data
  })
}
// 修改商户资源
export function resourceEdit(data) {
  return request({
    url: '/resource/edit',
    method: 'put',
    data: data
  })
}
// 删除商户资源
export function resourceDel(id) {
  return request({
    url: '/resource/'+id,
    method: 'delete'
  })
}
// 删除本地资源

export function deleteFile(url) {
  return request({
    url: '/resource/deleteFile',
    params:{
      url:url
    },
    method: 'post'
  })
}
// 获取商户资源详细信息
export function info(id) {
  return request({
    url: '/resource/'+id,
    method: 'get'
  })
}
// 获取商户资源分组列表
export function selectList(params) {
  return request({
    url: '/resource/group/list',
    method: 'get',
    params:params
  })
}
//审核资源
export function examine(query) {
  return request({
    url: '/resource/editStatus',
    method: 'post',
    params: query
  })
}
//查询最大可合成数量
export function getMaxMergeVideoCount(shopId){
  return request({
    url: '/resource/getMaxMergeCount?shopId='+shopId,
    method: 'get',
  })
}
