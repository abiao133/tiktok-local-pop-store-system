import request from '@/utils/request'

// 查询商户资源分组列表
export function list(query) {
  return request({
    url: '/resource/group/list',
    method: 'get',
    params: query
  })
}
// 新增商户资源分组
export function groupAdd(data) {
  return request({
    url: '/resource/group/add',
    method: 'post',
    data: data
  })
}
// 修改商户资源分组
export function groupEdit(data) {
  return request({
    url: '/resource/group/edit',
    method: 'put',
    data: data
  })
}
// 删除商户资源分组
export function groupDel(id) {
  return request({
    url: '/resource/group/'+id,
    method: 'delete'
  })
}
// 获取商户资源分组详细信息
export function info(id) {
  return request({
    url: '/resource/group/'+id,
    method: 'get'
  })
}
