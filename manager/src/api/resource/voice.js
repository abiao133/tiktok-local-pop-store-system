import request from '@/utils/request'

// 查询合成语音列表
export function listVoice(query) {
  return request({
    url: '/resource/voice/list',
    method: 'get',
    params: query
  })
}

// 获取合成语音详细
export function getVoice(id) {
  return request({
    url: '/resource/voice/' + id,
    method: 'get'
  })
}

// 新增合成语音
export function addVoice(data) {
  return request({
    url: '/resource/voice',
    method: 'post',
    data: data
  })
}

// 删除合成语音
export function delVoice(id) {
  return request({
    url: '/resource/voice/' + id,
    method: 'delete'
  })
}
