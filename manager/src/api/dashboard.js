/**
 * 数据看板 api
 */
import request from '@/utils/request'

// 获取所有商家的数量代理商数量
export function getShopCount() {
  return request({
    url: '/dashboard/shopAndProxyCount',
    method: 'get',
  })
}

// 优惠券活动信息各状态数量
export function getActivityStatus() {
  return request({
    url: '/dashboard/activityStatus',
    method: 'get',
  })
}

// 获取商户视频数据近七天的走势统计
export function getShopVideoStackedLine() {
  return request({
    url: '/dashboard/shopVideoStackedLine',
    method: 'get',
  })
}

// （管理员不可见）获取商户的优惠券领取、已使用、未使用、剩余数据统计
export function getShopCouponInfo() {
  return request({
    url: '/dashboard/shopCouponInfo',
    method: 'get',
  })
}

// 获取商户基本数据 （资源数量、剩余视频发送数量、发布优惠券活动数量、）
export function getShopInfo() {
  return request({
    url: '/dashboard/shopInfo',
    method: 'get',
  })
}

// 获取订单基本数据 （订单状态、金额）
export function getOrderData() {
  return request({
    url: '/dashboard/orderData',
    method: 'get',
  })
}




