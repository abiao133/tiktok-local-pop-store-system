import request from '@/utils/request'

// 查询代理商列表
export function listActing(query) {
  return request({
    url: '/system/proxy/org/list',
    method: 'get',
    params: query
  })
}

// 查询代理商列表（排除节点）
export function listActingExcludeChild(orgId) {
  return request({
    url: '/system/proxy/org/list',
    method: 'get',
    params:{
      id:orgId
    }
  })
}

// 查询代理商详细
export function getActing(orgId) {
  return request({
    url: '/system/proxy/org/' + orgId,
    method: 'get'
  })
}

// 查询代理商下拉树结构
export function treeselect() {
  return request({
    url: '/system/proxy/org/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询代理商树结构
export function roleActingTreeselect(roleId) {
  return request({
    url: '/system/proxy/org/roleActingTreeselect/' + roleId,
    method: 'get'
  })
}
// 代理商开关
export function actingStatus(params) {
  return request({
    url: '/system/proxy/org/state',
    method: 'get',
    params:params
  })
}

// 新增代理商
export function addActing(data) {
  return request({
    url: '/system/proxy/org',
    method: 'post',
    data: data
  })
}

// 修改代理商
export function updateActing(data) {
  return request({
    url: '/system/proxy/org',
    method: 'put',
    data: data
  })
}

// 删除代理商
export function delActing(id,inherit) {
  return request({
    url: '/system/proxy/org/'+id +'/'+inherit,
    method: 'delete',
  })
}
//查询代理下用户数量
export function proxyUserCount(id) {
  return request({
    url: '/system/proxy/org/proxyUserCount',
    method: 'get',
    params:{
      proxyId:id
    }
  })
}


// 获取代理商的视频转发余额
export function getProxyVideoCount(params) {
  return request({
    url: '/system/proxy/org/getProxyVideoCount',
    method: 'get',
    params:params
  })
}
//获取代理商配置详情
export function getCollocation(proxyId) {
  return request({
    url: "/system/branch/config/" + proxyId,
    method: "get",
  })
}
//修改代理商配置比例
export function editCollocation(data) {
  return request({
    url: '/system/branch/config',
    method: 'put',
    data: data
  })
}
//代理商Pc创建订单
export function createOrder(data) {
  return request({
    url: '/order/createOrder',
    method: 'get',
    params: data
  })
}
//查询订单结果查询
export function getPayStatus(num) {
  return request({
    url: '/order/isPay',
    method: 'get',
    params:{
      orderNum:num
    }
  })
}
