import request from '@/utils/request'

// 查询商户银行卡信息列表
export function listCard(query) {
  return request({
    url: '/system/proxy/card/list',
    method: 'get',
    params: query
  })
}

// 查询商户银行卡信息详细
export function getCard(id) {
  return request({
    url: '/system/proxy/card/' + id,
    method: 'get'
  })
}

// 新增商户银行卡信息
export function addCard(data) {
  return request({
    url: '/system/proxy/card',
    method: 'post',
    data: data
  })
}

// 修改商户银行卡信息
export function updateCard(data) {
  return request({
    url: '/system/proxy/card/edit',
    method: 'post',
    data: data
  })
}
//代理商获取银行卡详情
export function getCardDetail(proxyId) {
  return request({
    url: '/system/proxy/card/byProxyId/' + proxyId,
    method: 'get',
  })
}