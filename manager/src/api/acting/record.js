import request from '@/utils/request'

// 查询分销返利记录列表
export function listRecord(query) {
  return request({
    url: '/system/branch/record/list',
    method: 'get',
    params: query
  })
}

// 查询分销返利记录详细
export function getRecord(id) {
  return request({
    url: '/system/branch/record/' + id,
    method: 'get'
  })
}
