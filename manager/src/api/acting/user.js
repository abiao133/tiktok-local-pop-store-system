import request from '@/utils/request'

// 新增代理商用户
export function addProxyUser(data) {
  return request({
    url: '/system/proxy/user/addProxyUser',
    method: 'post',
    data: data
  })
}
// 修改代理商用户状态
export function changeStatus(data) {
  return request({
    url: '/system/proxy/user/changeStatus',
    method: 'put',
    data: data
  })
}
// 删除代理商用户
export function deleteProxyUser(id) {
  return request({
    url: '/system/proxy/user/deleteProxyUser/'+id,
    method: 'delete',
  })
}
// 查询代理商用户信息
export function proxyUserinfo(params) {
  return request({
    url: '/system/proxy/user/proxyUserinfo',
    method: 'get',
    params: params
  })
}
// 查询代理商下的用户
export function userList(params) {
  return request({
    url: '/system/proxy/user/userList',
    method: 'get',
    params: params
  })
}

// 修改代理商下面用户密码
export function resetPwd(data) {
  return request({
    url: '/system/proxy/user/resetPwd',
    method: 'put',
    data: data
  })
}
// 修改代理商用户信息
export function updateProxyUser(data) {
  return request({
    url: '/system/proxy/user/updateProxyUser',
    method: 'put',
    data: data
  })
}
// 查询用户代理组织列表
export function treelist(params) {
  return request({
    url: '/system/proxy/user/treelist',
    method: 'get',
    params: params
  })

}

// 管理员给代理或者商家充值
export function addVideCount(params) {
  return request({
    url: '/system/proxy/org/addVideCount',
    method: 'post',
    params: params
  })

}
// 代理商用户给商户转视频转发余额
export function transferVideoCount(params) {
  return request({
    url: '/system/proxy/org/transferVideoCount',
    method: 'post',
    params: params
  })

}
