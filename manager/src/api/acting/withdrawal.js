import request from '@/utils/request'
//查询代理商提现申请列表
export function getWithdrawal(query) {
    return request ({
        url: '/system/branch/apply/list',
        method: 'get',
        params: query
    })
}
//获取代理商提现申请详情
export function detailWithdrawal (id) {
    return request ({
        url: '/system/branch/apply/' + id,
        method: 'get',
    })
}
//审核提现
export function examine (query) {
    return request ({
        url: '/system/branch/apply/check',
        method: 'post',
        params: query
    })
}