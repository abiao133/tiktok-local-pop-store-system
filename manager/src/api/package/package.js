import request from '@/utils/request'

// 查询套餐列表
export function packagelist(query) {
  return request({
    url: '/system/goods/list',
    method: 'get',
    params: query
  })
}
//新增套餐
export function addpackage(data) {
  return request({
    url: '/system/goods',
    method: 'post',
    data: data
  })
}
//修改套餐
export function editpackage(data) {
  return request({
    url: '/system/goods',
    method: 'put',
    data: data
  })
}
// 删除套餐
export function delpackage(id) {
  return request({
    url: '/system/goods/' + id,
    method: 'delete'
  })
}
//获取套餐列表详细
export function getpackage(id) {
  return request({
    url: '/system/goods/' + id,
    method: 'get'
  })
}