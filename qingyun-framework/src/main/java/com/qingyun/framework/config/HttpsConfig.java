package com.qingyun.framework.config;

import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.servlet.api.SecurityConstraint;
import io.undertow.servlet.api.SecurityInfo;
import io.undertow.servlet.api.TransportGuaranteeType;
import io.undertow.servlet.api.WebResourceCollection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.undertow.UndertowBuilderCustomizer;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;

import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "server.ssl.enabled", havingValue = "true", matchIfMissing = false)
public class HttpsConfig {

    /**
     * http服务端口
     */
    @Value("${http.port}")
    private Integer httpPort;

    /**
     * https服务端口
     */
    @Value("${server.port}")
    private Integer httpsPort;


    @Bean
    public ServletWebServerFactory undertowFactory() {
		UndertowServletWebServerFactory undertowFactory = new UndertowServletWebServerFactory();
		UndertowBuilderCustomizer undertowBuilderCustomizer = new UndertowBuilderCustomizer() {
			@Override
			public void customize(Undertow.Builder builder) {
				builder.addHttpListener(httpPort, "0.0.0.0");
			}
		};
		undertowFactory.addBuilderCustomizers(undertowBuilderCustomizer);
		return undertowFactory;
//		UndertowServletWebServerFactory undertowFactory = new UndertowServletWebServerFactory();
//		undertowFactory.addBuilderCustomizers((Undertow.Builder builder) -> {
//			builder.addHttpListener(httpPort, "0.0.0.0");
//			// 开启 HTTP2
//			builder.setServerOption(UndertowOptions.ENABLE_HTTP2, true);
//		});
//		undertowFactory.addDeploymentInfoCustomizers(deploymentInfo -> {
//			// 开启 HTTP 自动跳转至 HTTPS
//			deploymentInfo.addSecurityConstraint(new SecurityConstraint()
//				.addWebResourceCollection(new WebResourceCollection().addUrlPattern("/*"))
//				.setTransportGuaranteeType(TransportGuaranteeType.CONFIDENTIAL)
//				.setEmptyRoleSemantic(SecurityInfo.EmptyRoleSemantic.PERMIT))
//				.setConfidentialPortManager(exchange -> httpsPort);
//		});
//		return undertowFactory;
    }

}
