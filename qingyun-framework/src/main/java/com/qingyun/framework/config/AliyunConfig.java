package com.qingyun.framework.config;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.teaopenapi.models.Config;
import com.qingyun.framework.config.properties.AliyunSmsProperties;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname AliyunConfig
 * @Author dyh
 * @Date 2021/9/16 19:00
 */
@Configuration
public class AliyunConfig {

	@Bean
	public Client client(AliyunSmsProperties aliyunProperties) throws Exception {
		Config config = new Config()
			.setAccessKeyId(aliyunProperties.getAccessKeyId())
			.setAccessKeySecret(aliyunProperties.getAccessKeySecret());
		config.setEndpoint(aliyunProperties.getEndpoint());
		Client client = new Client(config);
		return client;
	}


}
