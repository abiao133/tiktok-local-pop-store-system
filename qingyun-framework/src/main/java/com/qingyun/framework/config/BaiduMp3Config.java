package com.qingyun.framework.config;

import com.baidu.aip.speech.AipSpeech;
import com.qingyun.framework.config.properties.BaiduMp3Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname BaiduMp3Config
 * @Author dyh
 * @Date 2021/10/20 16:25
 */
@Configuration
public class BaiduMp3Config {

	@Autowired
	private BaiduMp3Properties baiduMp3Properties;

	@Bean
	public AipSpeech aipSpeech() {
		// 初始化一个AipSpeech
		AipSpeech client = new AipSpeech(baiduMp3Properties.getAppId(),
			baiduMp3Properties.getAppKey(), baiduMp3Properties.getSecretKey());
		// 可选：设置网络连接参数
		client.setConnectionTimeoutInMillis(2000);
		client.setSocketTimeoutInMillis(60000);
		return client;
	}
}
