package com.qingyun.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname BaiduMp3Config
 * @Author dyh
 * @Date 2021/10/20 16:17
 */
@ConfigurationProperties(prefix = "baidu")
@Component
@Data
public class BaiduMp3Properties {

	private String appId;

	private String appKey;

	private String secretKey;
}
