package com.qingyun.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname WxPayPropererties
 * @Author dyh
 * @Date 2021/6/25 14:00
 */
@ConfigurationProperties(prefix = "wx.pay")
@Data
public class WxPayPropererties {

	/**
	 * 微信支付的appid
	 */
	private String appId;
    /**
     * 微信支付mchId
     */
    private String mchId;

    /**
     * 微信支付mchKey
     */
    private String mchKey;

    /**
     * 支付证书路径
     */
    private String keyPath;


    private String privateKeyPath;


    private String privateCertPath;

    private String apiV3Key;
}
