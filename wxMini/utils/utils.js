//去除日期时间格式的秒
export function delTime(date){
    let newDate = /\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}/g.exec(date)

    return newDate

}
