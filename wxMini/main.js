// import App from './App'
// import uView from "uview-ui";

// // #ifndef VUE3
// import Vue from 'vue'
// Vue.config.productionTip = false
// App.mpType = 'app'
// Vue.use(uView);
// console.log(Vue.uView)
// const app = new Vue({
//     ...App
// })
// app.$mount()
// // #endif

// // #ifdef VUE3
// import { createSSRApp } from 'vue'
// export function createApp() {
//   const app = createSSRApp(App)
//   return {
//     app
//   }
// }
// // #endif
import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
import wLoading from "./components/w-loading/w-loading";
// import lLoading from './utils/loading'
Vue.component('w-loading',wLoading)
Vue.config.productionTip = false;
Vue.prototype.$wloadiung = wLoading;

App.mpType = 'app'
Vue.use(uView);
const app = new Vue({
    ...App
})
app.$mount()
