import request from "../utils/request";
//获取代理商余额
export function getBalance (data){
    return request.request('/miniapp/shop/getBalance','get',data,false,'formData')
}

//提现申请
export function apply (data){
    return request.request('/miniapp/branch/apply','post',data,false)
}

//查询代理商提现申请列表
export function applyList (data){
    return request.request('/miniapp/branch/apply/list','get',data,false,'formData')
}

