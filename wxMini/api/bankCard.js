import request from "../utils/request.js";
// 新增银行卡
export function addCard (data){
    return request.request('/miniapp/card','post',data,false,)
}
// 更改银行卡信息
export function editCard (data){
    return request.request('/miniapp/card/edit','post',data,false,)
}
// 获取代理商银行卡列表
export function cardList (data){
    return request.request('/miniapp/card/list','get',data,true,)

}
// 获取代理商银行卡信息详细信息
export function cardInfo (data){
    return request.request('/miniapp/card/list','post',data,false,'formData')
}
//银行卡三要素验证
export function realCheck (data){
    return request.request('/miniapp/card/realCheck','post',data,false)
}
// 删除银行卡
export function deleteBank (data){
    return request.request('/miniapp/card/'+data,'DELETE',{},false,'formData')
}
