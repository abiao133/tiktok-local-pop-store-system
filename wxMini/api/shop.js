import request from "../utils/request.js";
//发送数量
export function sendCount (data){
    return request.request('/miniapp/sendCount','get',data,false,'formData')
}
//套餐剩余次数
export function videoCount (data){
    return request.request('/miniapp/videoCount','get',data,false,'formData')
}

//我的页面商户数据
export function getShopCouponData (data){
    return request.request('/miniapp/shop/getShopCouponData','get',data,false,'formData')
}

//商户信息
export function shopInfo (data){
    return request.request('/miniapp/shop/info','get',data,false,'formData')
}
//修改商户信息
export function shopEdit (data){
    return request.request('/miniapp/shop/edit','post',data,false,)
}
//商家订单列表
export function orderList (data){
    return request.request('/miniapp/order/list','get',data,false)
}
// 获取最新10条用户参与活动记录
export function sendVideoTitleLamp (data){
    return request.request('/miniapp/sendVideoTitleLamp','get',data)
}
//套餐列表
export function goodsList (data){
    return request.request('/miniapp/goods/list','get',data,false,'formData')
}
//创建用户反馈
export function feedback (data){
    return request.request('/miniapp/feedback/add','post',data,false)
}
//优惠券列表
export function couponList (data){
    return request.request('/miniapp/coupon/list','get',data,false,'formData')
}

//活动列表
export function activityList (data){
    return request.request('/miniapp/activity/list','get',data,false,'formData')
}
//抖音授权二维码
export function getAuthQrCodeUrl (data){
    return request.request('/miniapp/getAuthQrCodeUrl','get',data,false,'formData')
}
//申请成为代理商
export function applyProxy (data){
    return request.request('/miniapp/shop/applyProxy','post',data,false,)
}
//查询申请商代理商状态
export function checkApplyProxy (data){
    return request.request('/miniapp/shop/checkApplyProxy','get',data,false,'formData')
}
//分销记录
export function getBranchList (data){
    return request.request('/miniapp/shop/getBranchList','get',data,false,'formData')
}
//查看活动二维码
export function getActivityQrcode (data){
    return request.request('/miniapp/activity/getActivityQrcode','get',data,false,'formData')
}
/** 以下微信支付接口 */
//通过商品生成订单
export function orderCreate(data){
    return request.request('/miniapp/order/create','get',data,false,'formData')
}

//通过订单号提交订单获取签名并且支付
export function orderPay(data){
    return request.request('/miniapp/order/pay','get',data,false,'formData')
}

//微信支付回调
export function payOrder(data){
    return request.request('/order/notice/payOrder','post',data,false,'formData')
}

