import request from "../utils/request.js";
//核销
export function checkCode (data){
    return request.request('/miniapp/check/check','get',data,true,'formData')
}

//获取剩余优惠卷数量和已经核销数量
export function getCouponAndCheckCounts (data){
    return request.request('/miniapp/check/getCouponAndCheckCounts','get',data,false,'formData')
}


//获取核销优惠卷列表 分页
export function getCheckCouponList (data){
    return request.request('/miniapp/check/getCheckCouponList','get',data,false,'formData')
}

//获取总推广人数和成功核销人数
export function getPushCount (data){
    return request.request('/miniapp/staff/getPushCount','get',data,false,'formData')
}

//员工推广记录
export function pushList (data){
    return request.request('/miniapp/staff/pushList','get',data,false,'formData')
}

