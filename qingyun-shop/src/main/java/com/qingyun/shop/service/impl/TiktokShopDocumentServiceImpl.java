package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.exception.ServiceException;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokShopDocument;
import com.qingyun.shop.domain.bo.resource.TiktokShopDocumentBo;
import com.qingyun.shop.domain.vo.TiktokShopDocumentVo;
import com.qingyun.shop.mapper.TiktokShopDocumentMapper;
import com.qingyun.shop.service.ITiktokShopDocumentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商户文案Service业务层处理
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Service
public class TiktokShopDocumentServiceImpl extends ServicePlusImpl<TiktokShopDocumentMapper, TiktokShopDocument, TiktokShopDocumentVo> implements ITiktokShopDocumentService {

    @Override
    public TiktokShopDocumentVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokShopDocumentVo> queryPageList(TiktokShopDocumentBo bo) {
		if (!SecurityUtils.isManager()) {
			bo.setShopId(SecurityUtils.getShop().getId());
		}
		Page<TiktokShopDocumentVo> result = baseMapper.selectDocumentList(PageUtils.buildPage(), bo.getShopId(),bo.getShopName());
		return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokShopDocumentVo> queryList(TiktokShopDocumentBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokShopDocument> buildQueryWrapper(TiktokShopDocumentBo bo) {
        LambdaQueryWrapper<TiktokShopDocument> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getShopId() != null, TiktokShopDocument::getShopId, bo.getShopId());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokShopDocumentBo bo) {
        TiktokShopDocument add = BeanUtil.toBean(bo, TiktokShopDocument.class);
		if(!SecurityUtils.isManager()) {
			add.setShopId(SecurityUtils.getShop().getId());
		}else{
			if(ObjectUtil.isNull(add.getShopId())){
				throw new ServiceException("管理员操作shopId不能为空");
			}
		}
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokShopDocumentBo bo) {
    	if(bo.getId()==null){
          return false;
		}
        TiktokShopDocument update = BeanUtil.toBean(bo, TiktokShopDocument.class);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokShopDocument entity){

    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
