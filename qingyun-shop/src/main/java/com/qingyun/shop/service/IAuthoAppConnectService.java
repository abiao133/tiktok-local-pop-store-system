package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.model.shop.AuthoAppConnect;
import com.qingyun.shop.domain.bo.AuthoAppConnectBo;
import com.qingyun.shop.domain.vo.AuthoAppConnectVo;


import java.util.Collection;
import java.util.List;

/**
 * 第三方用户数据Service接口
 *
 * @author qingyun
 * @date 2021-09-16
 */
public interface IAuthoAppConnectService extends IServicePlus<AuthoAppConnect, AuthoAppConnectVo> {
	/**
	 * 查询单个
	 * @return
	 */
	AuthoAppConnectVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<AuthoAppConnectVo> queryPageList(AuthoAppConnectBo bo);

	/**
	 * 查询列表
	 */
	List<AuthoAppConnectVo> queryList(AuthoAppConnectBo bo);

	/**
	 * 根据新增业务对象插入第三方用户数据
	 * @param bo 第三方用户数据新增业务对象
	 * @return
	 */
	Boolean insertByBo(AuthoAppConnectBo bo);

	/**
	 * 根据编辑业务对象修改第三方用户数据
	 * @param bo 第三方用户数据编辑业务对象
	 * @return
	 */
	Boolean updateByBo(AuthoAppConnectBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
