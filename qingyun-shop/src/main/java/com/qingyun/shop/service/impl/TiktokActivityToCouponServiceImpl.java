package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokActivityToCoupon;
import com.qingyun.shop.domain.bo.activity.TiktokActivityToCouponBo;
import com.qingyun.shop.mapper.TiktokActivityToCouponMapper;
import com.qingyun.shop.service.ITiktokActivityToCouponService;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.shop.domain.vo.TiktokActivityToCouponVo;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 活动与优惠券关联Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokActivityToCouponServiceImpl extends ServicePlusImpl<TiktokActivityToCouponMapper, TiktokActivityToCoupon, TiktokActivityToCouponVo> implements ITiktokActivityToCouponService {

    @Override
    public TiktokActivityToCouponVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokActivityToCouponVo> queryPageList(TiktokActivityToCouponBo bo) {
        PagePlus<TiktokActivityToCoupon, TiktokActivityToCouponVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokActivityToCouponVo> queryList(TiktokActivityToCouponBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokActivityToCoupon> buildQueryWrapper(TiktokActivityToCouponBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokActivityToCoupon> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getActivityId() != null, TiktokActivityToCoupon::getActivityId, bo.getActivityId());
        lqw.eq(bo.getCouponId() != null, TiktokActivityToCoupon::getCouponId, bo.getCouponId());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokActivityToCouponBo bo) {
        TiktokActivityToCoupon add = BeanUtil.toBean(bo, TiktokActivityToCoupon.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokActivityToCouponBo bo) {
        TiktokActivityToCoupon update = BeanUtil.toBean(bo, TiktokActivityToCoupon.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokActivityToCoupon entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
