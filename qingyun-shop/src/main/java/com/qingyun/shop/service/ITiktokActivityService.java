package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokActivity;
import com.qingyun.shop.domain.bo.TiktokActivityBo;
import com.qingyun.shop.domain.vo.TiktokActivityVo;

import java.util.Collection;
import java.util.List;

/**
 * 优惠券活动Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokActivityService extends IServicePlus<TiktokActivity, TiktokActivityVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokActivityVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokActivityVo> queryPageList(TiktokActivityBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokActivityVo> queryList(TiktokActivityBo bo);

	/**
	 * 根据新增业务对象插入优惠券活动
	 * @param bo 优惠券活动新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokActivityBo bo);

	/**
	 * 根据编辑业务对象修改优惠券活动
	 * @param bo 优惠券活动编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokActivityBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);





}
