package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokBranchRecord;
import com.qingyun.shop.domain.bo.branch.TiktokBranchRecordBo;
import com.qingyun.shop.domain.vo.TiktokBranchRecordVo;

import java.util.Collection;
import java.util.List;

/**
 * 分销返利记录Service接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface ITiktokBranchRecordService extends IServicePlus<TiktokBranchRecord, TiktokBranchRecordVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokBranchRecordVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokBranchRecordVo> queryPageList(TiktokBranchRecordBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokBranchRecordVo> queryList(TiktokBranchRecordBo bo);

	/**
	 * 根据新增业务对象插入分销返利记录
	 * @param bo 分销返利记录新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokBranchRecordBo bo);

	/**
	 * 根据编辑业务对象修改分销返利记录
	 * @param bo 分销返利记录编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokBranchRecordBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
