package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokBgm;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmAddBo;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmEditBo;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmQueryBo;
import com.qingyun.shop.domain.vo.TiktokBgmDetailVo;
import com.qingyun.shop.domain.vo.TiktokBgmVo;

import java.util.Collection;
import java.util.List;

/**
 * bgm资源Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokBgmService extends IServicePlus<TiktokBgm, TiktokBgmVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokBgmDetailVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokBgmVo> queryPageList(TiktokBgmQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokBgmVo> queryList(TiktokBgmQueryBo bo);

	/**
	 * 根据新增业务对象插入bgm资源
	 * @param bo bgm资源新增业务对象
	 */
	Boolean insertByBo(TiktokBgmAddBo bo);

	/**
	 * 根据编辑业务对象修改bgm资源
	 * @param bo bgm资源编辑业务对象
	 */
	Boolean updateByBo(TiktokBgmEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


	/**
	 * 随机获取一套商户活动的bgm
	 * @param shopId
	 * @param activityId
	 * @param duration
	 * @return
	 */
	TiktokBgm selectBgmBy(Long shopId,Long activityId,Double duration,Boolean size);

	/**
	 * 查询随机bgm
	 * @param shopId
	 * @param global
	 * @param totalDuration
	 * @param size
	 * @return
	 */
	TiktokBgm selectRandomBgm(Long shopId, Integer global, Double totalDuration, boolean size);
}
