package com.qingyun.shop.service;

import com.qingyun.shop.domain.TiktokShopStaffPush;
import com.qingyun.shop.domain.bo.TiktokShopStaffPushBo;
import com.qingyun.shop.domain.vo.TiktokShopStaffPushVo;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import org.redisson.client.protocol.convertor.IntegerReplayConvertor;

import java.util.Collection;
import java.util.List;

/**
 * 员工宣传记录Service接口
 *
 * @author qingyun
 * @date 2021-10-30
 */
public interface ITiktokShopStaffPushService extends IServicePlus<TiktokShopStaffPush, TiktokShopStaffPushVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokShopStaffPushVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokShopStaffPushVo> queryPageList(TiktokShopStaffPushBo bo);


	/**
	 * 获取推广记录
	 * @param type
	 * @return
	 */
	TableDataInfo<TiktokShopStaffPushVo> selectPageList(Long type,Long userId);

	/**
	 * 总推广人数
	 * @param shopUserId
	 * @return
	 */
	Integer selectTotalCount(Long shopUserId);


	/**
	 * 成功被核销的人数
	 * @param shopUserId
	 * @return
	 */
	Integer selectCheckCount(Long shopUserId);

	/**
	 * 查询列表
	 */
	List<TiktokShopStaffPushVo> queryList(TiktokShopStaffPushBo bo);

	/**
	 * 根据新增业务对象插入员工宣传记录
	 * @param bo 员工宣传记录新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokShopStaffPushBo bo);

	/**
	 * 根据编辑业务对象修改员工宣传记录
	 * @param bo 员工宣传记录编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokShopStaffPushBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
