package com.qingyun.shop.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokGoods;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsAddBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsEditBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsQueryBo;
import com.qingyun.shop.domain.vo.TiktokGoodsVo;

import java.util.Collection;
import java.util.List;

/**
 * 套餐Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokGoodsService extends IServicePlus<TiktokGoods, TiktokGoodsVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokGoodsVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokGoodsVo> queryPageList(TiktokGoodsQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokGoodsVo> queryList(TiktokGoodsQueryBo bo);

	/**
	 * 根据新增业务对象插入套餐
	 * @param bo 套餐新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokGoodsAddBo bo);

	/**
	 * 根据编辑业务对象修改套餐
	 * @param bo 套餐编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokGoodsEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 修改套餐上下架
	 * @param id  套餐id
	 * @param status 要修改的状态
	 * @return
	 */
	Boolean editStatus(Long id, Integer status);
}
