package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokResourceGroup;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupAddBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupQueryBo;
import com.qingyun.shop.domain.vo.TiktokResourceGroupVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 商户资源分组Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokResourceGroupService extends IServicePlus<TiktokResourceGroup, TiktokResourceGroupVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokResourceGroupVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokResourceGroupVo> queryPageList(TiktokResourceGroupQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokResourceGroupVo> queryList(TiktokResourceGroupQueryBo bo);

	/**
	 * 根据新增业务对象插入商户资源分组
	 * @param bo 商户资源分组新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokResourceGroupAddBo bo);

	/**
	 * 根据编辑业务对象修改商户资源分组
	 * @param bo 商户资源分组编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokResourceGroupBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 资源分类组下拉框
	 * @return
	 */
	Map<Long, String> getSelect();
}
