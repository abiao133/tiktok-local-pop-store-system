package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokOrderSettlement;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementBo;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementListVo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementVo;
import com.qingyun.shop.mapper.TiktokOrderSettlementMapper;
import com.qingyun.shop.service.ITiktokOrderSettlementService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Collection;

/**
 * 订单流水Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokOrderSettlementServiceImpl extends ServicePlusImpl<TiktokOrderSettlementMapper, TiktokOrderSettlement, TiktokOrderSettlementVo> implements ITiktokOrderSettlementService {

	@Override
	public TiktokOrderSettlementVo queryById(Long id) {
		return getVoById(id);
	}

	@Override
	public TableDataInfo<TiktokOrderSettlementListVo> queryPageList(TiktokOrderSettlementQueryBo bo) {
		Page<TiktokOrderSettlementListVo> result = baseMapper.selectOrderSettlementList(PageUtils.buildPage(), bo);
		return PageUtils.buildDataInfo(result);
	}

	@Override
	public List<TiktokOrderSettlementVo> queryList(TiktokOrderSettlementQueryBo bo) {
		return listVo(buildQueryWrapper(bo));
	}

	private LambdaQueryWrapper<TiktokOrderSettlement> buildQueryWrapper(TiktokOrderSettlementQueryBo bo) {
		LambdaQueryWrapper<TiktokOrderSettlement> lqw = Wrappers.lambdaQuery();
		lqw.eq(StringUtils.isNotBlank(bo.getOrderNum()), TiktokOrderSettlement::getOrderNum, bo.getOrderNum());
		lqw.eq(StringUtils.isNotBlank(bo.getPayNo()), TiktokOrderSettlement::getPayNo, bo.getPayNo());
		lqw.eq(bo.getPayType() != null, TiktokOrderSettlement::getPayType, bo.getPayType());
		lqw.eq(bo.getPayStatus() != null, TiktokOrderSettlement::getPayStatus, bo.getPayStatus());
		return lqw;
	}

	@Override
	public Boolean insertByBo(TiktokOrderSettlementBo bo) {
		TiktokOrderSettlement add = BeanUtil.toBean(bo, TiktokOrderSettlement.class);
		validEntityBeforeSave(add);
		return save(add);
	}

	@Override
	public Boolean updateByBo(TiktokOrderSettlementBo bo) {
		TiktokOrderSettlement update = BeanUtil.toBean(bo, TiktokOrderSettlement.class);
		validEntityBeforeSave(update);
		return updateById(update);
	}

	/**
	 * 保存前的数据校验
	 *
	 * @param entity 实体类数据
	 */
	private void validEntityBeforeSave(TiktokOrderSettlement entity) {
		//TODO 做一些数据校验,如唯一约束
	}

	@Override
	public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
		if (isValid) {
			//TODO 做一些业务上的校验,判断是否需要校验
		}
		return removeByIds(ids);
	}
}
