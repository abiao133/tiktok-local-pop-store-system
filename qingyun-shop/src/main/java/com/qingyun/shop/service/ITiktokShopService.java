package com.qingyun.shop.service;


import com.qingyun.common.core.domain.entity.SysUser;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.bo.shop.TiktokShopAddBo;
import com.qingyun.shop.domain.bo.shop.TiktokShopEditBo;
import com.qingyun.shop.domain.bo.shop.TiktokShopQueryBo;
import com.qingyun.shop.domain.bo.shop.TiktokShopUserListBo;
import com.qingyun.shop.domain.vo.TiktokShopUserListVo;
import com.qingyun.tiktok.basis.response.video.VideoPoiRes;

import java.util.List;

/**
 * 商家Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokShopService extends IServicePlus<TiktokShop, TiktokShopVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokShopVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokShopVo> queryPageList(TiktokShopQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokShopVo> queryList(TiktokShopQueryBo bo);

	/**
	 * 根据商家id查询用户信息列表
	 */
	TableDataInfo<TiktokShopUserListVo> queryUserListByShopId(TiktokShopUserListBo queryBo);

	/**
	 * 根据新增业务对象插入商家
	 * @param bo 商家新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokShopAddBo bo);

	/**
	 * 根据编辑业务对象修改商家
	 * @param bo 商家编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokShopEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Long id, Boolean isValid);

	/**
	 * 配置商家下的账户
	 * @param user 账户
	 * @param shopId 商家id
	 * @return
	 */
	Boolean configUser(SysUser user, Long shopId);


	/**
	 * 启用/停用商户
	 */
	Boolean startOrStop(Long id, Integer status);

	/**
	 * 通过地址字符串查询poi列表
	 * @param address
	 * @return
	 */
	List<VideoPoiRes.Poi> searchPOI(String address);

	/**
	 * 通过商户id修改商户名称
	 * @param shopId
	 * @param ShopName
	 * @return
	 */
	Boolean updateShopNameByShopId(Long shopId,String ShopName);
}
