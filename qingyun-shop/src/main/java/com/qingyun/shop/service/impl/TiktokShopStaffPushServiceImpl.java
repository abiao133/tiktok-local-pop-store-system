package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokShopStaffPush;
import com.qingyun.shop.domain.bo.TiktokShopStaffPushBo;
import com.qingyun.shop.domain.vo.TiktokShopStaffPushVo;
import com.qingyun.shop.mapper.TiktokShopStaffPushMapper;
import com.qingyun.shop.service.ITiktokShopStaffPushService;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 员工宣传记录Service业务层处理
 *
 * @author qingyun
 * @date 2021-10-30
 */
@Service
public class TiktokShopStaffPushServiceImpl extends ServicePlusImpl<TiktokShopStaffPushMapper, TiktokShopStaffPush, TiktokShopStaffPushVo> implements ITiktokShopStaffPushService {

    @Override
    public TiktokShopStaffPushVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokShopStaffPushVo> queryPageList(TiktokShopStaffPushBo bo) {
        PagePlus<TiktokShopStaffPush, TiktokShopStaffPushVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

	@Override
	public TableDataInfo<TiktokShopStaffPushVo> selectPageList(Long type,Long userId) {

		if (ObjectUtil.isNull(userId)) {
			userId = SecurityUtils.getAppLoginUser().getSysUser().getUserId();
		}

		Page<TiktokShopStaffPushVo> tiktokShopStaffPushVoPage = baseMapper.selectPageList(PageUtils.buildPage(),userId,type);
		return PageUtils.buildDataInfo(tiktokShopStaffPushVoPage);
	}

	@Override
    public List<TiktokShopStaffPushVo> queryList(TiktokShopStaffPushBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokShopStaffPush> buildQueryWrapper(TiktokShopStaffPushBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokShopStaffPush> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

	@Override
	public Integer selectTotalCount(Long shopUserId) {
		return baseMapper.selectTotalCount(shopUserId);
	}


	@Override
	public Integer selectCheckCount(Long shopUserId) {
		return baseMapper.selectCheckCount(shopUserId);
	}

	@Override
    public Boolean insertByBo(TiktokShopStaffPushBo bo) {
        TiktokShopStaffPush add = BeanUtil.toBean(bo, TiktokShopStaffPush.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokShopStaffPushBo bo) {
        TiktokShopStaffPush update = BeanUtil.toBean(bo, TiktokShopStaffPush.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokShopStaffPush entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
