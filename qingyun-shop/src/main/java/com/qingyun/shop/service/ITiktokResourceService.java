package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokResource;
import com.qingyun.shop.domain.bo.resource.TiktokResourceAddBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceEditBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceQueryBo;
import com.qingyun.shop.domain.vo.TiktokResourceVo;

import java.util.Collection;
import java.util.List;

/**
 * 资源Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokResourceService extends IServicePlus<TiktokResource, TiktokResourceVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokResourceVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokResourceVo> queryPageList(TiktokResourceQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokResourceVo> queryList(TiktokResourceQueryBo bo);

	/**
	 * 根据新增业务对象插入资源
	 * @param bo 资源新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokResourceAddBo bo);

	/**
	 * 根据编辑业务对象修改资源
	 * @param bo 资源编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokResourceEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 分组id校验
	 * @param groupId 资源分组id
	 */
	Boolean groupIdCheck(Long groupId);

	/**
	 * 通过分组查询一条随机资源
	 * @param groupId
	 * @return
	 */
	TiktokResource selectRandomResourceByGroupId(Long shopId,Long groupId,Integer format,Integer isFrag);

}
