package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokVoice;
import com.qingyun.shop.domain.bo.resource.TiktokVoiceBo;
import com.qingyun.shop.domain.vo.TiktokVoiceVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 合成语音Service接口
 *
 * @author qingyun
 * @date 2021-10-21
 */
public interface ITiktokVoiceService extends IServicePlus<TiktokVoice, TiktokVoiceVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokVoiceVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokVoiceVo> queryPageList(TiktokVoiceBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokVoiceVo> queryList(TiktokVoiceBo bo);

	/**
	 * 根据新增业务对象插入合成语音
	 * @param bo 合成语音新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokVoiceBo bo);

	/**
	 * 根据编辑业务对象修改合成语音
	 * @param bo 合成语音编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokVoiceBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 查找随机语音
	 * @param shopId
	 * @return
	 */
	TiktokVoice selectRandomVoice(Long shopId);
}
