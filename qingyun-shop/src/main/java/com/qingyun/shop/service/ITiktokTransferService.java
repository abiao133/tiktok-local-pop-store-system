package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokTransfer;
import com.qingyun.shop.domain.bo.TiktokTransferQueryBo;
import com.qingyun.shop.domain.bo.transfer.TiktokTransferBo;
import com.qingyun.shop.domain.vo.TiktokTransferVo;

import java.util.Collection;
import java.util.List;

/**
 * 代理给商户转播放量记录Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokTransferService extends IServicePlus<TiktokTransfer, TiktokTransferVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokTransferVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokTransferVo> queryPageList(TiktokTransferQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokTransferVo> queryList(TiktokTransferQueryBo bo);

	/**
	 * 根据新增业务对象插入代理给商户转播放量记录
	 * @param bo 代理给商户转播放量记录新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokTransferBo bo);

	/**
	 * 根据编辑业务对象修改代理给商户转播放量记录
	 * @param bo 代理给商户转播放量记录编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokTransferBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
