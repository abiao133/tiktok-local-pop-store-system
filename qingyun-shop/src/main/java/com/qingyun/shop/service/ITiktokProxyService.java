package com.qingyun.shop.service;


import com.qingyun.common.core.domain.entity.SysUser;
import com.qingyun.common.core.domain.model.TiktokProxyVo;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokProxy;
import com.qingyun.shop.domain.bo.TiktokProxyBo;
import com.qingyun.system.domain.ProxyUserRequest;
import com.qingyun.shop.domain.vo.TiktokProxyTreeVo;

import java.util.Collection;
import java.util.List;

/**
 * 代理组织Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokProxyService extends IServicePlus<TiktokProxy, TiktokProxyVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokProxyVo queryById(Long id);



	public TableDataInfo<SysUser> selectPageUserList(ProxyUserRequest user);


	public boolean insertProxyUserShop(SysUser user);

	/**
	 * 查询树
	 * @param bo
	 * @return
	 */
	public List<TiktokProxyVo> queryTreeList(TiktokProxyBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokProxyTreeVo> queryPageList(TiktokProxyBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokProxyVo> queryList(TiktokProxyBo bo);


	/**
	 * 查询代理用户状态
	 * @param userId
	 * @return
	 */
	Long selectProxyStatus(Long userId);

	/**
	 * 根据新增业务对象插入代理组织
	 * @param bo 代理组织新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokProxyBo bo);

	/**
	 * 根据编辑业务对象修改代理组织
	 * @param bo 代理组织编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokProxyBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid,Boolean inherit);


	/**
	 * 代理商给商户转账视频余额
	 * @param shopId
	 * @param videoCount
	 * @return
	 */
	Boolean transfer(Long shopId,Long videoCount);

	/**
	 * 根据商户下用户id查询代理商信息
	 * @param userId
	 * @return
	 */
	TiktokProxy selectProxyByUserId(Long userId);

	/**
	 * 获取代理商的视频转发余额
	 * @return
	 */
	Long selectVideoCount();
}
