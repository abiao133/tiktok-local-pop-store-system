package com.qingyun.shop.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokCoupon;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponAddBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponQueryBo;
import com.qingyun.shop.domain.vo.TiktokCouponListVo;
import com.qingyun.shop.domain.vo.TiktokCouponVo;
import com.qingyun.shop.domain.vo.TiktokUserCouponDrawList;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 优惠券Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokCouponService extends IServicePlus<TiktokCoupon, TiktokCouponVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokCouponVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokCouponListVo> queryPageList(TiktokCouponQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokCouponVo> queryList(TiktokCouponQueryBo bo);

	/**
	 * 根据新增业务对象插入优惠券
	 * @param bo 优惠券新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokCouponAddBo bo);

	/**
	 * 根据编辑业务对象修改优惠券
	 * @param bo 优惠券编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokCouponBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


	/**
	 * 通过活动id查询优惠卷列表
	 * @param ActivityId
	 * @return
	 */
	List<TiktokCouponVo> selectCouponVoByActivityId(Long ActivityId, Integer status);

	/**
	 * 查询领取优惠卷
	 * @param activityId
	 * @param
	 * @return
	 */
	List<TiktokCouponDraw>  selectCouponDownList(Long activityId);

	TableDataInfo<TiktokUserCouponDrawList>  selectCouponDownList(Long activityId, Integer status);

	/**
	 * 修改优惠券状态
	 * @param id
	 * @param status
	 * @return
	 */
    Boolean modifiCouponStatus(Long id, Integer status);


	/**
	 * 根据优惠卷id查询优惠卷核验情况
	 * @param shopId
	 * @param date
	 * @return
	 */
	Integer selectCouponCountByDate(Long shopId, Date date);

	/**
	 * 获取已经核销次数
	 * @return
	 */
	Integer selectCheckCount(Long shopId);

	/**
	 * 通过商户查询商户下的核销列表
	 * @param shopId
	 * @return
	 */
	Page<List<Map<String, Object>>> selectCheckDarwList(Long shopId);

	/**
	 * 今日核销
	 * @param id
	 * @return
	 */
	Integer selectCheckCountToDay(Long id);

	/**
	 * 昨日核销
	 * @param id
	 * @return
	 */
	Integer selectCheckCountYesterday(Long id);
}
