package com.qingyun.shop.service;

import com.qingyun.shop.domain.TiktokApplyProxy;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyAddBo;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyEditBo;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyQueryBo;
import com.qingyun.shop.domain.vo.TiktokApplyProxyListVo;
import com.qingyun.shop.domain.vo.TiktokApplyProxyVo;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;

import java.util.Collection;
import java.util.List;

/**
 * 代理商申请Service接口
 *
 * @author qingyun
 * @date 2021-09-10
 */
public interface ITiktokApplyProxyService extends IServicePlus<TiktokApplyProxy, TiktokApplyProxyVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokApplyProxyVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokApplyProxyListVo> queryPageList(TiktokApplyProxyQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokApplyProxyVo> queryList(TiktokApplyProxyQueryBo bo);

	/**
	 * 根据新增业务对象插入代理商申请
	 * @param bo 代理商申请新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokApplyProxyAddBo bo);

	/**
	 * 根据编辑业务对象修改代理商申请
	 * @param bo 代理商申请编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokApplyProxyEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 校验是否已申请，是否已是代理身份
	 */
	void checkShopIsProxy(Long shopId);

	/**
	 * 审核商家成为代理
	 * @param applyId
	 * @param status
	 * @return
	 */
	 Boolean checkShopApplyProxy(TiktokApplyProxy applyId,Integer status,String context);
}
