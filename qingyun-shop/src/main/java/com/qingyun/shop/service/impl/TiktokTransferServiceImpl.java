package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokTransfer;
import com.qingyun.shop.domain.bo.TiktokTransferQueryBo;
import com.qingyun.shop.domain.bo.transfer.TiktokTransferBo;
import com.qingyun.shop.domain.vo.TiktokTransferVo;
import com.qingyun.shop.mapper.TiktokTransferMapper;
import com.qingyun.shop.service.ITiktokTransferService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * 代理给商户转播放量记录Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokTransferServiceImpl extends ServicePlusImpl<TiktokTransferMapper, TiktokTransfer, TiktokTransferVo> implements ITiktokTransferService {

    @Override
    public TiktokTransferVo queryById(Long id) {
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokTransferVo> queryPageList(TiktokTransferQueryBo bo) {
        if (!SecurityUtils.isManager() && SecurityUtils.isProxy()) {
            bo.setProxyId(SecurityUtils.getProxy().getId());
        }

        PagePlus<TiktokTransfer, TiktokTransferVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokTransferVo> queryList(TiktokTransferQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokTransfer> buildQueryWrapper(TiktokTransferQueryBo bo) {

        LambdaQueryWrapper<TiktokTransfer> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProxyId() != null, TiktokTransfer::getProxyId, bo.getProxyId());
        lqw.eq(bo.getShopId() != null, TiktokTransfer::getShopId, bo.getShopId());

        lqw.like(StringUtils.isNotBlank(bo.getProxyName()), TiktokTransfer::getProxyName, bo.getProxyName());
        lqw.like(StringUtils.isNotBlank(bo.getShopName()), TiktokTransfer::getShopName, bo.getShopName());
        lqw.orderByDesc(TiktokTransfer::getCreateTime);
        return lqw;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertByBo(TiktokTransferBo bo) {
        TiktokTransfer add = BeanUtil.toBean(bo, TiktokTransfer.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateByBo(TiktokTransferBo bo) {
        TiktokTransfer update = BeanUtil.toBean(bo, TiktokTransfer.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokTransfer entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
