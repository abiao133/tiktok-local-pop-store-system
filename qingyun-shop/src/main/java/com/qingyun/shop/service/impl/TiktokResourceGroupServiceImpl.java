package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.http.HttpStatus;
import com.qingyun.common.exception.CustomException;
import com.qingyun.common.exception.ServiceException;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokResource;
import com.qingyun.shop.domain.TiktokResourceGroup;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupAddBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupQueryBo;
import com.qingyun.shop.domain.vo.TiktokResourceGroupVo;
import com.qingyun.shop.mapper.TiktokResourceGroupMapper;
import com.qingyun.shop.mapper.TiktokResourceMapper;
import com.qingyun.shop.mapper.TiktokUserShopMapper;
import com.qingyun.shop.service.ITiktokResourceGroupService;
import com.qingyun.shop.service.ITiktokShopService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 商户资源分组Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
@AllArgsConstructor
public class TiktokResourceGroupServiceImpl extends ServicePlusImpl<TiktokResourceGroupMapper, TiktokResourceGroup, TiktokResourceGroupVo> implements ITiktokResourceGroupService {

    private final TiktokUserShopMapper tiktokUserShopMapper;

    private final TiktokResourceMapper resourceMapper;

    private final ITiktokShopService tiktokShopService;

    @Override
    public TiktokResourceGroupVo queryById(Long id) {
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokResourceGroupVo> queryPageList(TiktokResourceGroupQueryBo bo) {

        if (!SecurityUtils.isManager()) {
            Long id = SecurityUtils.getShop().getId();
            bo.setShopId(id);
        }

        PagePlus<TiktokResourceGroup, TiktokResourceGroupVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));

        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokResourceGroupVo> queryList(TiktokResourceGroupQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokResourceGroup> buildQueryWrapper(TiktokResourceGroupQueryBo bo) {
        LambdaQueryWrapper<TiktokResourceGroup> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getShopId() != null, TiktokResourceGroup::getShopId, bo.getShopId());
        lqw.like(StringUtils.isNotBlank(bo.getGroupName()), TiktokResourceGroup::getGroupName, bo.getGroupName());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokResourceGroupAddBo bo) {
        TiktokResourceGroup add = BeanUtil.toBean(bo, TiktokResourceGroup.class);

        Long shopId = null;
        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();

        } else {
            shopId = add.getShopId();
        }
        if (shopId == null) {
            throw new CustomException("商户id不能为空");
        }
        Integer count = baseMapper.selectCount(Wrappers.query(new TiktokResourceGroup().setGroupName(add.getGroupName()).setShopId(shopId)));
        if (count > 0) {
            throw new CustomException("添加失败，分组名称重复");
        }
        add.setShopId(shopId);

        TiktokShop tiktokShop = tiktokShopService.getById(shopId);
        if (tiktokShop == null) {
            throw new CustomException("商户不存在");
        }
        add.setShopName(tiktokShop.getName());

        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokResourceGroupBo bo) {
        TiktokResourceGroup update = BeanUtil.toBean(bo, TiktokResourceGroup.class);
        if (!SecurityUtils.isManager()) {
            update.setShopId(SecurityUtils.getShop().getId());
        }
        if (update.getShopId() == null) {
            throw new ServiceException("商户id必传 请检查参数");
        }
        return updateById(update);
    }


    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        ids.forEach(id -> {
            Integer count = resourceMapper.selectCount(Wrappers.query(new TiktokResource().setGroupId(id)));
            if (count > 0) {
                throw new CustomException("删除失败，当前资源分组下有资源", HttpStatus.HTTP_BAD_REQUEST);
            }
        });
        return removeByIds(ids);
    }

    /**
     * 资源分类组下拉框
     *
     * @return
     */
    @Override
    public Map<Long, String> getSelect() {
        Long shopId = null;

        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();
        }

        LambdaQueryWrapper<TiktokResourceGroup> lqw = Wrappers.lambdaQuery();

        lqw.eq(shopId != null, TiktokResourceGroup::getShopId, shopId);

        List<TiktokResourceGroup> tiktokResourceGroups = baseMapper.selectList(lqw);



        return tiktokResourceGroups.stream().filter(t -> t.getGroupName() != null)
                .collect(Collectors.toMap(TiktokResourceGroup::getId, TiktokResourceGroup::getGroupName, (k1, k2) -> k2));
    }
}
