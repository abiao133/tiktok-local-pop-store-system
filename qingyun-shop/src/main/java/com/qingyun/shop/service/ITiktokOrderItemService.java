package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokOrderItem;
import com.qingyun.shop.domain.bo.order.TiktokOrderItemBo;
import com.qingyun.shop.domain.vo.TiktokOrderItemVo;

import java.util.Collection;
import java.util.List;

/**
 * 订单详情Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokOrderItemService extends IServicePlus<TiktokOrderItem, TiktokOrderItemVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokOrderItemVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokOrderItemVo> queryPageList(TiktokOrderItemBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokOrderItemVo> queryList(TiktokOrderItemBo bo);

	/**
	 * 根据新增业务对象插入订单详情
	 * @param bo 订单详情新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokOrderItemBo bo);

	/**
	 * 根据编辑业务对象修改订单详情
	 * @param bo 订单详情编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokOrderItemBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
