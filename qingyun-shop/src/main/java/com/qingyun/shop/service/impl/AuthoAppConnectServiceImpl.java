package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.model.shop.AuthoAppConnect;
import com.qingyun.shop.domain.bo.AuthoAppConnectBo;
import com.qingyun.shop.domain.vo.AuthoAppConnectVo;
import com.qingyun.shop.mapper.AuthoAppConnectMapper;
import com.qingyun.shop.service.IAuthoAppConnectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 第三方用户数据Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-16
 */
@Service
public class AuthoAppConnectServiceImpl extends ServicePlusImpl<AuthoAppConnectMapper, AuthoAppConnect, AuthoAppConnectVo> implements IAuthoAppConnectService {

    @Override
    public AuthoAppConnectVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<AuthoAppConnectVo> queryPageList(AuthoAppConnectBo bo) {
        PagePlus<AuthoAppConnect, AuthoAppConnectVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<AuthoAppConnectVo> queryList(AuthoAppConnectBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<AuthoAppConnect> buildQueryWrapper(AuthoAppConnectBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<AuthoAppConnect> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, AuthoAppConnect::getUserId, bo.getUserId());
        lqw.like(StringUtils.isNotBlank(bo.getNickName()), AuthoAppConnect::getNickName, bo.getNickName());
        lqw.eq(bo.getUserType() != null, AuthoAppConnect::getUserType, bo.getUserType());
        lqw.eq(bo.getEntryType() != null, AuthoAppConnect::getEntryType, bo.getEntryType());
        lqw.eq(StringUtils.isNotBlank(bo.getImageUrl()), AuthoAppConnect::getImageUrl, bo.getImageUrl());
        lqw.eq(StringUtils.isNotBlank(bo.getOpenId()), AuthoAppConnect::getOpenId, bo.getOpenId());
        lqw.eq(StringUtils.isNotBlank(bo.getUnionId()), AuthoAppConnect::getUnionId, bo.getUnionId());
        return lqw;
    }

    @Override
    public Boolean insertByBo(AuthoAppConnectBo bo) {
        AuthoAppConnect add = BeanUtil.toBean(bo, AuthoAppConnect.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(AuthoAppConnectBo bo) {
        AuthoAppConnect update = BeanUtil.toBean(bo, AuthoAppConnect.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(AuthoAppConnect entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
