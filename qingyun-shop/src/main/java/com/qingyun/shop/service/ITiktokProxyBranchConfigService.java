package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokProxyBranchConfig;
import com.qingyun.shop.domain.bo.branch.TiktokProxyBranchConfigBo;
import com.qingyun.shop.domain.vo.TiktokProxyBranchConfigVo;

import java.util.Collection;
import java.util.List;

/**
 * 代理商返利配置Service接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface ITiktokProxyBranchConfigService extends IServicePlus<TiktokProxyBranchConfig, TiktokProxyBranchConfigVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokProxyBranchConfigVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokProxyBranchConfigVo> queryPageList(TiktokProxyBranchConfigBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokProxyBranchConfigVo> queryList(TiktokProxyBranchConfigBo bo);

	/**
	 * 根据新增业务对象插入代理商返利配置
	 * @param bo 代理商返利配置新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokProxyBranchConfigBo bo);

	/**
	 * 根据编辑业务对象修改代理商返利配置
	 * @param bo 代理商返利配置编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokProxyBranchConfigBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
