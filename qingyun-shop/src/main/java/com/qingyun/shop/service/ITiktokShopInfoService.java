package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokShopInfo;
import com.qingyun.shop.domain.bo.shop.TiktokShopInfoBo;
import com.qingyun.shop.domain.bo.shop.TiktokShopInfoEditBo;
import com.qingyun.shop.domain.vo.TiktokShopInfoVo;

import java.util.Collection;
import java.util.List;

/**
 * 商家账号信息Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokShopInfoService extends IServicePlus<TiktokShopInfo, TiktokShopInfoVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokShopInfoVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokShopInfoVo> queryPageList(TiktokShopInfoBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokShopInfoVo> queryList(TiktokShopInfoBo bo);

	/**
	 * 根据新增业务对象插入商家账号信息
	 * @param bo 商家账号信息新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokShopInfoBo bo);

	/**
	 * 根据编辑业务对象修改商家账号信息
	 * @param bo 商家账号信息编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokShopInfoEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 根据商家id获取商家账号的详细信息
	 * @param shopId 商家id
	 * @return
	 */
	TiktokShopInfoVo queryByShopId(Long shopId);

	/**
	 * 通过活动id查询商户
	 * @param ActivityId
	 * @return
	 */
	TiktokShopInfo selectActivtyId(Long ActivityId);
}
