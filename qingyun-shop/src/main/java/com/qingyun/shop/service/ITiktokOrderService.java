package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokOrder;
import com.qingyun.shop.domain.bo.order.TiktokOrderBo;
import com.qingyun.shop.domain.bo.order.TiktokOrderCreateBo;
import com.qingyun.shop.domain.bo.order.TiktokOrderQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderDetailVo;
import com.qingyun.shop.domain.vo.TiktokOrderListVo;
import com.qingyun.shop.domain.vo.TiktokOrderVo;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 订单Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokOrderService extends IServicePlus<TiktokOrder, TiktokOrderVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokOrderDetailVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokOrderListVo> queryPageList(TiktokOrderQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokOrderListVo> queryList(TiktokOrderQueryBo bo);

	/**
	 * 根据新增业务对象插入订单
	 * @param bo 订单新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokOrderCreateBo bo);

	/**
	 * 根据编辑业务对象修改订单
	 * @param bo 订单编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokOrderBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	TiktokOrderDetailVo queryByOrderNum(String orderNum);

	/**
	 * 订单支付结果查询
	 * @param id 订单id
	 * @return
	 */
	Boolean getPayStatus(Long id);


	/**
	 * 查询失效订单
	 * @param date
	 * @return
	 */
	List<TiktokOrder> selectInvalidOrder(Date date);
}
