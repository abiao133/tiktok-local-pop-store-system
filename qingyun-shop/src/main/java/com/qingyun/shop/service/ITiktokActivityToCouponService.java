package com.qingyun.shop.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokActivityToCoupon;
import com.qingyun.shop.domain.bo.activity.TiktokActivityToCouponBo;
import com.qingyun.shop.domain.vo.TiktokActivityToCouponVo;
import java.util.Collection;
import java.util.List;

/**
 * 活动与优惠券关联Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokActivityToCouponService extends IServicePlus<TiktokActivityToCoupon, TiktokActivityToCouponVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokActivityToCouponVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokActivityToCouponVo> queryPageList(TiktokActivityToCouponBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokActivityToCouponVo> queryList(TiktokActivityToCouponBo bo);

	/**
	 * 根据新增业务对象插入活动与优惠券关联
	 * @param bo 活动与优惠券关联新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokActivityToCouponBo bo);

	/**
	 * 根据编辑业务对象修改活动与优惠券关联
	 * @param bo 活动与优惠券关联编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokActivityToCouponBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
