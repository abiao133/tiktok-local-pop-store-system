package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
    import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokOrderItem;
import com.qingyun.shop.domain.bo.order.TiktokOrderItemBo;
import com.qingyun.shop.domain.vo.TiktokOrderItemVo;
import com.qingyun.shop.mapper.TiktokOrderItemMapper;
import com.qingyun.shop.service.ITiktokOrderItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 订单详情Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokOrderItemServiceImpl extends ServicePlusImpl<TiktokOrderItemMapper, TiktokOrderItem, TiktokOrderItemVo> implements ITiktokOrderItemService {

    @Override
    public TiktokOrderItemVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokOrderItemVo> queryPageList(TiktokOrderItemBo bo) {
        PagePlus<TiktokOrderItem, TiktokOrderItemVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokOrderItemVo> queryList(TiktokOrderItemBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokOrderItem> buildQueryWrapper(TiktokOrderItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokOrderItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getOrderNum()), TiktokOrderItem::getOrderNum, bo.getOrderNum());
        lqw.eq(bo.getGoodsId() != null, TiktokOrderItem::getGoodsId, bo.getGoodsId());
        lqw.like(StringUtils.isNotBlank(bo.getGoodsName()), TiktokOrderItem::getGoodsName, bo.getGoodsName());
        lqw.eq(bo.getQuantity() != null, TiktokOrderItem::getQuantity, bo.getQuantity());
        lqw.eq(bo.getAmount() != null, TiktokOrderItem::getAmount, bo.getAmount());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokOrderItemBo bo) {
        TiktokOrderItem add = BeanUtil.toBean(bo, TiktokOrderItem.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokOrderItemBo bo) {
        TiktokOrderItem update = BeanUtil.toBean(bo, TiktokOrderItem.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokOrderItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
