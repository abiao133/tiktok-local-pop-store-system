package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
    import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokBranchRecord;
import com.qingyun.shop.domain.bo.branch.TiktokBranchRecordBo;
import com.qingyun.shop.domain.vo.TiktokBranchRecordVo;
import com.qingyun.shop.mapper.TiktokBranchRecordMapper;
import com.qingyun.shop.service.ITiktokBranchRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 分销返利记录Service业务层处理
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Service
public class TiktokBranchRecordServiceImpl extends ServicePlusImpl<TiktokBranchRecordMapper, TiktokBranchRecord, TiktokBranchRecordVo> implements ITiktokBranchRecordService {

    @Override
    public TiktokBranchRecordVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokBranchRecordVo> queryPageList(TiktokBranchRecordBo bo) {
		if (!SecurityUtils.isManager()) {
			bo.setProxyId(SecurityUtils.getProxy().getId());
		}
        PagePlus<TiktokBranchRecord, TiktokBranchRecordVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokBranchRecordVo> queryList(TiktokBranchRecordBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokBranchRecord> buildQueryWrapper(TiktokBranchRecordBo bo) {
        LambdaQueryWrapper<TiktokBranchRecord> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProxyId()!=null,TiktokBranchRecord::getProxyId,bo.getProxyId());
        lqw.eq(StringUtils.isNotBlank(bo.getOrderNum()), TiktokBranchRecord::getOrderNum, bo.getOrderNum());
        lqw.like(StringUtils.isNotBlank(bo.getPublicName()), TiktokBranchRecord::getPublicName, bo.getPublicName());
        lqw.orderByDesc(TiktokBranchRecord::getCreateTime);
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokBranchRecordBo bo) {
        TiktokBranchRecord add = BeanUtil.toBean(bo, TiktokBranchRecord.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokBranchRecordBo bo) {
        TiktokBranchRecord update = BeanUtil.toBean(bo, TiktokBranchRecord.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokBranchRecord entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
