package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokCoupon;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDranQueryBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDrawBo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawListVo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawVo;
import com.qingyun.shop.mapper.TiktokCouponDrawMapper;
import com.qingyun.shop.service.ITiktokCouponDrawService;
import com.qingyun.shop.service.ITiktokCouponService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 用户优惠卷领取Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokCouponDrawServiceImpl extends ServicePlusImpl<TiktokCouponDrawMapper, TiktokCouponDraw, TiktokCouponDrawVo> implements ITiktokCouponDrawService {



    @Override
    public TiktokCouponDrawVo queryById(Long id) {
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokCouponDrawListVo> queryPageList(TiktokCouponDranQueryBo bo) {

        Page<TiktokCouponDrawListVo> result = baseMapper.selectCouponDrawList(PageUtils.buildPage(), bo);

        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokCouponDrawVo> queryList(TiktokCouponDrawBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokCouponDraw> buildQueryWrapper(TiktokCouponDrawBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokCouponDraw> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCouponId() != null, TiktokCouponDraw::getCouponId, bo.getCouponId());
        lqw.eq(bo.getActivityId() != null, TiktokCouponDraw::getActivityId, bo.getActivityId());
        lqw.eq(bo.getUserId() != null, TiktokCouponDraw::getUserId, bo.getUserId());
        lqw.eq(StringUtils.isNotBlank(bo.getCouponCode()), TiktokCouponDraw::getCouponCode, bo.getCouponCode());
        lqw.eq(bo.getGetType() != null, TiktokCouponDraw::getGetType, bo.getGetType());
        lqw.eq(bo.getBeginDate() != null, TiktokCouponDraw::getBeginDate, bo.getBeginDate());
        lqw.eq(bo.getEndDate() != null, TiktokCouponDraw::getEndDate, bo.getEndDate());
        lqw.eq(bo.getUseStatus() != null, TiktokCouponDraw::getUseStatus, bo.getUseStatus());
        lqw.eq(bo.getUseTime() != null, TiktokCouponDraw::getUseTime, bo.getUseTime());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokCouponDrawBo bo) {
        TiktokCouponDraw add = BeanUtil.toBean(bo, TiktokCouponDraw.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokCouponDrawBo bo) {
        TiktokCouponDraw update = BeanUtil.toBean(bo, TiktokCouponDraw.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }



	/**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokCouponDraw entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

	@Override
	public TiktokCouponDraw selectOneWaitSendByUserId(Long tiktokUserId) {
		return baseMapper.selectOneWaitSendByUserId(tiktokUserId);
	}
}
