package com.qingyun.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokGoods;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsAddBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsEditBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsQueryBo;
import com.qingyun.shop.domain.vo.TiktokGoodsVo;
import com.qingyun.shop.mapper.TiktokGoodsMapper;
import com.qingyun.shop.service.ITiktokGoodsService;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 套餐Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
@AllArgsConstructor
public class TiktokGoodsServiceImpl extends ServicePlusImpl<TiktokGoodsMapper, TiktokGoods, TiktokGoodsVo> implements ITiktokGoodsService {

	private final MapperFacade mapperFacade;

    @Override
    public TiktokGoodsVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokGoodsVo> queryPageList(TiktokGoodsQueryBo bo) {
        PagePlus<TiktokGoods, TiktokGoodsVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokGoodsVo> queryList(TiktokGoodsQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokGoods> buildQueryWrapper(TiktokGoodsQueryBo bo) {

        LambdaQueryWrapper<TiktokGoods> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), TiktokGoods::getName, bo.getName());
//        lqw.eq(bo.getVoideCount() != null, TiktokGoods::getVoideCount, bo.getVoideCount());
//        lqw.eq(bo.getPrice() != null, TiktokGoods::getPrice, bo.getPrice());
//        lqw.eq(bo.getMarketPrice() != null, TiktokGoods::getMarketPrice, bo.getMarketPrice());
//        lqw.eq(bo.getProxyPrice() != null, TiktokGoods::getProxyPrice, bo.getProxyPrice());
        lqw.eq(bo.getStatus() != null, TiktokGoods::getStatus, bo.getStatus());
        return lqw;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertByBo(TiktokGoodsAddBo bo) {
		TiktokGoods goods = mapperFacade.map(bo, TiktokGoods.class);
		validEntityBeforeSave(goods);

		goods.setCreateBy(SecurityUtils.getUser().getNickName());
		goods.setCreateTime(new Date());
		goods.setUpdateBy(SecurityUtils.getUser().getNickName());
		goods.setUpdateTime(new Date());

        return save(goods);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateByBo(TiktokGoodsEditBo bo) {
		TiktokGoods goods = mapperFacade.map(bo, TiktokGoods.class);
        validEntityBeforeSave(goods);

		goods.setUpdateBy(SecurityUtils.getUser().getNickName());
		goods.setUpdateTime(new Date());

        return updateById(goods);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokGoods entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

	/**
	 * 修改套餐上下架
	 *
	 * @param id     套餐id
	 * @param status 要修改的状态
	 * @return
	 */
	@Override
	public Boolean editStatus(Long id, Integer status) {
		TiktokGoods goods = new TiktokGoods();
		goods.setId(id);
		goods.setStatus(status);
		goods.setUpdateBy(SecurityUtils.getUser().getNickName());
		goods.setUpdateTime(new Date());
		return baseMapper.updateById(goods) > 0;
	}
}
