package com.qingyun.shop.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDranQueryBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDrawBo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawListVo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawVo;

import java.util.Collection;
import java.util.List;

/**
 * 用户优惠卷领取Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokCouponDrawService extends IServicePlus<TiktokCouponDraw, TiktokCouponDrawVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokCouponDrawVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokCouponDrawListVo> queryPageList(TiktokCouponDranQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokCouponDrawVo> queryList(TiktokCouponDrawBo bo);

	/**
	 * 根据新增业务对象插入用户优惠卷领取
	 * @param bo 用户优惠卷领取新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokCouponDrawBo bo);

	/**
	 * 根据编辑业务对象修改用户优惠卷领取
	 * @param bo 用户优惠卷领取编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokCouponDrawBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 查询待发送的优惠券
	 * @param tiktokUserId
	 * @return
	 */
    TiktokCouponDraw selectOneWaitSendByUserId(Long tiktokUserId);
}
