package com.qingyun.shop.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokTag;
import com.qingyun.shop.domain.bo.tag.TiktokTagAddBo;
import com.qingyun.shop.domain.bo.tag.TiktokTagBo;
import com.qingyun.shop.domain.bo.tag.TiktokTagEditBo;
import com.qingyun.shop.domain.vo.TiktokTagSelectListVo;
import com.qingyun.shop.domain.vo.TiktokTagVo;

import java.util.Collection;
import java.util.List;

/**
 * 标签Service接口
 *
 * @author qingyun
 * @date 2021-09-26
 */
public interface ITiktokTagService extends IServicePlus<TiktokTag, TiktokTagVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokTagVo queryById(Long tId);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokTagVo> queryPageList(TiktokTagBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokTagVo> queryList(TiktokTagBo bo);

	/**
	 * 根据新增业务对象插入标签
	 * @param bo 标签新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokTagAddBo bo);

	/**
	 * 根据编辑业务对象修改标签
	 * @param bo 标签编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokTagEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<TiktokTagSelectListVo> getSelectTags();
}
