package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
    import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokProxyBranchConfig;
import com.qingyun.shop.domain.bo.branch.TiktokProxyBranchConfigBo;
import com.qingyun.shop.domain.vo.TiktokProxyBranchConfigVo;
import com.qingyun.shop.mapper.TiktokProxyBranchConfigMapper;
import com.qingyun.shop.service.ITiktokProxyBranchConfigService;
import org.springframework.stereotype.Service;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;


import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 代理商返利配置Service业务层处理
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Service
public class TiktokProxyBranchConfigServiceImpl extends ServicePlusImpl<TiktokProxyBranchConfigMapper, TiktokProxyBranchConfig, TiktokProxyBranchConfigVo> implements ITiktokProxyBranchConfigService {

    @Override
    public TiktokProxyBranchConfigVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokProxyBranchConfigVo> queryPageList(TiktokProxyBranchConfigBo bo) {
        PagePlus<TiktokProxyBranchConfig, TiktokProxyBranchConfigVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokProxyBranchConfigVo> queryList(TiktokProxyBranchConfigBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokProxyBranchConfig> buildQueryWrapper(TiktokProxyBranchConfigBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokProxyBranchConfig> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProxyId() != null, TiktokProxyBranchConfig::getProxyId, bo.getProxyId());
        lqw.eq(bo.getProxyPrice() != null, TiktokProxyBranchConfig::getProxyPrice, bo.getProxyPrice());
        lqw.eq(bo.getOneProxyRatio() != null, TiktokProxyBranchConfig::getOneProxyRatio, bo.getOneProxyRatio());
        lqw.eq(bo.getTowProxyRatio() != null, TiktokProxyBranchConfig::getTowProxyRatio, bo.getTowProxyRatio());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokProxyBranchConfigBo bo) {
        TiktokProxyBranchConfig add = BeanUtil.toBean(bo, TiktokProxyBranchConfig.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokProxyBranchConfigBo bo) {
        TiktokProxyBranchConfig update = BeanUtil.toBean(bo, TiktokProxyBranchConfig.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokProxyBranchConfig entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
