package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokShopDocument;
import com.qingyun.shop.domain.bo.resource.TiktokShopDocumentBo;
import com.qingyun.shop.domain.vo.TiktokShopDocumentVo;

import java.util.Collection;
import java.util.List;

/**
 * 商户文案Service接口
 *
 * @author qingyun
 * @date 2021-10-21
 */
public interface ITiktokShopDocumentService extends IServicePlus<TiktokShopDocument, TiktokShopDocumentVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokShopDocumentVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokShopDocumentVo> queryPageList(TiktokShopDocumentBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokShopDocumentVo> queryList(TiktokShopDocumentBo bo);

	/**
	 * 根据新增业务对象插入商户文案
	 * @param bo 商户文案新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokShopDocumentBo bo);

	/**
	 * 根据编辑业务对象修改商户文案
	 * @param bo 商户文案编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokShopDocumentBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
