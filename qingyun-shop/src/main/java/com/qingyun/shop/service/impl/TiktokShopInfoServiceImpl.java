package com.qingyun.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.shop.domain.TiktokShopInfo;
import com.qingyun.shop.domain.bo.shop.TiktokShopInfoBo;
import com.qingyun.shop.domain.bo.shop.TiktokShopInfoEditBo;
import com.qingyun.shop.domain.vo.TiktokShopInfoVo;
import com.qingyun.shop.mapper.TiktokShopInfoMapper;
import com.qingyun.shop.service.ITiktokShopInfoService;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 商家账号信息Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
@AllArgsConstructor
public class TiktokShopInfoServiceImpl extends ServicePlusImpl<TiktokShopInfoMapper, TiktokShopInfo, TiktokShopInfoVo> implements ITiktokShopInfoService {

	private final MapperFacade mapperFacade;

    @Override
    public TiktokShopInfoVo queryById(Long id){
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokShopInfoVo> queryPageList(TiktokShopInfoBo bo) {
        PagePlus<TiktokShopInfo, TiktokShopInfoVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokShopInfoVo> queryList(TiktokShopInfoBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokShopInfo> buildQueryWrapper(TiktokShopInfoBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TiktokShopInfo> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getShopId() != null, TiktokShopInfo::getShopId, bo.getShopId());
        lqw.eq(StringUtils.isNotBlank(bo.getOpenId()), TiktokShopInfo::getOpenId, bo.getOpenId());
        lqw.like(StringUtils.isNotBlank(bo.getNickname()), TiktokShopInfo::getNickname, bo.getNickname());
        lqw.eq(StringUtils.isNotBlank(bo.getPoiId()), TiktokShopInfo::getPoiId, bo.getPoiId());
        lqw.like(StringUtils.isNotBlank(bo.getPoiName()), TiktokShopInfo::getPoiName, bo.getPoiName());
        lqw.eq(StringUtils.isNotBlank(bo.getMiniAppId()), TiktokShopInfo::getMiniAppId, bo.getMiniAppId());
        lqw.eq(StringUtils.isNotBlank(bo.getMiniAppTitle()), TiktokShopInfo::getMiniAppTitle, bo.getMiniAppTitle());
        lqw.eq(StringUtils.isNotBlank(bo.getMiniAppUrl()), TiktokShopInfo::getMiniAppUrl, bo.getMiniAppUrl());
        lqw.eq(StringUtils.isNotBlank(bo.getAddress()), TiktokShopInfo::getAddress, bo.getAddress());
        lqw.eq(StringUtils.isNotBlank(bo.getLat()), TiktokShopInfo::getLat, bo.getLat());
        lqw.eq(StringUtils.isNotBlank(bo.getLog()), TiktokShopInfo::getLog, bo.getLog());
        lqw.eq(StringUtils.isNotBlank(bo.getDouyinUrl()), TiktokShopInfo::getDouyinUrl, bo.getDouyinUrl());
        return lqw;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
	@CacheEvict(cacheNames = "GetAcitvity", allEntries=true)
    public Boolean insertByBo(TiktokShopInfoBo bo) {
        TiktokShopInfo add = BeanUtil.toBean(bo, TiktokShopInfo.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
	@CacheEvict(cacheNames = "GetAcitvity", allEntries=true)
    public Boolean updateByBo(TiktokShopInfoEditBo bo) {
        TiktokShopInfo update = BeanUtil.toBean(bo, TiktokShopInfo.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

	@Override
	public TiktokShopInfo selectActivtyId(Long ActivityId) {
		return baseMapper.selectActivtyId(ActivityId);
	}

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokShopInfo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
	@CacheEvict(cacheNames = "GetAcitvity", allEntries=true)
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

	/**
	 * 根据商家id获取商家账号的详细信息
	 *
	 * @param shopId 商家id
	 * @return
	 */
	@Override
	public TiktokShopInfoVo queryByShopId(Long shopId) {
		TiktokShopInfo tiktokShopInfo = this.baseMapper.selectOne(Wrappers.query(new TiktokShopInfo().setShopId(shopId)));
		return mapperFacade.map(tiktokShopInfo, TiktokShopInfoVo.class);
	}
}
