package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokWithdrawApply;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyAddBo;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyBo;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyQueryBo;
import com.qingyun.shop.domain.vo.TiktokWithdrawApplyVo;

import java.util.Collection;
import java.util.List;

/**
 * 商户提现申请Service接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface ITiktokWithdrawApplyService extends IServicePlus<TiktokWithdrawApply, TiktokWithdrawApplyVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokWithdrawApplyVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokWithdrawApplyVo> queryPageList(TiktokWithdrawApplyQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokWithdrawApplyVo> queryList(TiktokWithdrawApplyQueryBo bo);

	/**
	 * 根据新增业务对象插入商户提现申请
	 * @param bo 商户提现申请新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokWithdrawApplyAddBo bo);

	/**
	 * 根据编辑业务对象修改商户提现申请
	 * @param bo 商户提现申请编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokWithdrawApplyBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


	Boolean check(Long id, Integer status, String rejectReason);
}
