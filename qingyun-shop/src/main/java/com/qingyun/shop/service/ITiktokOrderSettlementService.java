package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokOrderSettlement;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementBo;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementListVo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementVo;

import java.util.Collection;
import java.util.List;

/**
 * 订单流水Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokOrderSettlementService extends IServicePlus<TiktokOrderSettlement, TiktokOrderSettlementVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokOrderSettlementVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokOrderSettlementListVo> queryPageList(TiktokOrderSettlementQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokOrderSettlementVo> queryList(TiktokOrderSettlementQueryBo bo);

	/**
	 * 根据新增业务对象插入订单流水
	 * @param bo 订单流水新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokOrderSettlementBo bo);

	/**
	 * 根据编辑业务对象修改订单流水
	 * @param bo 订单流水编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokOrderSettlementBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
