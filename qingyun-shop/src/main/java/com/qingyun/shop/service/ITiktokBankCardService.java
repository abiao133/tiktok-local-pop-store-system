package com.qingyun.shop.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.shop.domain.TiktokBankCard;
import com.qingyun.shop.domain.bo.shop.*;
import com.qingyun.shop.domain.vo.TiktokBankCardExcelVo;
import com.qingyun.shop.domain.vo.TiktokBankCardListVo;
import com.qingyun.shop.domain.vo.TiktokBankCardVo;

import java.util.Collection;
import java.util.List;

/**
 * 商户银行卡信息Service接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface ITiktokBankCardService extends IServicePlus<TiktokBankCard, TiktokBankCardVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokBankCardVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokBankCardListVo> queryPageList(TiktokBankCardQueryBo bo);

	/**
	 * 查询导出
	 */
	List<TiktokBankCardExcelVo> queryList(TiktokBankCardQueryBo bo);

	/**
	 * 根据新增业务对象插入商户银行卡信息
	 * @param bo 商户银行卡信息新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokBankCardAddBo bo);

	/**
	 * 根据编辑业务对象修改商户银行卡信息
	 * @param bo 商户银行卡信息编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokBankCardEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	List<TiktokBankCardVo> queryByProxyId(Long proxyId);

    Boolean editStatus(Long id, Integer status);

	/**
	 * 银行卡三要素校验
	 */
	Boolean realCheck(BankCardRealCheckBo checkBo);
}
