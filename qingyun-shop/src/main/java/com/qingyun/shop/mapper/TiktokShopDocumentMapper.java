package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokShopDocument;
import com.qingyun.shop.domain.vo.TiktokShopDocumentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商户文案Mapper接口
 *
 * @author qingyun
 * @date 2021-10-21
 */
public interface TiktokShopDocumentMapper extends BaseMapperPlus<TiktokShopDocument> {

	Page<TiktokShopDocumentVo> selectDocumentList(@Param("page") Page<Object> buildPage, @Param("shopId") Long shopId,@Param("shopName") String shopName);

}
