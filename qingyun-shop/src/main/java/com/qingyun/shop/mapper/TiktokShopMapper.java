package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.bo.shop.TiktokShopUserListBo;
import com.qingyun.shop.domain.vo.TiktokShopUserListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 商家Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokShopMapper extends BaseMapperPlus<TiktokShop> {

    Page<TiktokShopUserListVo> selectUserListByShopId(@Param("page") Page<Object> buildPage, @Param("user") TiktokShopUserListBo user);

	/**
	 * 通过商户id修改商户名称
	 * @param shopId
	 * @param ShopName
	 * @return
	 */
	Boolean updateShopNameByShopId(@Param("shopId") Long shopId,@Param("shopName") String ShopName);


}
