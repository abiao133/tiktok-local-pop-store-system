package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokOrderSettlement;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 订单流水Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokOrderSettlementMapper extends BaseMapperPlus<TiktokOrderSettlement> {

    Page<TiktokOrderSettlementListVo> selectOrderSettlementList(@Param("page") Page<Object> page, @Param("query") TiktokOrderSettlementQueryBo query);
}
