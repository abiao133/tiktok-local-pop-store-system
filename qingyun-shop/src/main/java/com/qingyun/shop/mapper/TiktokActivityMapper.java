package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokActivity;
import com.qingyun.shop.domain.TiktokShop;
import org.apache.ibatis.annotations.Param;

/**
 * 优惠券活动Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokActivityMapper extends BaseMapperPlus<TiktokActivity> {

}
