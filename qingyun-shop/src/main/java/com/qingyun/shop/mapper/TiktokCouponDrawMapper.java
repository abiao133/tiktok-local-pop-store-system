package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDranQueryBo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawListVo;
import com.qingyun.shop.domain.vo.TiktokUserCouponDrawList;
import org.apache.ibatis.annotations.Param;

/**
 * 用户优惠卷领取Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokCouponDrawMapper extends BaseMapperPlus<TiktokCouponDraw> {

    Page<TiktokCouponDrawListVo> selectCouponDrawList(@Param("page") Page<Object> page, @Param("query") TiktokCouponDranQueryBo query);

    Page<TiktokUserCouponDrawList> selectTiktokUserCouponDrawList(@Param("page") Page<Object> page,
                                                                  @Param("userId") Long userId,
                                                                  @Param("activityId") Long activityId,
                                                                  @Param("status") Integer status);


	TiktokCouponDraw selectOneWaitSendByUserId(Long tiktokUserId);
}
