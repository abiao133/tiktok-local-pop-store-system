package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokGoods;

/**
 * 套餐Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokGoodsMapper extends BaseMapperPlus<TiktokGoods> {

}
