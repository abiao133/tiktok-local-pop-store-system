package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokTransfer;

/**
 * 代理给商户转播放量记录Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokTransferMapper extends BaseMapperPlus<TiktokTransfer> {

}
