package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.security.model.shop.AuthoAppConnect;
import java.util.List;

/**
 * 第三方用户数据Mapper接口
 *
 * @author qingyun
 * @date 2021-09-16
 */
public interface AuthoAppConnectMapper extends BaseMapperPlus<AuthoAppConnect> {

	List<String> selectOpenIdByShopId(Long shopId);

	

}
