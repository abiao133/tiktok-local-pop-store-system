package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokOrder;
import com.qingyun.shop.domain.bo.order.TiktokOrderQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderDetailVo;
import com.qingyun.shop.domain.vo.TiktokOrderListVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 订单Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokOrderMapper extends BaseMapperPlus<TiktokOrder> {

	/**
	 * 动态条件查询订单列表信息
	 */
    Page<TiktokOrderListVo> selectOrderList(@Param("page") Page<Object> page, @Param("query") TiktokOrderQueryBo query);

	/**
	 * 订单id查询订单详细信息
	 */
	TiktokOrderDetailVo selectOrderDetailById(@Param("id") Long id);

	/**
	 * 根据订单编号获取订单详细信息
	 */
	TiktokOrderDetailVo selectOrderDetailByOrderNum(@Param("orderNum") String orderNum);

	/**
	 * 查询过期订单
	 * @param date
	 * @return
	 */
	List<TiktokOrder> selectInvalidOrder(Date date);
}
