package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokTag;
import com.qingyun.shop.domain.vo.TiktokTagSelectListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 标签Mapper接口
 *
 * @author qingyun
 * @date 2021-09-26
 */
public interface TiktokTagMapper extends BaseMapperPlus<TiktokTag> {

    List<TiktokTagSelectListVo> getSelectTags( @Param("shopId") Long shopId);
}
