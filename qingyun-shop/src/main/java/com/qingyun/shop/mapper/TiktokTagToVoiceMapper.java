package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokTagToVoice;

import java.util.List;
import java.util.Map;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author qingyun
 * @date 2021-10-21
 */
public interface TiktokTagToVoiceMapper extends BaseMapperPlus<TiktokTagToVoice> {


	Map<String,String> selectRandTagVoice(Long activityId);

	List<String> selectVoiceTagStr(Long voiceId);
}
