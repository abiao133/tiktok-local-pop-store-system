package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokResourceGroup;

/**
 * 商户资源分组Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokResourceGroupMapper extends BaseMapperPlus<TiktokResourceGroup> {

}
