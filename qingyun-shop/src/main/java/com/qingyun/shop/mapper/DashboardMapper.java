package com.qingyun.shop.mapper;

import com.qingyun.shop.eneity.VideoBasicData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据看板 mapper
 */
public interface DashboardMapper {
    // 获取商户数量
    Long getShopCount(@Param("proxyId") Long proxyId);

    // 获取代理商数量
    Long getProxyCount(@Param("proxyId") Long proxyId);

    // 获取优惠券id列表
    List<Long> selectIdByShopId(@Param("shopId") Long userId, @Param("limit") Integer limit);

    // 获取领取优惠券各状态数量
    Map<String, Object> selectCouponDrawStatus(Long id);

    // 获取资源数量
    Integer selectResourceCount(@Param("shopId") Long shopId);

    // 剩余视频发送数量
    Integer selectVideoSentCount(@Param("shopId") Long shopId);

    // 发布优惠券活动数量
    Integer selectActivity(@Param("shopId") Long shopId);

    // 获取领取订单各状态数量
    Map<String, Object> selectOrderStatusCount(@Param("shopId") Long shopId);

    List<VideoBasicData> selectVideoData7Day(@Param("shopId") Long shopId);

    // 获取优惠券活动各状态数量
    Map<String, Object> selectActivityStatus(@Param("shopId") Long shopId);
}
