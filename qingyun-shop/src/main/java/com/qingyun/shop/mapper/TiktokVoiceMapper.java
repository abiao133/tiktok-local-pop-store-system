package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokVoice;

import java.util.Map;

/**
 * 合成语音Mapper接口
 *
 * @author qingyun
 * @date 2021-10-21
 */
public interface TiktokVoiceMapper extends BaseMapperPlus<TiktokVoice> {

	TiktokVoice selectRandomVoice(Long shopId);
}
