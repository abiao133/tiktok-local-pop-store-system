package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.TiktokShopInfo;

/**
 * 商家账号信息Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokShopInfoMapper extends BaseMapperPlus<TiktokShopInfo> {

	TiktokShopInfo selectActivtyId(Long activityId);
}
