package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokResource;
import org.apache.ibatis.annotations.Param;

/**
 * 资源Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokResourceMapper extends BaseMapperPlus<TiktokResource> {

	TiktokResource selectRandomResourceByGroupId(@Param("shopId") Long shopId,@Param("groupId") Long groupId,@Param("format") Integer format,@Param("isFrag") Integer isFrag);

}
