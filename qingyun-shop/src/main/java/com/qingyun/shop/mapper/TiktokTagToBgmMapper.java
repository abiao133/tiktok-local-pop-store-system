package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokTagToBgm;

/**
 * 标签与背景音乐多对多关联Mapper接口
 *
 * @author qingyun
 * @date 2021-09-26
 */
public interface TiktokTagToBgmMapper extends BaseMapperPlus<TiktokTagToBgm> {

}
