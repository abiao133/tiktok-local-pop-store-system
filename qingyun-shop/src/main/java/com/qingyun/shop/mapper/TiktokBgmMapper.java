package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokBgm;

import org.apache.ibatis.annotations.Param;


/**
 * bgm资源Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokBgmMapper extends BaseMapperPlus<TiktokBgm> {

	/**
	 * 根据商户活动查询bgm
	 * @param shopId
	 * @param activityId
	 * @param duration
	 * @return
	 */
	TiktokBgm selectBgmBy(@Param("shopId") Long shopId, @Param("activityId") Long activityId, @Param("duration") Double duration,@Param("size") Boolean size);


	/**
	 * 根据商户活动查询bgm
	 * @param shopId
	 * @param global
	 * @param duration
	 * @return
	 */
	TiktokBgm selectRandomBgm(@Param("shopId") Long shopId, @Param("global") Integer global, @Param("duration") Double duration,@Param("size") Boolean size);

}
