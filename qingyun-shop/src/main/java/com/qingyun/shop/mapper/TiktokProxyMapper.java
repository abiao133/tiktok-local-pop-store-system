package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokProxy;

/**
 * 代理组织Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokProxyMapper extends BaseMapperPlus<TiktokProxy> {

	Long selectProxyStatus(Long userId);

}
