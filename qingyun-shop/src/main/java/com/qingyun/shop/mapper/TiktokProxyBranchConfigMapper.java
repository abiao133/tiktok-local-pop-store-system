package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokProxyBranchConfig;

/**
 * 代理商返利配置Mapper接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface TiktokProxyBranchConfigMapper extends BaseMapperPlus<TiktokProxyBranchConfig> {

}
