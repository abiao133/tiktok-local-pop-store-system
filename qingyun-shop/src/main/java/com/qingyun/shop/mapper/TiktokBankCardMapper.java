package com.qingyun.shop.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokBankCard;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardQueryBo;
import com.qingyun.shop.domain.vo.TiktokBankCardExcelVo;
import com.qingyun.shop.domain.vo.TiktokBankCardListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商户银行卡信息Mapper接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface TiktokBankCardMapper extends BaseMapperPlus<TiktokBankCard> {

    Page<TiktokBankCardListVo> selectBankCardList(@Param("page") Page<Object> buildPage, @Param("query") TiktokBankCardQueryBo query);

    List<TiktokBankCardExcelVo> selectBankCardExcelList(@Param("query") TiktokBankCardQueryBo query);
}
