package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokActivityTag;

/**
 * 优惠券活动与标签多对多关联Mapper接口
 *
 * @author qingyun
 * @date 2021-09-26
 */
public interface TiktokActivityTagMapper extends BaseMapperPlus<TiktokActivityTag> {

}
