package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokCoupon;
import com.qingyun.shop.domain.bo.coupon.TiktokAppCouponQueryBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponQueryBo;
import com.qingyun.shop.domain.vo.TiktokAppCouponListVo;
import com.qingyun.shop.domain.vo.TiktokCouponListVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 优惠券Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokCouponMapper extends BaseMapperPlus<TiktokCoupon> {

    List<TiktokCoupon> selectCouponByActivityId(@Param("activityId") Long activityId, @Param("status") Integer status);

    Page<TiktokCouponListVo> selectCouponList(@Param("page") Page<Object> page, @Param("query") TiktokCouponQueryBo query);

    /**
     * 根据商户id查询商户优惠卷核验情况
     *
     * @param shopId
     * @param date
     * @return
     */
    Integer selectCouponCountByDate(@Param("shopId") Long shopId, @Param("date") Date date);

    Page<TiktokAppCouponListVo> selectAppCouponList(@Param("page") Page<Object> page, @Param("query") TiktokAppCouponQueryBo query);


    Integer selectCheckCount(Long shopId);

	Page<List<Map<String, Object>>> selectCheckDarwList(@Param("page") Page<Object> page, @Param("shopId") Long shopId);
    /**
     * 获取指定商户的优惠券有效剩余数量
     * @param shopId 商户id
     * @return
     */
    Long getResidueByShopId(@Param("shopId") Long shopId);

    /**
     * 今日核销
     * @param id
     * @return
     */
    Integer selectCheckCountToDay(Long id);

    /**
     * 昨日核销
     * @param id
     * @return
     */
    Integer selectCheckCountYesterday(Long id);
}
