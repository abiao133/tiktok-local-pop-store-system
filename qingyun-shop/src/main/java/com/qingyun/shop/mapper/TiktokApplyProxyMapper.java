package com.qingyun.shop.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.shop.domain.TiktokApplyProxy;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyQueryBo;
import com.qingyun.shop.domain.vo.TiktokApplyProxyListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 代理商申请Mapper接口
 *
 * @author qingyun
 * @date 2021-09-10
 */
public interface TiktokApplyProxyMapper extends BaseMapperPlus<TiktokApplyProxy> {

	Page<TiktokApplyProxyListVo> selectApplyProxyList(@Param("page") Page<Object> buildPage, @Param("query") TiktokApplyProxyQueryBo bo);
}
