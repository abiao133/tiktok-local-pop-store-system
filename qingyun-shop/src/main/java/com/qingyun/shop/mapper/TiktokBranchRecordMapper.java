package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokBranchRecord;

/**
 * 分销返利记录Mapper接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface TiktokBranchRecordMapper extends BaseMapperPlus<TiktokBranchRecord> {

}
