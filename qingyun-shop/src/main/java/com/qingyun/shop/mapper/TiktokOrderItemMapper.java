package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokOrderItem;

/**
 * 订单详情Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokOrderItemMapper extends BaseMapperPlus<TiktokOrderItem> {

}
