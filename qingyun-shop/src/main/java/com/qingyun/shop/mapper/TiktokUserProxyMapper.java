package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokUserProxy;

/**
 * 用户与代理关联表Mapper接口
 *
 * @author qingyun
 * @date 2021-09-09
 */
public interface TiktokUserProxyMapper extends BaseMapperPlus<TiktokUserProxy> {

}
