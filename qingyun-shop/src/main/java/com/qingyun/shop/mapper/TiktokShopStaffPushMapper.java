package com.qingyun.shop.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokShopStaffPush;
import com.qingyun.shop.domain.vo.TiktokShopStaffPushVo;
import org.apache.ibatis.annotations.Param;


/**
 * 员工宣传记录Mapper接口
 *
 * @author qingyun
 * @date 2021-10-30
 */
public interface TiktokShopStaffPushMapper extends BaseMapperPlus<TiktokShopStaffPush> {

	Page<TiktokShopStaffPushVo> selectPageList(@Param("page") Page<Object> buildPage,@Param("shopUserId") Long shopUserId,@Param("type") Long type);

	Integer selectTotalCount(@Param("shopUserId") Long shopUserId);

	Integer selectCheckCount(@Param("shopUserId") Long shopUserId);
}
