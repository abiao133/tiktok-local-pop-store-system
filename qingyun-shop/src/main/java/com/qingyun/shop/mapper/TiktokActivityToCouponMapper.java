package com.qingyun.shop.mapper;


import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokActivityToCoupon;

import java.util.List;

/**
 * 活动与优惠券关联Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokActivityToCouponMapper extends BaseMapperPlus<TiktokActivityToCoupon> {

	List<String> getActTitleByCoupId(Long couponId);

}
