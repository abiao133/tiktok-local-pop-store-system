package com.qingyun.shop.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.shop.domain.TiktokWithdrawApply;

/**
 * 商户提现申请Mapper接口
 *
 * @author qingyun
 * @date 2021-10-19
 */
public interface TiktokWithdrawApplyMapper extends BaseMapperPlus<TiktokWithdrawApply> {

}
