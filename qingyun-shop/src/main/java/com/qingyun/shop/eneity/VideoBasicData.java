package com.qingyun.shop.eneity;

import lombok.Data;

@Data
public class VideoBasicData {
    private String time;
    //点赞数量
    private Long diggCount;
    //下载数量
    private Long downloadCount;
    //转发数量
    private Long forwardCount;
    //播放数量
    private Long playCount;
    //分享数量
    private Long shareCount;
    //评论数量
    private Long commentCount;
}
