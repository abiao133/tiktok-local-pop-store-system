package com.qingyun.shop.eneity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 折线图
 */
@Data
@Builder
public class StackedLine {

    private String name;

    private String type;

    private String stack;

    private List<Long> data;
}
