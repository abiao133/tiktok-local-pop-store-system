package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 分销返利记录视图对象 tiktok_branch_record
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@ApiModel("分销返利记录视图对象")
@ExcelIgnoreUnannotated
public class TiktokBranchRecordVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 1代理商给代理商 2 商家给代理商
     */
	@ExcelProperty(value = "1代理商给代理商 2 商家给代理商")
	@ApiModelProperty("1代理商给代理商 2 商家给代理商")
	private String subRole;

    /**
     * 商品名称
     */
	@ExcelProperty(value = "商品名称")
	@ApiModelProperty("商品名称")
	private String goodName;

    /**
     * 商品id
     */
	@ExcelProperty(value = "商品id")
	@ApiModelProperty("商品id")
	private Long goodId;

    /**
     * 总金额
     */
	@ExcelProperty(value = "总金额")
	@ApiModelProperty("总金额")
	private BigDecimal totalAmount;

    /**
     * 订单号
     */
	@ExcelProperty(value = "订单号")
	@ApiModelProperty("订单号")
	private String orderNum;

    /**
     * 用户id
     */
	@ExcelProperty(value = "用户id")
	@ApiModelProperty("用户id")
	private Long userId;

	/**
	 * 代理商名称
	 */
	@ApiModelProperty("代理商名称")
	private String proxyName;
    /**
     * 商户名称
     */
	@ExcelProperty(value = "代理商或者商家名称")
	@ApiModelProperty("代理商或者商家名称")
	private String publicName;

    /**
     * 一级代理商获取的金额
     */
	@ExcelProperty(value = "一级代理商获取的金额")
	@ApiModelProperty("一级代理商获取的金额")
	private BigDecimal rebateAmount;

    /**
     * 如果role为1 折此字段是代理商id 2此字段为商家id
     */
	@ExcelProperty(value = "如果role为1 折此字段是代理商id 2此字段为商家id")
	@ApiModelProperty("如果role为1 折此字段是代理商id 2此字段为商家id")
	private Long publicId;


	@ApiModelProperty("分红时间")
	private Date createTime;

}
