package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 优惠券活动与标签多对多关联对象 tiktok_activity_tag
 *
 * @author qingyun
 * @date 2021-09-26
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_activity_tag")
public class TiktokActivityTag implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 标签id
     */
    private Long tagId;

}
