package com.qingyun.shop.domain.bo.resource;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 资源业务对象 tiktok_resource
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("资源业务对象")
public class TiktokResourceBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;


    @ApiModelProperty(value = "商户名称")
    private String shopName;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 资源分组id
     */
    @ApiModelProperty(value = "资源分组id", required = true)
    @NotNull(message = "资源分组id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long groupId;

    /**
     * 资源类型 0图片 1视频
     */
    @ApiModelProperty(value = "资源类型 0图片 1视频 ", required = true)
    @NotNull(message = "资源类型 0图片 1视频 不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer resourceType;

    /**
     * 资源地址
     */
    @ApiModelProperty(value = "资源地址", required = true)
    @NotBlank(message = "资源地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String resourceUrl;

    /**
     * 资源名称
     */
    @ApiModelProperty(value = "资源名称", required = true)
    @NotBlank(message = "资源名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String resourceName;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
