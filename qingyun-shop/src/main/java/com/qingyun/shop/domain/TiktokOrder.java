package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 订单对象 tiktok_order
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_order")
public class TiktokOrder implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 商户id
     */
    private Long shopId;

	/**
	 * 代理商id
	 */
	private Long proxyId;

    /**
     * 充值用户id
     */
    private Long userId;

	/**
	 * 增加次数
	 */
	private Long count;

    /**
     * 产品名称
     */
    private String goodsName;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 订单来源：1.管理后台 2.小程序 3手机APP
     */
    private Integer source;

    /**
     * 订单来源：0待支付 1交易成功 2交易失败 3超时
     */
    private Integer status;

    /**
     * 订单备注
     */
    private String remarks;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 支付时间
     */
    private Date payTime;

	/**
	 * 订单关闭时间
	 */
	private Date closeTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 删除标记
     */
    @TableLogic
    private Integer delFlag;

}
