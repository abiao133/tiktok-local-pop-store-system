package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * bgm资源对象 tiktok_bgm
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_bgm")
public class TiktokBgm implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

	/**
	 * 视频标题
	 */
	private String title;

    /**
     * 视频地址
     */
    private String audioUrl;

    /**
     * 状态 0 可用 1禁用
     */
    private Integer status;

    /**
     * 播放时长(毫秒)
     */
    private Long duration;

    /**
     * 是否全局bgm 0是 1否
     */
    private Integer global;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

}
