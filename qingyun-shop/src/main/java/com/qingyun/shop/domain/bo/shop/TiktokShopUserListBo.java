package com.qingyun.shop.domain.bo.shop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;


@Data
@ApiModel("商户下用户列表查询对象")
public class TiktokShopUserListBo  {
	/**
	 * 用户昵称
	 */
	@ApiModelProperty( value = "用户昵称 模糊查询")
	private String nickName;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	@ApiModelProperty( value = "帐号状态（0正常 1停用）")
	private String status;

	/**
	 *  请求参数
	 *
	 */
	@ApiModelProperty( value = "自定义查询参数，比如，创建时间开始与结束等字段")
	private Map<String, Object> params = new HashMap<>();

	@ApiModelProperty( value = "商户id")
	private Long shopId;



	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;


}
