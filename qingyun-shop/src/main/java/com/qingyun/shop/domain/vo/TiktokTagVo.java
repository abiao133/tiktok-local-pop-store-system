package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 标签视图对象 tiktok_tag
 *
 * @author qingyun
 * @date 2021-09-26
 */
@Data
@ApiModel("标签视图对象")
@ExcelIgnoreUnannotated
public class TiktokTagVo {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("标签id")
	private Long tId;

	@ExcelProperty(value = "标签名")
	@ApiModelProperty("标签名")
	private String tagName;

	@ApiModelProperty("所属商户")
	private Long shopId;

	@ApiModelProperty("商户名称")
	private String shopName;

	@ExcelProperty(value = "是否启用 0 未启用 1启用")
	@ApiModelProperty("是否启用 0 未启用 1启用")
	private Integer status;

	@ExcelProperty(value = "是否是默认 0 平台创建为默认不可删除 1商户创建")
	@ApiModelProperty("是否是默认 0 平台创建为默认不可删除 1商户创建")
	private Integer isDefault;

	@ExcelProperty(value = "标签类型 0 音乐资源标签 1 视频资源标签")
	@ApiModelProperty("标签类型 0 音乐资源标签 1 视频资源标签")
	private Integer tagType;
}
