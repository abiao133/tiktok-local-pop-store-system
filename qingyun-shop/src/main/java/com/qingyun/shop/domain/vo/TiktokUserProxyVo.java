package com.qingyun.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 用户与代理关联表视图对象 tiktok_user_proxy
 *
 * @author qingyun
 * @date 2021-09-09
 */
@Data
@ApiModel("用户与代理关联表视图对象")
@ExcelIgnoreUnannotated
public class TiktokUserProxyVo {

	private static final long serialVersionUID = 1L;

	/**
     *  id
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 用户id
     */
	@ExcelProperty(value = "用户id")
	@ApiModelProperty("用户id")
	private Long userId;

    /**
     * 代理id
     */
	@ExcelProperty(value = "代理id")
	@ApiModelProperty("代理id")
	private Long proxyId;


}
