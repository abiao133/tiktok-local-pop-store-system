package com.qingyun.shop.domain.bo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("订单查询对象")
public class TiktokOrderQueryBo {

	/**
	 * 订单编号
	 */
	@ApiModelProperty(value = "订单编号")
	private String orderNum;

	/**
	 * 商户id
	 */
	@ApiModelProperty(value = "商户id")
	private Long shopId;

	@ApiModelProperty(value = "代理商id")
	private Long proxyId;

	/**
	 * 产品名称
	 */
	@ApiModelProperty(value = "产品名称(模糊查询)")
	private String goodsName;

	/**
	 * 金额
	 */
	@ApiModelProperty(value = "订单金额范围-开始")
	private BigDecimal amountStart;

	/**
	 * 金额
	 */
	@ApiModelProperty(value = "订单金额范围-结束")
	private BigDecimal amountEnd;

	/**
	 * 订单来源：1.管理后台 2.小程序 3手机APP
	 */
	@ApiModelProperty(value = "订单来源：1.管理后台 2.小程序 3手机APP")
	private Integer source;

	/**
	 * 订单来源：0待支付 1交易成功 2交易失败 3超时
	 */
	@ApiModelProperty(value = "订单状态：0待支付 1交易成功 2交易失败 3超时")
	private Integer status;

	/**
	 * 支付时间
	 */
	@ApiModelProperty(value = "支付时间范围-开始")
	private Date payTimeStart;

	@ApiModelProperty(value = "支付时间范围-结束")
	private Date payTimeEnd;


	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
