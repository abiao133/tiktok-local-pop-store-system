package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TiktokShopUserListVo {

	@ApiModelProperty("用户id")
	private Long userId;

	/**
	 * 用户账号
	 */
	@ApiModelProperty("用户账号")
	private String userName;



	/**
	 * 用户昵称
	 */
	@ApiModelProperty("用户昵称")
	private String nickName;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	@ApiModelProperty("帐号状态（0正常 1停用）")
	private String status;

	/**
	 * 用户类型（00系统用户）01代理 02商家
	 */
	@ApiModelProperty("用户类型（00系统用户）01代理 02商家")
	private String userType;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("商户名称")
	private String shopName;
}
