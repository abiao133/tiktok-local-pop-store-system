package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("订单流水视图对象")
@ExcelIgnoreUnannotated
public class TiktokOrderSettlementListVo {

    @ApiModelProperty("订单流水id")
    private Long id;

    @ExcelProperty(value = "所属商户")
    @ApiModelProperty("所属商户")
    private String shopName;

    @ExcelProperty(value = "订单编号")
    @ApiModelProperty("订单编号")
    private String orderNum;

    @ExcelProperty(value = "支付流水号")
    @ApiModelProperty("支付流水号")
    private String payNo;

    @ExcelProperty(value = "支付类型 0微信 1支付宝")
    @ApiModelProperty("支付类型 0微信 1支付宝")
    private Integer payType;

    @ExcelProperty(value = "支付金额")
    @ApiModelProperty("支付金额")
    private BigDecimal payAmount;

    @ExcelProperty(value = "支付状态 0支付失败 1支付成功")
    @ApiModelProperty("支付状态 0支付失败 1支付成功")
    private Integer payStatus;

    @ExcelProperty("生成时间")
    @ApiModelProperty("生成时间")
    private Date createTime;
}
