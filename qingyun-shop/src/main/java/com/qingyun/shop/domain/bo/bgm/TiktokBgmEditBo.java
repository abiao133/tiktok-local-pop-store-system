package com.qingyun.shop.domain.bo.bgm;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TiktokBgmEditBo {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 视频标题
     */
    @ApiModelProperty(value = "视频标题", required = true)
    @NotBlank(message = "视频标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;


    /**
     * 视频地址
     */
    @ApiModelProperty(value = "视频地址", required = true)
    @NotBlank(message = "视频地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String audioUrl;

    /**
     * 状态 0 可用 1禁用
     */
    @ApiModelProperty(value = "状态 0 可用 1禁用", required = true)
    @NotNull(message = "状态 0 可用 1禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 播放时长(毫秒)
     */
    @ApiModelProperty(value = "播放时长(毫秒)", required = true)
    @NotNull(message = "播放时长(毫秒)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long duration;

    /**
     * 是否全局bgm 0是 1否
     */
    @ApiModelProperty(value = "是否全局bgm 0是 1否", required = true)
    @NotNull(message = "是否全局bgm 0是 1否不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer global;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    @ApiModelProperty("标签 id 数组")
    private List<Long> tags;
}
