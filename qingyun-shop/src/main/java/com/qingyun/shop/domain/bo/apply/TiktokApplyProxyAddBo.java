package com.qingyun.shop.domain.bo.apply;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("代理商申请业务对象")
public class TiktokApplyProxyAddBo {

	/**
	 * 申请人真实姓名
	 */
	@ApiModelProperty(value = "申请人真实姓名", required = true)
	@NotBlank(message = "申请人真实姓名不能为空", groups = { AddGroup.class, EditGroup.class })
	private String realName;

	/**
	 * 申请人电话
	 */
	@ApiModelProperty(value = "申请人电话", required = true)
	@NotBlank(message = "申请人电话不能为空", groups = { AddGroup.class, EditGroup.class })
	private String userPhone;

	/**
	 * 申请理由
	 */
	@ApiModelProperty(value = "申请理由", required = true)
	@NotBlank(message = "申请理由不能为空", groups = { AddGroup.class, EditGroup.class })
	private String applyReason;
}
