package com.qingyun.shop.domain.vo;

import lombok.Data;

@Data
public class TiktokTagSelectListVo {

    private String label;

    private Long value;
}
