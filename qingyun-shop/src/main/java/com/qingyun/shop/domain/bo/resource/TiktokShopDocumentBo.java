package com.qingyun.shop.domain.bo.resource;

import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 商户文案业务对象 tiktok_shop_document
 *
 * @author qingyun
 * @date 2021-10-21
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户文案业务对象")
public class TiktokShopDocumentBo extends BaseEntity {

	private Long id;
    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long shopId;

    @ApiModelProperty(value = "商户名称")
    private String shopName;

    @ApiModelProperty("文案内容（抖音视频标题限制30字）")
    private String context;

    @ApiModelProperty("评论内容多条用，隔开 每条限制50字")
    private String common;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
