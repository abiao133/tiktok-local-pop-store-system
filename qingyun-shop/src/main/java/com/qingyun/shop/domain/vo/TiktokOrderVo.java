package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 订单视图对象 tiktok_order
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("订单视图对象")
@ExcelIgnoreUnannotated
public class TiktokOrderVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 订单编号
     */
	@ExcelProperty(value = "订单编号")
	@ApiModelProperty("订单编号")
	private String orderNum;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;


	@ApiModelProperty("代理商id")
	private Long proxyId;

    /**
     * 充值用户id
     */
	@ExcelProperty(value = "充值用户id")
	@ApiModelProperty("充值用户id")
	private Long userId;

    /**
     * 产品名称
     */
	@ExcelProperty(value = "产品名称")
	@ApiModelProperty("产品名称")
	private String goodsName;

    /**
     * 金额
     */
	@ExcelProperty(value = "金额")
	@ApiModelProperty("金额")
	private BigDecimal amount;

    /**
     * 订单来源：1.管理后台 2.小程序 3手机APP
     */
	@ExcelProperty(value = "订单来源：1.管理后台 2.小程序 3手机APP")
	@ApiModelProperty("订单来源：1.管理后台 2.小程序 3手机APP")
	private Integer source;

    /**
     * 订单来源：0待支付 1交易成功 2交易失败 3超时
     */
	@ExcelProperty(value = "订单来源：0待支付 1交易成功 2交易失败 3超时")
	@ApiModelProperty("订单来源：0待支付 1交易成功 2交易失败 3超时")
	private Integer status;

    /**
     * 订单备注
     */
	@ExcelProperty(value = "订单备注")
	@ApiModelProperty("订单备注")
	private String remarks;

    /**
     * 支付时间
     */
	@ExcelProperty(value = "支付时间")
	@ApiModelProperty("支付时间")
	private Date payTime;


}
