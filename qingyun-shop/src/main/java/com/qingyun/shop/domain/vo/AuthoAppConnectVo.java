package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 第三方用户数据视图对象 autho_app_connect
 *
 * @author qingyun
 * @date 2021-09-16
 */
@Data
@ApiModel("第三方用户数据视图对象")
@ExcelIgnoreUnannotated
public class AuthoAppConnectVo {

	private static final long serialVersionUID = 1L;

	/**
     *  0管家 1普通用户
     */
	@ApiModelProperty("0管家 1普通用户")
	private Long id;

    /**
     * 用户表用户id
     */
	@ExcelProperty(value = "用户表用户id")
	@ApiModelProperty("用户表用户id")
	private Long userId;

    /**
     * 用户名称（第三方）
     */
	@ExcelProperty(value = "用户名称", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "第=三方")
	@ApiModelProperty("用户名称（第三方）")
	private String nickName;

    /**
     * 用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等
     */
	@ExcelProperty(value = "用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等")
	@ApiModelProperty("用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等")
	private Integer userType;

    /**
     * 2企业管理员 1普通用户
     */
	@ExcelProperty(value = "2企业管理员 1普通用户")
	@ApiModelProperty("2企业管理员 1普通用户")
	private Integer entryType;

    /**
     * 用户头像
     */
	@ExcelProperty(value = "用户头像")
	@ApiModelProperty("用户头像")
	private String imageUrl;

    /**
     * 第三方open_id
     */
	@ExcelProperty(value = "第三方open_id")
	@ApiModelProperty("第三方open_id")
	private String openId;

    /**
     * 第三方union_id
     */
	@ExcelProperty(value = "第三方union_id")
	@ApiModelProperty("第三方union_id")
	private String unionId;


}
