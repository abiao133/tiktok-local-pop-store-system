package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 套餐视图对象 tiktok_goods
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("套餐视图对象")
@ExcelIgnoreUnannotated
public class TiktokGoodsVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 套餐名称
     */
	@ExcelProperty(value = "套餐名称")
	@ApiModelProperty("套餐名称")
	private String name;

    /**
     * 套餐发送数量
     */
	@ExcelProperty(value = "套餐发送数量")
	@ApiModelProperty("套餐发送数量")
	private Long voideCount;


	/**
	 * 赠送数量
	 */
	private Long voideGive;

    /**
     * 现价
     */
	@ExcelProperty(value = "现价")
	@ApiModelProperty("现价")
	private BigDecimal price;

    /**
     * 原价
     */
	@ExcelProperty(value = "原价")
	@ApiModelProperty("原价")
	private BigDecimal marketPrice;

    /**
     * 代理商价格
     */
	@ExcelProperty(value = "代理商价格")
	@ApiModelProperty("代理商价格")
	private BigDecimal proxyPrice;

    /**
     * 状态 0上架 1下架
     */
	@ExcelProperty(value = "状态 0上架 1下架")
	@ApiModelProperty("状态 0上架 1下架")
	private Integer status;

    /**
     * $column.columnComment
     */
	@ExcelProperty(value = "状态 0上架 1下架")
	@ApiModelProperty("$column.columnComment")
	private String remark;


}
