package com.qingyun.shop.domain.bo.order;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("订单创建对象")
public class TiktokOrderCreateBo {

	@ApiModelProperty(value = "产品id", required = true)
	@NotNull(message = "产品id不能为空")
	private Long goodsId;

	/**
	 * 产品名称
	 */
	@ApiModelProperty(value = "产品名称", required = true)
	@NotBlank(message = "产品名称不能为空")
	private String goodsName;

	/**
	 * 购买数量
	 */
	@ApiModelProperty(value = "购买数量", required = true)
	@NotNull(message = "购买数量不能为空")
	private Long quantity;

	/**
	 * 订单来源：1.管理后台 2.小程序 3手机APP
	 */
	@ApiModelProperty(value = "订单来源：1.管理后台 2.小程序 3手机APP", required = true)
	@NotNull(message = "订单来源：1.管理后台 2.小程序 3手机APP不能为空")
	private Integer source;

	/**
	 * 订单备注
	 */
	@ApiModelProperty(value = "订单备注")
	private String remarks;
}
