package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("代理商提现申请对象")
public class TiktokWithdrawApplyAddBo {

    /**
     * 关联的银行卡id
     */
    @ApiModelProperty(value = "关联的银行卡id", required = true)
    @NotNull(message = "关联的银行卡id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long bankId;

    /**
     * 提现金额
     */
    @ApiModelProperty(value = "提现金额", required = true)
    @NotNull(message = "提现金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal withdrawAmount;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
