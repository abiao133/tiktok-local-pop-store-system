package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 套餐对象 tiktok_goods
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_goods")
public class TiktokGoods implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 套餐名称
     */
    private String name;

    /**
     * 套餐发送数量
     */
    private Long voideCount;

	/**
	 * 套餐赠送
	 */
	private Long voideGive;

    /**
     * 现价
     */
    private BigDecimal price;

    /**
     * 原价
     */
    private BigDecimal marketPrice;

    /**
     * 代理商价格
     */
    private BigDecimal proxyPrice;

    /**
     * 状态 0上架 1下架
     */
    private Integer status;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * $column.columnComment
     */
    private String remark;


}
