package com.qingyun.shop.domain.bo.bgm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("bgm资源查询对象")
public class TiktokBgmQueryBo {

	/**
	 * 视频标题
	 */
	@ApiModelProperty(value = "视频标题")
	private String title;

	/**
	 * 状态 0 可用 1禁用
	 */
	@ApiModelProperty(value = "状态 0 可用 1禁用")
	private Integer status;

	/**
	 * 是否全局bgm 0是 1否
	 */
	@ApiModelProperty(value = "是否全局bgm 0是 1否")
	private Integer global;

	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
