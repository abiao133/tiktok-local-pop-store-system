package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标签对象 tiktok_tag
 *
 * @author qingyun
 * @date 2021-09-26
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_tag")
public class TiktokTag implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 标签id
     */
    @TableId(value = "t_id")
    private Long tId;

    /**
     * 标签名
     */
    private String tagName;

    /**
     * 所属商户
     */
    private Long shopId;

    /**
     * 是否启用 0 未启用 1启用
     */
    private Integer status;

    /**
     * 是否是默认 0 平台创建为默认不可删除 1商户创建
     */
    private Integer isDefault;

    /**
     * 标签类型 0 音乐资源标签 1 视频资源标签
     */
    private Integer tagType;

}
