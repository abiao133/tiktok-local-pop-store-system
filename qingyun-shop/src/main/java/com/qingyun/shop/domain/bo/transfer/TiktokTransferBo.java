package com.qingyun.shop.domain.bo.transfer;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 代理给商户转播放量记录业务对象 tiktok_transfer
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理给商户转播放量记录业务对象")
public class TiktokTransferBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 代理商id
     */
    @ApiModelProperty(value = "代理商id", required = true)
    @NotNull(message = "代理商id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long proxyId;

    /**
     * 商家id
     */
    @ApiModelProperty(value = "商家id", required = true)
    @NotNull(message = "商家id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 消耗播放量
     */
    @ApiModelProperty(value = "消耗播放量", required = true)
    @NotBlank(message = "消耗播放量不能为空", groups = { AddGroup.class, EditGroup.class })
    private String videoCount;

    /**
     * 代理商名称
     */
    @ApiModelProperty(value = "代理商名称")
    private String proxyName;

    /**
     * 商家名称
     */
    @ApiModelProperty(value = "商家名称", required = true)
    @NotBlank(message = "商家名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String shopName;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
