package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 员工宣传记录视图对象 tiktok_shop_staff_push
 *
 * @author qingyun
 * @date 2021-10-30
 */
@Data
@ApiModel("员工宣传记录视图对象")
@ExcelIgnoreUnannotated
public class TiktokShopStaffPushVo {

	private static final long serialVersionUID = 1L;

   private Integer activityType;

   private Integer useStatus;

   private String couponTitle;

   private String activityTitle;

   private String nickName;

}
