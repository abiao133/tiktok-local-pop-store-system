package com.qingyun.shop.domain.bo.goods;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("套餐查询对象")
public class TiktokGoodsQueryBo {

	/**
	 * 套餐名称
	 */
	@ApiModelProperty(value = "套餐名称")
	private String name;

	/**
	 * 状态 0上架 1下架
	 */
	@ApiModelProperty(value = "状态 0上架 1下架")
	private Integer status;

	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
