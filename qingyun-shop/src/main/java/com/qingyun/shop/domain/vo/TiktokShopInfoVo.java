package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 商家账号信息视图对象 tiktok_shop_info
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("商家账号信息视图对象")
@ExcelIgnoreUnannotated
public class TiktokShopInfoVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

    /**
     * openid
     */
	@ExcelProperty(value = "openid")
	@ApiModelProperty("openid")
	private String openId;

    /**
     * 用户名称
     */
	@ExcelProperty(value = "用户名称")
	@ApiModelProperty("用户名称")
	private String nickname;

    /**
     * 地理位置id
     */
	@ExcelProperty(value = "地理位置id")
	@ApiModelProperty("地理位置id")
	private String poiId;

    /**
     * 地址位置名称
     */
	@ExcelProperty(value = "地址位置名称")
	@ApiModelProperty("地址位置名称")
	private String poiName;

    /**
     * 抖音小程序id
     */
	@ExcelProperty(value = "抖音小程序id")
	@ApiModelProperty("抖音小程序id")
	private String miniAppId;

    /**
     * 抖音小程序标题
     */
	@ExcelProperty(value = "抖音小程序标题")
	@ApiModelProperty("抖音小程序标题")
	private String miniAppTitle;

    /**
     * 抖音小程序地址
     */
	@ExcelProperty(value = "抖音小程序地址")
	@ApiModelProperty("抖音小程序地址")
	private String miniAppUrl;

    /**
     * 地址
     */
	@ExcelProperty(value = "地址")
	@ApiModelProperty("地址")
	private String address;

    /**
     * 纬度
     */
	@ExcelProperty(value = "纬度")
	@ApiModelProperty("纬度")
	private String lat;

    /**
     * 经度
     */
	@ExcelProperty(value = "经度")
	@ApiModelProperty("经度")
	private String log;

    /**
     * 商家抖音主页
     */
	@ExcelProperty(value = "商家抖音主页")
	@ApiModelProperty("商家抖音主页")
	private String douyinUrl;


}
