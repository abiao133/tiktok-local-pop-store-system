package com.qingyun.shop.domain.bo.order;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 订单业务对象 tiktok_order
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("订单业务对象")
public class TiktokOrderBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String orderNum;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 充值用户id
     */
    @ApiModelProperty(value = "充值用户id", required = true)
    @NotNull(message = "充值用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 产品名称
     */
    @ApiModelProperty(value = "产品名称", required = true)
    @NotBlank(message = "产品名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String goodsName;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额", required = true)
    @NotNull(message = "金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal amount;

    /**
     * 订单来源：1.管理后台 2.小程序 3手机APP
     */
    @ApiModelProperty(value = "订单来源：1.管理后台 2.小程序 3手机APP", required = true)
    @NotNull(message = "订单来源：1.管理后台 2.小程序 3手机APP不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer source;

    /**
     * 订单来源：0待支付 1交易成功 2交易失败 3超时
     */
    @ApiModelProperty(value = "订单来源：0待支付 1交易成功 2交易失败 3超时", required = true)
    @NotNull(message = "订单来源：0待支付 1交易成功 2交易失败 3超时不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 订单备注
     */
    @ApiModelProperty(value = "订单备注")
    private String remarks;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private Date payTime;

    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记", required = true)
    @NotNull(message = "删除标记不能为空", groups = { AddGroup.class })
    private Integer delFlag;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
