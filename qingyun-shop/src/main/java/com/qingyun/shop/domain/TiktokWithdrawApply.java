package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 商户提现申请对象 tiktok_withdraw_apply
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_withdraw_apply")
public class TiktokWithdrawApply implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 关联的银行卡id
     */
    private Long bankId;

    /**
     * 所属代理商id
     */
    private Long proxyId;

    /**
     * 提现金额
     */
    private BigDecimal withdrawAmount;

    /**
     * 提现完成时间
     */
    private Date finishTime;

    /**
     * 状态 0受理中 1完成 2驳回
     */
    private Integer status;

    /**
     * 提现人姓名
     */
    private String userName;

    /**
     * 提现人id
     */
    private Long userId;

    /**
     * 驳回理由
     */
    private String rejectReason;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 删除标识
     */
    @TableLogic
    private Integer delFlag;

    /**
     * 备注
     */
    private String remark;

}
