package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class TiktokBgmDetailVo {

    private static final long serialVersionUID = 1L;

    /**
     *  $pkColumn.columnComment
     */
    @ApiModelProperty("$pkColumn.columnComment")
    private Long id;

    /**
     * 视频地址
     */
    @ExcelProperty(value = "视频地址")
    @ApiModelProperty("视频地址")
    private String audioUrl;

    /**
     * 视频标题
     */
    @ExcelProperty(value = "视频标题")
    @ApiModelProperty("视频标题")
    private String title;

    /**
     * 状态 0 可用 1禁用
     */
    @ExcelProperty(value = "状态 0 可用 1禁用")
    @ApiModelProperty("状态 0 可用 1禁用")
    private Integer status;

    /**
     * 播放时长(毫秒)
     */
    @ExcelProperty(value = "播放时长")
    @ApiModelProperty("播放时长")
    private Long duration;

    @ApiModelProperty("播放时长（时间格式）")
    private String durationStr;

    /**
     * 是否全局bgm 0是 1否
     */
    @ExcelProperty(value = "是否全局bgm 0是 1否")
    @ApiModelProperty("是否全局bgm 0是 1否")
    private Integer global;

    /**
     * 商户id
     */
    @ExcelProperty(value = "商户id")
    @ApiModelProperty("商户id")
    private Long shopId;

    @ApiModelProperty("标签列表")
    private List<Long> tags;
}
