package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("商户银行卡信息列表视图对象")
public class TiktokBankCardListVo {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("银行卡号")
    private String bankNumber;

    @ApiModelProperty("开户行")
    private String bankDeposit;

    @ApiModelProperty("支行名称")
    private String branchName;

    @ApiModelProperty("银行卡预留手机号")
    private String bankPhone;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty("身份证号")
    private String idCard;

    @ApiModelProperty("所属代理商名称")
    private String proxyName;

    @ApiModelProperty("0正常 1禁用")
    private Integer status;

    @ApiModelProperty("创建时间")
    private Date createTime;
}
