package com.qingyun.shop.domain.bo.resource;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("资源组新增对象")
public class TiktokResourceGroupAddBo {

	@ApiModelProperty(value = "组名", required = true)
	@NotBlank(message = "组名不能为空", groups = { AddGroup.class, EditGroup.class })
	private String groupName;

	@ApiModelProperty(value = "商户id 超级管理员才传入")
	private Long shopId;

	@ApiModelProperty(value = "备注")
	private String remark;

}
