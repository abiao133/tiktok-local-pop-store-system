package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券列表视图对象
 *
 */
@Data
@ApiModel("优惠券视图对象")
@ExcelIgnoreUnannotated
public class TiktokCouponListVo {

	@ApiModelProperty("优惠券id")
	private Long id;

	@ApiModelProperty("商户id")
	private Long shopId;

	@ApiModelProperty("商户名称")
	private String shopName;

	@ExcelProperty(value = "折扣券标题")
	@ApiModelProperty("折扣券标题")
	private String title;

	@ExcelProperty(value = "折扣")
	@ApiModelProperty("折扣")
	private Double discount;

	@ExcelProperty(value = "有效期开始日期")
	@ApiModelProperty("有效期开始日期")
	private Date startTime;

	@ExcelProperty(value = "有效期结束日期")
	@ApiModelProperty("有效期结束日期")
	private Date endTime;

	@ExcelProperty(value = "基于领取时间的有效天数days。")
	@ApiModelProperty("基于领取时间的有效天数days。")
	private Integer days;

	@ExcelProperty(value = "封面图片")
	@ApiModelProperty("封面图片")
	private String coverImg;

	@ExcelProperty(value = "减免金额")
	@ApiModelProperty("减免金额")
	private BigDecimal amount;

	@ExcelProperty(value = "0：代金券；1：折扣券；2：兑换券")
	@ApiModelProperty("0：代金券；1：折扣券；2：兑换券")
	private Integer type;

	@ExcelProperty(value = "数量：99999999为不限量")
	@ApiModelProperty("数量：99999999为不限量")
	private Long quantity;

	@ApiModelProperty("有效时间限制，0，基于领取时间的有效天数days；1，则start_time和end_time是优惠券有效期；")
	private Integer timeType;

	@ApiModelProperty("优惠券有效期剩余时间")
	private String timeRemaining;

	@ExcelProperty(value = "优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
	@ApiModelProperty("优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
	private Integer status;

	@ApiModelProperty("是否绑定了活动")
	private Boolean isRelevancy;

	@ApiModelProperty("活动名称")
	private String activityName;
}
