package com.qingyun.shop.domain.bo.resource;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("资源分组查询对象")
public class TiktokResourceGroupQueryBo {

	/**
	 * $column.columnComment
	 */
	@ApiModelProperty(value = "$column.columnComment")
	private Long id;

	/**
	 * 商户id
	 */
	@ApiModelProperty(value = "商户id")
	private Long shopId;


	@ApiModelProperty(value = "商户名称")
	private String shopName;

	/**
	 * 组名
	 */
	@ApiModelProperty(value = "组名")
	private String groupName;


	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
