package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 商家账号信息对象 tiktok_shop_info
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_shop_info")
public class TiktokShopInfo implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * openid
     */
    private String openId;

    /**
     * 用户名称
     */
    private String nickname;

    /**
     * 地理位置id
     */
    private String poiId;

    /**
     * 地址位置名称
     */
    private String poiName;

    /**
     * 抖音小程序id
     */
    private String miniAppId;

    /**
     * 抖音小程序标题
     */
    private String miniAppTitle;

    /**
     * 抖音小程序地址
     */
    private String miniAppUrl;

    /**
     * 地址
     */
    private String address;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 经度
     */
    private String log;

    /**
     * 商家抖音主页
     */
    private String douyinUrl;

}
