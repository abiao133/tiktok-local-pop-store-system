package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 【请填写功能名称】对象 tiktok_tag_to_voice
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_tag_to_voice")
public class TiktokTagToVoice implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * $column.columnComment
     */
    private Long tagId;

    /**
     * $column.columnComment
     */
    private Long voiceId;

}
