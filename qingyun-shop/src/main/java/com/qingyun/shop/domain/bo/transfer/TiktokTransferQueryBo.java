package com.qingyun.shop.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("代理给商户转播放量记录查询对象")
public class TiktokTransferQueryBo {


    @ApiModelProperty(value = "代理商id")
    private Long proxyId;

    @ApiModelProperty(value = "商家id")
    private Long shopId;

    @ApiModelProperty(value = "代理商名称")
    private String proxyName;

    @ApiModelProperty(value = "商家名称")
    private String shopName;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;
}
