package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 用户优惠卷领取视图对象 tiktok_coupon_draw
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("用户优惠卷领取视图对象")
@ExcelIgnoreUnannotated
public class TiktokCouponDrawVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 优惠卷id
     */
	@ExcelProperty(value = "优惠卷id")
	@ApiModelProperty("优惠卷id")
	private Long couponId;

	/**
	 * 优惠卷
	 */
	@ApiModelProperty("优惠卷")
	private TiktokCouponVo coupon;

    /**
     * 活动id
     */
	@ExcelProperty(value = "活动id")
	@ApiModelProperty("活动id")
	private Long activityId;

	/**
	 * 活动
	 */
	@ApiModelProperty("活动")
	private TiktokActivityVo activity;

    /**
     * 用户id
     */
	@ExcelProperty(value = "用户id")
	@ApiModelProperty("用户id")
	private Long userId;

    /**
     * 优惠卷code
     */
	@ExcelProperty(value = "优惠卷code")
	@ApiModelProperty("优惠卷code")
	private String couponCode;

    /**
     * 获取途径
     */
	@ExcelProperty(value = "获取途径")
	@ApiModelProperty("获取途径")
	private Integer getType;

    /**
     * 优惠卷有效开始时间
     */
	@ExcelProperty(value = "优惠卷有效开始时间")
	@ApiModelProperty("优惠卷有效开始时间")
	private Date beginDate;

    /**
     * 优惠卷有效结束时间
     */
	@ExcelProperty(value = "优惠卷有效结束时间")
	@ApiModelProperty("优惠卷有效结束时间")
	private Date endDate;

    /**
     * 使用状态 0->未使用；1->已使用；2->已过期
     */
	@ExcelProperty(value = "使用状态 0->未使用；1->已使用；2->已过期")
	@ApiModelProperty("使用状态 0->未使用；1->已使用；2->已过期")
	private Integer useStatus;

    /**
     * 使用时间
     */
	@ExcelProperty(value = "使用时间")
	@ApiModelProperty("使用时间")
	private Date useTime;

	@ApiModelProperty("商家信息")
	private TiktokShopH5Vo shop;


}
