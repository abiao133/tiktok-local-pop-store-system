package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 商户银行卡信息视图对象 tiktok_bank_card
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@ApiModel("商户银行卡信息视图对象")
@ExcelIgnoreUnannotated
public class TiktokBankCardVo {

	private static final long serialVersionUID = 1L;

	/**
     *  id
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 银行卡号
     */
	@ExcelProperty(value = "银行卡号")
	@ApiModelProperty("银行卡号")
	private String bankNumber;

    /**
     * 开户行
     */
	@ExcelProperty(value = "开户行")
	@ApiModelProperty("开户行")
	private String bankDeposit;

	@ApiModelProperty("身份证信息")
	private String idCard;
    /**
     * 支行名称
     */
	@ExcelProperty(value = "支行名称")
	@ApiModelProperty("支行名称")
	private String branchName;

    /**
     * 银行卡预留手机号
     */
	@ExcelProperty(value = "银行卡预留手机号")
	@ApiModelProperty("银行卡预留手机号")
	private String bankPhone;

    /**
     * 所属商户id
     */
	@ExcelProperty(value = "所属代理商id")
	@ApiModelProperty("所属代理商id")
	private Long proxyId;

	/**
	 * 真实姓名
	 */
	@ExcelProperty(value = "真实姓名")
	@ApiModelProperty("真实姓名")
	private String realName;

    /**
     * 0正常 1禁用
     */
	@ExcelProperty(value = "0正常 1禁用")
	@ApiModelProperty("0正常 1禁用")
	private Integer status;


}
