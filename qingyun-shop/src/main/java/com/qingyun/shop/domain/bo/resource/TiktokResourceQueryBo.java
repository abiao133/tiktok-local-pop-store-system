package com.qingyun.shop.domain.bo.resource;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("资源查询对象")
public class TiktokResourceQueryBo {

	/**
	 * 商户id
	 */
	@ApiModelProperty(value = "商户id 平台端使用")
	private Long shopId;

	/**
	 * 资源分组id
	 */
	@ApiModelProperty(value = "资源分组id")
	private Long groupId;


	@ApiModelProperty(value = "资源商户id")
	private String shopName;

	@ApiModelProperty(value = "视频方向 0纵向 1横向")
	private  Integer format;

	/**
	 * 资源名称
	 */
	@ApiModelProperty(value = "资源名称")
	private String resourceName;

	@ApiModelProperty(value = "是否是碎片")
	private Integer isFrag;


	/**
	 * 资源类型 0图片 1视频
	 */
	@ApiModelProperty(value = "资源类型 0图片 1视频 ")
	private Integer resourceType;


	@ApiModelProperty(value = "状态")
	private Integer status;
	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
