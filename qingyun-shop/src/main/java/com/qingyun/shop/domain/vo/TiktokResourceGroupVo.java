package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 商户资源分组视图对象 tiktok_resource_group
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("商户资源分组视图对象")
@ExcelIgnoreUnannotated
public class TiktokResourceGroupVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

	@ExcelProperty(value = "商户名称")
	@ApiModelProperty("商户名称")
	private String shopName;

    /**
     * 组名
     */
	@ExcelProperty(value = "组名")
	@ApiModelProperty("组名")
	private String groupName;

    /**
     * $column.columnComment
     */
	@ExcelProperty(value = "备注")
	@ApiModelProperty("备注")
	private String remark;


}
