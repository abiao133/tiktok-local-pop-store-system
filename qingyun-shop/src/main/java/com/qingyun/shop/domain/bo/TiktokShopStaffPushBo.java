package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 员工宣传记录业务对象 tiktok_shop_staff_push
 *
 * @author qingyun
 * @date 2021-10-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("员工宣传记录业务对象")
public class TiktokShopStaffPushBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 活动 id
     */
    @ApiModelProperty(value = "活动 id")
    private Long activityId;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long shopId;

    /**
     * 商户员工id
     */
    @ApiModelProperty(value = "商户员工id")
    private Long shopUserId;

    /**
     * 活动类型0领劵1纯推广2抽奖
     */
    @ApiModelProperty(value = "活动类型0领劵1纯推广2抽奖")
    private Integer activityType;

    /**
     * 领劵记录id
     */
    @ApiModelProperty(value = "领劵记录id")
    private Long couponDrawId;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
