package com.qingyun.shop.domain.bo.shop;


import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("商家查询业务对象")
public class TiktokShopQueryBo extends BaseEntity {


	/**
	 * 商户id
	 */
	private Long id;
	/**
	 * 代理商id -1为平台
	 */
	@ApiModelProperty(value = "代理商id -1为平台")
	private Long proxyId;

	/**
	 * 商家名称
	 */
	@ApiModelProperty(value = "商家名称")
	private String name;

	/**
	 * 负责人
	 */
	@ApiModelProperty(value = "负责人")
	private String leader;

	/**
	 * 显示顺序
	 */
	@ApiModelProperty(value = "显示顺序")
	private Integer orderNum;

	/**
	 * 商户手机号码
	 */
	@ApiModelProperty(value = "商户手机号码")
	private String phone;

	/**
	 * 商户邮箱
	 */
	@ApiModelProperty(value = "商户邮箱")
	private String email;

	@ApiModelProperty(value = "是否是代理商")
	private Integer isProxy;

	/**
	 * 商户状态 0启用 1禁用
	 */
	@ApiModelProperty(value = "商户状态 0启用 1禁用")
	private Integer status;


	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;


	/**
	 * 代理商列表
	 */
	private List<Long> ids;
}
