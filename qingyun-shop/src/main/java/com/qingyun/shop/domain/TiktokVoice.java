package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 合成语音对象 tiktok_voice
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_voice")
public class TiktokVoice implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;


	private String shopName;

    /**
     * 语音标题
     */
    private String title;

    /**
     * 语音文案
     */
    private String context;

    /**
     * 语音合成后地址
     */
    private String voice;

	/**
	 * 字幕文件地址
	 */
	private String srt;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
