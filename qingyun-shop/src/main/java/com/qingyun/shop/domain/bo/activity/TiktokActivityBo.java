package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 优惠券活动业务对象 tiktok_activity
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("优惠券活动业务对象")
public class TiktokActivityBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 活动标题
     */
    @ApiModelProperty(value = "活动标题", required = true)
    @NotBlank(message = "活动标题不能为空", groups = { AddGroup.class})
    private String title;

    /**
     * 活动描述
     */
	@NotBlank(message = "活动描述不能为空", groups = { AddGroup.class})
    @ApiModelProperty(value = "活动描述")
    private String description;

    /**
     * 活动内容（富文本
     */
	@NotBlank(message = "活动内容不能为空", groups = { AddGroup.class})
    @ApiModelProperty(value = "活动内容（富文本")
    private String context;

    /**
     * 活动开始时间
     */
    @ApiModelProperty(value = "活动开始时间", required = true)
    @NotNull(message = "活动开始时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date beginDate;

    /**
     * 活动结束时间
     */
    @ApiModelProperty(value = "活动结束时间", required = true)
    @NotNull(message = "活动结束时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date endDate;

    /**
     * 每个人可参与次数：0为不限次
     */
    @ApiModelProperty(value = "每个人可参与次数：0为不限次")
	@Min(value = 0,message = "参与次数值不能小于0",groups = {AddGroup.class})
    private Long joinCount;

    /**
     * 活动类型：0扫码领券；1纯推广2；抽奖
     */
    @ApiModelProperty(value = "活动类型：0扫码领券；1纯推广2；抽奖")
	@NotBlank(message = "活动类型不能为空", groups = { AddGroup.class})
    private String actityType;

    /**
     * 延迟发送视频：小时为单位，0为不延迟
     */
    @ApiModelProperty(value = "延迟发送视频：小时为单位，0为不延迟")
	@Min(value = 0,message = "发送视频延迟不能小于0",groups = {AddGroup.class,EditGroup.class})
    private Long laterSend;

    /**
     * 活动主图
     */
    @ApiModelProperty(value = "活动主图", required = true)
    @NotBlank(message = "活动主图不能为空", groups = { AddGroup.class, EditGroup.class })
    private String imgUrl;


	@ApiModelProperty("是否使用文案库")
	private Integer isDoc;

    /**
     * 活动状态：0待审核；1待上线；2上线中；3已下线
     */
    @ApiModelProperty(value = "活动状态：0待审核；1待上线；2上线中；3已下线", required = true)
    private Long status;

    /**
     * 活动宣传视频
     */
    @ApiModelProperty(value = "活动宣传视频")
    private String videoUrl;

    /**
     * 所属商家
     */
    @ApiModelProperty(value = "所属商家", required = true)
    private Long shopId;

    @ApiModelProperty("商户名称")
    private String shopName;
    /**
     * 抖音话题
     */
    @ApiModelProperty(value = "抖音话题")
	@NotBlank(message = "抖音话题不能为空", groups = { AddGroup.class})
    private String douyinTitle;

    /**
     * 转发渠道：0抖音，1快手，2视频号，3微信朋友圈
     */
    @ApiModelProperty(value = "转发渠道：0抖音，1快手，2视频号，3微信朋友圈", required = true)
    private Integer forwardChannel;

    /**
     * 视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择
     */
    @ApiModelProperty(value = "视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择")
    private Integer videoSource;


    @ApiModelProperty(value = "0分组碎片混剪 1随机混剪")
    private Integer isRandFrag;

    /**
     * 视频资源分组数组
     */
    @ApiModelProperty(value = "视频资源分组数组")
    private String resourceGroupId;

    /**
     * 地理位置id
     */
    @ApiModelProperty(value = "地理位置id")
    private String poiId;

    /**
     * 地理位置名称
     */
    @ApiModelProperty(value = "地理位置名称")
    private String poiName;

    /**
     * @的openid
     */
    @ApiModelProperty(value = "@的openid")
    private String atOpenids;

    /**
     * @的nickname
     */
    @ApiModelProperty(value = "@的nickname")
    private String atNicknames;

    /**
     * 小程序id
     */
    @ApiModelProperty(value = "小程序id", required = true)
    private String miniAppId;

    /**
     * 小程序标题
     */
    @ApiModelProperty(value = "小程序标题", required = true)
    private String miniAppTitle;

    /**
     * 吊起小程序时的url参数
     */
    @ApiModelProperty(value = "吊起小程序时的url参数")
    private String miniAppUrl;

    /**
     * 视频标题
     */
    @ApiModelProperty(value = "视频标题")
    private String videoTitle;

    /**
     * 参与后跳转类型：0优惠券页面1商家抖音主页
     */
    @ApiModelProperty(value = "参与后跳转类型：0优惠券页面1商家抖音主页")
    private String redirectType;


    @ApiModelProperty(value = "优惠卷ids")
    private List<Long> couponIds;

    @ApiModelProperty("标签id 数组")
    private List<Long> tags;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

    @ApiModelProperty("是否需要用户关注")
    private Integer isFocus;

    @ApiModelProperty("是否同步优惠券的有效期 0 否 1 是")
    private Integer isSynchronizationCoupon;
}
