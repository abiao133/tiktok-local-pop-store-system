package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("商家账号信息修改对象")
public class TiktokShopInfoEditBo {

	/**
	 * $column.columnComment
	 */
	@ApiModelProperty(value = "$column.columnComment")
	private Long id;

	/**
	 * openid
	 */
	@ApiModelProperty(value = "openid", required = true)
	//@NotBlank(message = "openid不能为空", groups = { AddGroup.class, EditGroup.class })
	private String openId;

	/**
	 * 用户名称
	 */
	@ApiModelProperty(value = "用户名称", required = true)
	//@NotBlank(message = "用户名称不能为空", groups = { AddGroup.class, EditGroup.class })
	private String nickname;

	/**
	 * 地理位置id
	 */
	@ApiModelProperty(value = "地理位置id")
	private String poiId;

	/**
	 * 地址位置名称
	 */
	@ApiModelProperty(value = "地址位置名称")
	private String poiName;

	/**
	 * 抖音小程序id
	 */
	@ApiModelProperty(value = "抖音小程序id")
	private String miniAppId;

	/**
	 * 抖音小程序标题
	 */
	@ApiModelProperty(value = "抖音小程序标题")
	private String miniAppTitle;

	/**
	 * 抖音小程序地址
	 */
	@ApiModelProperty(value = "抖音小程序地址")
	private String miniAppUrl;

	/**
	 * 地址
	 */
	@ApiModelProperty(value = "地址")
	private String address;

	/**
	 * 纬度
	 */
	@ApiModelProperty(value = "纬度")
	private String lat;

	/**
	 * 经度
	 */
	@ApiModelProperty(value = "经度")
	private String log;

	/**
	 * 商家抖音主页
	 */
	@ApiModelProperty(value = "商家抖音主页")
	private String douyinUrl;
}
