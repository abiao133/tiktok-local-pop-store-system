package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_user_shop")
public class TiktokUserShop {

	@TableId(value = "id")
	private Long id;

	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 * 商家id
	 */
	private Long shopId;
}
