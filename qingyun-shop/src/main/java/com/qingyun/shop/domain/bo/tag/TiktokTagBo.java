package com.qingyun.shop.domain.bo.tag;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 标签业务对象 tiktok_tag
 *
 * @author qingyun
 * @date 2021-09-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("标签业务对象")
public class TiktokTagBo extends BaseEntity {

    /**
     * 标签id
     */
    @ApiModelProperty(value = "标签id")
    private Long tId;

    /**
     * 标签名
     */
    @ApiModelProperty(value = "标签名", required = true)
    @NotBlank(message = "标签名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tagName;

    /**
     * 所属商户
     */
    @ApiModelProperty(value = "所属商户", required = true)
    @NotNull(message = "所属商户不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 是否启用 0 未启用 1启用
     */
    @ApiModelProperty(value = "是否启用 0 未启用 1启用", required = true)
    @NotNull(message = "是否启用 0 未启用 1启用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 是否是默认 0 平台创建为默认不可删除 1商户创建
     */
    @ApiModelProperty(value = "是否是默认 0 平台创建为默认不可删除 1商户创建", required = true)
    @NotNull(message = "是否是默认 0 平台创建为默认不可删除 1商户创建不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer isDefault;

    /**
     * 标签类型 0 音乐资源标签 1 视频资源标签
     */
    @ApiModelProperty(value = "标签类型 0 音乐资源标签 1 视频资源标签", required = true)
    @NotNull(message = "标签类型 0 音乐资源标签 1 视频资源标签不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer tagType;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
