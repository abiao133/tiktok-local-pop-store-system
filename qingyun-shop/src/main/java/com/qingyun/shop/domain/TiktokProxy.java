package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 代理组织对象 tiktok_proxy
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_proxy")
public class TiktokProxy implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 代理商id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 代理商名称
     */
    private String name;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 代理商手机号码
     */
    private String phone;

    /**
     * 代理商邮箱
     */
    private String email;

    /**
     * 数量
     */
    private Long videoCount;

    /**
     * 代理商状态 0启用 1禁用
     */
    private Integer status;

	/**
	 * 代理商的商户id
	 */
	private Long shopId;

	/**
	 * 总余额
	 */
	private BigDecimal totalBalance;
	/**
	 * 可用余额
	 */
	private BigDecimal usableBalance;

	/**
	 * 冻结余额
	 */
	private BigDecimal freezeBalance;

    /**
     * $column.columnComment
     */
    @TableLogic
    private Integer delFlag;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

}
