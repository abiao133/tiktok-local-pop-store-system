package com.qingyun.shop.domain.bo.apply;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("代理商申请查询对象")
public class TiktokApplyProxyQueryBo {

	/**
	 * 商户id
	 */
	@ApiModelProperty(value = "商户id")
	private Long shopId;

	/**
	 * 申请人用户id
	 */
	@ApiModelProperty(value = "申请人用户id")
	private Long userId;

	/**
	 * 申请人真实姓名
	 */
	@ApiModelProperty(value = "申请人真实姓名")
	private String realName;

	/**
	 * 申请人电话
	 */
	@ApiModelProperty(value = "申请人电话")
	private String userPhone;

	/**
	 * 申请时间开始
	 */
	@ApiModelProperty(value = "申请时间")
	private Date applyTimeStart;

	/**
	 * 申请时间结束
	 */
	@ApiModelProperty(value = "申请时间")
	private Date applyTimeEnd;

	/**
	 * 流程状态 0审核中 1通过 2未通过
	 */
	@ApiModelProperty(value = "流程状态 0审核中 1通过 2未通过")
	private Integer status;

	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
}
