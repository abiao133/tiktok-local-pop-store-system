package com.qingyun.shop.domain.bo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("订单流水查询对象")
public class TiktokOrderSettlementQueryBo {

	@ApiModelProperty("商户id")
	private Long shopId;

	/**
	 * 订单编号
	 */
	@ApiModelProperty(value = "订单编号")
	private String orderNum;

	/**
	 * 支付流水号
	 */
	@ApiModelProperty(value = "支付流水号")
	private String payNo;

	/**
	 * 支付类型 0微信 1支付宝
	 */
	@ApiModelProperty(value = "支付类型 0微信 1支付宝")
	private Integer payType;


	/**
	 * 支付状态 0支付失败 1支付成功
	 */
	@ApiModelProperty(value = "支付状态 0支付失败 1支付成功")
	private Integer payStatus;


	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/**
	 * 排序列
	 */
	@ApiModelProperty("排序列")
	private String orderByColumn;

	/**
	 * 排序的方向desc或者asc
	 */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;
}
