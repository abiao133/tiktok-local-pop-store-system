package com.qingyun.shop.domain.bo.resource;

import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;
import java.util.List;


/**
 * 合成语音业务对象 tiktok_voice
 *
 * @author qingyun
 * @date 2021-10-21
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("合成语音业务对象")
public class TiktokVoiceBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long shopId;

	@ApiModelProperty("账户名称")
	private String shopName;

    /**
     * 语音标题
     */
    @ApiModelProperty(value = "语音标题")
    private String title;

    /**
     * 语音文案
     */
    @ApiModelProperty(value = "语音文案")
    private String context;


    @ApiModelProperty(value = "标签id列表")
    private List<Long> tags;

    @ApiModelProperty(value = "语音声音")
    private String per;
    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
