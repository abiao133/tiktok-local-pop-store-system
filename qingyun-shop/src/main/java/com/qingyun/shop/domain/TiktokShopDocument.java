package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 商户文案对象 tiktok_shop_document
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_shop_document")
public class TiktokShopDocument implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 文案内容
     */
    private String context;

    /**
     * 评论内容多条用，隔开
     */
    private String common;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
