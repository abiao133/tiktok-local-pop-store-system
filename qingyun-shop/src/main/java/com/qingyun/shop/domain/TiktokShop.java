package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 商家对象 tiktok_shop
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_shop")
public class TiktokShop implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;


    /**
     * 代理商id -1为平台
     */
    private Long proxyId;

    /**
     * 代理商名称
     */
    private String proxyName;

	/**
	 * 商家头像
	 */
	private String pic;
    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 商家名称
     */
    private String name;

    /**
     * 营业时间
     */
    private String businessTime;

    /**
     * 热度
     */
    private String heat;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 商户手机号码
     */
    private String phone;

    /**
     * 商户邮箱
     */
    private String email;

    /**
     * 商户余额
     */
    private Long videoCount;

	/**
	 * 是否是代理商
	 */
	private Integer isProxy;

    /**
     * 商户状态 0启用 1禁用
     */
    private Integer status;

	/**
	 * 提醒次数
	 */
	private Integer remindCount;

    /**
     * $column.columnComment
     */
    @TableLogic
    private Integer delFlag;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

}
