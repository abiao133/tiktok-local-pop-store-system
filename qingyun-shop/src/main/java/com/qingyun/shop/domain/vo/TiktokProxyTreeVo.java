package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname TiktokProxyVo
 * @Author dyh
 * @Date 2021/9/10 14:40
 */
@Data
@ApiModel("代理商树结构实体")
public class TiktokProxyTreeVo {

	@ApiModelProperty("代理商id")
	private Integer id;

	@ApiModelProperty("代理商名称")
	private String label;

	@ApiModelProperty("代理商下级")
	private List<TiktokProxyTreeVo> children=new ArrayList<>();
}
