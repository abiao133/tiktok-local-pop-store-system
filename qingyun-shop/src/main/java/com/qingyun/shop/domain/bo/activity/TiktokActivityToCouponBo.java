package com.qingyun.shop.domain.bo.activity;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 活动与优惠券关联业务对象 tiktok_activity_to_coupon
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("活动与优惠券关联业务对象")
public class TiktokActivityToCouponBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 活动表id
     */
    @ApiModelProperty(value = "活动表id", required = true)
    @NotNull(message = "活动表id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activityId;

    /**
     * 优惠券表id
     */
    @ApiModelProperty(value = "优惠券表id", required = true)
    @NotNull(message = "优惠券表id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long couponId;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
