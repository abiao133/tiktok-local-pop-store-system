package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@ApiModel("银行卡真实性校验")
public class BankCardRealCheckBo {

    @ApiModelProperty(value = "银行卡号", required = true)
    @NotBlank(message = "银行卡号不能为空", groups = { AddGroup.class, EditGroup.class })
    @Size(min = 16, max = 19, message = "银行卡号必须在16到19位")
    private String bankNumber;

    @ApiModelProperty(value = "身份证号", required = true)
    @NotBlank(message = "身份证号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String idCard;

    @ApiModelProperty(value = "真实姓名", required = true)
    @NotBlank(message = "真实姓名", groups = { AddGroup.class, EditGroup.class })
    private String realName;
}
