package com.qingyun.shop.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("商户提现申请查询对象")
public class TiktokWithdrawApplyQueryBo {

    /**
     * 所属商户id
     */
    @ApiModelProperty(value = "所属代理商id ")
    private Long proxyId;

    @ApiModelProperty(value = "状态 0受理中 1完成 2驳回")
    private Integer status;

    @ApiModelProperty(value = "提现人姓名")
    private String userName;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;
}
