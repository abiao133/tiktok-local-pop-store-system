package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 活动与优惠券关联视图对象 tiktok_activity_to_coupon
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("活动与优惠券关联视图对象")
@ExcelIgnoreUnannotated
public class TiktokActivityToCouponVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 活动表id
     */
	@ExcelProperty(value = "活动表id")
	@ApiModelProperty("活动表id")
	private Long activityId;

    /**
     * 优惠券表id
     */
	@ExcelProperty(value = "优惠券表id")
	@ApiModelProperty("优惠券表id")
	private Long couponId;


}
