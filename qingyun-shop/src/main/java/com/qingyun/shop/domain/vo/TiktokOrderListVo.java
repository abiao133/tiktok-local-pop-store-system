package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.qingyun.common.convert.ExcelOrderSourceConvert;
import com.qingyun.common.convert.ExcelOrderStatusConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("订单列表视图对象")
public class TiktokOrderListVo {

	@ExcelIgnore
	@ApiModelProperty(value = "订单id")
	private Long id;

	@ExcelProperty(value = "订单编号", index = 0)
	@ApiModelProperty(value = "订单编号")
	private String orderNum;

	@ExcelProperty(value = "所属商户", index = 1)
	@ApiModelProperty("所属商户")
	private String shopName;

	@ApiModelProperty("代理商名称")
	private String proxyName;


	@ExcelProperty(value = "产品名称", index = 2)
	@ApiModelProperty(value = "产品名称")
	private String goodsName;

	@ExcelProperty(value = "购买数量", index = 3)
	@ApiModelProperty(value = "购买数量")
	private Integer quantity;

	@ExcelProperty(value = "金额", index = 4)
	@ApiModelProperty(value = "金额")
	private BigDecimal amount;

	@ExcelProperty(value = "订单来源", index = 5, converter = ExcelOrderSourceConvert.class)
	@ApiModelProperty(value = "订单来源：1.管理后台 2.小程序 3手机APP")
	private Integer source;

	@ExcelProperty(value = "订单状态", index = 6, converter = ExcelOrderStatusConvert.class)
	@ApiModelProperty(value = "订单状态：0待支付 1交易成功 2交易失败 3超时")
	private Integer status;

	@ExcelProperty(value = "付款人", index = 7)
	@ApiModelProperty("付款人")
	private String payName;

	@ExcelProperty(value = "支付时间", index = 8)
	@ApiModelProperty(value = "支付时间")
	@DateTimeFormat("yyyy-MM-dd HH:mm:ss")
	private Date payTime;

	@DateTimeFormat("yyyy-MM-dd HH:mm")
	private Date createTime;

	@DateTimeFormat("yyyy-MM-dd HH:mm")
	private Date closeTime;
}
