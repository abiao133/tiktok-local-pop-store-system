package com.qingyun.shop.domain.bo.coupon;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class TiktokAppCouponQueryBo {

    @ApiModelProperty(value = "商户id", hidden = true)
    private Long shopId;

    /**
     * 有效期开始日期
     */
    @ApiModelProperty(value = "有效期开始日期")
    @DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date startTime;

    /**
     * 有效期结束日期
     */
    @ApiModelProperty(value = "有效期结束日期")
    @DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date endTime;

    /**
     * 0：代金券；1：折扣券；2：兑换券
     */
    @ApiModelProperty(value = "0：代金券；1：折扣券；2：兑换券")
    private Integer type;

    /**
     * 优惠券状态，0待上架；1已上架; 2 已过期; 3已下架
     */
    @ApiModelProperty(value = "优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
    private Integer status;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

}
