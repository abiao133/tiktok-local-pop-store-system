package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 商家业务对象 tiktok_shop
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("商家业务对象")
public class TiktokShopBo extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 代理商id -1为平台
     */
    @ApiModelProperty(value = "代理商id -1为平台", required = true)
    @NotNull(message = "代理商id -1为平台不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long proxyId;

    /**
     * 代理商名称
     */
    @ApiModelProperty(value = "代理商名称", required = true)
    @NotBlank(message = "代理商名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String proxyName;

	@ApiModelProperty(value = "商家头像", required = true)
	@NotBlank(message = "商家头像", groups = { AddGroup.class, EditGroup.class })
    private String pic;
    /**
     * 祖级列表
     */
    @ApiModelProperty(value = "祖级列表", required = true)
    @NotBlank(message = "祖级列表不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ancestors;

    /**
     * 商家名称
     */
    @ApiModelProperty(value = "商家名称", required = true)
    @NotBlank(message = "商家名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 营业时间
     */
    @ApiModelProperty(value = "营业时间")
    private String businessTime;

//    /**
//     * 热度
//     */
//    @ApiModelProperty(value = "热度", required = true)
//    @NotNull(message = "热度不能为空", groups = { AddGroup.class, EditGroup.class })
//    private Long heat;

    /**
     * 负责人
     */
    @ApiModelProperty(value = "负责人")
    private String leader;

    /**
     * 显示顺序
     */
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;

    /**
     * 商户手机号码
     */
    @ApiModelProperty(value = "商户手机号码", required = true)
    @NotBlank(message = "商户手机号码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String phone;

    /**
     * 商户邮箱
     */
    @ApiModelProperty(value = "商户邮箱")
    private String email;

    /**
     * 商户余额
     */
    @ApiModelProperty(value = "商户余额")
    private Long videoCount;

    /**
     * 商户状态 0启用 1禁用
     */
    @ApiModelProperty(value = "商户状态 0启用 1禁用", required = true)
    @NotNull(message = "商户状态 0启用 1禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

//    /**
//     * $column.columnComment
//     */
//    @ApiModelProperty(value = "$column.columnComment")
//    private Integer delFlag;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
