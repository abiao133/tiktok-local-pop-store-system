package com.qingyun.shop.domain.vo;

import com.qingyun.shop.domain.TiktokGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Classname OrderVo
 * @Author dyh
 * @Date 2021/9/18 19:06
 */
@ApiModel("订单结果")
@Data
public class OrderVo {

	@ApiModelProperty("商品")
	private TiktokGoods tiktokGoods;

	@ApiModelProperty("商品数量")
	private Long count;

	@ApiModelProperty("订单编号")
	private  String orderNum;

	@ApiModelProperty("待支付金额")
	private BigDecimal amount;

	@ApiModelProperty("总的转发次数")
	private Long totalVideoCount;
}
