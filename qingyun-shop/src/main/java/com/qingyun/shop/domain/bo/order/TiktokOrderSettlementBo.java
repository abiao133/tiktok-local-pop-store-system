package com.qingyun.shop.domain.bo.order;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;

/**
 * 订单流水业务对象 tiktok_order_settlement
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("订单流水业务对象")
public class TiktokOrderSettlementBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String orderNum;

    /**
     * 支付流水号
     */
    @ApiModelProperty(value = "支付流水号", required = true)
    @NotBlank(message = "支付流水号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String payNo;

    /**
     * 支付类型 0微信 1支付宝
     */
    @ApiModelProperty(value = "支付类型 0微信 1支付宝", required = true)
    @NotNull(message = "支付类型 0微信 1支付宝不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer payType;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额", required = true)
    @NotNull(message = "支付金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal payAmount;

    /**
     * 支付状态 0支付失败 1支付成功
     */
    @ApiModelProperty(value = "支付状态 0支付失败 1支付成功", required = true)
    @NotNull(message = "支付状态 0支付失败 1支付成功不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer payStatus;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
