package com.qingyun.shop.domain.bo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TiktokBankCardQueryBo {

    @ApiModelProperty(value = "银行卡号 (模糊查询)")
    private String bankNumber;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "代理商名 (模糊查询)")
    private String proxyName;

    @ApiModelProperty(value = "0正常 1禁用")
    private Integer status;

    @ApiModelProperty(value = "代理商id")
    private Long proxyId;


    @ApiModelProperty("分页大小")
    private Integer pageSize;


    @ApiModelProperty("当前页数")
    private Integer pageNum;
}
