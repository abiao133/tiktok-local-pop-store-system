package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标签与背景音乐多对多关联对象 tiktok_tag_to_bgm
 *
 * @author qingyun
 * @date 2021-09-26
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_tag_to_bgm")
public class TiktokTagToBgm implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 标签id
     */
    private Long tagId;

    /**
     * bgm id
     */
    private Long bgmId;


    private Long shopId;

}
