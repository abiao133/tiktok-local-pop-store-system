package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("小程序端优惠券列表视图对象")
public class TiktokAppCouponListVo {

    @ApiModelProperty("优惠券id")
    private Long id;

    @ApiModelProperty("折扣券标题")
    private String title;

    @ApiModelProperty("有效期开始日期")
    private Date startTime;

    @ApiModelProperty("有效期结束日期")
    private Date endTime;

    @ApiModelProperty("封面图片")
    private String coverImg;

    @ApiModelProperty("优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
    private Integer status;

    @ApiModelProperty("是否绑定了活动")
    private Boolean isRelevancy;

    @ApiModelProperty("活动名称")
    private String activityName;
}
