package com.qingyun.shop.domain.bo.branch;

import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;

/**
 * 代理商返利配置业务对象 tiktok_proxy_branch_config
 *
 * @author qingyun
 * @date 2021-10-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理商返利配置业务对象")
public class TiktokProxyBranchConfigBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 代理商id
     */
    @ApiModelProperty(value = "代理商id")
    private Long proxyId;

    /**
     * 代理商pc充值单价
     */
    @ApiModelProperty(value = "代理商pc充值单价")
    private BigDecimal proxyPrice;

    /**
     * 一级返利比例
     */
    @ApiModelProperty(value = "一级返利比例")
    private BigDecimal oneProxyRatio;

    /**
     * 二级返利比例
     */
    @ApiModelProperty(value = "二级返利比例")
    private BigDecimal towProxyRatio;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
