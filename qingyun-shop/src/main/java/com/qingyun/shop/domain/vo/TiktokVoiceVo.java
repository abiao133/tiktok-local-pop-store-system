package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import com.qingyun.shop.domain.TiktokTag;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * 合成语音视图对象 tiktok_voice
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Data
@ApiModel("合成语音视图对象")
@ExcelIgnoreUnannotated
public class TiktokVoiceVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;


	@ApiModelProperty("账户名称")
	private String shopName;

    /**
     * 语音标题
     */
	@ExcelProperty(value = "语音标题")
	@ApiModelProperty("语音标题")
	private String title;

    /**
     * 语音文案
     */
	@ExcelProperty(value = "语音文案")
	@ApiModelProperty("语音文案")
	private String context;

    /**
     * 语音合成后地址
     */
	@ExcelProperty(value = "语音合成后地址")
	@ApiModelProperty("语音合成后地址")
	private String voice;

	@ApiModelProperty("标签")
	private List<String> tagList;


	@ApiModelProperty("创建时间")
	private Date createTime;


}
