package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 商户银行卡信息对象 tiktok_bank_card
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_bank_card")
public class TiktokBankCard implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 银行卡号
     */
    private String bankNumber;

    /**
     * 开户行
     */
    private String bankDeposit;

    /**
     * 支行名称
     */
    private String branchName;

    /**
     * 银行卡预留手机号
     */
    private String bankPhone;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 所属代理商id
     */
    private Long proxyId;

    /**
     * 0正常 1禁用
     */
    private Integer status;

    /**
     * 软删除标识
     */
    @TableLogic
    private Integer delFlag;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

}
