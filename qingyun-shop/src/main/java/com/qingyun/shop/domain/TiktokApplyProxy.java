package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 代理商申请对象 tiktok_apply_proxy
 *
 * @author qingyun
 * @date 2021-09-10
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_apply_proxy")
public class TiktokApplyProxy implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 主键id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 申请人用户id
     */
    private Long userId;

    /**
     * 申请人真实姓名
     */
    private String realName;

    /**
     * 申请人电话
     */
    private String userPhone;

    /**
     * 申请理由
     */
    private String applyReason;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * 流程状态 0审核中 1通过 2未通过
     */
    private Integer status;

    /**
     * 通过时间
     */
    private Date passTime;

    /**
     * 驳回理由
     */
    private String reject;

}
