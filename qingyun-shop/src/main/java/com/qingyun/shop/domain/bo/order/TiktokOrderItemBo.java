package com.qingyun.shop.domain.bo.order;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;

/**
 * 订单详情业务对象 tiktok_order_item
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("订单详情业务对象")
public class TiktokOrderItemBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = true)
    @NotBlank(message = "订单编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String orderNum;

    /**
     * 商品
     */
    @ApiModelProperty(value = "商品", required = true)
    @NotNull(message = "商品不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long goodsId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", required = true)
    @NotBlank(message = "商品名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String goodsName;

    /**
     * 购买数量
     */
    @ApiModelProperty(value = "购买数量", required = true)
    @NotNull(message = "购买数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long quantity;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额", required = true)
    @NotNull(message = "金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal amount;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
