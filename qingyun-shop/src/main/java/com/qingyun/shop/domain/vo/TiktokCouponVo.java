package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 优惠券视图对象 tiktok_coupon
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("优惠券视图对象")
@ExcelIgnoreUnannotated
public class TiktokCouponVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * $column.columnComment
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

	@ApiModelProperty("商户名称")
	private String shopName;

    /**
     * 折扣券标题
     */
	@ExcelProperty(value = "折扣券标题")
	@ApiModelProperty("折扣券标题")
	private String title;

    /**
     * 折扣
     */
	@ExcelProperty(value = "折扣")
	@ApiModelProperty("折扣")
	private Double discount;

    /**
     * 有效期开始日期
     */
	@ExcelProperty(value = "有效期开始日期")
	@ApiModelProperty("有效期开始日期")
	private Date startTime;

    /**
     * 有效期结束日期
     */
	@ExcelProperty(value = "有效期结束日期")
	@ApiModelProperty("有效期结束日期")
	private Date endTime;

    /**
     * 基于领取时间的有效天数days。
     */
	@ExcelProperty(value = "基于领取时间的有效天数days。")
	@ApiModelProperty("基于领取时间的有效天数days。")
	private Integer days;

    /**
     * 每个用户领券上限，如不填，则默认为1
     */
	@ExcelProperty(value = "每个用户领券上限，如不填，则默认为1")
	@ApiModelProperty("每个用户领券上限，如不填，则默认为1")
	private Integer collectionTimes;

    /**
     * 封面图片
     */
	@ExcelProperty(value = "封面图片")
	@ApiModelProperty("封面图片")
	private String coverImg;

    /**
     * 封面简介
     */
	@ExcelProperty(value = "封面简介")
	@ApiModelProperty("封面简介")
	private String coverInfo;

    /**
     * 使用须知
     */
	@ExcelProperty(value = "使用须知")
	@ApiModelProperty("使用须知")
	private String useNotice;

    /**
     * 减免金额
     */
	@ExcelProperty(value = "减免金额")
	@ApiModelProperty("减免金额")
	private BigDecimal amount;

    /**
     * 最低消费：0代表不限制
     */
	@ExcelProperty(value = "最低消费：0代表不限制")
	@ApiModelProperty("最低消费：0代表不限制")
	private BigDecimal minCost;

    /**
     * 0：代金券；1：折扣券；2：兑换券
     */
	@ExcelProperty(value = "0：代金券；1：折扣券；2：兑换券")
	@ApiModelProperty("0：代金券；1：折扣券；2：兑换券")
	private Integer type;

    /**
     * 数量：99999999为无限量
     */
	@ExcelProperty(value = "数量：99999999为无限量")
	@ApiModelProperty("数量：99999999为无限量")
	private Long quantity;

    /**
     * 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     */
	@ExcelProperty(value = "商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。")
	@ApiModelProperty("商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。")
	private Integer goodsType;

    /**
     * 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     */
	@ExcelProperty(value = "商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。")
	@ApiModelProperty("商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。")
	private String goodsValue;

    /**
     * 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     */
	@ExcelProperty(value = "有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；")
	@ApiModelProperty("有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；")
	private Integer timeType;

    /**
     * 优惠券状态，0待上架；1已上架; 2 已过期; 3已下架
     */
	@ExcelProperty(value = "优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
	@ApiModelProperty("优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
	private Integer status;

	@ApiModelProperty("是否绑定了活动")
	private Boolean isRelevancy;

	@ApiModelProperty("活动名称")
	private String activityName;
}
