package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 用户优惠卷领取对象 tiktok_coupon_draw
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_coupon_draw")
public class TiktokCouponDraw implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 优惠卷id
     */
    private Long couponId;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 优惠卷code
     */
    private String couponCode;

    /**
     * 获取途径
     */
    private Integer getType;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 优惠卷有效开始时间
     */
    private Date beginDate;

    /**
     * 优惠卷有效结束时间
     */
    private Date endDate;

    /**
     * 使用状态 0->未使用；1->已使用；2->已过期
     */
    private Integer useStatus;

    /**
     * 使用时间
     */
    private Date useTime;

	/**
	 * 优惠券发送状态：0=待发送；1=已发送
	 */
	private Integer status;


    @TableField(exist = false)
    private TiktokCoupon coupon;

}
