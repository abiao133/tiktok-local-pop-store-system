package com.qingyun.shop.domain.bo.resource;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("资源修改对象")
public class TiktokResourceEditBo {

	@ApiModelProperty(value = "id", required = true)
	@NotNull(message = "id不能为空", groups = {AddGroup.class, EditGroup.class})
	private Long id;

	/**
	 * 资源类型 0图片 1视频
	 */
	@ApiModelProperty(value = "资源类型 0图片 1视频 2音频 ", required = true)
	@NotNull(message = "资源类型 0图片 1视频 2音频 不能为空", groups = {AddGroup.class, EditGroup.class})
	private Integer resourceType;


	@ApiModelProperty(value = "分组id")
	@NotBlank(message = "分组id不能为空")
	private Long groupId;

	@ApiModelProperty(value = "是否是碎片")
	private Integer isFrag;

	/**
	 * 资源地址
	 */
	@ApiModelProperty(value = "资源地址", required = true)
	@NotBlank(message = "资源地址不能为空", groups = {AddGroup.class, EditGroup.class})
	private String resourceUrl;

	/**
	 * 资源名称
	 */
	@ApiModelProperty(value = "资源名称", required = true)
	@NotBlank(message = "资源名称不能为空", groups = {AddGroup.class, EditGroup.class})
	private String resourceName;

	@ApiModelProperty("备注")
	private String remark;
}
