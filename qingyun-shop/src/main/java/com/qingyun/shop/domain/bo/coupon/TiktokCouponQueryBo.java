package com.qingyun.shop.domain.bo.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@ApiModel("优惠券查询对象")
public class TiktokCouponQueryBo {

    @ApiModelProperty(value = "商户id")
    private Long shopId;

    /**
     * 折扣券标题
     */
    @ApiModelProperty(value = "折扣券标题")
    private String title;

    /**
     * 有效期开始日期
     */
    @ApiModelProperty(value = "有效期开始日期")
	@DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date startTime;

    /**
     * 有效期结束日期
     */
    @ApiModelProperty(value = "有效期结束日期")
	@DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date endTime;

    /**
     * 0：代金券；1：折扣券；2：兑换券
     */
    @ApiModelProperty(value = "0：代金券；1：折扣券；2：兑换券")
    private Integer type;

    /**
     * 优惠券状态，0待上架；1已上架; 2 已过期; 3已下架
     */
    @ApiModelProperty(value = "优惠券状态，0待上架；1已上架; 2 已过期; 3已下架")
    private Integer status;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;
}
