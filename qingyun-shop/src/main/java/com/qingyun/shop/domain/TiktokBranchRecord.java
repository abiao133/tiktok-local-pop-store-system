package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 分销返利记录对象 tiktok_branch_record
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_branch_record")
public class TiktokBranchRecord implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 1代理商给代理商 2 商家给代理商
     */
    private String subRole;

    /**
     * 商品名称
     */
    private String goodName;

    /**
     * 商品id
     */
    private Long goodId;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 公共名称
     */
    private String publicName;

	/**
	 * 代理商id
	 */
	private Long proxyId;

	/**
	 * 代理商名称
	 */
	private String proxyName;
    /**
     * 一级代理商获取的金额
     */
    private BigDecimal rebateAmount;

    /**
     * 如果role为1 折此字段是代理商id 2此字段为商家id
     */
    private Long publicId;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
