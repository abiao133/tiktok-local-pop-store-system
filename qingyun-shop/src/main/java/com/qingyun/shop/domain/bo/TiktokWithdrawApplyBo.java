package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商户提现申请业务对象 tiktok_withdraw_apply
 *
 * @author qingyun
 * @date 2021-10-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理商提现申请业务对象")
public class TiktokWithdrawApplyBo extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 关联的银行卡id
     */
    @ApiModelProperty(value = "关联的银行卡id", required = true)
    @NotNull(message = "关联的银行卡id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long bankId;

    /**
     * 所属商户id
     */
    @ApiModelProperty(value = "所属代理商id", required = true)
    @NotNull(message = "所属代理商id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long proxyId;

    /**
     * 提现金额
     */
    @ApiModelProperty(value = "提现金额", required = true)
    @NotNull(message = "提现金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal withdrawAmount;

    /**
     * 提现完成时间
     */
    @ApiModelProperty(value = "提现完成时间")
    private Date finishTime;

    /**
     * 状态 0受理中 1完成 2驳回
     */
    @ApiModelProperty(value = "状态 0受理中 1完成 2驳回", required = true)
    @NotNull(message = "状态 0受理中 1完成 2驳回不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 提现人姓名
     */
    @ApiModelProperty(value = "提现人姓名", required = true)
    @NotBlank(message = "提现人姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String userName;

    /**
     * 提现人id
     */
    @ApiModelProperty(value = "提现人id", required = true)
    @NotNull(message = "提现人id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 驳回理由
     */
    @ApiModelProperty(value = "驳回理由")
    private String rejectReason;

    /**
     * 删除标识
     */
    @ApiModelProperty(value = "删除标识", required = true)
    @NotNull(message = "删除标识不能为空", groups = { AddGroup.class })
    private Integer delFlag;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
