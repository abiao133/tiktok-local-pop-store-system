package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 代理给商户转播放量记录视图对象 tiktok_transfer
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("代理给商户转播放量记录视图对象")
@ExcelIgnoreUnannotated
public class TiktokTransferVo {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 代理商id
     */
	@ExcelProperty(value = "代理商id")
	@ApiModelProperty("代理商id")
	private Long proxyId;

    /**
     * 商家id
     */
	@ExcelProperty(value = "商家id")
	@ApiModelProperty("商家id")
	private Long shopId;

    /**
     * 消耗播放量
     */
	@ExcelProperty(value = "消耗播放量")
	@ApiModelProperty("消耗播放量")
	private String videoCount;

    /**
     * 代理商名称
     */
	@ExcelProperty(value = "代理商名称")
	@ApiModelProperty("代理商名称")
	private String proxyName;

    /**
     * 商家名称
     */
	@ExcelProperty(value = "商家名称")
	@ApiModelProperty("商家名称")
	private String shopName;

    /**
     * 备注
     */
	@ExcelProperty(value = "备注")
	@ApiModelProperty("备注")
	private String remark;

	@ApiModelProperty("创建时间")
	private Date createTime;
}
