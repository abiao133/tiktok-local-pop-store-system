package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 商户文案视图对象 tiktok_shop_document
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Data
@ApiModel("商户文案视图对象")
@ExcelIgnoreUnannotated
public class TiktokShopDocumentVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	private String shopName;
    /**
     * 文案内容
     */
	@ExcelProperty(value = "文案内容")
	@ApiModelProperty("文案内容 标题 最多30字")
	private String context;

    /**
     * 评论内容多条用，隔开
     */
	@ExcelProperty(value = "评论内容多条用，隔开")
	@ApiModelProperty("评论内容多条用，隔开")
	private String common;


}
