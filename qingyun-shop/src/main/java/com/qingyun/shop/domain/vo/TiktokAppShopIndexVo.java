package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Classname TiktokAppShopIndexVo
 * @Author dyh
 * @Date 2021/9/25 10:51
 */
@Data
@ApiModel("小程序首页数据vo")
public class TiktokAppShopIndexVo {

	@ApiModelProperty("总发送数据")
	private Integer total;

	@ApiModelProperty("今日发送量")
	private Integer today;

	@ApiModelProperty("昨日发送量")
	private Integer yesterday;


}
