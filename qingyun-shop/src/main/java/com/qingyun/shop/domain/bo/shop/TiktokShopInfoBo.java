package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 商家账号信息业务对象 tiktok_shop_info
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("商家账号信息业务对象")
public class TiktokShopInfoBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "商家账号信息id")
    private Long id;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * openid
     */
    @ApiModelProperty(value = "openid", required = true)
    @NotBlank(message = "openid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String openId;

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称", required = true)
    @NotBlank(message = "用户名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nickname;

    /**
     * 地理位置id
     */
    @ApiModelProperty(value = "地理位置id")
    private String poiId;

    /**
     * 地址位置名称
     */
    @ApiModelProperty(value = "地址位置名称")
    private String poiName;

    /**
     * 抖音小程序id
     */
    @ApiModelProperty(value = "抖音小程序id")
    private String miniAppId;

    /**
     * 抖音小程序标题
     */
    @ApiModelProperty(value = "抖音小程序标题")
    private String miniAppTitle;

    /**
     * 抖音小程序地址
     */
    @ApiModelProperty(value = "抖音小程序地址")
    private String miniAppUrl;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度")
    private String lat;

    /**
     * 经度
     */
    @ApiModelProperty(value = "经度")
    private String log;

    /**
     * 商家抖音主页
     */
    @ApiModelProperty(value = "商家抖音主页")
    private String douyinUrl;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
