package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 订单流水视图对象 tiktok_order_settlement
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("订单流水视图对象")
@ExcelIgnoreUnannotated
public class TiktokOrderSettlementVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 订单编号
     */
	@ExcelProperty(value = "订单编号")
	@ApiModelProperty("订单编号")
	private String orderNum;

    /**
     * 支付流水号
     */
	@ExcelProperty(value = "支付流水号")
	@ApiModelProperty("支付流水号")
	private String payNo;

    /**
     * 支付类型 0微信 1支付宝
     */
	@ExcelProperty(value = "支付类型 0微信 1支付宝")
	@ApiModelProperty("支付类型 0微信 1支付宝")
	private Integer payType;

    /**
     * 支付金额
     */
	@ExcelProperty(value = "支付金额")
	@ApiModelProperty("支付金额")
	private BigDecimal payAmount;

    /**
     * 支付状态 0支付失败 1支付成功
     */
	@ExcelProperty(value = "支付状态 0支付失败 1支付成功")
	@ApiModelProperty("支付状态 0支付失败 1支付成功")
	private Integer payStatus;


}
