package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("订单信息详细视图")
public class TiktokOrderDetailVo {

	@ApiModelProperty("订单id")
	private Long id;

	@ApiModelProperty("订单编号")
	private String orderNum;

	@ApiModelProperty("商户名称")
	private String shopName;

	@ApiModelProperty("充值用户名称")
	private String userName;

	@ApiModelProperty("产品名称")
	private String goodsName;

	@ApiModelProperty("购买数量")
	private Long quantity;

	@ApiModelProperty("金额")
	private BigDecimal amount;

	@ApiModelProperty("订单来源：1.管理后台 2.小程序 3手机APP")
	private Integer source;

	@ApiModelProperty("订单来源：0待支付 1交易成功 2交易失败 3超时")
	private Integer status;

	@ApiModelProperty("订单备注")
	private String remarks;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("订单关闭时间")
	private Date closeTime;

	@ApiModelProperty("支付时间")
	private Date payTime;
}
