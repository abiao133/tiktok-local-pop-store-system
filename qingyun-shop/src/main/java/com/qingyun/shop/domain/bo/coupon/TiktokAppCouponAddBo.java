package com.qingyun.shop.domain.bo.coupon;

import com.qingyun.common.core.validate.AddGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ApiModel("小程序优惠券新增对象")
public class TiktokAppCouponAddBo {
    @ApiModelProperty(value = "商户id", hidden = true)
    private Long shopId;

    @NotBlank(message = "标题不能为空", groups = { AddGroup.class })
    @ApiModelProperty(value = "折扣券标题")
    private String title;

    @ApiModelProperty(value = "折扣")
    private Double discount;

    @ApiModelProperty(value = "有效期开始日期")
    private Date startTime;

    @ApiModelProperty(value = "有效期结束日期")
    private Date endTime;

    @ApiModelProperty(value = "基于领取时间的有效天数days。")
    @Min(value = 1, message = "优惠卷有效时间不能小于1")
    private Integer days;

    @ApiModelProperty(value = "每个用户领券上限，如不填，则默认为1")
    private Integer collectionTimes;

    @ApiModelProperty(value = "封面图片", required = true)
    private String coverImg;

    @ApiModelProperty(value = "封面简介")
    private String coverInfo;

    @ApiModelProperty(value = "使用须知")
    private String useNotice;

    @ApiModelProperty(value = "减免金额")
    private Long amount;

    @ApiModelProperty(value = "最低消费：0代表不限制", required = true)
    @NotNull(message = "最低消费：0代表不限制不能为空", groups = { AddGroup.class })
    private Long minCost;

    @ApiModelProperty(value = "0：代金券；1：折扣券；2：兑换券")
    @NotNull(message = "类型不能为空", groups = { AddGroup.class })
    private Integer type;

    @ApiModelProperty(value = "数量：99999999为无限量", required = true)
    @Max(value = 99999998, message = "发放数量不能大于99999998！")
    @Min(value = 1, message = "发放数量不能小于1")
    private Long quantity;

    @ApiModelProperty(value = "商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。", required = true)
    private Integer goodsType;

    @ApiModelProperty(value = "商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。")
    private String goodsValue;

    @ApiModelProperty(value = "有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；")
    private Integer timeType;

    @ApiModelProperty(value = "优惠券状态，0待上架；1已上架; 2 已过期; 3已下架", required = true)
    @NotNull(message = "优惠券状态不能为空", groups = { AddGroup.class})
    private Integer status;
}
