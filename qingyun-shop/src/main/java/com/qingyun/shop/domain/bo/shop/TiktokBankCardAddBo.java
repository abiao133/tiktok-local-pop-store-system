package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户银行卡信息业务对象")
public class TiktokBankCardAddBo extends BaseEntity {

    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号", required = true)
    @NotBlank(message = "银行卡号不能为空", groups = { AddGroup.class, EditGroup.class })
    @Size(min = 16, max = 19, message = "银行卡号必须在16到19位")
    private String bankNumber;

    /**
     * 开户行
     */
    @ApiModelProperty(value = "开户行", required = true)
    @NotBlank(message = "开户行不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bankDeposit;

    /**
     * 支行名称
     */
    @ApiModelProperty(value = "支行名称", required = true)
    @NotBlank(message = "支行名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String branchName;

    /**
     * 银行卡预留手机号
     */
    @ApiModelProperty(value = "银行卡预留手机号", required = true)
    @NotBlank(message = "银行卡预留手机号不能为空", groups = { AddGroup.class, EditGroup.class })

    private String bankPhone;

    @ApiModelProperty(value = "身份证号", required = true)
    @NotBlank(message = "身份证号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String idCard;

    @ApiModelProperty(value = "真实姓名", required = true)
    @NotBlank(message = "真实姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String realName;

    /**
     * 所属商户id
     */
    @ApiModelProperty(value = "所属代理商id 平台创建才需要传代理商id")
    private Long proxyId;

}
