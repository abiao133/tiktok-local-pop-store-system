package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * 优惠券活动视图对象 tiktok_activity
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("优惠券活动视图对象")
@ExcelIgnoreUnannotated
public class TiktokActivityVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 活动标题
     */
	@ExcelProperty(value = "活动标题")
	@ApiModelProperty("活动标题")
	private String title;

    /**
     * 活动描述
     */
	@ExcelProperty(value = "活动描述")
	@ApiModelProperty("活动描述")
	private String description;

    /**
     * 活动内容（富文本
     */
	@ExcelProperty(value = "活动内容", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "活动内容（富文本")
	@ApiModelProperty("活动内容（富文本")
	private String context;

    /**
     * 活动开始时间
     */
	@ExcelProperty(value = "活动开始时间")
	@ApiModelProperty("活动开始时间")
	private Date beginDate;

    /**
     * 活动结束时间
     */
	@ExcelProperty(value = "活动结束时间")
	@ApiModelProperty("活动结束时间")
	private Date endDate;

    /**
     * 每个人可参与次数：0为不限次
     */
	@ExcelProperty(value = "每个人可参与次数：0为不限次")
	@ApiModelProperty("每个人可参与次数：0为不限次")
	private Long joinCount;

    /**
     * 活动类型：0扫码领券；1纯推广2；抽奖
     */
	@ExcelProperty(value = "活动类型：0扫码领券；1纯推广2；抽奖")
	@ApiModelProperty("活动类型：0扫码领券；1纯推广2；抽奖")
	private String actityType;

    /**
     * 延迟发送视频：小时为单位，0为不延迟
     */
	@ExcelProperty(value = "延迟发送视频：小时为单位，0为不延迟")
	@ApiModelProperty("延迟发送视频：小时为单位，0为不延迟")
	private Long laterSend;

    /**
     * 活动主图
     */
	@ExcelProperty(value = "活动主图")
	@ApiModelProperty("活动主图")
	private String imgUrl;

    /**
     * 活动状态：0待审核；1待上线；2上线中；3已下线
     */
	@ExcelProperty(value = "活动状态：0待审核；1待上线；2上线中；3已下线")
	@ApiModelProperty("活动状态：0待审核；1待上线；2上线中；3已下线")
	private Long status;

    /**
     * 活动宣传视频
     */
	@ExcelProperty(value = "活动宣传视频")
	@ApiModelProperty("活动宣传视频")
	private String videoUrl;

    /**
     * 所属商家
     */
	@ExcelProperty(value = "所属商家")
	@ApiModelProperty("所属商家id")
	private Long shopId;

	@ApiModelProperty("商户名称")
	private String shopName;

    /**
     * 抖音话题
     */
	@ExcelProperty(value = "抖音话题")
	@ApiModelProperty("抖音话题")
	private String douyinTitle;

    /**
     * 转发渠道：0抖音，1快手，2视频号，3微信朋友圈
     */
	@ExcelProperty(value = "转发渠道：0抖音，1快手，2视频号，3微信朋友圈")
	@ApiModelProperty("转发渠道：0抖音，1快手，2视频号，3微信朋友圈")
	private Integer forwardChannel;

    /**
     * 视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择
     */
	@ExcelProperty(value = "视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择")
	@ApiModelProperty("视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择")
	private Integer videoSource;

	@ApiModelProperty(value = "0为分组碎片混剪 1为随机")
	private Integer isRandFrag;

    /**
     * 视频资源分组数组
     */
	@ExcelProperty(value = "视频资源分组数组")
	@ApiModelProperty("视频资源分组数组")
	private String resourceGroupId;

    /**
     * 地理位置id
     */
	@ExcelProperty(value = "地理位置id")
	@ApiModelProperty("地理位置id")
	private String poiId;

    /**
     * 地理位置名称
     */
	@ExcelProperty(value = "地理位置名称")
	@ApiModelProperty("地理位置名称")
	private String poiName;

    /**
     * @的openid
     */
	@ExcelProperty(value = "@的openid")
	@ApiModelProperty("@的openid")
	private String atOpenids;

    /**
     * @的nickname
     */
	@ExcelProperty(value = "@的nickname")
	@ApiModelProperty("@的nickname")
	private String atNicknames;

    /**
     * 小程序id
     */
	@ExcelProperty(value = "小程序id")
	@ApiModelProperty("小程序id")
	private String miniAppId;

    /**
     * 小程序标题
     */
	@ExcelProperty(value = "小程序标题")
	@ApiModelProperty("小程序标题")
	private String miniAppTitle;

    /**
     * 吊起小程序时的url参数
     */
	@ExcelProperty(value = "吊起小程序时的url参数")
	@ApiModelProperty("吊起小程序时的url参数")
	private String miniAppUrl;

    /**
     * 视频标题
     */
	@ExcelProperty(value = "视频标题")
	@ApiModelProperty("视频标题")
	private String videoTitle;

    /**
     * 备注
     */
	@ExcelProperty(value = "备注")
	@ApiModelProperty("备注")
	private String remark;

    /**
     * 参与后跳转类型：0优惠券页面1商家抖音主页
     */
	@ExcelProperty(value = "参与后跳转类型：0优惠券页面1商家抖音主页")
	@ApiModelProperty("参与后跳转类型：0优惠券页面1商家抖音主页")
	private String redirectType;


	@ApiModelProperty("抖音商家主页（h5的时候使用）")
	private String douyinUrl;

	/**
	 * 是否需要用户关注商家才可领卷
	 */
	@ApiModelProperty("是否需要用户关注商家")
	private Integer isFocus;

	@ApiModelProperty("是否使用文案库")
	private Integer isDoc;


	@ApiModelProperty("活动选中的优惠卷列表")
	private List<Long> couponIds;


	@ApiModelProperty("商家地址")
	private String address;

	@ApiModelProperty("商家数据")
	private TiktokShopH5Vo shop;


	@ApiModelProperty("标签id 数组")
	private List<Long> tags;

	@ApiModelProperty("是否同步优惠券的有效期 0 否 1 是")
	private Integer isSynchronizationCoupon;
}
