package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 资源视图对象 tiktok_resource
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("资源视图对象")
@ExcelIgnoreUnannotated
public class TiktokResourceVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

    /**
     * 资源分组id
     */
	@ExcelProperty(value = "资源分组id")
	@ApiModelProperty("资源分组id")
	private Long groupId;


	@ApiModelProperty("资源分组名称")
	private String groupName;


	@ApiModelProperty("视频方向 0 纵向 1横向")
	private Integer format;

	@ApiModelProperty(value = "是否是碎片")
	private Integer isFrag;

	@ApiModelProperty(value = "播放时长")
	private Integer duration;

	@ApiModelProperty(value = "播放时长格式化")
	private String durationStr;

	@ApiModelProperty("状态")
	private Integer status;

    /**
     * 资源类型 0图片 1视频
     */
	@ExcelProperty(value = "资源类型 0图片 1视频 ")
	@ApiModelProperty("资源类型 0图片 1视频 ")
	private Integer resourceType;

    /**
     * 资源地址
     */
	@ExcelProperty(value = "资源地址")
	@ApiModelProperty("资源地址")
	private String resourceUrl;

    /**
     * 资源名称
     */
	@ExcelProperty(value = "资源名称")
	@ApiModelProperty("资源名称")
	private String resourceName;

    /**
     * $column.columnComment
     */
	@ExcelProperty(value = "资源名称")
	@ApiModelProperty("$column.columnComment")
	private String remark;


	@ApiModelProperty("商户名称")
	private String shopName;


}
