package com.qingyun.shop.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 代理商申请视图对象 tiktok_apply_proxy
 *
 * @author qingyun
 * @date 2021-09-10
 */
@Data
@ApiModel("代理商申请视图对象")
@ExcelIgnoreUnannotated
public class TiktokApplyProxyVo {

	private static final long serialVersionUID = 1L;

	/**
     *  主键id
     */
	@ApiModelProperty("主键id")
	private Long id;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;


	@ApiModelProperty("商户名称")
	private String shopName;

    /**
     * 申请人用户id
     */
	@ExcelProperty(value = "申请人用户id")
	@ApiModelProperty("申请人用户id")
	private Long userId;

    /**
     * 申请人真实姓名
     */
	@ExcelProperty(value = "申请人真实姓名")
	@ApiModelProperty("申请人真实姓名")
	private String realName;

    /**
     * 申请人电话
     */
	@ExcelProperty(value = "申请人电话")
	@ApiModelProperty("申请人电话")
	private String userPhone;

    /**
     * 申请理由
     */
	@ExcelProperty(value = "申请理由")
	@ApiModelProperty("申请理由")
	private String applyReason;

    /**
     * 申请时间
     */
	@ExcelProperty(value = "申请时间")
	@ApiModelProperty("申请时间")
	private Date applyTime;

    /**
     * 流程状态 0审核中 1通过 2未通过
     */
	@ExcelProperty(value = "流程状态 0审核中 1通过 2未通过")
	@ApiModelProperty("流程状态 0审核中 1通过 2未通过")
	private Integer status;

    /**
     * 通过时间
     */
	@ExcelProperty(value = "通过时间")
	@ApiModelProperty("通过时间")
	private Date passTime;

    /**
     * 驳回理由
     */
	@ExcelProperty(value = "驳回理由")
	@ApiModelProperty("驳回理由")
	private String reject;


}
