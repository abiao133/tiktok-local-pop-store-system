package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 资源对象 tiktok_resource
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_resource")
public class TiktokResource implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;

	/**
	 * 商户名称
	 */
	private String shopName;

    /**
     * 资源分组id
     */
    private Long groupId;

    /**
     * 资源类型 0图片 1视频
     */
    private Integer resourceType;

	/**
	 * 横向纵向
	 */
	private Integer format;

	/**
	 * 是否片段
	 */
	private Integer isFrag;

	/**
	 * 播放时长
	 */
	private Double duration;

    /**
     * 资源地址
     */
    private String resourceUrl;

    /**
     * 资源名称
     */
    private String resourceName;


	/**
	 * 状态
	 */
	private Integer status;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    private String remark;

}
