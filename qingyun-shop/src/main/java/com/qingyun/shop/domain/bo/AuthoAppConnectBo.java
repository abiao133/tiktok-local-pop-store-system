package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 第三方用户数据业务对象 autho_app_connect
 *
 * @author qingyun
 * @date 2021-09-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("第三方用户数据业务对象")
public class AuthoAppConnectBo extends BaseEntity {

    /**
     * 0管家 1普通用户
     */
    @ApiModelProperty(value = "0管家 1普通用户")
    private Long id;

    /**
     * 用户表用户id
     */
    @ApiModelProperty(value = "用户表用户id", required = true)
    @NotNull(message = "用户表用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 用户名称（第三方）
     */
    @ApiModelProperty(value = "用户名称（第三方）")
    private String nickName;

    /**
     * 用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等
     */
    @ApiModelProperty(value = "用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等", required = true)
    @NotNull(message = "用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer userType;

    /**
     * 2企业管理员 1普通用户
     */
    @ApiModelProperty(value = "2企业管理员 1普通用户", required = true)
    @NotNull(message = "2企业管理员 1普通用户不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer entryType;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String imageUrl;

    /**
     * 第三方open_id
     */
    @ApiModelProperty(value = "第三方open_id")
    private String openId;

    /**
     * 第三方union_id
     */
    @ApiModelProperty(value = "第三方union_id")
    private String unionId;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
