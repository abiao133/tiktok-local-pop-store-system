package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 商户银行卡信息业务对象 tiktok_bank_card
 *
 * @author qingyun
 * @date 2021-10-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户银行卡信息业务对象")
public class TiktokBankCardBo extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号", required = true)
    @NotBlank(message = "银行卡号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bankNumber;

    /**
     * 开户行
     */
    @ApiModelProperty(value = "开户行", required = true)
    @NotBlank(message = "开户行不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bankDeposit;

    /**
     * 支行名称
     */
    @ApiModelProperty(value = "支行名称", required = true)
    @NotBlank(message = "支行名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String branchName;

    /**
     * 银行卡预留手机号
     */
    @ApiModelProperty(value = "银行卡预留手机号", required = true)
    @NotBlank(message = "银行卡预留手机号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bankPhone;

    /**
     * 所属商户id
     */
    @ApiModelProperty(value = "所属代理商id", required = true)
    @NotNull(message = "所属代理商id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long proxyId;

    /**
     * 银行卡负责人姓名
     */
    @ApiModelProperty(value = "银行卡负责人姓名", required = true)
    @NotBlank(message = "银行卡负责人姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String principalName;

    /**
     * 银行卡负责人手机号
     */
    @ApiModelProperty(value = "银行卡负责人手机号", required = true)
    @NotBlank(message = "银行卡负责人手机号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String principalPhone;

    /**
     * 0正常 1禁用
     */
    @ApiModelProperty(value = "0正常 1禁用", required = true)
    @NotNull(message = "0正常 1禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
