package com.qingyun.shop.domain.bo.apply;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 代理商申请业务对象 tiktok_apply_proxy
 *
 * @author qingyun
 * @date 2021-09-10
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理商申请业务对象")
public class TiktokApplyProxyBo extends BaseEntity {

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private Long id;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 申请人用户id
     */
    @ApiModelProperty(value = "申请人用户id", required = true)
    @NotNull(message = "申请人用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 申请人真实姓名
     */
    @ApiModelProperty(value = "申请人真实姓名", required = true)
    @NotBlank(message = "申请人真实姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String realName;

    /**
     * 申请人电话
     */
    @ApiModelProperty(value = "申请人电话", required = true)
    @NotBlank(message = "申请人电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String userPhone;

    /**
     * 申请理由
     */
    @ApiModelProperty(value = "申请理由", required = true)
    @NotBlank(message = "申请理由不能为空", groups = { AddGroup.class, EditGroup.class })
    private String applyReason;

    /**
     * 申请时间
     */
    @ApiModelProperty(value = "申请时间", required = true)
    @NotNull(message = "申请时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date applyTime;

    /**
     * 流程状态 0审核中 1通过 2未通过
     */
    @ApiModelProperty(value = "流程状态 0审核中 1通过 2未通过", required = true)
    @NotNull(message = "流程状态 0审核中 1通过 2未通过不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 通过时间
     */
    @ApiModelProperty(value = "通过时间", required = true)
    @NotNull(message = "通过时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date passTime;

    /**
     * 驳回理由
     */
    @ApiModelProperty(value = "驳回理由")
    private String reject;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
