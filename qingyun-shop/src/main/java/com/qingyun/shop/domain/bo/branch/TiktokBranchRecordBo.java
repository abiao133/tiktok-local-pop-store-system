package com.qingyun.shop.domain.bo.branch;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;

/**
 * 分销返利记录业务对象 tiktok_branch_record
 *
 * @author qingyun
 * @date 2021-10-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("分销返利记录业务对象")
public class TiktokBranchRecordBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 1代理商给代理商 2 商家给代理商
     */
    @ApiModelProperty(value = "1代理商给代理商 2 商家给代理商", required = true)
    @NotBlank(message = "1代理商给代理商 2 商家给代理商不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subRole;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodName;

    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", required = true)
    @NotNull(message = "商品id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long goodId;

    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额")
    private BigDecimal totalAmount;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", required = true)
    @NotBlank(message = "订单号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String orderNum;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 商户名称
     */
    @ApiModelProperty(value = "商户或者代理商名称")
    private String publicName;

	/**
	 * 代理商id
	 */
	@ApiModelProperty(value = "代理商id")
	private Long proxyId;

	/**
	 * 代理商名称
	 */
	@ApiModelProperty("代理商名称")
	private String proxyName;
    /**
     * 一级代理商获取的金额
     */
    @ApiModelProperty(value = "一级代理商获取的金额")
    private BigDecimal rebateAmount;

    /**
     * 如果role为1 折此字段是代理商id 2此字段为商家id
     */
    @ApiModelProperty(value = "如果role为1 折此字段是代理商id 2此字段为商家id", required = true)
    @NotNull(message = "如果role为1 折此字段是代理商id 2此字段为商家id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long publicId;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
