package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 优惠券活动对象 tiktok_activity
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_activity")
public class TiktokActivity implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 活动标题
     */
    private String title;

    /**
     * 活动描述
     */
    private String description;

    /**
     * 活动内容（富文本
     */
    private String context;

    /**
     * 活动开始时间
     */
    private Date beginDate;

    /**
     * 活动结束时间
     */
    private Date endDate;

    /**
     * 每个人可参与次数：0为不限次
     */
    private Long joinCount;

    /**
     * 活动类型：0扫码领券；1纯推广2；抽奖
     */
    private String actityType;

    /**
     * 延迟发送视频：小时为单位，0为不延迟
     */
    private Long laterSend;

    /**
     * 活动主图
     */
    private String imgUrl;

    /**
     * 活动状态：0待审核；1待上线；2上线中；3已下线
     */
    private Long status;

    /**
     * 活动宣传视频
     */
    private String videoUrl;

    /**
     * 所属商家
     */
    private Long shopId;

	/**
	 * 所属商家名称
	 */
	private String shopName;

    /**
     * 抖音话题
     */
    private String douyinTitle;

    /**
     * 转发渠道：0抖音，1快手，2视频号，3微信朋友圈
     */
    private Integer forwardChannel;

    /**
     * 视频来源：0按资源分组，1按单个文件，为避免重复导致的抖音限流请按分组选择
     */
    private Integer videoSource;


	private Integer isRandFrag;

    /**
     * 视频资源分组数组
     */
    private String resourceGroupId;

    /**
     * 地理位置id
     */
    private String poiId;

    /**
     * 地理位置名称
     */
    private String poiName;

    /**
     * @的openid
     */
    private String atOpenids;

    /**
     * @的nickname
     */
    private String atNicknames;

    /**
     * 小程序id
     */
    private String miniAppId;

    /**
     * 小程序标题
     */
    private String miniAppTitle;

    /**
     * 吊起小程序时的url参数
     */
    private String miniAppUrl;

    /**
     * 视频标题
     */
    private String videoTitle;



	private Integer isDoc;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 参与后跳转类型：0优惠券页面1商家抖音主页
     */
    private String redirectType;


	/**
	 * 是否需要关注商家抖音才能参与活动 1是0否
	 */
	private Integer isFocus;

}
