package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("代理商申请列表视图对象")
public class TiktokApplyProxyListVo {

	/**
	 *  主键id
	 */
	@ApiModelProperty("主键id")
	private Long id;


	@ApiModelProperty("商户名称")
	private String shopName;

	/**
	 * 申请人真实姓名
	 */
	@ApiModelProperty("申请人真实姓名")
	private String realName;

	/**
	 * 申请人电话
	 */
	@ApiModelProperty("申请人电话")
	private String userPhone;

	/**
	 * 申请理由
	 */
	@ApiModelProperty("申请理由")
	private String applyReason;

	/**
	 * 申请时间
	 */
	@ApiModelProperty("申请时间")
	private Date applyTime;

	/**
	 * 流程状态 0审核中 1通过 2未通过
	 */
	@ApiModelProperty("流程状态 0审核中 1通过 2未通过")
	private Integer status;

	@ApiModelProperty("驳回理由")
	private String reject;

	/**
	 * 通过时间
	 */
	@ApiModelProperty("通过时间")
	private Date passTime;
}
