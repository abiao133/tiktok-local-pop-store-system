package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("代理商银行卡信息Excel导出对象")
@ExcelIgnoreUnannotated
public class TiktokBankCardExcelVo {

    @ExcelProperty(value = "代理商名称", index = 0)
    private String proxyName;

    @ExcelProperty(value = "银行卡号", index = 1)
    private String bankNumber;

    @ExcelProperty(value = "开户行", index = 2)
    private String bankDeposit;

    @ExcelProperty(value = "支行名称", index = 3)
    private String branchName;

    @ExcelProperty(value = "银行卡预留手机号", index = 4)
    private String bankPhone;

    @ExcelProperty(value = "真实姓名", index = 5)
    private String realName;

    @ExcelProperty(value = "身份证号", index = 6)
    private String idCard;

    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class, index = 7)
    @ExcelDictFormat(readConverterExp = "0=正常,1=禁用")
    private Integer status;

    @ExcelProperty(value = "是否被删除", converter = ExcelDictConvert.class, index = 8)
    @ExcelDictFormat(readConverterExp = "0=否,2=是")
    private Integer delFlag;
}
