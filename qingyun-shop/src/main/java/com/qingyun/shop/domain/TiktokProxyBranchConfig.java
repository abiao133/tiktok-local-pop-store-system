package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 代理商返利配置对象 tiktok_proxy_branch_config
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_proxy_branch_config")
public class TiktokProxyBranchConfig implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 代理商id
     */
    private Long proxyId;

    /**
     * 代理商pc充值单价
     */
    private BigDecimal proxyPrice;

    /**
     * 一级返利比例
     */
    private BigDecimal oneProxyRatio;

    /**
     * 二级返利比例
     */
    private BigDecimal towProxyRatio;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
