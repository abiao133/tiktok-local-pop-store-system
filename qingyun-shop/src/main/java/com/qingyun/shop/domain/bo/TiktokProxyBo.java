package com.qingyun.shop.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 代理组织业务对象 tiktok_proxy
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理组织业务对象")
public class TiktokProxyBo extends BaseEntity {

    /**
     * 代理商id
     */
    @ApiModelProperty(value = "代理商id")
    private Long id;

    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    @NotNull(message = "父id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parentId;

    /**
     * 祖级列表
     */
    @ApiModelProperty(value = "祖级列表")
    private String ancestors;

    /**
     * 代理商名称
     */
    @ApiModelProperty(value = "代理商名称",required = true)
    @NotBlank(message = "代理商名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 显示顺序
     */
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;

    /**
     * 负责人
     */
    @ApiModelProperty(value = "负责人",required = true)
    @NotBlank(message = "负责人不能为空", groups = { AddGroup.class, EditGroup.class })
    private String leader;

    /**
     * 代理商手机号码
     */
    @ApiModelProperty(value = "代理商手机号码",required = true)
    @NotBlank(message = "代理商手机号码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String phone;

    /**
     * 代理商邮箱
     */
    @ApiModelProperty(value = "代理商邮箱")
    private String email;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private Long videoCount;

    /**
     * 代理商状态 0启用 1禁用
     */
    @ApiModelProperty(value = "代理商状态 0启用 1禁用",required = true)
    @NotNull(message = "代理商状态 0启用 1禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Integer delFlag;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;


    @ApiModelProperty(value = "商户头像")
	@NotNull(message = "头像不能为空", groups = { AddGroup.class })
    private String pic;

}
