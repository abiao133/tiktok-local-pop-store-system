package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 代理给商户转播放量记录对象 tiktok_transfer
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_transfer")
public class TiktokTransfer implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 代理商id
     */
    private Long proxyId;

    /**
     * 商家id
     */
    private Long shopId;

    /**
     * 消耗播放量
     */
    private String videoCount;

    /**
     * 代理商名称
     */
    private String proxyName;

    /**
     * 商家名称
     */
    private String shopName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
