package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 订单详情视图对象 tiktok_order_item
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("订单详情视图对象")
@ExcelIgnoreUnannotated
public class TiktokOrderItemVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 订单编码
     */
	@ExcelProperty(value = "订单编码")
	@ApiModelProperty("订单编码")
	private String orderNum;

    /**
     * 商品
     */
	@ExcelProperty(value = "商品")
	@ApiModelProperty("商品")
	private Long goodsId;

    /**
     * 商品名称
     */
	@ExcelProperty(value = "商品名称")
	@ApiModelProperty("商品名称")
	private String goodsName;

    /**
     * 购买数量
     */
	@ExcelProperty(value = "购买数量")
	@ApiModelProperty("购买数量")
	private Long quantity;

    /**
     * 金额
     */
	@ExcelProperty(value = "金额")
	@ApiModelProperty("金额")
	private BigDecimal amount;


}
