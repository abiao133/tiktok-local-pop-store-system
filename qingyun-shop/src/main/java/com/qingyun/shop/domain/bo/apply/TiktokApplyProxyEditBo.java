package com.qingyun.shop.domain.bo.apply;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("代理商申请修改对象")
public class TiktokApplyProxyEditBo {

	/**
	 * 主键id
	 */
	@ApiModelProperty(value = "主键id")
	private Long id;

	/**
	 * 流程状态 0审核中 1通过 2未通过
	 */
	@ApiModelProperty(value = "流程状态 0审核中 1通过 2未通过", required = true)
	@NotNull(message = "流程状态 0审核中 1通过 2未通过不能为空", groups = { AddGroup.class, EditGroup.class })
	private Integer status;

	/**
	 * 驳回理由
	 */
	@ApiModelProperty(value = "驳回理由(未通过才需要填写)")
	private String reject;
}
