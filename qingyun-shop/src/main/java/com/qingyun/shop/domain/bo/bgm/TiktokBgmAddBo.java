package com.qingyun.shop.domain.bo.bgm;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("bgm添加对象")
public class TiktokBgmAddBo {

	/**
	 * 视频标题
	 */
	@ApiModelProperty(value = "背景音乐标题", required = true)
	@NotBlank(message = "背景音乐不能为空", groups = { AddGroup.class, EditGroup.class })
	private String title;

	/**
	 * 视频地址
	 */
	@ApiModelProperty(value = "背景音乐地址", required = true)
	@NotBlank(message = "背景音乐地址不能为空", groups = { AddGroup.class, EditGroup.class })
	private String audioUrl;

	/**
	 * 状态 0 可用 1禁用
	 */
	@ApiModelProperty(value = "状态 0 可用 1禁用", required = true)
	@NotNull(message = "状态不能为空", groups = { AddGroup.class, EditGroup.class })
	private Integer status;

	/**
	 * 是否全局bgm 0是 1否
	 */
	@ApiModelProperty(value = "是否全局bgm 0是 1否", required = true)
	private Integer global;


	@ApiModelProperty(value = "商户id 超级管理员必传字段")
	private Long shopId;

	@ApiModelProperty("标签tag id数组")
	private List<Long> tags;
}
