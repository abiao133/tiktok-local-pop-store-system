package com.qingyun.shop.domain.bo.goods;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;

/**
 * 套餐业务对象 tiktok_goods
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("套餐业务对象")
public class TiktokGoodsBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 套餐名称
     */
    @ApiModelProperty(value = "套餐名称", required = true)
    @NotBlank(message = "套餐名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 套餐发送数量
     */
    @ApiModelProperty(value = "套餐发送数量", required = true)
    @NotNull(message = "套餐发送数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long voideCount;

    /**
     * 现价
     */
    @ApiModelProperty(value = "现价", required = true)
    @NotNull(message = "现价不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal price;

    /**
     * 原价
     */
    @ApiModelProperty(value = "原价")
    private BigDecimal marketPrice;

    /**
     * 代理商价格
     */
    @ApiModelProperty(value = "代理商价格")
    private BigDecimal proxyPrice;

    /**
     * 状态 0上架 1下架
     */
    @ApiModelProperty(value = "状态 0上架 1下架", required = true)
    @NotNull(message = "状态 0上架 1下架不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
