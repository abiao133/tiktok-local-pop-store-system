package com.qingyun.shop.domain.bo.shop;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("商家新增对象")
public class TiktokShopAddBo {

	/**
	 * 商家名称
	 */
	@ApiModelProperty(value = "商家名称", required = true)
	@NotBlank(message = "商家名称不能为空", groups = { AddGroup.class, EditGroup.class })
	private String name;

	/**
	 * 商家头像
	 */
	@ApiModelProperty(value = "商家头像url", required = true)
	@NotBlank(message = "商家头像不能为空")
	private String pic;

	/**
	 * 营业时间
	 */
	@ApiModelProperty(value = "营业时间")
	private String businessTime;

	/**
	 * 负责人
	 */
	@ApiModelProperty(value = "负责人")
	private String leader;

	/**
	 * 显示顺序
	 */
	@ApiModelProperty(value = "显示顺序 默认为1")
	private Integer orderNum;

	/**
	 * 商户手机号码
	 */
	@ApiModelProperty(value = "商户手机号码", required = true)
	@NotBlank(message = "商户手机号码不能为空", groups = { AddGroup.class, EditGroup.class })
	private String phone;

	/**
	 * 商户邮箱
	 */
	@ApiModelProperty(value = "商户邮箱", required = true)
	@Email(message = "请输入正确的邮箱格式")
	private String email;


	@ApiModelProperty("热度")
	private String heat;
	/**
	 * 商户状态 0启用 1禁用
	 */
	@ApiModelProperty(value = "商户状态 0启用 1禁用", required = true)
	@NotNull(message = "商户状态 0启用 1禁用不能为空", groups = { AddGroup.class, EditGroup.class })
	private Integer status;
}
