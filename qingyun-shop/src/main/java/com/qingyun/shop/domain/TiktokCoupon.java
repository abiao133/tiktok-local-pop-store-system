package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 优惠券对象 tiktok_coupon
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_coupon")
public class TiktokCoupon implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 折扣券标题
     */
    private String title;

    /**
     * 折扣
     */
    private Double discount;

    /**
     * 有效期开始日期
     */
    private Date startTime;

    /**
     * 有效期结束日期
     */
    private Date endTime;

    /**
     * 基于领取时间的有效天数days。
     */
    private Integer days;

    /**
     * 每个用户领券上限，如不填，则默认为1
     */
    private Integer collectionTimes;

    /**
     * 封面图片
     */
    private String coverImg;

    /**
     * 封面简介
     */
    private String coverInfo;

    /**
     * 使用须知
     */
    private String useNotice;

    /**
     * 减免金额
     */
    private BigDecimal amount;

    /**
     * 最低消费：0代表不限制
     */
    private BigDecimal minCost;

    /**
     * 0：代金券；1：折扣券；2：兑换券
     */
    private Integer type;

    /**
     * 数量：99999999为无限量
     */
    private Long quantity;

    /**
     * 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     */
    private Integer goodsType;

    /**
     * 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     */
    private String goodsValue;

    /**
     * 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     */
    private Integer timeType;

    /**
     * 优惠券状态，0待上架；1已上架; 2 已过期; 3已下架
     */
    private Integer status;

}
