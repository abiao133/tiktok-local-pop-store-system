package com.qingyun.shop.domain.bo.coupon;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 用户优惠卷领取业务对象 tiktok_coupon_draw
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户优惠卷领取业务对象")
public class TiktokCouponDrawBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 优惠卷id
     */
    @ApiModelProperty(value = "优惠卷id", required = true)
    @NotNull(message = "优惠卷id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long couponId;

    /**
     * 活动id
     */
    @ApiModelProperty(value = "活动id", required = true)
    @NotNull(message = "活动id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activityId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 优惠卷code
     */
    @ApiModelProperty(value = "优惠卷code")
    private String couponCode;

    /**
     * 获取途径
     */
    @ApiModelProperty(value = "获取途径", required = true)
    @NotNull(message = "获取途径不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer getType;

    /**
     * 优惠卷有效开始时间
     */
    @ApiModelProperty(value = "优惠卷有效开始时间", required = true)
    @NotNull(message = "优惠卷有效开始时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date beginDate;

    /**
     * 优惠卷有效结束时间
     */
    @ApiModelProperty(value = "优惠卷有效结束时间", required = true)
    @NotNull(message = "优惠卷有效结束时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date endDate;

    /**
     * 使用状态 0->未使用；1->已使用；2->已过期
     */
    @ApiModelProperty(value = "使用状态 0->未使用；1->已使用；2->已过期", required = true)
    @NotNull(message = "使用状态 0->未使用；1->已使用；2->已过期不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer useStatus;

    /**
     * 使用时间
     */
    @ApiModelProperty(value = "使用时间")
    private Date useTime;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;




}
