package com.qingyun.shop.domain.bo.tag;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TiktokTagEditBo {

    /**
     * 标签id
     */
    @ApiModelProperty(value = "标签id", required = true)
    @NotNull(message = "标签id不能为空")
    private Long tId;

    @ApiModelProperty(value = "标签名", required = true)
    @NotBlank(message = "标签名不能为空", groups = {AddGroup.class, EditGroup.class})
    private String tagName;

    @ApiModelProperty(value = "所属商户 平台帮商户创建才要传")
    private Long shopId;

    @ApiModelProperty(value = "是否启用 0 未启用 1启用", required = true)
    @NotNull(message = "是否启用 0 未启用 1启用不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer status;

    @ApiModelProperty(value = "标签类型 0 音乐资源标签 1 视频资源标签", required = true)
    @NotNull(message = "标签类型 0 音乐资源标签 1 视频资源标签不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer tagType;
}
