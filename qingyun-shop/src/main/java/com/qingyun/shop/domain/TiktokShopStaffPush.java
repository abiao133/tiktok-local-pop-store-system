package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 员工宣传记录对象 tiktok_shop_staff_push
 *
 * @author qingyun
 * @date 2021-10-30
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_shop_staff_push")
public class TiktokShopStaffPush implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 活动 id
     */
    private Long activityId;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 商户员工id
     */
    private Long shopUserId;

    /**
     * 活动类型0领劵1存推广2抽奖
     */
    private Integer activityType;

    /**
     * 领劵记录id
     */
    private Long couponDrawId;

	/**
	 * 参与活动的抖音用户id
	 */
	private Long tiktokUserId;
    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
