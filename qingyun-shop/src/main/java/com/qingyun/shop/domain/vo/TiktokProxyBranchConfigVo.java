package com.qingyun.shop.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 代理商返利配置视图对象 tiktok_proxy_branch_config
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@ApiModel("代理商返利配置视图对象")
@ExcelIgnoreUnannotated
public class TiktokProxyBranchConfigVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 代理商id
     */
	@ExcelProperty(value = "代理商id")
	@ApiModelProperty("代理商id")
	private Long proxyId;

    /**
     * 代理商pc充值单价
     */
	@ExcelProperty(value = "代理商pc充值单价")
	@ApiModelProperty("代理商pc充值单价")
	private BigDecimal proxyPrice;

    /**
     * 一级返利比例
     */
	@ExcelProperty(value = "一级返利比例")
	@ApiModelProperty("一级返利比例")
	private BigDecimal oneProxyRatio;

    /**
     * 二级返利比例
     */
	@ExcelProperty(value = "二级返利比例")
	@ApiModelProperty("二级返利比例")
	private BigDecimal towProxyRatio;


}
