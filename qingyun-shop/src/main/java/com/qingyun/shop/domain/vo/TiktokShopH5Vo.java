package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Classname TiktokShopH5Vo
 * @Author dyh
 * @Date 2021/9/24 22:08
 */
@ApiModel("商家信息")
@Data
public class TiktokShopH5Vo {

	@ApiModelProperty("头像")
	private String pic;

	@ApiModelProperty("名称")
	private String name;

	@ApiModelProperty("营业时间")
	private String businessTime;

	@ApiModelProperty("热度")
	private String heat;

	@ApiModelProperty("收集号码")
	private String phone;

	@ApiModelProperty("负责人")
	private String leader;
}
