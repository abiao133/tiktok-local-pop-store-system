package com.qingyun.shop.domain.bo.goods;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("套餐新增对象")
public class TiktokGoodsAddBo {

	/**
	 * 套餐名称
	 */
	@ApiModelProperty(value = "套餐名称", required = true)
	@NotBlank(message = "套餐名称不能为空", groups = { AddGroup.class, EditGroup.class })
	private String name;

	/**
	 * 套餐发送数量
	 */
	@ApiModelProperty(value = "套餐发送数量", required = true)
	@NotNull(message = "套餐发送数量不能为空", groups = { AddGroup.class, EditGroup.class })
	private Long voideCount;


	/**
	 * 赠送数量
	 */
	@ApiModelProperty(value = "赠送数量", required = true)
	@NotNull(message = "套餐发送数量不能为空", groups = { AddGroup.class, EditGroup.class })
	private Long voideGive;

	/**
	 * 现价
	 */
	@ApiModelProperty(value = "现价", required = true)
	@NotNull(message = "现价不能为空", groups = { AddGroup.class, EditGroup.class })
	private BigDecimal price;

	/**
	 * 原价
	 */
	@ApiModelProperty(value = "原价")
	private BigDecimal marketPrice;

	/**
	 * 代理商价格
	 */
	@ApiModelProperty(value = "代理商价格")
	private BigDecimal proxyPrice;

	/**
	 * 状态 0上架 1下架
	 */
	@ApiModelProperty(value = "状态 0上架 1下架", required = true)
	@NotNull(message = "状态 0上架 1下架不能为空", groups = { AddGroup.class, EditGroup.class })
	private Integer status;

	@ApiModelProperty("备注")
	private String remark;
}
