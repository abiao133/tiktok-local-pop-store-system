package com.qingyun.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 用户与代理关联表对象 tiktok_user_proxy
 *
 * @author qingyun
 * @date 2021-09-09
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_user_proxy")
public class TiktokUserProxy implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 代理id
     */
    private Long proxyId;

}
