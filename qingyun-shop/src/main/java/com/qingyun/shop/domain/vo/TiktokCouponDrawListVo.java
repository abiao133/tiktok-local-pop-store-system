package com.qingyun.shop.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("用户优惠卷领取列表视图对象")
public class TiktokCouponDrawListVo {


    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "优惠卷id")
    private Long couponId;

    @ApiModelProperty("优惠券标题")
    private String couponName;

    @ApiModelProperty(value = "活动id")
    private Long activityId;

    @ApiModelProperty("活动标题")
    private String activityName;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty("抖音用户名称")
    private String userName;

    @ApiModelProperty(value = "获取途径")
    private Integer getType;

    @ApiModelProperty(value = "使用状态 0->未使用；1->已使用；2->已过期")
    private Integer useStatus;

    @ApiModelProperty("优惠卷code")
    private String couponCode;

    @ApiModelProperty(value = "使用时间")
    private Date useTime;
}
