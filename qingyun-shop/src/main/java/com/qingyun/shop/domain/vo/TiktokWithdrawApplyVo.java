package com.qingyun.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 商户提现申请视图对象 tiktok_withdraw_apply
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Data
@ApiModel("代理商提现申请视图对象")
@ExcelIgnoreUnannotated
public class TiktokWithdrawApplyVo {

	private static final long serialVersionUID = 1L;

	/**
     *  id
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 关联的银行卡id
     */
	@ExcelProperty(value = "关联的银行卡id")
	@ApiModelProperty("关联的银行卡id")
	private Long bankId;

    /**
     * 所属商户id
     */
	@ExcelProperty(value = "所属代理商id")
	@ApiModelProperty("所属代理商id")
	private Long proxyId;

    /**
     * 提现金额
     */
	@ExcelProperty(value = "提现金额")
	@ApiModelProperty("提现金额")
	private BigDecimal withdrawAmount;

    /**
     * 提现完成时间
     */
	@ExcelProperty(value = "提现完成时间")
	@ApiModelProperty("提现完成时间")
	private Date finishTime;

    /**
     * 状态 0受理中 1完成 2驳回
     */
	@ExcelProperty(value = "状态 0受理中 1完成 2驳回")
	@ApiModelProperty("状态 0受理中 1完成 2驳回")
	private Integer status;

    /**
     * 提现人姓名
     */
	@ExcelProperty(value = "提现人姓名")
	@ApiModelProperty("提现人姓名")
	private String userName;

    /**
     * 提现人id
     */
	@ExcelProperty(value = "提现人id")
	@ApiModelProperty("提现人id")
	private Long userId;

    /**
     * 驳回理由
     */
	@ExcelProperty(value = "驳回理由")
	@ApiModelProperty("驳回理由")
	private String rejectReason;

    /**
     * 备注
     */
	@ExcelProperty(value = "备注")
	@ApiModelProperty("备注")
	private String remark;

	@ApiModelProperty("银行卡信息")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private TiktokBankCardVo bankCard;
}
