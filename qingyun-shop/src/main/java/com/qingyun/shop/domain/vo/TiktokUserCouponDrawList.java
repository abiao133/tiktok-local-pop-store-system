package com.qingyun.shop.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.qingyun.shop.domain.TiktokCoupon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TiktokUserCouponDrawList {

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("优惠卷id")
    private Long couponId;

    @ApiModelProperty("活动id")
    private Long activityId;

    @ApiModelProperty("优惠卷code")
    private String couponCode;

    @ApiModelProperty("获取途径")
    private Integer getType;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("优惠卷有效开始时间")
    private Date beginDate;

    @ApiModelProperty("优惠卷有效结束时间")
    private Date endDate;


    @ApiModelProperty("使用状态 0->未使用；1->已使用；2->已过期")
    private Integer useStatus;

    @ApiModelProperty("使用时间")
    private Date useTime;

    @ApiModelProperty("商户名")
    private String shopName;

    @ApiModelProperty("活动名")
    private String activityName;

    @TableField(exist = false)
    private TiktokCoupon coupon;
}
