package com.qingyun.video.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 混剪结果记录表
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_video_merge")
public class TiktokVideoMerge implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 任务id
     */
    private Long taskId;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 视频或图片id列表
     */
    private String resourceIds;

	/**
	 * 视频资源url
	 */
	private String resourceUrl;

	/**
	 * 视频播放地址
	 */
	private String playUrl;

    /**
     * bgmid
     */
    private String resourceBgm;

    /**
     * 混剪状态，0成功 1失败
     */
    private Integer status;
	/**
	 * 发送状态：0=未发送；1=发送
	 */
	private Integer sendStatus;

    /**
     * $column.columnComment
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * $column.columnComment
     */
    private String errorMsg;

}
