package com.qingyun.video.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 【请填写功能名称】业务对象 tiktok_video_merge
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("【请填写功能名称】业务对象")
public class TiktokVideoMergeBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private Long id;

    /**
     * 任务id
     */
    @ApiModelProperty(value = "任务id", required = true)
    @NotNull(message = "任务id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long taskId;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 视频或图片id列表
     */
    @ApiModelProperty(value = "视频或图片id列表")
    private String resourceIds;

    /**
     * bgmid
     */
    @ApiModelProperty(value = "bgmid", required = true)
    @NotBlank(message = "bgmid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String resourceBgm;

    /**
     * 混剪状态，0成功 1失败
     */
    @ApiModelProperty(value = "混剪状态，0成功 1失败", required = true)
    @NotNull(message = "混剪状态，0成功 1失败不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment")
    private String errorMsg;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
