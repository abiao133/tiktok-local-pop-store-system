package com.qingyun.video.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TiktokVideoMergeDetailVo {
    /**
     * $pkColumn.columnComment
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 混剪状态，0成功 1失败
     */
    @ApiModelProperty("混剪状态，0成功 1失败")
    private Integer status;

    /**
     * $column.columnComment
     */
    @ApiModelProperty("错误消息")
    private String errorMsg;

    @ApiModelProperty("商户名")
    private String shopName;

//    @ApiModelProperty("任务名")
//    private String jobName;
	/**
	 *
	 */
	@ApiModelProperty("资源地址")
	private String resourceUrl;

    @ApiModelProperty("bgm")
    private String bgmName;


    private Date createTime;

    @ApiModelProperty("视频或图片id列表（英文逗号隔开）")
    private String resourceIds;

	/***
	 * 云存储的播放地址
	 */
	private String playUrl;
}
