package com.qingyun.video.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("视频合成记录列表视图对象")
public class TiktokVideoMergeListVo {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("资源地址")
    private String resourceUrl;

	@ApiModelProperty("播放地址")
	private String playUrl;

    @ApiModelProperty("商户名称")
    private String shopName;

    @ApiModelProperty("bmg名称")
    private String bgmName;

    @ApiModelProperty("混剪状态，0成功 1失败")
    private Integer status;

    @ApiModelProperty("混剪失败错误原因")
    private String errorMsg;

    @ApiModelProperty("合成时间")
    private Date createTime;
}
