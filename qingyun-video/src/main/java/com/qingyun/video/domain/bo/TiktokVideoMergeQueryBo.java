package com.qingyun.video.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("视频合成记录查询")
public class TiktokVideoMergeQueryBo {

    @ApiModelProperty("商户id")
    private Long shopId;

    /**
     * 混剪状态，0成功 1失败
     */
    @ApiModelProperty(value = "混剪状态，0成功 1失败")
    private Integer status;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;
}
