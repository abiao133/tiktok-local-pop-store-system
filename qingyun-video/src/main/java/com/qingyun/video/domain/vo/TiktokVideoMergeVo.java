package com.qingyun.video.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 【请填写功能名称】视图对象 tiktok_video_merge
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("【请填写功能名称】视图对象")
@ExcelIgnoreUnannotated
public class TiktokVideoMergeVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 任务id
     */
	@ExcelProperty(value = "任务id")
	@ApiModelProperty("任务id")
	private Long taskId;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

	/**
	 *
	 */
	@ExcelProperty(value="视频资源地址")
	@ApiModelProperty("视频资源地址")
	private String resourceUrl;

    /**
     * 视频或图片id列表
     */
	@ExcelProperty(value = "视频或图片id列表")
	@ApiModelProperty("视频或图片id列表")
	private String resourceIds;

    /**
     * bgmid
     */
	@ExcelProperty(value = "bgmid")
	@ApiModelProperty("bgmid")
	private String resourceBgm;

    /**
     * 混剪状态，0成功 1失败
     */
	@ExcelProperty(value = "混剪状态，0成功 1失败")
	@ApiModelProperty("混剪状态，0成功 1失败")
	private Integer status;

    /**
     * $column.columnComment
     */
	@ApiModelProperty("$column.columnComment")
	private String errorMsg;


}
