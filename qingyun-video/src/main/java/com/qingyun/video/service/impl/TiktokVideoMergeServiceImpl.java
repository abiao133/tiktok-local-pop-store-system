package com.qingyun.video.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.config.QingYunConfig;
import com.qingyun.common.constant.Constants;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.common.utils.StringUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.video.domain.TiktokVideoMerge;
import com.qingyun.video.domain.bo.TiktokVideoMergeBo;
import com.qingyun.video.domain.bo.TiktokVideoMergeQueryBo;
import com.qingyun.video.domain.vo.TiktokVideoMergeDetailVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeListVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeVo;
import com.qingyun.video.mapper.TiktokVideoMergeMapper;
import com.qingyun.video.service.ITiktokVideoMergeService;

import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokVideoMergeServiceImpl extends ServicePlusImpl<TiktokVideoMergeMapper, TiktokVideoMerge, TiktokVideoMergeVo> implements ITiktokVideoMergeService {

    @Override
    public TiktokVideoMergeDetailVo queryById(Long id){
        return baseMapper.selectTiktokVideoMergeDetail(id);
    }

    @Override
    public TableDataInfo<TiktokVideoMergeListVo> queryPageList(TiktokVideoMergeQueryBo bo) {
		if (!SecurityUtils.isManager()) {
			bo.setShopId(SecurityUtils.getShop().getId());
		}
        Page<TiktokVideoMergeListVo> result = baseMapper.selectTiktokVideoMergeList(PageUtils.buildPage(), bo);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokVideoMergeVo> queryList(TiktokVideoMergeQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokVideoMerge> buildQueryWrapper(TiktokVideoMergeQueryBo bo) {
        LambdaQueryWrapper<TiktokVideoMerge> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getStatus() != null, TiktokVideoMerge::getStatus, bo.getStatus());
        lqw.eq(bo.getShopId()!=null, TiktokVideoMerge::getShopId,bo.getShopId());
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokVideoMergeBo bo) {
        TiktokVideoMerge add = BeanUtil.toBean(bo, TiktokVideoMerge.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokVideoMergeBo bo) {
        TiktokVideoMerge update = BeanUtil.toBean(bo, TiktokVideoMerge.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokVideoMerge entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
		for (TiktokVideoMerge videoMerge : listByIds(ids)) {
			File file = new File(videoMerge.getResourceUrl());
			if (file.exists()) {
				file.delete();
			}
		}
        return removeByIds(ids);
    }

	@Override
	public TiktokVideoMerge selectRandomVideo(Long shopId) {
		return baseMapper.selectRandomVideo( shopId);
	}

}
