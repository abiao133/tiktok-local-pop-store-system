package com.qingyun.video.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.video.domain.TiktokVideoMerge;
import com.qingyun.video.domain.bo.TiktokVideoMergeBo;
import com.qingyun.video.domain.bo.TiktokVideoMergeQueryBo;
import com.qingyun.video.domain.vo.TiktokVideoMergeDetailVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeListVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeVo;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokVideoMergeService extends IServicePlus<TiktokVideoMerge, TiktokVideoMergeVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokVideoMergeDetailVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokVideoMergeListVo> queryPageList(TiktokVideoMergeQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokVideoMergeVo> queryList(TiktokVideoMergeQueryBo bo);

	/**
	 * 根据新增业务对象插入【请填写功能名称】
	 * @param bo 【请填写功能名称】新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokVideoMergeBo bo);

	/**
	 * 根据编辑业务对象修改【请填写功能名称】
	 * @param bo 【请填写功能名称】编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokVideoMergeBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 获取一条可发送的随机视频
	 * @param shopId
	 * @return
	 */
	TiktokVideoMerge selectRandomVideo(Long shopId);
}
