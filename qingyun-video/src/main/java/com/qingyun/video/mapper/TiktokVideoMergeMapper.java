package com.qingyun.video.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.video.domain.TiktokVideoMerge;
import com.qingyun.video.domain.bo.TiktokVideoMergeQueryBo;
import com.qingyun.video.domain.vo.TiktokVideoMergeDetailVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokVideoMergeMapper extends BaseMapperPlus<TiktokVideoMerge> {

    TiktokVideoMergeDetailVo selectTiktokVideoMergeDetail(Long id);

    Page<TiktokVideoMergeListVo> selectTiktokVideoMergeList(@Param("page") Page<Object> buildPage, @Param("query") TiktokVideoMergeQueryBo bo);

	TiktokVideoMerge selectRandomVideo(Long shopId);
}
