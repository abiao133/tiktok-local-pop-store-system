package com.qingyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动程序
 *
 * @author qingyun
 */

@SpringBootApplication
public class QingYunApplication
{
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(QingYunApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ QingYun-Vue-Plus启动成功  ლ(´ڡ`ლ)ﾞ");
    }

}
