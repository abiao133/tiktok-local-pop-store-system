package com.qingyun.web.controller.wxapi;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.redis.RedisCache;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokCoupon;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.service.ITiktokCouponDrawService;
import com.qingyun.shop.service.ITiktokCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Classname ShopCheckController
 * @Author dyh
 * @Date 2021/9/22 10:24
 */
@Api(tags = "商家核销二维码")
@RestController
@RequestMapping("/miniapp/check")
@AllArgsConstructor
public class ShopCheckController {

	private ITiktokCouponDrawService couponDrawService;

	private ITiktokCouponService couponService;

	private RedisCache redisCache;

	@ApiOperation("商家核销用户二维码")
	@GetMapping("check")
	public AjaxResult checkQrcode(String qrCode){
		TiktokShopVo shop = SecurityUtils.getAppLoginUser().getShop();
		TiktokCouponDraw one = couponDrawService.lambdaQuery().eq(TiktokCouponDraw::getCouponCode, qrCode).eq(TiktokCouponDraw::getUseStatus, 0).one();
		if (ObjectUtil.isNull(one)) {
			return AjaxResult.error("优惠卷已使用或不存在");
		}
		TiktokCoupon coupon = couponService.getById(one.getCouponId());
		if (!coupon.getShopId().equals(shop.getId())) {
			return AjaxResult.error("只能核验本店的优惠卷");
		}
		one.setUseStatus(1);
		one.setUseTime(new Date());
		couponDrawService.updateById(one);
		redisCache.setCacheObject("use-couponDraw-" + qrCode,"success",10, TimeUnit.MINUTES);
		return AjaxResult.success("核验成功");
	}


	@ApiOperation("获取剩余优惠卷数量和已经核销数量 今日核销 昨日核销")
	@GetMapping("getCouponAndCheckCounts")
	public AjaxResult getCouponAndCheckCounts(){
		Long id = SecurityUtils.getAppLoginUser().getShop().getId();
		List<TiktokCoupon> list = couponService.lambdaQuery().select(TiktokCoupon::getQuantity).eq(TiktokCoupon::getShopId, id).in(TiktokCoupon::getStatus,0,1).list();
		long sum = list.stream().mapToLong(k -> k.getQuantity()).sum();
		Integer checkCount = couponService.selectCheckCount(id);
		Map<String, Object> map = new HashMap<>();
		map.put("couponCount",sum);
		map.put("checkCount",checkCount);
		return AjaxResult.success(map);
	}

	@ApiOperation("获取核销优惠卷列表 分页")
	@GetMapping("/getCheckCouponList")
	public TableDataInfo<List<Map<String, Object>>> getCheckCouponList(Integer pageNum,Integer pageSize){
		Long id = SecurityUtils.getAppLoginUser().getShop().getId();
		Page<List<Map<String, Object>>> listPage = couponService.selectCheckDarwList(id);
		TableDataInfo<List<Map<String, Object>>> listTableDataInfo = PageUtils.buildDataInfo(listPage);
		return listTableDataInfo;
	}

}
