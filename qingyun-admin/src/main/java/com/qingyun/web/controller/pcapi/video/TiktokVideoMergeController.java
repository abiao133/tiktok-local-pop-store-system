package com.qingyun.web.controller.pcapi.video;

import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.config.QingYunConfig;
import com.qingyun.common.constant.Constants;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.quartz.task.VideoSendService;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.TiktokBgm;
import com.qingyun.shop.domain.TiktokResource;
import com.qingyun.shop.domain.TiktokVoice;
import com.qingyun.shop.mapper.TiktokTagToVoiceMapper;
import com.qingyun.shop.service.ITiktokBgmService;
import com.qingyun.shop.service.ITiktokVoiceService;
import com.qingyun.video.domain.bo.TiktokVideoMergeQueryBo;
import com.qingyun.video.domain.vo.TiktokVideoMergeDetailVo;
import com.qingyun.video.domain.vo.TiktokVideoMergeListVo;
import com.qingyun.video.service.ITiktokVideoMergeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 混剪记录
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Api(value = "混剪记录", tags = {"^混剪记录管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/video/merge")
public class   TiktokVideoMergeController extends BaseController {

    private final ITiktokVideoMergeService iTiktokVideoMergeService;

	private final VideoSendService videoSendService;

	/**
	 * 混剪记录列表
	 */
	@ApiOperation("^查询视频合成记录列表 video:merge:list")
	@PreAuthorize("@ss.hasPermi('video:merge:list')")
	@GetMapping("/list")
	public TableDataInfo<TiktokVideoMergeListVo> list(@Validated TiktokVideoMergeQueryBo bo) {
		return iTiktokVideoMergeService.queryPageList(bo);
	}


    /**
     * 添加混剪任务
     */
    @ApiOperation("^添加混剪任务 video:merge:list")
    @PreAuthorize("@ss.hasPermi('video:merge:list')")
    @GetMapping("/add")
    public AjaxResult add(Long shopId,Integer quantity,Integer global) {
		try {
			videoSendService.asyncAddMergeTask(shopId,global,quantity);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return AjaxResult.success("混剪任务添加成功，稍后请在列表中查看混剪结果");
    }



    /**
     * 获取混剪记录详细信息
     */
    @ApiOperation("^获取 视频合成列表详细信息 video:merge:query")
    @PreAuthorize("@ss.hasPermi('video:merge:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokVideoMergeDetailVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokVideoMergeService.queryById(id));
    }



    @ApiOperation("删除 视频合成列表 video:merge:remove")
    @PreAuthorize("@ss.hasPermi('video:merge:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iTiktokVideoMergeService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }



}
