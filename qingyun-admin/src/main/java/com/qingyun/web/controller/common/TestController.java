package com.qingyun.web.controller.common;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.quartz.task.VideoSendService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

/**
 * @Classname TestController
 * @Author dyh
 * @Date 2021/9/17 15:48
 */
@RestController
@RequestMapping("/test")
public class TestController {

	private VideoSendService sendService;

	@GetMapping("/sendVideo")
	public AjaxResult sendVideo() throws FileNotFoundException {
		sendService.asyncMergeVideoSend(1L,1L,null);
		return AjaxResult.success();
	}
}
