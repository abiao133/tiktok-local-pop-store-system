package com.qingyun.web.controller.wxapi;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.exception.ServiceException;
import com.qingyun.common.utils.StringUtils;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.system.domain.SysFeedback;
import com.qingyun.system.domain.SysOss;
import com.qingyun.system.domain.bo.SysFeedbackAddBo;
import com.qingyun.system.domain.bo.SysFeedbackQueryBo;
import com.qingyun.system.domain.vo.SysFeedbackListVo;
import com.qingyun.system.domain.vo.SysFeedbackVo;
import com.qingyun.system.mapper.SysFeedbackMapper;
import com.qingyun.system.service.ISysFeedbackService;
import com.qingyun.system.service.ISysOssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "小程序-用户反馈管理")
@RequestMapping("/miniapp/feedback")
@RestController
@AllArgsConstructor
public class ShopFeedbackController extends BaseController {

    private final MapperFacade mapperFacade;

	private final ISysOssService iSysOssService;

    private final ISysFeedbackService feedbackService;

    @ApiOperation("提交反馈")
    @RepeatSubmit()
    @PostMapping("/add")
    @Transactional
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody SysFeedbackAddBo bo) {

        SysFeedback feedback = mapperFacade.map(bo, SysFeedback.class);

        if (CollectionUtil.isNotEmpty(bo.getPicsList())) {
            feedback.setPics(StringUtils.join(bo.getPicsList(),","));
        }

        feedback.setUserId(SecurityUtils.getAppLoginUser().getSysUser().getUserId());
        feedback.setShopId(SecurityUtils.getAppLoginUser().getShop().getId());
        feedback.setCreateTime(new Date());
        feedback.setUpdateTime(new Date());

        return toAjax(feedbackService.save(feedback));
    }

    @ApiOperation("查看反馈列表 不用传商户id")
    @GetMapping("/list")
    public TableDataInfo<SysFeedbackListVo> list(@Validated SysFeedbackQueryBo bo) {
        bo.setShopId(SecurityUtils.getAppLoginUser().getShop().getId());
        return feedbackService.queryPageList(bo);
    }

    @ApiOperation("查询详情")
    @GetMapping("/{id}")
    public AjaxResult<SysFeedbackVo> getById(@PathVariable("id") Long id){
        return AjaxResult.success(feedbackService.queryById(id));
    }

	/**
	 * 上传OSS云存储
	 */
	@ApiOperation("上传OSS云存储")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "file", value = "文件", dataType = "java.io.File", required = true),
	})
	@RepeatSubmit
	@PostMapping("/upload")
	public AjaxResult<Map<String, Object>> upload(@RequestPart("file") MultipartFile file) {
		if (ObjectUtil.isNull(file)) {
			throw new ServiceException("上传文件不能为空");
		}
		SysOss oss = iSysOssService.upload(file);
		Map<String, Object> map = new HashMap<>(2);
		map.put("url", oss.getUrl());
		map.put("fileName", oss.getFileName());
		map.put("ossId",oss.getOssId());
		return AjaxResult.success(map);
	}

}
