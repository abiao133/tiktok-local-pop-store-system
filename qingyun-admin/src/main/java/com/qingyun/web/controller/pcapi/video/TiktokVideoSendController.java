package com.qingyun.web.controller.pcapi.video;

import cn.hutool.http.HttpStatus;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.controller.BaseController;
import com.qingyun.tiktok.basis.response.video.VideoListRes;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.domain.TiktokVideoSend;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendListVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendVo;
import com.qingyun.tiktok.service.ITiktokVideoSendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 发送视频记录信息Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "发送视频记录信息控制器", tags = {"^发送视频记录信息管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/shop/video/send")
public class TiktokVideoSendController extends BaseController {

    private final ITiktokVideoSendService iTiktokVideoSendService;

    private final DyBasisService dyBasisService;

    /**
     * 查询发送视频记录信息列表
     */
    @ApiOperation("^查询发送视频记录信息列表 video:send:list")
    @PreAuthorize("@ss.hasPermi('video:send:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokVideoSendListVo> list(@Validated TiktokVideoSendQueryBo bo) {
        return iTiktokVideoSendService.queryPageList(bo);
    }


    /**
     * 获取发送视频记录信息详细信息
     */
    @ApiOperation("^获取发送视频记录信息详细信息 video:send:query")
    @PreAuthorize("@ss.hasPermi('video:send:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokVideoSendVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokVideoSendService.queryById(id));
    }

    /**
     * 获取发送到抖音的视频的六维数据
     */
    @ApiOperation("抓取一次抖音视频数据 注：每个视频最多抓十次，如果已经5次了应禁用此按钮")
    @PreAuthorize("@ss.hasPermi('video:send:refresh')")
    @GetMapping("/refresh/{id}")
    public AjaxResult<Void> refresh(@PathVariable("id") Long id) {

        TiktokVideoSend videoSend = iTiktokVideoSendService.getById(id);

        if (videoSend.getIteration() >= 5L) {
            return AjaxResult.error(HttpStatus.HTTP_BAD_REQUEST, "抓取失败! 此条视频已经抓取5次了");
        }

        String[] videoIds = {videoSend.getItemId()};

        VideoListRes.VideoListResData data = dyBasisService.getVideoService().getVideoData(videoSend.getOpenId(), videoIds, ApiPlatform.DOU_YIN);

        List<VideoListRes.VideoDetail> details = data.getList();

        if (details == null || details.size() < 1) {
            return AjaxResult.success();
        }
        VideoListRes.VideoStatistics statistics = details.get(0).getStatistics();
        if (statistics == null) {
            return AjaxResult.success();
        }

        videoSend.setDiggCount((long) statistics.getDiggCount());
        videoSend.setDownloadCount((long) statistics.getDownloadCount());
        videoSend.setForwardCount((long) statistics.getForwardCount());
        videoSend.setPlayCount((long) statistics.getPlayCount());
        videoSend.setShareCount((long) statistics.getShareCount());
        videoSend.setCommentCount((long) statistics.getCommentCount());

        // 抓取次数加1
        videoSend.setIteration(videoSend.getIteration() + 1);

        iTiktokVideoSendService.updateById(videoSend);

        return AjaxResult.success();
    }
}
