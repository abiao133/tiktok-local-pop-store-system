package com.qingyun.web.controller.wxapi;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.TiktokGoods;
import com.qingyun.shop.mapper.TiktokGoodsMapper;
import com.qingyun.shop.service.ITiktokGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "小程序-套餐服务")
@RestController
@RequestMapping("/miniapp/goods")
@AllArgsConstructor
public class ShopGoodsController extends BaseController {

    private final ITiktokGoodsService iTiktokGoodsService;

    @ApiOperation("套餐列表")
    @GetMapping("/list")
    public AjaxResult list() {
        LambdaQueryWrapper<TiktokGoods> lqw = Wrappers.lambdaQuery();
        lqw.eq(TiktokGoods::getStatus, 0);
        lqw.orderByAsc(TiktokGoods::getPrice);
        List<TiktokGoods> tiktokGoods = iTiktokGoodsService.list(lqw);
        return AjaxResult.success(tiktokGoods);
    }
}
