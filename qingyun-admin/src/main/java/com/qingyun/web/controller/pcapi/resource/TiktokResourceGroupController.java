package com.qingyun.web.controller.pcapi.resource;

import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupAddBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupBo;
import com.qingyun.shop.domain.bo.resource.TiktokResourceGroupQueryBo;
import com.qingyun.shop.domain.vo.TiktokResourceGroupVo;
import com.qingyun.shop.service.ITiktokResourceGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 商户资源分组Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "^商户资源分组控制器", tags = {"^商户资源分组管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/resource/group")
public class TiktokResourceGroupController extends BaseController {

    private final ITiktokResourceGroupService iTiktokResourceGroupService;

    /**
     * 查询商户资源分组列表
     */
    @ApiOperation("查询商户资源分组列表 resource:group:list")
    @PreAuthorize("@ss.hasPermi('resource:group:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokResourceGroupVo> list(@Validated TiktokResourceGroupQueryBo bo) {
        return iTiktokResourceGroupService.queryPageList(bo);
    }


    /**
     * 获取商户资源分组详细信息
     */
    @ApiOperation("获取商户资源分组详细信息 resource:group:query")
    @PreAuthorize("@ss.hasPermi('resource:group:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokResourceGroupVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokResourceGroupService.queryById(id));
    }

    /**
     * 新增商户资源分组
     */
    @ApiOperation("新增商户资源分组 resource:group:add")
    @PreAuthorize("@ss.hasPermi('resource:group:add')")
    @RepeatSubmit()
    @PostMapping("/add")
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokResourceGroupAddBo bo) {
        return toAjax(iTiktokResourceGroupService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户资源分组
     */
    @ApiOperation("修改商户资源分组 resource:group:edit")
    @PreAuthorize("@ss.hasPermi('resource:group:edit')")
    @RepeatSubmit()
    @PutMapping("/edit")
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokResourceGroupBo bo) {
        return toAjax(iTiktokResourceGroupService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户资源分组
     */
    @ApiOperation("删除商户资源分组 resource:group:remove")
    @PreAuthorize("@ss.hasPermi('resource:group:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokResourceGroupService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @ApiOperation("资源分组select下拉框")
    @GetMapping("/getSelectList")
    public AjaxResult getSelectList() {
        Map<Long, String> result = iTiktokResourceGroupService.getSelect();
        return AjaxResult.success(result);
    }

}
