package com.qingyun.web.controller.tiktok;

import cn.hutool.core.lang.Validator;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.framework.web.service.TokenService;
import com.qingyun.security.model.tiktok.TikTokLoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Classname TikTokAuthController
 * @Author dyh
 * @Date 2021/9/14 15:58
 */
@Api(tags = "抖音h5 用户详情信息")
@RestController
@RequestMapping("/tikTok/api")
@AllArgsConstructor
public class TikTokAuthController {

	private TokenService tokenService;

	@ApiOperation("获取用户详情")
	@GetMapping("/getInfo")
	public AjaxResult<TiktokUser> getInfo(HttpServletRequest request){
		TikTokLoginUser tikTokUser = tokenService.getTikTokUser(request);
		return AjaxResult.success(tikTokUser.getTikTokUser());
	}

	@ApiOperation("验证token是否过期")
	@GetMapping("/checkToken")
	public AjaxResult verificationToken(HttpServletRequest request){
		TikTokLoginUser tikTokUser = tokenService.getTikTokUser(request);
		if (Validator.isNotNull(tikTokUser)) {
			return AjaxResult.success(true);
		}
		return AjaxResult.success(false);
	}

}
