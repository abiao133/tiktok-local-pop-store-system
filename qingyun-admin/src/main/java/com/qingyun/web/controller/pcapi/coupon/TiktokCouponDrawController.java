package com.qingyun.web.controller.pcapi.coupon;

import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.redis.RedisCache;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokCoupon;
import com.qingyun.shop.domain.TiktokCouponDraw;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponDranQueryBo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawListVo;
import com.qingyun.shop.domain.vo.TiktokCouponDrawVo;
import com.qingyun.shop.service.ITiktokCouponDrawService;
import com.qingyun.shop.service.impl.TiktokCouponServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 用户优惠卷领取Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "用户优惠卷领取控制器", tags = {"^用户优惠卷领取管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/shop/couponDraw")
public class TiktokCouponDrawController extends BaseController {

    private final ITiktokCouponDrawService iTiktokCouponDrawService;

    private final TiktokCouponServiceImpl tiktokCouponService;

    private final RedisCache redisCache;

    /**
     * 查询用户优惠卷领取列表
     */
    @ApiOperation("^查询用户优惠卷领取列表 system:couponDraw:list")
    @PreAuthorize("@ss.hasPermi('system:couponDraw:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokCouponDrawListVo> list(@Validated TiktokCouponDranQueryBo bo) {
        return iTiktokCouponDrawService.queryPageList(bo);
    }

//    /**
//     * 导出用户优惠卷领取列表
//     */
//    @ApiOperation("导出用户优惠卷领取列表 system:couponDraw:export")
//    @PreAuthorize("@ss.hasPermi('system:couponDraw:export')")
//    @Log(title = "用户优惠卷领取", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public void export(@Validated TiktokCouponDrawBo bo, HttpServletResponse response) {
//        List<TiktokCouponDrawVo> list = iTiktokCouponDrawService.queryList(bo);
//        ExcelUtil.exportExcel(list, "用户优惠卷领取", TiktokCouponDrawVo.class, response);
//    }

    /**
     * 获取用户优惠卷领取详细信息
     */
    @ApiOperation("^获取用户优惠卷领取详细信息 system:couponDraw:query")
    @PreAuthorize("@ss.hasPermi('system:couponDraw:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokCouponDrawVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokCouponDrawService.queryById(id));
    }


    @ApiOperation("商户核销优惠卷 system:couponDraw:checkQrcode")
	@RepeatSubmit
    @PostMapping("/checkQrcode")
	@PreAuthorize("@ss.hasPermi('system:couponDraw:checkQrcode')")
    public AjaxResult checkQrcode(String code){

		TiktokCouponDraw one = iTiktokCouponDrawService.lambdaQuery().eq(TiktokCouponDraw::getCouponCode, code).eq(TiktokCouponDraw::getUseStatus, 0).one();
		if (ObjectUtil.isNull(one)) {
			return AjaxResult.error("优惠卷已使用或不存在");
		}
		if (SecurityUtils.isShop()) {
			Long shopId = SecurityUtils.getShop().getId();
			TiktokCoupon coupon = tiktokCouponService.getById(one.getCouponId());
			if (!coupon.getShopId().equals(shopId)) {
				return AjaxResult.error("只能核验本店的优惠卷");
			}
		}
		one.setUseStatus(1);
		one.setUseTime(new Date());
		iTiktokCouponDrawService.updateById(one);
		redisCache.setCacheObject("use-couponDraw-" + code,"success",10, TimeUnit.MINUTES);
		return AjaxResult.success("核验成功");
	}

}
