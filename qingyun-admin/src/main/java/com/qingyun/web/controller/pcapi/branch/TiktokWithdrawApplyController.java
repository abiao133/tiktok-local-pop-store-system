package com.qingyun.web.controller.pcapi.branch;

import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyAddBo;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyQueryBo;
import com.qingyun.shop.domain.vo.TiktokWithdrawApplyVo;
import com.qingyun.shop.service.ITiktokWithdrawApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商户提现申请Controller
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Validated
@Api(value = "代理商提现申请控制器", tags = {"^代理商提现申请管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/branch/apply")
public class TiktokWithdrawApplyController extends BaseController {

    private final ITiktokWithdrawApplyService iTiktokWithdrawApplyService;

    /**
     * 查询商户提现申请列表
     */
    @ApiOperation("查询代理商提现申请列表 system:branchApply:list")
    @PreAuthorize("@ss.hasPermi('system:branchApply:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokWithdrawApplyVo> list(@Validated TiktokWithdrawApplyQueryBo bo) {
        return iTiktokWithdrawApplyService.queryPageList(bo);
    }

    /**
     * 导出商户提现申请列表
     */
    @ApiOperation("导出代理商提现申请列表 system:branchApply:export")
    @PreAuthorize("@ss.hasPermi('system:branchApply:export')")
    @Log(title = "代理商提现申请", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TiktokWithdrawApplyQueryBo bo, HttpServletResponse response) {
        List<TiktokWithdrawApplyVo> list = iTiktokWithdrawApplyService.queryList(bo);
        ExcelUtil.exportExcel(list, "代理商提现申请表", TiktokWithdrawApplyVo.class, response);
    }

    /**
     * 获取商户提现申请详细信息
     */
    @ApiOperation("获取代理商提现申请详细信息 system:branchApply:query")
    @PreAuthorize("@ss.hasPermi('system:branchApply:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokWithdrawApplyVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokWithdrawApplyService.queryById(id));
    }

    /**
     * 新增商户提现申请
     */
    @ApiOperation("pc代理商提现申请 system:branchApply:add")
    @PreAuthorize("@ss.hasPermi('system:branchApply:add')")
    @Log(title = "代理商提现申请", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokWithdrawApplyAddBo bo) {
        return toAjax(iTiktokWithdrawApplyService.insertByBo(bo) ? 1 : 0);
    }

    @ApiOperation("平台审核提现 status 1 通过 2 驳回  system:branchApply:check")
    @PreAuthorize("@ss.hasPermi('system:branchApply:check')")
    @Log(title = "平台审核提现", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/check")
    public AjaxResult<Void> check(@RequestParam("id") Long id,
                                  @RequestParam("status") Integer status,
                                  @RequestParam(value = "rejectReason", required = false) String rejectReason) {

        return toAjax(iTiktokWithdrawApplyService.check(id, status, rejectReason));
    }

//    /**
//     * 修改商户提现申请
//     */
//    @ApiOperation("修改商户提现申请")
//    @PreAuthorize("@ss.hasPermi('system:branchApply:edit')")
//    @Log(title = "商户提现申请", businessType = BusinessType.UPDATE)
//    @RepeatSubmit()
//    @PutMapping()
//    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokWithdrawApplyBo bo) {
//        return toAjax(iTiktokWithdrawApplyService.updateByBo(bo) ? 1 : 0);
//    }

//    /**
//     * 删除商户提现申请
//     */
//    @ApiOperation("删除代理商提现申请")
//    @PreAuthorize("@ss.hasPermi('system:branchApply:remove')")
//    @Log(title = "商户提现申请" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iTiktokWithdrawApplyService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
