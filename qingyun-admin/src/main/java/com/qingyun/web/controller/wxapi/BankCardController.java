package com.qingyun.web.controller.wxapi;

import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RateLimiter;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.enums.LimitType;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.bo.shop.BankCardRealCheckBo;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardAddBo;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardEditBo;
import com.qingyun.shop.domain.vo.TiktokBankCardVo;
import com.qingyun.shop.service.ITiktokBankCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Validated
@Api(value = "小程序-商户银行卡信息控制器", tags = {"小程序-商户银行卡信息管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/miniapp/card")
public class BankCardController extends BaseController {

    private final ITiktokBankCardService iTiktokBankCardService;

    /**
     * 获取商户银行卡信息详细信息
     */
    @ApiOperation("获取代理商银行卡信息详细信息")
    @GetMapping("/{id}")
    public AjaxResult<TiktokBankCardVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokBankCardService.queryById(id));
    }

    /**
     * 获取商户银行卡信息详细信息
     */
    @ApiOperation("获取代理商银行卡列表")
    @GetMapping("/list")
    public AjaxResult<List<TiktokBankCardVo>> getInfo() {
        Long proxyId = SecurityUtils.getProxy().getId();
        List<TiktokBankCardVo> result = iTiktokBankCardService.queryByProxyId(proxyId);
        return AjaxResult.success(result);
    }

    /**
     * 新增商户银行卡信息
     */
    @ApiOperation("新增代理商银行卡信息")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokBankCardAddBo bo) {
//        if (!CheckBankCard.checkBankCard(bo.getBankNumber())) {
//            return AjaxResult.error(HttpStatus.HTTP_BAD_REQUEST, "您输入的银行卡号格式不正确");
//        }

        return toAjax(iTiktokBankCardService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户银行卡信息
     */
    @ApiOperation("修改代理商银行卡信息")
    @RepeatSubmit()
    @PostMapping("/edit")
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokBankCardEditBo bo) {
//        if (!CheckBankCard.checkBankCard(bo.getBankNumber())) {
//            return AjaxResult.error(HttpStatus.HTTP_BAD_REQUEST, "您输入的银行卡号格式不正确");
//        }
        return toAjax(iTiktokBankCardService.updateByBo(bo) ? 1 : 0);
    }

    @ApiOperation("修改银行卡状态")
    @GetMapping("/editStatus")
    public AjaxResult<Void> editStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        return toAjax(iTiktokBankCardService.editStatus(id, status));
    }

    /**
     * 删除商户银行卡信息
     */
    @ApiOperation("删除代理商银行卡信息")
    @Log(title = "商户银行卡信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokBankCardService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @ApiOperation("银行卡三要素验证  姓名 身份证号 银行卡号")
    @PostMapping("/realCheck")
    public AjaxResult realCheck(@Validated @RequestBody BankCardRealCheckBo checkBo) {
        return toAjax(iTiktokBankCardService.realCheck(checkBo));
    }
}
