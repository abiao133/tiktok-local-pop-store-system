package com.qingyun.web.controller.pcapi.order;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.order.TiktokOrderSettlementQueryBo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementListVo;
import com.qingyun.shop.domain.vo.TiktokOrderSettlementVo;
import com.qingyun.shop.service.ITiktokOrderSettlementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 订单流水Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "订单流水控制器", tags = {"订单流水管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/order/settlement")
public class TiktokOrderSettlementController extends BaseController {

    private final ITiktokOrderSettlementService iTiktokOrderSettlementService;

    /**
     * 查询订单流水列表
     */
    @ApiOperation("查询订单流水列表 system:settlement:list")
    @PreAuthorize("@ss.hasPermi('system:settlement:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokOrderSettlementListVo> list(@Validated TiktokOrderSettlementQueryBo bo) {
        return iTiktokOrderSettlementService.queryPageList(bo);
    }

//    /**
//     * 导出订单流水列表
//     */
//    @ApiOperation("导出订单流水列表")
//    @PreAuthorize("@ss.hasPermi('system:settlement:export')")
//    @Log(title = "订单流水", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public void export(@Validated TiktokOrderSettlementQueryBo bo, HttpServletResponse response) {
//        List<TiktokOrderSettlementVo> list = iTiktokOrderSettlementService.queryList(bo);
//        ExcelUtil.exportExcel(list, "订单流水", TiktokOrderSettlementVo.class, response);
//    }

    /**
     * 获取订单流水详细信息
     */
    @ApiOperation("获取订单流水详细信息 system:settlement:query")
    @PreAuthorize("@ss.hasPermi('system:settlement:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokOrderSettlementVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokOrderSettlementService.queryById(id));
    }
}
