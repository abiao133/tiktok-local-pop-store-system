package com.qingyun.web.controller.pcapi.shop;

import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.TiktokApplyProxy;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyAddBo;
import com.qingyun.shop.domain.bo.apply.TiktokApplyProxyQueryBo;
import com.qingyun.shop.domain.vo.TiktokApplyProxyListVo;
import com.qingyun.shop.domain.vo.TiktokApplyProxyVo;
import com.qingyun.shop.service.ITiktokApplyProxyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 代理商申请Controller
 *
 * @author qingyun
 * @date 2021-09-10
 */
@Validated
@Api(value = "代理商申请控制器", tags = {"^代理商申请管理"})
@AllArgsConstructor
@RestController
@RequestMapping("system/shop/applyProxy/")
public class TiktokApplyProxyController extends BaseController {

    private final ITiktokApplyProxyService iTiktokApplyProxyService;

    /**
     * 查询代理商申请列表
     */
    @ApiOperation("^平台-查询代理商申请列表 system:applyProxy:list")
    @PreAuthorize("@ss.hasPermi('system:applyProxy:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokApplyProxyListVo> list(@Validated TiktokApplyProxyQueryBo bo) {
        return iTiktokApplyProxyService.queryPageList(bo);
    }

//    /**
//     * 导出代理商申请列表
//     */
//    @ApiOperation("导出代理商申请列表")
//    @PreAuthorize("@ss.hasPermi('system:proxy:export')")
//    @Log(title = "代理商申请", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public void export(@Validated TiktokApplyProxyBo bo, HttpServletResponse response) {
//        List<TiktokApplyProxyVo> list = iTiktokApplyProxyService.queryList(bo);
//        ExcelUtil.exportExcel(list, "代理商申请", TiktokApplyProxyVo.class, response);
//    }

    /**
     * 获取代理商申请详细信息
     */
    @ApiOperation("^获取代理商申请详细信息 system:applyProxy:query")
    @PreAuthorize("@ss.hasPermi('system:applyProxy:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokApplyProxyVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokApplyProxyService.queryById(id));
    }


    @PreAuthorize("@ss.hasPermi('system:applyProxy:check')")
    @ApiOperation("审核代理商状态")
    @PostMapping("/check")
    public AjaxResult toCheck(Long applyId,Integer status,String context){
		TiktokApplyProxy applyproxy = iTiktokApplyProxyService.getById(applyId);
		if (ObjectUtil.isNull(applyproxy)||!applyproxy.getStatus().equals(0)) {
			return AjaxResult.error("此记录不能进行审核");
		}
		//效验审核状态
		iTiktokApplyProxyService.checkShopIsProxy(applyproxy.getShopId());
		//进行审核修改
		Boolean apply = iTiktokApplyProxyService.checkShopApplyProxy(applyproxy, status,context);
		return toAjax(apply);
	}

//    /**
//     * 新增代理商申请
//     */
//    @ApiOperation("^商户-新增代理商申请")
//    @PreAuthorize("@ss.hasPermi('system:proxy:add')")
//    @Log(title = "代理商申请", businessType = BusinessType.INSERT)
//    @RepeatSubmit()
//    @PostMapping()
//    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokApplyProxyAddBo bo) {
//		iTiktokApplyProxyService.checkShopIsProxy();
//        return toAjax(iTiktokApplyProxyService.insertByBo(bo) ? 1 : 0);
//    }

//    /**
//     * 修改代理商申请
//     */
//    @ApiOperation("^修改代理商申请")
//    @PreAuthorize("@ss.hasPermi('system:proxy:edit')")
//    @Log(title = "代理商申请", businessType = BusinessType.UPDATE)
//    @RepeatSubmit()
//    @PutMapping("/edit")
//    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokApplyProxyEditBo bo) {
//        return toAjax(iTiktokApplyProxyService.updateByBo(bo) ? 1 : 0);
//    }

//    /**
//     * 删除代理商申请
//     */
//    @ApiOperation("删除代理商申请")
//    @PreAuthorize("@ss.hasPermi('system:proxy:remove')")
//    @Log(title = "代理商申请" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iTiktokApplyProxyService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
