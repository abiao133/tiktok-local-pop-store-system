package com.qingyun.web.controller.wxapi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.common.utils.QrcodeGeneratorUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.TiktokShopInfo;
import com.qingyun.shop.domain.vo.TiktokAppShopIndexVo;
import com.qingyun.shop.service.ITiktokCouponService;
import com.qingyun.shop.service.ITiktokShopInfoService;
import com.qingyun.shop.service.ITiktokShopService;
import com.qingyun.tiktok.config.DyConfig;
import com.qingyun.tiktok.domain.vo.TiktokAppVideoCountsVo;
import com.qingyun.tiktok.service.ITiktokVideoSendService;
import com.qingyun.tiktok.util.TikTokUserByCodeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @Classname ShopIndexController
 * @Author dyh
 * @Date 2021/9/22 16:00
 */
@Api(tags = "商家小程序首页api")
@RequestMapping("miniapp")
@RestController
@AllArgsConstructor
public class ShopIndexController {

	private ITiktokVideoSendService videoSendService;

	private ITiktokCouponService couponService;

	private ITiktokShopService shopService;

	private ITiktokShopInfoService tiktokShopInfoService;

	private DyConfig dyConfig;

	private TikTokUserByCodeUtil tikTokUserByCodeUtil;

	@ApiOperation("获取发送数量")
	@GetMapping("/sendCount")
	public AjaxResult getSendCount(Integer type){
		Integer total=null;
		Integer today=null;
		Integer yesterday=null;

		//发送数据
		TiktokShopVo shop = SecurityUtils.getAppLoginUser().getShop();
		if (type.equals(0)) {
			//所有发送次数
			total = videoSendService.selectSendVideoCountByDate(shop.getId(), null);
			//今天
			today = videoSendService.selectSendVideoCountByDate(shop.getId(), new Date());
			//昨天
			 yesterday = videoSendService.selectSendVideoCountByDate(shop.getId(), DateUtil.offsetDay(new Date(), -1));
		}else if(type.equals(1)){
			TiktokAppVideoCountsVo tiktokAppVideoCountsVo = videoSendService.selectAppVideoCounts(shop.getId());
			return AjaxResult.success(tiktokAppVideoCountsVo);
		}else if(type.equals(2)){
			 total = couponService.selectCouponCountByDate(shop.getId(), null);
			 today = couponService.selectCouponCountByDate(shop.getId(), new Date());
			 yesterday = couponService.selectCouponCountByDate(shop.getId(),DateUtil.offsetDay(new Date(), -1));
		}
		TiktokAppShopIndexVo tiktokAppShopIndexVo = new TiktokAppShopIndexVo();
		tiktokAppShopIndexVo.setTotal(total);
		tiktokAppShopIndexVo.setToday(today);
		tiktokAppShopIndexVo.setYesterday(yesterday);

		return AjaxResult.success(tiktokAppShopIndexVo);
	}

	@ApiOperation("获取剩余发送数量")
	@GetMapping("videoCount")
	public AjaxResult getVideoCount(){
        //套餐次数
		TiktokShopVo shop = SecurityUtils.getAppLoginUser().getShop();
		TiktokShop tikShop = shopService.getById(shop.getId());
		Long videoCount = tikShop.getVideoCount();
		return AjaxResult.success(videoCount);
	}


	@ApiOperation("获取最新10条用户参与活动记录")
	@GetMapping("sendVideoTitleLamp")
	public AjaxResult getSendVideoTitle(){
		//10条记录
		TiktokShopVo shop = SecurityUtils.getAppLoginUser().getShop();
		List<String> titles = videoSendService.selectSendTitleByCount(shop.getId(), 10);
		return AjaxResult.success(titles);
	}

	@ApiOperation("/抖音授权二维码地址")
	@GetMapping("/getAuthQrCodeUrl")
	public AjaxResult qrCodeUrl() {
		Long shopId = SecurityUtils.getAppLoginUser().getShop().getId();
		StringBuilder builder = new StringBuilder();
		builder.append("https://open.douyin.com/platform/oauth/connect/?client_key=");
		builder.append(dyConfig.getAppId());
		builder.append("&response_type=code&scope=trial.whitelist,user_info&redirect_uri=");
		builder.append(dyConfig.getShopRedirectUri() + "?shopId=" + shopId);
		String baseQrcodeBase64 = QrcodeGeneratorUtils.createBaseQrcodeBase64(builder.toString());
		return AjaxResult.success(baseQrcodeBase64);
	}
	/**
	 * 商家抖音授权
	 *
	 * @return
	 */
	@ApiOperation("商家抖音授权")
	@GetMapping("/shopAuth")
	public AjaxResult shopAuth(String code, Long shopId) {
		TiktokUser tiktokUser = tikTokUserByCodeUtil.getTiktokUser(code);
		TiktokShopInfo one = tiktokShopInfoService.lambdaQuery().eq(TiktokShopInfo::getShopId, shopId).one();
		if (ObjectUtil.isNull(one)) {
			TiktokShopInfo tiktokShopInfo = new TiktokShopInfo();
			tiktokShopInfo.setOpenId(tiktokUser.getOpenId());
			tiktokShopInfo.setShopId(shopId);
			tiktokShopInfo.setNickname(tiktokUser.getNickName());
			tiktokShopInfoService.save(tiktokShopInfo);
		} else {
			one.setNickname(tiktokUser.getNickName());
			one.setOpenId( tiktokUser.getOpenId());
			tiktokShopInfoService.updateById(one);
		}
		return AjaxResult.success("授权成功");
	}


}
