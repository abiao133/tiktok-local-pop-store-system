package com.qingyun.web.controller.system;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.config.QingYunConfig;
import com.qingyun.common.constant.Constants;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.exception.ServiceException;
import com.qingyun.common.utils.JsonUtils;
import com.qingyun.common.utils.file.FileUtils;
import com.qingyun.oss.constant.CloudConstant;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.system.domain.SysConfig;
import com.qingyun.system.domain.SysOss;
import com.qingyun.system.domain.bo.SysOssBo;
import com.qingyun.system.domain.vo.SysOssVo;
import com.qingyun.system.service.ISysConfigService;
import com.qingyun.system.service.ISysOssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传 控制层
 *
 * @author qingyun
 */
@Validated
@Api(value = "OSS云存储控制器", tags = {"OSS云存储管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/oss")
public class SysOssController extends BaseController {

	private final ISysOssService iSysOssService;
	private final ISysConfigService iSysConfigService;

	/**
	 * 查询OSS云存储列表
	 */
	@ApiOperation("查询OSS云存储列表")
	@PreAuthorize("@ss.hasPermi('system:oss:list')")
	@GetMapping("/list")
	public TableDataInfo<SysOssVo> list(@Validated SysOssBo bo) {
		return iSysOssService.queryPageList(bo);
	}

	/**
	 * 上传OSS云存储
	 */
	@ApiOperation("上传OSS云存储")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "file", value = "文件", dataType = "java.io.File", required = true),
	})
	@PreAuthorize("@ss.hasPermi('system:oss:upload')")
	@Log(title = "OSS云存储", businessType = BusinessType.INSERT)
	@RepeatSubmit
	@PostMapping("/upload")
	public AjaxResult<Map<String, Object>> upload(@RequestPart("file") MultipartFile file) {
		if (Validator.isNull(SecurityUtils.getAuthentication())) {
			return  AjaxResult.oauthFail(MapUtil.newHashMap());
		}
		if (ObjectUtil.isNull(file)) {
			throw new ServiceException("上传文件不能为空");
		}
		SysOss oss = iSysOssService.upload(file);
		Map<String, Object> map = new HashMap<>(2);
		map.put("url", oss.getUrl());
		map.put("fileName", oss.getFileName());
		map.put("ossId",oss.getOssId());
		return AjaxResult.success(map);
	}

	@ApiOperation("下载OSS云存储")
	@PreAuthorize("@ss.hasPermi('system:oss:download')")
	@GetMapping("/download/{ossId}")
	public void download(@PathVariable Long ossId, HttpServletResponse response) throws IOException {
		SysOss sysOss = iSysOssService.getById(ossId);
		if (ObjectUtil.isNull(sysOss)) {
			throw new ServiceException("文件数据不存在!");
		}
		response.reset();
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
		FileUtils.setAttachmentResponseHeader(response, URLEncoder.encode(sysOss.getOriginalName(), StandardCharsets.UTF_8.toString()));
		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE + "; charset=UTF-8");
		long data = HttpUtil.download(sysOss.getUrl(), response.getOutputStream(), false);
		response.setContentLength(Convert.toInt(data));
	}

	/**
	 * 删除OSS云存储
	 */
	@ApiOperation("删除OSS云存储")
	@PreAuthorize("@ss.hasPermi('system:oss:remove')")
	@Log(title = "OSS云存储" , businessType = BusinessType.DELETE)
	@DeleteMapping("/{ossIds}")
	public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
								   @PathVariable Long[] ossIds) {
		return toAjax(iSysOssService.deleteWithValidByIds(Arrays.asList(ossIds), true) ? 1 : 0);
	}



	/**
	 * 变更图片列表预览状态
	 */
	@ApiOperation("变更图片列表预览状态")
	@PreAuthorize("@ss.hasPermi('system:oss:edit')")
	@Log(title = "OSS云存储" , businessType = BusinessType.UPDATE)
	@PutMapping("/changePreviewListResource")
	public AjaxResult<Void> changePreviewListResource(@RequestBody String body) {
		Map<String, Boolean> map = JsonUtils.parseMap(body);
		SysConfig config = iSysConfigService.getOne(new LambdaQueryWrapper<SysConfig>()
			.eq(SysConfig::getConfigKey, CloudConstant.PEREVIEW_LIST_RESOURCE_KEY));
		config.setConfigValue(map.get("previewListResource").toString());
		return toAjax(iSysConfigService.updateConfig(config));
	}

}
