package com.qingyun.web.controller.common;

import com.qingyun.common.config.QingYunConfig;
import com.qingyun.common.constant.Constants;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.utils.file.FileUploadUtils;
import com.qingyun.common.utils.file.MimeTypeUtils;
import com.qingyun.framework.config.ServerConfig;
import com.qingyun.security.controller.BaseController;
import com.qingyun.video.utils.Mp4Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "本地上传")
@AllArgsConstructor
@RestController
@RequestMapping("/common")
public class UploadController extends BaseController {

	private QingYunConfig qingYunConfig;

	/**
	 * 通用上传请求
	 */
	@ApiOperation("音视频上传请求")
	@PostMapping("/video/upload")
	public AjaxResult uploadFile(@RequestPart("file") MultipartFile file) {
		try {
			FileUploadUtils.assertAllowed(file, MimeTypeUtils.MEDIA_EXTENSION);
			// 上传文件路径
			String filePath = QingYunConfig.getUploadPath();
			// 上传并返回新文件名称
			String fileName = FileUploadUtils.upload(filePath, file);
			String localPath = QingYunConfig.getProfile() + StringUtils.substringAfter(fileName, Constants.RESOURCE_PREFIX);
			Mp4Utils.checkFormat(localPath);
			String url = QingYunConfig.getProjectUrl() + fileName;

			Map<String, Object> map = new HashMap<>();
			map.put("name", fileName);
			map.put("url", url);
			List<Map<String, Object>> result = new ArrayList<>();
			result.add(map);
			return AjaxResult.success(result);
		} catch (Exception e) {
			return AjaxResult.error(e.getMessage(), null);
		}
	}

	@ApiOperation("批量音视频上传请求")
	@PostMapping("/video/uploads")
	public AjaxResult uploadVideos(@RequestPart("file") MultipartFile[] file) {
		try {
			List<Map<String, Object>> result = new ArrayList<>();
			for (MultipartFile f : file) {
				FileUploadUtils.assertAllowed(f, MimeTypeUtils.MEDIA_EXTENSION);
				// 上传文件路径
				String filePath = QingYunConfig.getUploadPath();
				// 上传并返回新文件名称
				String fileName = FileUploadUtils.upload(filePath, f);
				//验证分辨率格式
				String localPath = QingYunConfig.getProfile() + StringUtils.substringAfter(fileName, Constants.RESOURCE_PREFIX);
				if(System.getProperty("os.name").toLowerCase().contains("windows")){
					String windowsPath = localPath.replaceAll("/","\\\\\\\\");
					Mp4Utils.checkFormat(windowsPath);
				}else{
					Mp4Utils.checkFormat(localPath);
				}

				String url = QingYunConfig.getProjectUrl() + fileName;
				Map<String, Object> map = new HashMap<>();
				map.put("name", fileName);
				map.put("url", url);
				result.add(map);
			}

			return AjaxResult.success(result);
		} catch (Exception e) {
			return AjaxResult.error(e.getMessage(), null);
		}
	}





}
