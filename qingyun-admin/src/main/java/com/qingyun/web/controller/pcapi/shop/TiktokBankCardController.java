package com.qingyun.web.controller.pcapi.shop;

import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.shop.BankCardRealCheckBo;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardAddBo;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardEditBo;
import com.qingyun.shop.domain.bo.shop.TiktokBankCardQueryBo;
import com.qingyun.shop.domain.vo.TiktokBankCardExcelVo;
import com.qingyun.shop.domain.vo.TiktokBankCardListVo;
import com.qingyun.shop.domain.vo.TiktokBankCardVo;
import com.qingyun.shop.service.ITiktokBankCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;


/**
 * 商户银行卡信息Controller
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Validated
@Api(value = "代理商银行卡信息控制器", tags = {"代理商银行卡信息管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/proxy/card")
public class TiktokBankCardController extends BaseController {

    private final ITiktokBankCardService iTiktokBankCardService;

    /**
     * 查询商户银行卡信息列表
     */
    @ApiOperation("查询代理商银行卡信息列表 system:card:list")
    @PreAuthorize("@ss.hasPermi('system:card:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokBankCardListVo> list(@Validated TiktokBankCardQueryBo bo) {
        return iTiktokBankCardService.queryPageList(bo);
    }

    /**
     * 导出商户银行卡信息列表
     */
    @ApiOperation("代理商银行卡信息表 system:card:export")
    @PreAuthorize("@ss.hasPermi('system:card:export')")
    @GetMapping("/export")
    public void export(@Validated TiktokBankCardQueryBo bo, HttpServletResponse response) {
        List<TiktokBankCardExcelVo> list = iTiktokBankCardService.queryList(bo);
        ExcelUtil.exportExcel(list, "代理商银行卡信息表", TiktokBankCardExcelVo.class, response);
    }

    /**
     * 获取商户银行卡信息详细信息
     */
    @ApiOperation("获取代理商银行卡信息详细信息 system:card:query")
    @PreAuthorize("@ss.hasPermi('system:card:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokBankCardVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokBankCardService.queryById(id));
    }

    /**
     * 获取商户银行卡信息详细信息
     */
    @ApiOperation("获取代理商银行卡列表 通过代理商id   system:card:query")
    @PreAuthorize("@ss.hasPermi('system:card:query')")
    @GetMapping("/byProxyId/{proxyId}")
    public AjaxResult<List<TiktokBankCardVo>> getInfoByShopId(@PathVariable("proxyId") Long proxyId) {
        List<TiktokBankCardVo> result = iTiktokBankCardService.queryByProxyId(proxyId);
        return AjaxResult.success(result);
    }

    /**
     * 新增商户银行卡信息
     */
    @ApiOperation("新增代理商银行卡信息 system:card:add")
    @PreAuthorize("@ss.hasPermi('system:card:add')")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokBankCardAddBo bo) {
//        if (!CheckBankCard.checkBankCard(bo.getBankNumber())) {
//            return AjaxResult.error(HttpStatus.HTTP_BAD_REQUEST, "您输入的银行卡号格式不正确");
//        }
        return toAjax(iTiktokBankCardService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户银行卡信息
     */
    @ApiOperation("修改代理商银行卡信息 system:card:edit")
    @PreAuthorize("@ss.hasPermi('system:card:edit')")
    @RepeatSubmit()
    @PostMapping("/edit")
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokBankCardEditBo bo) {
//        if (!CheckBankCard.checkBankCard(bo.getBankNumber())) {
//            return AjaxResult.error(HttpStatus.HTTP_BAD_REQUEST, "您输入的银行卡号格式不正确");
//        }
        return toAjax(iTiktokBankCardService.updateByBo(bo) ? 1 : 0);
    }

    @ApiOperation("修改银行卡状态 system:card:editStatus")
    @PreAuthorize("@ss.hasPermi('system:card:editStatus')")
    @GetMapping("/editStatus")
    public AjaxResult<Void> editStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        return toAjax(iTiktokBankCardService.editStatus(id, status));
    }

    /**
     * 删除商户银行卡信息
     */
    @ApiOperation("删除代理商银行卡信息")
    @PreAuthorize("@ss.hasPermi('system:card:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokBankCardService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @ApiOperation("银行卡三要素验证  姓名 身份证号 银行卡号")
    @PostMapping("/realCheck")
    public AjaxResult<Boolean> realCheck(@Validated @RequestBody BankCardRealCheckBo checkBo) {
        return AjaxResult.success(iTiktokBankCardService.realCheck(checkBo));
    }
}
