package com.qingyun.web.controller.wxapi;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.system.domain.SysNotice;
import com.qingyun.system.service.ISysNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "小程序-通告管理")
@RestController
@RequestMapping("/miniapp/notice")
@AllArgsConstructor
public class ShopNoticeController {

    private final ISysNoticeService noticeService;

    @ApiOperation("通告列表")
    @GetMapping("/list")
    public TableDataInfo<SysNotice> list(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("pageNum", pageNum);
        params.put("pageSize", pageSize);

        SysNotice sysNotice = new SysNotice();
        sysNotice.setStatus("0");
        sysNotice.setParams(params);

        return noticeService.selectPageNoticeList(sysNotice);
    }

    @ApiOperation("首页展示最新一条通告记录")
    @GetMapping("/newest")
    public AjaxResult<SysNotice> newest() {
        return AjaxResult.success(noticeService.getNewest());
    }

    @ApiOperation("查看公告详情")
    @GetMapping("/{id}")
    public AjaxResult<SysNotice> getNotice(@PathVariable("id") Long id) {
        return AjaxResult.success(noticeService.selectNoticeById(id));
    }
}
