package com.qingyun.web.controller.pcapi.goods;

import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsAddBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsEditBo;
import com.qingyun.shop.domain.bo.goods.TiktokGoodsQueryBo;
import com.qingyun.shop.domain.vo.TiktokGoodsVo;
import com.qingyun.shop.service.ITiktokGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * 套餐Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "套餐控制器", tags = {"^套餐管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/goods")
public class TiktokGoodsController extends BaseController {

    private final ITiktokGoodsService iTiktokGoodsService;

    /**
     * 查询套餐列表
     */
    @ApiOperation("查询套餐列表 system:goods:list")
    @PreAuthorize("@ss.hasPermi('system:goods:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokGoodsVo> list(@Validated TiktokGoodsQueryBo bo) {
        return iTiktokGoodsService.queryPageList(bo);
    }

    /**
     * 获取套餐详细信息
     */
    @ApiOperation("获取套餐详细信息 system:goods:query")
    @PreAuthorize("@ss.hasPermi('system:goods:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokGoodsVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokGoodsService.queryById(id));
    }

    /**
     * 新增套餐
     */
    @ApiOperation("新增套餐 system:goods:add")
    @PreAuthorize("@ss.hasPermi('system:goods:add')")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokGoodsAddBo bo) {
        return toAjax(iTiktokGoodsService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改套餐
     */
    @ApiOperation("修改套餐 system:goods:edit")
    @PreAuthorize("@ss.hasPermi('system:goods:edit')")
    @RepeatSubmit()
    @PutMapping()
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokGoodsEditBo bo) {
        return toAjax(iTiktokGoodsService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除套餐
     */
    @ApiOperation("删除套餐 system:goods:remove")
    @PreAuthorize("@ss.hasPermi('system:goods:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokGoodsService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

	@ApiOperation("修改上下架 system:goods:edit")
	@PreAuthorize("@ss.hasPermi('system:goods:edit')")
	@RepeatSubmit()
	@PostMapping("/editStatus/{id}/{status}")
	public AjaxResult<Void> editStatus(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
		return toAjax(iTiktokGoodsService.editStatus(id, status));
	}
}
