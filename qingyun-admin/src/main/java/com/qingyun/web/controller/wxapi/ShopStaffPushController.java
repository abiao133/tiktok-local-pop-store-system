package com.qingyun.web.controller.wxapi;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.vo.TiktokShopStaffPushVo;
import com.qingyun.shop.service.ITiktokShopStaffPushService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


/**
 * @Classname ShopStaffPushController
 * @Author dyh
 * @Date 2021/10/30 10:59
 */
@Api(tags = "小程序-员工推广记录")
@RestController
@RequestMapping("/miniapp/staff")
@AllArgsConstructor
public class ShopStaffPushController {

	private ITiktokShopStaffPushService shopStaffPushService;

	@ApiOperation("员工推广记录列表")
	@GetMapping("pushList")
	public TableDataInfo<TiktokShopStaffPushVo> staffPushList(Integer pageNum,Integer pageSize,Long type){
		TableDataInfo<TiktokShopStaffPushVo> tiktokShopStaffPushVoTableDataInfo = shopStaffPushService.selectPageList(type,null);
		return tiktokShopStaffPushVoTableDataInfo;
	}

	@ApiOperation("获取总推广人数和成功核销人数")
	@GetMapping("getPushCount")
    public AjaxResult getStaffPushCount(){
		Long userId = SecurityUtils.getAppLoginUser().getSysUser().getUserId();
		Integer totalCount = shopStaffPushService.selectTotalCount(userId);
		Integer checkCount = shopStaffPushService.selectCheckCount(userId);
		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put("totalCount",totalCount);
		resultMap.put("checkCount",checkCount);
		return AjaxResult.success(resultMap);
	}
}
