package com.qingyun.web.controller.tiktok;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.exception.CustomException;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokResource;
import com.qingyun.shop.domain.TiktokShop;
import com.qingyun.shop.domain.TiktokShopInfo;
import com.qingyun.shop.domain.vo.TiktokActivityVo;
import com.qingyun.shop.domain.vo.TiktokShopH5Vo;
import com.qingyun.shop.service.ITiktokActivityService;
import com.qingyun.shop.service.ITiktokResourceService;
import com.qingyun.shop.service.ITiktokShopInfoService;
import com.qingyun.shop.service.ITiktokShopService;
import com.qingyun.tiktok.basis.response.user.FansCheckRes;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.common.exception.LandExpirationException;
import com.qingyun.tiktok.service.ITiktokVideoSendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.rmi.ServerException;
import java.util.List;

/**
 * @Classname TikTokActivityController
 * @Author dyh
 * @Date 2021/9/14 16:08
 */
@Api(tags = "抖音h5端 活动页面")
@RestController()
@RequestMapping("/tikTok/Activity/api")
@AllArgsConstructor
@Slf4j
public class TikTokActivityController {

	private ITiktokActivityService tiktokActivityService;

	private ITiktokShopInfoService iTiktokShopInfoService;

	private ITiktokShopService tiktokShopService;

	private MapperFacade mapperFacade;

	private ITiktokVideoSendService sendService;

	private ITiktokResourceService resourceService;

	private DyBasisService dyBasisService;

	@ApiOperation( "获取活动信息")
	@GetMapping("/getActivity")
	@Cacheable(cacheNames = "GetAcitvity", key = "#activityId")
	public AjaxResult getAcitvity(Long activityId){
		if (ObjectUtil.isNull(activityId)) {
			return AjaxResult.error("活动不存在请重新扫码");
		}
		TiktokActivityVo tiktokActivityVo = tiktokActivityService.getVoById(activityId);
		TiktokShopInfo one = iTiktokShopInfoService.getOne(Wrappers.lambdaQuery(TiktokShopInfo.class).eq(TiktokShopInfo::getShopId, tiktokActivityVo.getShopId()));
		if(ObjectUtil.isNull(one)){
			return AjaxResult.error("商户还没授权");
		}
		if(!tiktokActivityVo.getVideoSource().equals(1)){
			int count = resourceService.count(Wrappers.lambdaQuery(TiktokResource.class).eq(TiktokResource::getShopId, tiktokActivityVo.getShopId()));
			if(count<=0){
				return AjaxResult.error("商家还没添加视频，请联系商家");
			}
		}
		tiktokActivityVo.setDouyinUrl(one.getDouyinUrl());
		tiktokActivityVo.setAddress(one.getAddress());
		TiktokShop shop = tiktokShopService.getById(tiktokActivityVo.getShopId());
		tiktokActivityVo.setShop(mapperFacade.map(shop, TiktokShopH5Vo.class));
		return AjaxResult.success(tiktokActivityVo);
	}

	@ApiOperation("是否关注商户")
	@GetMapping("isAttention")
	public AjaxResult getIsAttention(Long activityId){
		TiktokShopInfo tiktokShopInfo = iTiktokShopInfoService.selectActivtyId(activityId);
		if (ObjectUtil.isNotNull(tiktokShopInfo)) {
			String openId = SecurityUtils.getTikTokLoginUser().getOpenId();
			FansCheckRes.FansCheckResData fansCheckResData=null;
			try {
				 fansCheckResData = dyBasisService.getUserManagerService().fansCheck(tiktokShopInfo.getOpenId(),openId);
			}catch (CustomException ex){
				log.info("进入了异常{}",ex.getMessage());
				return AjaxResult.error(ex.getMessage());
			}
			catch (Exception e){
				e.printStackTrace();
				//商家关注列表不可见等情况 也让用户领取优惠卷
				return AjaxResult.success(true);
			}
			boolean follower = fansCheckResData.isFollower();
			return AjaxResult.success(follower);
		}
		return AjaxResult.success(false);
	}

	@ApiOperation("获取最新10条参与活动列表")
	@GetMapping("/pulsList")
	public AjaxResult getPlusActivityList(Long shopId){
		List<String> titles = sendService.selectSendTitleByCount(shopId, 10);
		return AjaxResult.success(titles);
	}



}
