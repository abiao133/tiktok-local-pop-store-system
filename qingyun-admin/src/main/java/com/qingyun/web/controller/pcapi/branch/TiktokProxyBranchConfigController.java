package com.qingyun.web.controller.pcapi.branch;

import java.util.List;
import java.util.Arrays;

import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.TiktokProxyBranchConfig;
import com.qingyun.shop.domain.bo.branch.TiktokProxyBranchConfigBo;
import com.qingyun.shop.domain.vo.TiktokProxyBranchConfigVo;
import com.qingyun.shop.service.ITiktokProxyBranchConfigService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.annotation.Log;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;

import com.qingyun.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 代理商返利配置Controller
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Validated
@Api(value = "代理商返利配置控制器", tags = {"代理商返利配置管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/branch/config")
public class TiktokProxyBranchConfigController extends BaseController {

    private final ITiktokProxyBranchConfigService iTiktokProxyBranchConfigService;


    /**
     * 获取代理商返利配置详细信息
     */
    @ApiOperation("获取代理商返利配置详细信息 system:branch:config:query")
    @PreAuthorize("@ss.hasPermi('system:branch:config:query')")
    @GetMapping("/{proxyId}")
    public AjaxResult<TiktokProxyBranchConfig> getInfo(@PathVariable("proxyId") Long proxyId) {
		TiktokProxyBranchConfig one = iTiktokProxyBranchConfigService.lambdaQuery().eq(TiktokProxyBranchConfig::getProxyId, proxyId).one();
		return AjaxResult.success(one);
    }


    /**
     * 修改代理商返利配置
     */
    @ApiOperation("修改代理商返利配置 system:branch:config:edit")
    @PreAuthorize("@ss.hasPermi('system:branch:config:edit')")
    @Log(title = "代理商返利配置", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokProxyBranchConfigBo bo) {
        return toAjax(iTiktokProxyBranchConfigService.updateByBo(bo) ? 1 : 0);
    }


}
