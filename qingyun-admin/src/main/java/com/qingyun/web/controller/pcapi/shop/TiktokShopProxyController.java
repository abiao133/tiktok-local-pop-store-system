package com.qingyun.web.controller.pcapi.shop;


import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.service.ITiktokApplyProxyService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * `商户申请代理商管理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(tags = {"商户申请代理商管理"})
@AllArgsConstructor
@RestController
@RequestMapping("system/shop/applyProxy")
public class TiktokShopProxyController extends BaseController {

	private ITiktokApplyProxyService applyProxyService;




}
