package com.qingyun.web.controller.pcapi.shop;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.TiktokTransferQueryBo;
import com.qingyun.shop.domain.vo.TiktokTransferVo;
import com.qingyun.shop.service.ITiktokTransferService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 代理给商户转播放量记录Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "代理给商户转播放量记录控制器", tags = {"^代理给商户转播放量记录管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/shop/transfer")
public class TiktokTransferController extends BaseController {

    private final ITiktokTransferService iTiktokTransferService;

    /**
     * 查询代理给商户转播放量记录列表
     */
    @ApiOperation("^查询代理给商户转播放量记录列表 system:transfer:list")
    @PreAuthorize("@ss.hasPermi('system:transfer:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokTransferVo> list(@Validated TiktokTransferQueryBo bo) {
        return iTiktokTransferService.queryPageList(bo);
    }

//    /**
//     * 导出代理给商户转播放量记录列表
//     */
//    @ApiOperation("导出代理给商户转播放量记录列表 system:transfer:export")
//    @PreAuthorize("@ss.hasPermi('system:transfer:export')")
//    @Log(title = "代理给商户转播放量记录", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public void export(@Validated TiktokTransferBo bo, HttpServletResponse response) {
//        List<TiktokTransferVo> list = iTiktokTransferService.queryList(bo);
//        ExcelUtil.exportExcel(list, "代理给商户转播放量记录", TiktokTransferVo.class, response);
//    }


    /**
     * 获取代理给商户转播放量记录详细信息
     */
    @ApiOperation("^获取代理给商户转播放量记录详细信息 system:transfer:query")
    @PreAuthorize("@ss.hasPermi('system:transfer:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokTransferVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokTransferService.queryById(id));
    }

//    /**
//     * 删除代理给商户转播放量记录
//     */
//    @ApiOperation("删除代理给商户转播放量记录 system:transfer:remove")
//    @PreAuthorize("@ss.hasPermi('system:transfer:remove')")
//    @Log(title = "代理给商户转播放量记录" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iTiktokTransferService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
