package com.qingyun.web.controller.pcapi.order;

import java.util.List;
import java.util.Arrays;

import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.order.TiktokOrderItemBo;
import com.qingyun.shop.domain.vo.TiktokOrderItemVo;
import com.qingyun.shop.service.ITiktokOrderItemService;
import lombok.AllArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.qingyun.common.annotation.RepeatSubmit;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.utils.poi.ExcelUtil;

import com.qingyun.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 订单详情Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "订单详情控制器", tags = {"订单详情管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/item")
public class TiktokOrderItemController extends BaseController {

    private final ITiktokOrderItemService iTiktokOrderItemService;

    /**
     * 查询订单详情列表
     */
    @ApiOperation("查询订单详情列表")
    @GetMapping("/list")
    public TableDataInfo<TiktokOrderItemVo> list(@Validated TiktokOrderItemBo bo) {
        return iTiktokOrderItemService.queryPageList(bo);
    }

    /**
     * 导出订单详情列表
     */
    @ApiOperation("导出订单详情列表")
    @GetMapping("/export")
    public void export(@Validated TiktokOrderItemBo bo, HttpServletResponse response) {
        List<TiktokOrderItemVo> list = iTiktokOrderItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单详情", TiktokOrderItemVo.class, response);
    }

    /**
     * 获取订单详情详细信息
     */
    @ApiOperation("获取订单详情详细信息")
    @GetMapping("/{id}")
    public AjaxResult<TiktokOrderItemVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokOrderItemService.queryById(id));
    }

    /**
     * 新增订单详情
     */
    @ApiOperation("新增订单详情")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokOrderItemBo bo) {
        return toAjax(iTiktokOrderItemService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改订单详情
     */
    @ApiOperation("修改订单详情")
    @RepeatSubmit()
    @PutMapping()
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokOrderItemBo bo) {
        return toAjax(iTiktokOrderItemService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除订单详情
     */
    @ApiOperation("删除订单详情")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iTiktokOrderItemService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
