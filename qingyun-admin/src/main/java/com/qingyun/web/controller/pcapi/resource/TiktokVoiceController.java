package com.qingyun.web.controller.pcapi.resource;

import java.util.List;
import java.util.Arrays;

import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.resource.TiktokVoiceBo;
import com.qingyun.shop.domain.vo.TiktokVoiceVo;
import com.qingyun.shop.service.ITiktokVoiceService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.annotation.Log;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;

import com.qingyun.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 合成语音Controller
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Validated
@Api(value = "合成语音控制器", tags = {"合成语音管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/resource/voice")
public class TiktokVoiceController extends BaseController {

    private final ITiktokVoiceService iTiktokVoiceService;

    /**
     * 查询合成语音列表
     */
    @ApiOperation("查询合成语音列表 resource:voice:list")
    @PreAuthorize("@ss.hasPermi('resource:voice:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokVoiceVo> list(@Validated TiktokVoiceBo bo) {
        return iTiktokVoiceService.queryPageList(bo);
    }

    /**
     * 获取合成语音详细信息
     */
    @ApiOperation("获取合成语音详细信息 resource:voice:query")
    @PreAuthorize("@ss.hasPermi('resource:voice:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokVoiceVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokVoiceService.queryById(id));
    }

    /**
     * 新增合成语音
     */
    @ApiOperation("新增合成语音 resource:voice:add")
    @PreAuthorize("@ss.hasPermi('resource:voice:add')")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokVoiceBo bo) {
        return toAjax(iTiktokVoiceService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 删除合成语音
     */
    @ApiOperation("删除合成语音 resource:voice:remove")
    @PreAuthorize("@ss.hasPermi('resource:voice:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokVoiceService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
