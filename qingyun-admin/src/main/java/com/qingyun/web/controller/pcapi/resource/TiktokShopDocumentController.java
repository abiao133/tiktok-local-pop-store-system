package com.qingyun.web.controller.pcapi.resource;

import java.util.List;
import java.util.Arrays;

import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.resource.TiktokShopDocumentBo;
import com.qingyun.shop.domain.vo.TiktokShopDocumentVo;
import com.qingyun.shop.service.ITiktokShopDocumentService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 商户文案Controller
 *
 * @author qingyun
 * @date 2021-10-21
 */
@Validated
@Api(value = "商户文案控制器", tags = {"商户文案管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/resource/document")
public class TiktokShopDocumentController extends BaseController {

    private final ITiktokShopDocumentService iTiktokShopDocumentService;

    /**
     * 查询商户文案列表
     */
    @ApiOperation("查询商户文案列表 resource:document:list'")
    @PreAuthorize("@ss.hasPermi('resource:document:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokShopDocumentVo> list(TiktokShopDocumentBo bo) {
        return iTiktokShopDocumentService.queryPageList(bo);
    }

    /**
     * 导出商户文案列表
     */
    @ApiOperation("导出商户文案列表 resource:document:export")
    @PreAuthorize("@ss.hasPermi('resource:document:export')")
    @GetMapping("/export")
    public void export(@Validated TiktokShopDocumentBo bo, HttpServletResponse response) {
        List<TiktokShopDocumentVo> list = iTiktokShopDocumentService.queryList(bo);
        ExcelUtil.exportExcel(list, "商户文案", TiktokShopDocumentVo.class, response);
    }

    /**
     * 获取商户文案详细信息
     */
    @ApiOperation("获取商户文案详细信息 resource:document:query")
    @PreAuthorize("@ss.hasPermi('resource:document:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokShopDocumentVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokShopDocumentService.queryById(id));
    }

    /**
     * 新增商户文案
     */
    @ApiOperation("新增商户文案 resource:document:add")
    @PreAuthorize("@ss.hasPermi('resource:document:add')")
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokShopDocumentBo bo) {
        return toAjax(iTiktokShopDocumentService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户文案
     */
    @ApiOperation("修改商户文案 resource:document:edit")
    @PreAuthorize("@ss.hasPermi('resource:document:edit')")
    @RepeatSubmit()
    @PutMapping()
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokShopDocumentBo bo) {
        return toAjax(iTiktokShopDocumentService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户文案
     */
    @ApiOperation("删除商户文案 resource:document:remove")
    @PreAuthorize("@ss.hasPermi('resource:document:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokShopDocumentService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
