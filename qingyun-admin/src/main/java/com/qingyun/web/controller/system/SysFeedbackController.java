package com.qingyun.web.controller.system;

import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.security.controller.BaseController;
import com.qingyun.system.domain.bo.SysFeedbackAddBo;
import com.qingyun.system.domain.bo.SysFeedbackQueryBo;
import com.qingyun.system.domain.bo.SysFeedbackReplyBo;
import com.qingyun.system.domain.vo.SysFeedbackListVo;
import com.qingyun.system.domain.vo.SysFeedbackVo;
import com.qingyun.system.service.ISysFeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 用户反馈Controller
 *
 * @author qingyun
 * @date 2021-09-24
 */
@Validated
@Api(value = "用户反馈控制器", tags = {"用户反馈管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/feedback")
public class SysFeedbackController extends BaseController {

    private final ISysFeedbackService iSysFeedbackService;

    /**
     * 查询用户反馈列表
     */
    @ApiOperation("查询用户反馈列表 system:feedback:list")
    @PreAuthorize("@ss.hasPermi('system:feedback:list')")
    @GetMapping("/list")
    public TableDataInfo<SysFeedbackListVo> list(@Validated SysFeedbackQueryBo bo) {
        return iSysFeedbackService.queryPageList(bo);
    }


    /**
     * 获取用户反馈详细信息
     */
    @ApiOperation("获取用户反馈详细信息 system:feedback:query")
    @PreAuthorize("@ss.hasPermi('system:feedback:query')")
    @GetMapping("/{id}")
    public AjaxResult<SysFeedbackVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iSysFeedbackService.queryById(id));
    }

    /**
     * 新增用户反馈
     */
    @ApiOperation("商户端 创建用户反馈 system:feedback:add")
    @PreAuthorize("@ss.hasPermi('system:feedback:add')")
    @Log(title = "用户反馈", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody SysFeedbackAddBo bo) {
        return toAjax(iSysFeedbackService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 回复用户反馈
     */
    @ApiOperation("管理员回复反馈信息 system:feedback:reply")
    @PreAuthorize("@ss.hasPermi('system:feedback:reply')")
    @RepeatSubmit()
    @PutMapping("/reply")
    public AjaxResult<Void> reply(@Validated(EditGroup.class) @RequestBody SysFeedbackReplyBo bo) {
        return toAjax(iSysFeedbackService.reply(bo) ? 1 : 0);
    }

    /**
     * 删除用户反馈
     */
    @ApiOperation("删除用户反馈 system:feedback:remove")
    @PreAuthorize("@ss.hasPermi('system:feedback:remove')")
    @Log(title = "用户反馈" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iSysFeedbackService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
