package com.qingyun.web.controller.pcapi.statistics;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.eneity.StackedLine;
import com.qingyun.shop.eneity.VideoBasicData;
import com.qingyun.shop.mapper.DashboardMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Validated
@Api(value = "数据看板", tags = {"^pc端数据看板"})
@AllArgsConstructor
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

    private final DashboardMapper dashboardMapper;


    @ApiOperation("^获取所有商家的数量代理商数量")
    @GetMapping("/shopAndProxyCount")
    public AjaxResult getShopCount() {
        Long shopCount = dashboardMapper.getShopCount(null);
        Long proxyCount = dashboardMapper.getProxyCount(null);

        Map<String, Long> map = new HashMap<>();
        map.put("shopCount", shopCount);
        map.put("proxyCount", proxyCount);

        return AjaxResult.success(map);
    }

    //todo 活动数量 抖音用户数量
    @ApiOperation("^优惠券活动各状态数量")
    @GetMapping("/activityStatus")
    public AjaxResult getActivityStatus() {
        Long shopId;
        Map<String, Object> map;
        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();
            map = dashboardMapper.selectActivityStatus(shopId);
        } else {
            map = dashboardMapper.selectActivityStatus(null);
        }

        return AjaxResult.success(map);
    }

    @ApiOperation("^获取商户视频数据近七天的走势统计(折线图堆叠)")
    @GetMapping("/shopVideoStackedLine")
    public AjaxResult shopVideoStackedLine() {
        Long shopId = null;
        List<VideoBasicData> dataList;
        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();
            dataList = dashboardMapper.selectVideoData7Day(shopId);
        } else {
            dataList = dashboardMapper.selectVideoData7Day(null);
        }

        List<Object> result = new ArrayList<>();

        List<String> timeList = new ArrayList<>();

        List<Long> diggCount = new ArrayList<>();
        List<Long> downloadCount = new ArrayList<>();
        List<Long> forwardCount = new ArrayList<>();
        List<Long> playCount = new ArrayList<>();
        List<Long> shareCount = new ArrayList<>();
        List<Long> commentCount = new ArrayList<>();
        for (VideoBasicData videoBasicData : dataList) {
            timeList.add(videoBasicData.getTime());

            diggCount.add(videoBasicData.getDiggCount());
            downloadCount.add(videoBasicData.getDownloadCount());
            forwardCount.add(videoBasicData.getForwardCount());
            playCount.add(videoBasicData.getPlayCount());
            shareCount.add(videoBasicData.getShareCount());
            commentCount.add(videoBasicData.getCommentCount());
        }

        result.add(timeList);
        result.add(StackedLine.builder().name("点赞数量").type("line").stack("Total").data(diggCount).build());
        result.add(StackedLine.builder().name("下载数量").type("line").stack("Total").data(downloadCount).build());
        result.add(StackedLine.builder().name("转发数量").type("line").stack("Total").data(forwardCount).build());
        result.add(StackedLine.builder().name("播放数量").type("line").stack("Total").data(playCount).build());
        result.add(StackedLine.builder().name("分享数量").type("line").stack("Total").data(shareCount).build());
        result.add(StackedLine.builder().name("评论数量").type("line").stack("Total").data(commentCount).build());

        return AjaxResult.success(result);
    }

    @ApiOperation("^（管理员不可见）获取商户的优惠券领取、已使用、未使用、剩余数据统计")
    @GetMapping("/shopCouponInfo")
    public AjaxResult<Object> getShopCouponInfo() {

        List<Map<String, Object>> list = new ArrayList<>();

        if (!SecurityUtils.isManager()) {
            Long shopId = SecurityUtils.getShop().getId();

            // 获取优惠券id列表，取前5条
            List<Long> ids = dashboardMapper.selectIdByShopId(shopId, 5);
            ids.removeIf(Objects::isNull);
            //List<Long> couponIds = ids.stream().filter(Objects::nonNull).collect(Collectors.toList());

            ids.forEach(id -> {
                // 获取领取 领取、已使用、未使用、剩余数据统计
                Map<String, Object> map = dashboardMapper.selectCouponDrawStatus(id);

                list.add(map);
            });
        }

        return AjaxResult.success(list);

    }

    @ApiOperation("^获取商户基本数据 （资源数量、剩余视频发送数量、发布优惠券活动数量、）")
    @GetMapping("/shopInfo")
    public AjaxResult getShopInfo() {
        Map<String, Object> map = new HashMap<>();

        Long shopId = null;
        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();
        }

        Integer resourceCount = dashboardMapper.selectResourceCount(shopId);

        Integer videoSentCount = dashboardMapper.selectVideoSentCount(shopId);

        Integer activityCount = dashboardMapper.selectActivity(shopId);

        map.put("resourceCount", resourceCount);// 资源数量
        map.put("videoSentCount", videoSentCount);// 剩余视频发送数量
        map.put("activityCount", activityCount);// 发布优惠券活动数量
        return AjaxResult.success(map);
    }

    @ApiOperation("^获取订单基本数据 （订单状态、金额）")
    @GetMapping("/orderData")
    public AjaxResult getOrderData() {
        Long shopId;
        Map<String, Object> map;
        if (!SecurityUtils.isManager()) {
            shopId = SecurityUtils.getShop().getId();
            // 获取领取订单各状态数量和订单总金额
            map = dashboardMapper.selectOrderStatusCount(shopId);
        } else {
            map = dashboardMapper.selectOrderStatusCount(null);
        }

        return AjaxResult.success(map);
    }

}
