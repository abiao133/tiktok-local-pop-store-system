package com.qingyun.web.controller.pcapi.resource;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.exception.ServiceException;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokTag;
import com.qingyun.shop.domain.bo.tag.TiktokTagAddBo;
import com.qingyun.shop.domain.bo.tag.TiktokTagBo;
import com.qingyun.shop.domain.bo.tag.TiktokTagEditBo;
import com.qingyun.shop.domain.vo.TiktokTagSelectListVo;
import com.qingyun.shop.domain.vo.TiktokTagVo;
import com.qingyun.shop.service.ITiktokTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 标签Controller
 *
 * @author qingyun
 * @date 2021-09-26
 */
@Validated
@Api(value = "标签控制器", tags = {"^标签管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/resource/tag")
public class TiktokTagController extends BaseController {

    private final ITiktokTagService iTiktokTagService;

    /**
     * 查询标签列表
     */
    @ApiOperation("查询标签列表 resource:tag:list")
    @PreAuthorize("@ss.hasPermi('resource:tag:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokTagVo> list(@Validated TiktokTagBo bo) {
        return iTiktokTagService.queryPageList(bo);
    }

//    /**
//     * 导出标签列表
//     */
//    @ApiOperation("导出标签列表")
//    @PreAuthorize("@ss.hasPermi('system:tag:export')")
//    @Log(title = "标签", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public void export(@Validated TiktokTagBo bo, HttpServletResponse response) {
//        List<TiktokTagVo> list = iTiktokTagService.queryList(bo);
//        ExcelUtil.exportExcel(list, "标签", TiktokTagVo.class, response);
//    }

    /**
     * 获取标签详细信息
     */
    @ApiOperation("获取标签详细信息 resource:tag:query")
    @PreAuthorize("@ss.hasPermi('resource:tag:query')")
    @GetMapping("/{tId}")
    public AjaxResult<TiktokTagVo> getInfo(@PathVariable("tId") Long tId) {
        return AjaxResult.success(iTiktokTagService.queryById(tId));
    }

    /**
     * 新增标签
     */
    @ApiOperation("新增标签 resource:tag:add")
    @PreAuthorize("@ss.hasPermi('resource:tag:add')")
    @Log(title = "标签", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/add")
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokTagAddBo bo) {
        return toAjax(iTiktokTagService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改标签
     */
    @ApiOperation("修改标签 resource:tag:edit")
    @PreAuthorize("@ss.hasPermi('resource:tag:edit')")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/edit")
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokTagEditBo bo) {
        return toAjax(iTiktokTagService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除标签
     */
    @ApiOperation("删除标签 resource:tag:remove")
    @PreAuthorize("@ss.hasPermi('resource:tag:remove')")
    @Log(title = "标签" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{tIds}")
    public AjaxResult<Void> remove(@PathVariable Long[] tIds) {
        return toAjax(iTiktokTagService.deleteWithValidByIds(Arrays.asList(tIds), true) ? 1 : 0);
    }

    @ApiOperation("启用或禁用标签 resource:tag:nodifyStatus")
    @PreAuthorize("@ss.hasPermi('resource:tag:nodifyStatus')")
    @PostMapping("/nodifyStatus")
    @RepeatSubmit()
    public AjaxResult<Void> nodifyStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        TiktokTag tag = iTiktokTagService.getOne(Wrappers.query(new TiktokTag().setTId(id)));

        if (!SecurityUtils.isManager() && tag.getIsDefault() == 0) {
            throw new ServiceException("您不能修改平台创建的标签");
        }

        tag.setTId(id);
        tag.setStatus(status);
        return toAjax(iTiktokTagService.updateById(tag));
    }

    @ApiOperation("标签select选择框")
    @GetMapping("/getSelectTags")
    public AjaxResult getSelectTags() {
        List<TiktokTagSelectListVo> result = iTiktokTagService.getSelectTags();
        return AjaxResult.success(result);
    }
}
