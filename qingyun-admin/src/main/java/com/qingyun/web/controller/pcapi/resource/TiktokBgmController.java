package com.qingyun.web.controller.pcapi.resource;

import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.config.QingYunConfig;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.file.FileUploadUtils;
import com.qingyun.common.utils.file.MimeTypeUtils;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmAddBo;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmEditBo;
import com.qingyun.shop.domain.bo.bgm.TiktokBgmQueryBo;
import com.qingyun.shop.domain.vo.TiktokBgmDetailVo;
import com.qingyun.shop.domain.vo.TiktokBgmVo;
import com.qingyun.shop.service.ITiktokBgmService;
import com.qingyun.system.service.ISysOssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * bgm资源Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "bgm资源控制器", tags = {"^bgm资源管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/resource/bgm")
public class TiktokBgmController extends BaseController {

    private final ISysOssService iSysOssService;

    private final ITiktokBgmService iTiktokBgmService;

    private final QingYunConfig qingYunConfig;
    /**
     * 查询bgm资源列表
     */
    @ApiOperation("^查询bgm资源列表 resource:bgm:list")
    @PreAuthorize("@ss.hasPermi('resource:bgm:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokBgmVo> list(TiktokBgmQueryBo bo) {
        return iTiktokBgmService.queryPageList(bo);
    }

    /**
     * 导出bgm资源列表
     */
    @ApiOperation("导出bgm资源列表 resource:bgm:export")
    @PreAuthorize("@ss.hasPermi('resource:bgm:export')")
    @Log(title = "bgm资源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(TiktokBgmQueryBo bo, HttpServletResponse response) {
        List<TiktokBgmVo> list = iTiktokBgmService.queryList(bo);
        ExcelUtil.exportExcel(list, "bgm资源", TiktokBgmVo.class, response);
    }

    /**
     * 获取bgm资源详细信息
     */
    @ApiOperation("^获取bgm资源详细信息 resource:bgm:query")
    @PreAuthorize("@ss.hasPermi('resource:bgm:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokBgmDetailVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokBgmService.queryById(id));
    }

    /**
     * 新增bgm资源
     */
    @ApiOperation("^新增bgm资源 resource:bgm:add")
    @PreAuthorize("@ss.hasPermi('resource:bgm:add')")
    @Log(title = "bgm资源", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokBgmAddBo bo) {
        return toAjax(iTiktokBgmService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改bgm资源
     */
    @ApiOperation("^修改bgm资源 resource:bgm:edit")
    @PreAuthorize("@ss.hasPermi('resource:bgm:edit')")
    @Log(title = "bgm资源", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokBgmEditBo bo) {
        return toAjax(iTiktokBgmService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除bgm资源
     */
    @ApiOperation("^删除bgm资源")
    @PreAuthorize("@ss.hasPermi('resource:bgm:remove')")
    @Log(title = "bgm资源" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTiktokBgmService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    /**
     * 上传音乐文件 mp3格式
     */
    @ApiOperation("^上传音乐文件 mp3格式")
    @PreAuthorize("@ss.hasPermi('resource:bgm:upload')")
    @PostMapping("/upload")
    public AjaxResult upload(@RequestPart("file") MultipartFile file)  {
    	try {
			if (ObjectUtil.isNull(file) || file.getSize() < 1) {
				return AjaxResult.success("上传文件不能为空", null);
			}
			FileUploadUtils.assertAllowed(file, MimeTypeUtils.MP3);
//        SysOss oss = iSysOssService.upload(file);
			String filePath = QingYunConfig.getUploadPath();
			// 上传并返回新文件名称
			String fileName = FileUploadUtils.upload(filePath, file);
			String url = QingYunConfig.getProjectUrl() + fileName;
			Map<String, String> map = new HashMap<>(2);
			ArrayList<Map<String, String>> reslut = new ArrayList<>();
			reslut.add(map);
			map.put("url", url);
			map.put("fileName", fileName);
			return AjaxResult.success(reslut);
		} catch (Exception e) {
		return AjaxResult.error(e.getMessage(), null);
	    }
    }
}
