package com.qingyun.web.controller.pcapi.branch;

import java.util.List;
import java.util.Arrays;

import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.TiktokProxy;
import com.qingyun.shop.domain.bo.branch.TiktokBranchRecordBo;
import com.qingyun.shop.domain.vo.TiktokBranchRecordVo;
import com.qingyun.shop.service.ITiktokBranchRecordService;
import com.qingyun.shop.service.ITiktokProxyService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 分销返利记录Controller
 *
 * @author qingyun
 * @date 2021-10-19
 */
@Validated
@Api(value = "分销返利记录控制器", tags = {"分销返利记录管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/branch/record")
public class TiktokBranchRecordController extends BaseController {

    private final ITiktokBranchRecordService iTiktokBranchRecordService;

    private final ITiktokProxyService proxyService;

    /**
     * 查询分销返利记录列表
     */
    @ApiOperation("查询分销返利记录列表")
    @PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokBranchRecordVo> list(@Validated TiktokBranchRecordBo bo) {
        return iTiktokBranchRecordService.queryPageList(bo);
    }

    /**
     * 导出分销返利记录列表
     */
    @ApiOperation("导出分销返利记录列表")
    @PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "分销返利记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TiktokBranchRecordBo bo, HttpServletResponse response) {
        List<TiktokBranchRecordVo> list = iTiktokBranchRecordService.queryList(bo);
        ExcelUtil.exportExcel(list, "分销返利记录", TiktokBranchRecordVo.class, response);
    }

    /**
     * 获取分销返利记录详细信息
     */
    @ApiOperation("获取分销返利记录详细信息")
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokBranchRecordVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokBranchRecordService.queryById(id));
    }

	@ApiOperation("代理商获取余额")
	@GetMapping("getBalance")
	public AjaxResult getBalance(){
		if (!SecurityUtils.isProxy()) {
			return AjaxResult.error("你没有权限获取此数据");
		}
		Long id = SecurityUtils.getProxy().getId();
		TiktokProxy one = proxyService.lambdaQuery()
			.select(TiktokProxy::getTotalBalance,
				TiktokProxy::getUsableBalance,
				TiktokProxy::getFreezeBalance)
			.eq(TiktokProxy::getId, id).one();
		return AjaxResult.success(one);
	}

}
