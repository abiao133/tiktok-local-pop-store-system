package com.qingyun.web.controller.wxapi;

import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.security.controller.BaseController;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyAddBo;
import com.qingyun.shop.domain.bo.TiktokWithdrawApplyQueryBo;
import com.qingyun.shop.domain.vo.TiktokWithdrawApplyVo;
import com.qingyun.shop.service.ITiktokWithdrawApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@Api(value = "代理商提现申请控制器", tags = {"小程序-代理商提现申请管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/miniapp/branch/apply")
public class WithdrawApplyController extends BaseController {

    private final ITiktokWithdrawApplyService iTiktokWithdrawApplyService;

    /**
     * 查询商户提现申请列表
     */
    @ApiOperation("查询代理商提现申请列表")
    @GetMapping("/list")
    public TableDataInfo<TiktokWithdrawApplyVo> list(@Validated TiktokWithdrawApplyQueryBo bo) {
        return iTiktokWithdrawApplyService.queryPageList(bo);
    }

    /**
     * 获取商户提现申请详细信息
     */
    @ApiOperation("获取代理商提现申请详细信息")
    @GetMapping("/{id}")
    public AjaxResult<TiktokWithdrawApplyVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokWithdrawApplyService.queryById(id));
    }

    /**
     * 新增商户提现申请
     */
    @ApiOperation("小程序代理商提现申请")
    @Log(title = "小程序平台代理商提现申请", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokWithdrawApplyAddBo bo) {
        return toAjax(iTiktokWithdrawApplyService.insertByBo(bo) ? 1 : 0);
    }
}
