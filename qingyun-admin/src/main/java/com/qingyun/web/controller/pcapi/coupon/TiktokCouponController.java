package com.qingyun.web.controller.pcapi.coupon;

import cn.hutool.core.util.ObjectUtil;
import com.qingyun.common.annotation.Log;
import com.qingyun.common.annotation.RepeatSubmit;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.core.validate.EditGroup;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.security.controller.BaseController;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponAddBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponBo;
import com.qingyun.shop.domain.bo.coupon.TiktokCouponQueryBo;
import com.qingyun.shop.domain.vo.TiktokCouponListVo;
import com.qingyun.shop.domain.vo.TiktokCouponVo;
import com.qingyun.shop.service.ITiktokCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 优惠券Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api(value = "优惠券控制器", tags = {"^优惠券管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/shop/coupon")
public class    TiktokCouponController extends BaseController {

    private final ITiktokCouponService iTiktokCouponService;

    /**
     * 查询优惠券列表
     */
    @ApiOperation("查询优惠券列表 shop:coupon:list")
    @PreAuthorize("@ss.hasPermi('shop:coupon:list')")
    @GetMapping("/list")
    public TableDataInfo<TiktokCouponListVo> list( TiktokCouponQueryBo bo) {
        return iTiktokCouponService.queryPageList(bo);
    }

    /**
     * 导出优惠券列表
     */
    @ApiOperation("导出优惠券列表 shop:coupon:export")
    @PreAuthorize("@ss.hasPermi('shop:coupon:export')")
    @Log(title = "优惠券", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TiktokCouponQueryBo bo, HttpServletResponse response) {
        List<TiktokCouponVo> list = iTiktokCouponService.queryList(bo);
        ExcelUtil.exportExcel(list, "优惠券", TiktokCouponVo.class, response);
    }

    /**
     * 获取优惠券详细信息
     */
    @ApiOperation("获取优惠券详细信息 shop:coupon:query")
    @PreAuthorize("@ss.hasPermi('shop:coupon:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokCouponVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokCouponService.queryById(id));
    }

    /**
     * 新增优惠券
     */
    @ApiOperation("新增优惠券 shop:coupon:add")
    @PreAuthorize("@ss.hasPermi('shop:coupon:add')")
    @Log(title = "优惠券", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public AjaxResult<Void> add(@Valid @RequestBody TiktokCouponAddBo bo) {
		if (!SecurityUtils.isManager()) {
			if(ObjectUtil.isNotNull(bo.getShopId())){
				return AjaxResult.error("你只能创建自己的优惠卷哦！");
			}
		}
        return toAjax(iTiktokCouponService.insertByBo(bo) ? 1 : 0);
    }


    /**
     * 修改优惠券
     */
    @ApiOperation("修改优惠券 shop:coupon:edit")
    @PreAuthorize("@ss.hasPermi('shop:coupon:edit')")
    @Log(title = "优惠券", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
	@CacheEvict(cacheNames = "selectCoupon",allEntries = true)
    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokCouponBo bo) {
		if (!SecurityUtils.isManager()) {
			if(ObjectUtil.isNull(bo.getShopId())){
				return AjaxResult.error("你只能修改自己的优惠卷哦！");
			}
		}
        return toAjax(iTiktokCouponService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除优惠券
     */
    @ApiOperation("删除优惠券 shop:coupon:remove")
    @PreAuthorize("@ss.hasPermi('shop:coupon:remove')")
    @Log(title = "优惠券" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iTiktokCouponService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @ApiOperation("上下架优惠券 1待上架 2已上架")
    @PreAuthorize("@ss.hasPermi('shop:coupon:edit')")
    @PostMapping("/modifiStatus")
    @RepeatSubmit(intervalTime = 5000)
	@CacheEvict(cacheNames = "selectCoupon",allEntries = true)
    public AjaxResult<Void> modifiStatus(@RequestParam("id") Long id,
                                         @RequestParam("status") Integer status) {
        return toAjax(iTiktokCouponService.modifiCouponStatus(id, status));
    }
}
