package com.qingyun.web.security.handle;

import cn.hutool.core.util.StrUtil;

import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.security.handle.AuthenticationEntryPointImpl;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author dangyonghang
 */
@Component
public class AdminAuthenticationEntryPointImpl extends AuthenticationEntryPointImpl {
    @Override
    public AjaxResult<Void> callback(HttpServletRequest request, AuthenticationException e) {
        String msg = StrUtil.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        return  AjaxResult.oauthFail(msg);
    }
}
