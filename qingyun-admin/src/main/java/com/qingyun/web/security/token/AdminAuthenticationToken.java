package com.qingyun.web.security.token;


import com.qingyun.security.token.MyAuthenticationToken;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

@Setter
@Getter
public class AdminAuthenticationToken extends MyAuthenticationToken {

    private String code;
    private String uuid;

    public AdminAuthenticationToken(Object principal, Object credentials,String code,String uuid) {
        super(principal, credentials);
        this.code=code;
        this.uuid=uuid;
    }
    public AdminAuthenticationToken(UserDetails principal, Object credentials) {
        super(principal, credentials, principal.getAuthorities());
    }

}
