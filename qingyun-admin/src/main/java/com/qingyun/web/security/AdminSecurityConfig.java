package com.qingyun.web.security;


import com.qingyun.framework.web.service.AsyncService;
import com.qingyun.security.config.SecurityConfig;
import com.qingyun.web.security.filter.*;
import com.qingyun.web.security.handle.LoginAuthFailedHandler;
import com.qingyun.web.security.handle.LoginAuthSuccessHandler;
import com.qingyun.web.security.handle.LogoutSuccessHandlerImpl;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;


/**
 * @author dangyonghang
 */
@Component
@AllArgsConstructor
public class AdminSecurityConfig extends SecurityConfig {

    /**
     * 退出处理类
     */
    private LogoutSuccessHandlerImpl logoutSuccessHandler;


    private LoginAuthenticationFilter loginAuthenticationFilter;
    /**
     * 认证判断过滤器
     */
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;


    private LoginAuthFailedHandler loginAuthFailedHandler;

    private LoginAuthSuccessHandler loginAuthSuccessHandler;

    private AsyncService asyncService;


    @SneakyThrows
    @Override
    public void httpDistribute(HttpSecurity httpSecurity) {
        httpSecurity.authorizeRequests()
                // 过滤请求
                // 对于登录login 验证码captchaImage 允许匿名访问
                .antMatchers("/admin/login","/tiktok/login","/captchaImage","/miniapp/login").anonymous()
                .antMatchers("/dev-api/**").permitAll()
//                .antMatchers("/upload/**").permitAll()
                .antMatchers(
                        HttpMethod.GET,
                        "/*.html",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                ).permitAll()
			    //抖音授权回调地址
			    .antMatchers("/system/shop/manager/shopAuth").permitAll()
			    .antMatchers("/resource/bgm/upload").permitAll()
                .antMatchers("/admin/**").permitAll()
			    //公共资源上传
                .antMatchers("/common/**").permitAll()
			     .antMatchers("/tikTok/api/checkToken").permitAll()
			     //支付回调
			     .antMatchers("/miniapp/order/notice/payOrder").permitAll()
			     .antMatchers("/order/notice/payOrder").permitAll()
			      .antMatchers("/system/oss/upload").permitAll()
			    //资源访问路径
                .antMatchers("/profile/**").anonymous()
			     .antMatchers("/test/**").permitAll()
			    .antMatchers("/Sms/getLoginCatcha").anonymous()
                .antMatchers("/swagger-ui.html").anonymous()
                .antMatchers("/swagger-resources/**").anonymous()
                .antMatchers("/webjars/**").anonymous()
                .antMatchers("/druid/**").anonymous()
			.antMatchers("/webhooks/**").anonymous()


//测试
			.antMatchers("/tikTok/Coupon/api/getCouponVideo").permitAll()
			.antMatchers("/tikTok/Coupon/api/getH5Scheme").permitAll()
			.antMatchers("/system/tkUser/getShareId").permitAll()

               // .antMatchers("/tenant/**").anonymous()
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated()
                .and()
                .headers().frameOptions().disable();


       // httpSecurity.exceptionHandling().authenticationEntryPoint(unauthorizedHandler);

        httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
        // 添加JWT filter
        httpSecurity.addFilterBefore(miniAppPwdLoginAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		httpSecurity.addFilterBefore(tikTokLoginAuthenticationFilter(),UsernamePasswordAuthenticationFilter.class);
        httpSecurity.addFilterBefore(loginAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

    }

    @Bean
  public TikTokLoginAuthenticationFilter tikTokLoginAuthenticationFilter() throws Exception {
	  TikTokLoginAuthenticationFilter tikTokLoginAuthenticationFilter = new TikTokLoginAuthenticationFilter();
	  tikTokLoginAuthenticationFilter.setAuthenticationManager(super.authenticationManagerBean());
	  tikTokLoginAuthenticationFilter.setAuthenticationFailureHandler(loginAuthFailedHandler);
	  tikTokLoginAuthenticationFilter.setAuthenticationSuccessHandler(loginAuthSuccessHandler);
	  return tikTokLoginAuthenticationFilter;
  }

  @Bean
  public MiniAppLoginAuthenticationFilter miniAppPwdLoginAuthenticationFilter() throws Exception {
	  MiniAppLoginAuthenticationFilter miniAppPwdLoginAuthenticationFilter = new MiniAppLoginAuthenticationFilter();
	  miniAppPwdLoginAuthenticationFilter.setAuthenticationManager(super.authenticationManagerBean());
	  miniAppPwdLoginAuthenticationFilter.setAuthenticationFailureHandler(loginAuthFailedHandler);
	  miniAppPwdLoginAuthenticationFilter.setAuthenticationSuccessHandler(loginAuthSuccessHandler);
	  return miniAppPwdLoginAuthenticationFilter;
  }


    /**
     * 强散列哈希加密实现
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

}
