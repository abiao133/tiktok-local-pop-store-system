/*
 * Copyright (c) 2018-2999 广州亚米信息科技有限公司 All rights reserved.
 *
 * https://www.gz-yami.com/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.qingyun.web.security.handle;

import cn.hutool.core.util.CharsetUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.qingyun.common.constant.Constants;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.domain.model.LoginUser;
import com.qingyun.common.core.redis.RedisCache;
import com.qingyun.framework.web.service.TokenService;
import com.qingyun.security.model.shop.AppLoginUser;
import com.qingyun.security.model.tiktok.TikTokLoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


/**
 * 登录成功，返回oauth token
 * @author dangyonghang
 */
@Component
@Slf4j
public class   LoginAuthSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TokenService tokenService;

    @Autowired
    private RedisCache redisCache;

    /**
     * Called when a user has been successfully authenticated.
     * 调用spring security oauth API 生成 oAuth2AccessToken
     *
     * @param request        the request which caused the successful authentication
     * @param response       the response
     * @param authentication the <tt>Authentication</tt> object which was created during
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        String token;
		HashMap<String, Object> map = new HashMap<>();
        if (request.getRequestURI().contains("/tiktok/login")){
			TikTokLoginUser tikTokLoginUser = (TikTokLoginUser) authentication.getPrincipal();
			token=tokenService.createTiktokUserToken(tikTokLoginUser);
			map.put(Constants.ISONELOGIN,tikTokLoginUser.isOneLogin());
		}else if (request.getRequestURI().contains("/miniapp/login")){
			AppLoginUser appLoginUser=(AppLoginUser)authentication.getPrincipal();
			token=tokenService.createAppToken(appLoginUser);
		}else{
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            token = tokenService.createToken(loginUser);

        }

		map.put(Constants.TOKEN, token);
		AjaxResult ajax = AjaxResult.success(map);
        response.setCharacterEncoding(CharsetUtil.UTF_8);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        response.setStatus(HttpStatus.OK.value());
        PrintWriter printWriter = response.getWriter();
        printWriter.append(objectMapper.writeValueAsString(ajax));
        printWriter.flush();
        printWriter.close();
    }


}
