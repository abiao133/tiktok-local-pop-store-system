package com.qingyun.web.security.token;


import com.qingyun.security.token.MyAuthenticationToken;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @Classname TikTokAuthenticationToken
 * @Author dyh
 * @Date 2021/9/8 15:25
 */
@Setter
@Getter
public class TikTokAuthenticationToken extends MyAuthenticationToken {

	private String code;

	private Integer shopId;

	public TikTokAuthenticationToken( String code,Integer shopId) {
		super(code, shopId);
		this.code=code;
		this.shopId=shopId;
	}

	public TikTokAuthenticationToken(UserDetails principal, Object credentials) {
		super(principal, credentials, principal.getAuthorities());
	}
}
