package com.qingyun.web.security.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HttpCodeStatus {
    //认证成功状态
    SUCCESS(200),
    //认证失败状态
    Fail(401);
    private Integer code;


}
