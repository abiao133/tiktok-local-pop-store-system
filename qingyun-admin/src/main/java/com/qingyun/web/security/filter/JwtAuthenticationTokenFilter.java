package com.qingyun.web.security.filter;

import cn.hutool.core.lang.Validator;

import com.qingyun.common.core.domain.model.LoginUser;
import com.qingyun.framework.web.service.TokenService;
import com.qingyun.security.model.shop.AppLoginUser;
import com.qingyun.security.model.tiktok.TikTokLoginUser;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.web.security.token.AdminAuthenticationToken;
import com.qingyun.web.security.token.TikTokAuthenticationToken;
import com.qingyun.web.security.token.WxMiniAppAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token过滤器 验证token有效性
 *
 * @author ruoyi
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter
{
    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {
        if (request.getRequestURI().contains("/tikTok")) {
			TikTokLoginUser tikTokUser = tokenService.getTikTokUser(request);
			if (Validator.isNotNull(tikTokUser) && Validator.isNull(SecurityUtils.getAuthentication())) {
                tokenService.verifyTikTokToken(tikTokUser);
				TikTokAuthenticationToken tikTokAuthenticationToken = new TikTokAuthenticationToken(tikTokUser, null);
				tikTokAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(tikTokAuthenticationToken);
            }
        }else if(request.getRequestURI().contains("/miniapp")){
			AppLoginUser appUser = tokenService.getAppUser(request);
			if (Validator.isNotNull(appUser) && Validator.isNull(SecurityUtils.getAuthentication())) {
				tokenService.verifyAppToken(appUser);
				WxMiniAppAuthenticationToken authenticationToken = new WxMiniAppAuthenticationToken(appUser,null);
				authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		} else {
            LoginUser loginUser = tokenService.getLoginUser(request);
            if (Validator.isNotNull(loginUser) && Validator.isNull(SecurityUtils.getAuthentication())) {
                tokenService.verifyToken(loginUser);
                AdminAuthenticationToken authenticationToken = new AdminAuthenticationToken(loginUser, null);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        chain.doFilter(request, response);
    }
}
