package com.qingyun.web.security.token;


import com.qingyun.security.enums.LoginType;
import com.qingyun.security.token.MyAuthenticationToken;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

@Setter
@Getter
public class WxMiniAppAuthenticationToken extends MyAuthenticationToken {


    private LoginType loginType;
    private String wxcode;

    public WxMiniAppAuthenticationToken(Object principal, Object credentials, String wxcode , LoginType loginType) {
        super(principal, credentials);
        this.wxcode=wxcode;
        this.loginType=loginType;
    }

    public WxMiniAppAuthenticationToken(UserDetails principal, Object credentials) {
        super(principal, credentials, principal.getAuthorities());
    }

}
