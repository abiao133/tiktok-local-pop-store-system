package com.qingyun.system.domain.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class SysFeedbackAddBo {

    /**
     * 反馈的内容
     */
    @ApiModelProperty(value = "反馈的内容")
    @NotBlank(message = "反馈的内容不能为空")
    private String content;

    /**
     * 反馈的类型
     */
    @ApiModelProperty(value = "反馈的类型", required = true)
    @NotBlank(message = "反馈的类型不能为空")
    private String type;

    @ApiModelProperty("图片地址 数组")
    private List<String> picsList;
}
