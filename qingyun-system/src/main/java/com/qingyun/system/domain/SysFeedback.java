package com.qingyun.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户反馈对象 sys_feedback
 *
 * @author qingyun
 * @date 2021-09-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_feedback")
public class SysFeedback implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 商户id
     */
    private Long shopId;

    /**
     * 反馈的内容
     */
    private String content;

    /**
     * 图片组 多张用英文逗号隔开
     */
    private String pics;

    /**
     * 反馈的类型
     */
    private String type;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 反馈状态：0 进行中 1 已完成 2 已拒绝
     */
    private Integer feedbackStatus;

    /**
     * 回复
     */
    private String reply;

    /**
     * 处理的管理员id
     */
    private Long adminId;

    /**
     * 删除标记
     */
    @TableLogic
    private Integer delFlag;

}
