package com.qingyun.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 用户反馈视图对象 sys_feedback
 *
 * @author qingyun
 * @date 2021-09-24
 */
@Data
@ApiModel("用户反馈视图对象")
@ExcelIgnoreUnannotated
public class SysFeedbackVo {

	private static final long serialVersionUID = 1L;

	/**
     *  id
     */
	@ApiModelProperty("id")
	private Long id;

    /**
     * 用户id
     */
	@ExcelProperty(value = "用户id")
	@ApiModelProperty("用户id")
	private Long userId;

    /**
     * 商户id
     */
	@ExcelProperty(value = "商户id")
	@ApiModelProperty("商户id")
	private Long shopId;

    /**
     * 反馈的内容
     */
	@ExcelProperty(value = "反馈的内容")
	@ApiModelProperty("反馈的内容")
	private String content;

	@ApiModelProperty("图片列表")
	private List<String> picList;

    /**
     * 反馈的类型
     */
	@ExcelProperty(value = "反馈的类型")
	@ApiModelProperty("反馈的类型")
	private String type;

    /**
     * 反馈状态：0 进行中 1 已完成 2 已拒绝
     */
	@ExcelProperty(value = "反馈状态：0 进行中 1 已完成 2 已拒绝")
	@ApiModelProperty("反馈状态：0 进行中 1 已完成 2 已拒绝")
	private Integer feedbackStatus;

    /**
     * 回复
     */
	@ExcelProperty(value = "管理员回复")
	@ApiModelProperty("管理员回复")
	private String reply;

    /**
     * 处理的管理员id
     */
	@ExcelProperty(value = "处理的管理员id")
	@ApiModelProperty("处理的管理员id")
	private Long adminId;


}
