package com.qingyun.system.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 用户反馈业务对象 sys_feedback
 *
 * @author qingyun
 * @date 2021-09-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户反馈业务对象")
public class SysFeedbackBo extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    @NotNull(message = "商户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long shopId;

    /**
     * 反馈的内容
     */
    @ApiModelProperty(value = "反馈的内容")
    private String content;

    /**
     * 反馈的类型
     */
    @ApiModelProperty(value = "反馈的类型", required = true)
    @NotBlank(message = "反馈的类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 反馈状态：0 进行中 1 已完成 2 已拒绝
     */
    @ApiModelProperty(value = "反馈状态：0 进行中 1 已完成 2 已拒绝")
    private Integer feedbackStatus;

    /**
     * 回复
     */
    @ApiModelProperty(value = "回复")
    private String reply;

    /**
     * 处理的管理员id
     */
    @ApiModelProperty(value = "处理的管理员id")
    private Long adminId;

    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记", required = true)
    @NotNull(message = "删除标记不能为空", groups = { AddGroup.class })
    private Integer delFlag;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
