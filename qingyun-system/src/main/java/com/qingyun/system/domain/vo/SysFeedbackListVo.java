package com.qingyun.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class SysFeedbackListVo {

    @ApiModelProperty("反馈表id")
    private Long id;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("商户名")
    private String shopName;

    @ApiModelProperty("反馈的类型")
    private String type;

    @ApiModelProperty("反馈状态：0 进行中 1 已完成 2 已拒绝")
    private Integer feedbackStatus;

    @ApiModelProperty("创建时间")
    private Date createTime;
}
