package com.qingyun.system.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户反馈业务对象")
public class SysFeedbackQueryBo {

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long shopId;

    /**
     * 反馈状态：0 进行中 1 已完成 2 已拒绝
     */
    @ApiModelProperty(value = "反馈状态：0 进行中 1 已完成 2 已拒绝")
    private Integer feedbackStatus;

    /**
     * 反馈的类型
     */
    @ApiModelProperty(value = "反馈的类型")
    private String type;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;
}
