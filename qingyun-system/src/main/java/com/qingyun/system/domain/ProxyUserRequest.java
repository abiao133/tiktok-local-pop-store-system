package com.qingyun.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.qingyun.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * @Classname ProxyUserVo
 * @Author dyh
 * @Date 2021/9/10 18:23
 */
@Data
@ApiModel("代理商用户参数对象")
public class ProxyUserRequest extends BaseEntity {

	@ApiModelProperty("代理商名称")
	private String proxyName;

	@ApiModelProperty("代理商负责人")
	private String leader;

	@ApiModelProperty(value = "代理商id",required = true)
	private Long proxyId;
	/**
	 * 用户昵称
	 */
	@Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
	@ApiModelProperty("用户账号")
	private String userName;

	@ApiModelProperty("名称")
	private String nickName;
	/**
	 * 用户类型（00系统用户）01代理 02商家
	 */
	private String userType;

	/**
	 * 手机号码
	 */
	@ApiModelProperty("手机号码")
	private String phonenumber;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	@ApiModelProperty("账号状态")
	private String status;

	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建开始时间")
	private Date beginTime;

	@ApiModelProperty("创建结束时间")
	private Date endTime;


	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

    private List<Long> ids;
}
