package com.qingyun.system.domain.bo;

import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@ApiModel("云存储配置查询对象")
public class SysOssConfigQueryBo {

	/**
	 * 桶名称
	 */
	@ApiModelProperty(value = "bucketName")
	private String bucketName;

	/**
	 * 状态（0=正常,1=停用）
	 */
	@ApiModelProperty("状态（0=正常,1=停用）")
	private String status;

	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
}
