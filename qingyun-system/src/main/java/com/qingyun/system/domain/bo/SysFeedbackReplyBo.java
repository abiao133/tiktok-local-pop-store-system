package com.qingyun.system.domain.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SysFeedbackReplyBo {

    @ApiModelProperty(value = "id")
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     * 反馈状态：0 进行中 1 已完成 2 已拒绝
     */
    @ApiModelProperty(value = "反馈状态：0 进行中 1 已完成 2 已拒绝")
    private Integer feedbackStatus;

    /**
     * 回复
     */
    @ApiModelProperty(value = "回复")
    private String reply;

}
