package com.qingyun.system.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author qingyun
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenu> {

}
