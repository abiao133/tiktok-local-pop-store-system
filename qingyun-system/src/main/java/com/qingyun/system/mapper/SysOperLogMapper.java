package com.qingyun.system.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author qingyun
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLog> {

}
