package com.qingyun.system.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.SysRoleDept;

/**
 * 角色与部门关联表 数据层
 *
 * @author ruoyi
 */
public interface SysRoleDeptMapper extends BaseMapperPlus<SysRoleDept> {

}
