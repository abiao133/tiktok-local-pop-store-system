package com.qingyun.system.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.system.domain.SysFeedback;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.bo.SysFeedbackQueryBo;
import com.qingyun.system.domain.vo.SysFeedbackListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 用户反馈Mapper接口
 *
 * @author qingyun
 * @date 2021-09-24
 */
public interface SysFeedbackMapper extends BaseMapperPlus<SysFeedback> {

    Page<SysFeedbackListVo> selectFeedbackList(@Param("page") Page<Object> page, @Param("query") SysFeedbackQueryBo query);
}
