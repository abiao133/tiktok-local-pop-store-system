package com.qingyun.system.mapper;

import com.qingyun.common.core.domain.entity.SysDictType;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author ruoyi
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictType> {

}
