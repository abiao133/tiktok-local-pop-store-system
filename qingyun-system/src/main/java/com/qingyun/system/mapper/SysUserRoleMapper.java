package com.qingyun.system.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.SysUserRole;

/**
 * 用户与角色关联表 数据层
 *
 * @author qingyun
 */
public interface SysUserRoleMapper extends BaseMapperPlus<SysUserRole> {

}
