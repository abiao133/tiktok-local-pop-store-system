package com.qingyun.system.mapper;

import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.system.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author ruoyi
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininfor> {

}
