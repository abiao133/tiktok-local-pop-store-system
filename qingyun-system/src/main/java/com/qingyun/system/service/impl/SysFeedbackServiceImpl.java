package com.qingyun.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.system.domain.SysFeedback;
import com.qingyun.system.domain.bo.SysFeedbackAddBo;
import com.qingyun.system.domain.bo.SysFeedbackBo;
import com.qingyun.system.domain.bo.SysFeedbackQueryBo;
import com.qingyun.system.domain.bo.SysFeedbackReplyBo;
import com.qingyun.system.domain.vo.SysFeedbackListVo;
import com.qingyun.system.domain.vo.SysFeedbackVo;
import com.qingyun.system.mapper.SysFeedbackMapper;
import com.qingyun.system.service.ISysFeedbackService;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 用户反馈Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-24
 */
@Service
@AllArgsConstructor
public class SysFeedbackServiceImpl extends ServicePlusImpl<SysFeedbackMapper, SysFeedback, SysFeedbackVo> implements ISysFeedbackService {

    private final MapperFacade mapperFacade;

    @Override
    public SysFeedbackVo queryById(Long id) {
        SysFeedback feedback = baseMapper.selectById(id);
        SysFeedbackVo vo = mapperFacade.map(feedback, SysFeedbackVo.class);
        if (Objects.nonNull(feedback.getPics())) {
            vo.setPicList(Arrays.asList(feedback.getPics().split(",")));
        }
        return vo;
    }

    @Override
    public TableDataInfo<SysFeedbackListVo> queryPageList(SysFeedbackQueryBo bo) {
        Page<SysFeedbackListVo> result = baseMapper.selectFeedbackList(PageUtils.buildPage(), bo);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<SysFeedbackVo> queryList(SysFeedbackQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<SysFeedback> buildQueryWrapper(SysFeedbackQueryBo bo) {

        LambdaQueryWrapper<SysFeedback> lqw = Wrappers.lambdaQuery();

        lqw.eq(bo.getShopId() != null, SysFeedback::getShopId, bo.getShopId());
        lqw.eq(bo.getType() != null, SysFeedback::getType, bo.getType());

        lqw.eq(bo.getFeedbackStatus() != null, SysFeedback::getFeedbackStatus, bo.getFeedbackStatus());
        lqw.orderByDesc(SysFeedback::getCreateTime);
        return lqw;
    }

    @Override
    public Boolean insertByBo(SysFeedbackAddBo bo) {
        SysFeedback add = BeanUtil.toBean(bo, SysFeedback.class);

        if (bo.getPicsList() != null && !bo.getPicsList().isEmpty()) {
            add.setPics(String.join(",", bo.getPicsList()));
        }

        add.setUserId(SecurityUtils.getUser().getUserId());
        add.setShopId(SecurityUtils.getShop().getId());
        add.setCreateTime(new Date());
        add.setUpdateTime(new Date());

        return save(add);
    }

    @Override
    public Boolean updateByBo(SysFeedbackBo bo) {
        SysFeedback update = BeanUtil.toBean(bo, SysFeedback.class);

        update.setUpdateTime(new Date());

        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SysFeedback entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Override
    public Boolean reply(SysFeedbackReplyBo bo) {
        SysFeedback feedback = BeanUtil.toBean(bo, SysFeedback.class);

        feedback.setAdminId(1L);
        feedback.setUpdateTime(new Date());

        return updateById(feedback);
    }
}
