package com.qingyun.system.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.system.domain.SysOss;
import com.qingyun.system.domain.bo.SysOssBo;
import com.qingyun.system.domain.vo.SysOssVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Collection;

/**
 * 文件上传 服务层
 *
 * @author Lion Li
 */
public interface ISysOssService extends IServicePlus<SysOss, SysOssVo> {

	TableDataInfo<SysOssVo> queryPageList(SysOssBo sysOss);

	SysOss upload(MultipartFile file);

	SysOss upload(File file);

	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
