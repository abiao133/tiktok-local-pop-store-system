package com.qingyun.system.service;

import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.system.domain.SysFeedback;
import com.qingyun.system.domain.bo.SysFeedbackAddBo;
import com.qingyun.system.domain.bo.SysFeedbackBo;
import com.qingyun.system.domain.bo.SysFeedbackQueryBo;
import com.qingyun.system.domain.bo.SysFeedbackReplyBo;
import com.qingyun.system.domain.vo.SysFeedbackListVo;
import com.qingyun.system.domain.vo.SysFeedbackVo;

import java.util.Collection;
import java.util.List;

/**
 * 用户反馈Service接口
 *
 * @author qingyun
 * @date 2021-09-24
 */
public interface ISysFeedbackService extends IServicePlus<SysFeedback, SysFeedbackVo> {
	/**
	 * 查询单个
	 * @return
	 */
	SysFeedbackVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysFeedbackListVo> queryPageList(SysFeedbackQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysFeedbackVo> queryList(SysFeedbackQueryBo bo);

	/**
	 * 根据新增业务对象插入用户反馈
	 * @param bo 用户反馈新增业务对象
	 * @return
	 */
	Boolean insertByBo(SysFeedbackAddBo bo);

	/**
	 * 根据编辑业务对象修改用户反馈
	 * @param bo 用户反馈编辑业务对象
	 * @return
	 */
	Boolean updateByBo(SysFeedbackBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	Boolean reply(SysFeedbackReplyBo bo);
}
