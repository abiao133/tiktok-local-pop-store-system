# 抖音爆店系统

#### 描述
抖音同城爆店系统，java 语言SpringBoot+MybatisPlus，前后端分离工程，ffmpeg视频混剪，bgm标签合成，语音合成，字幕合成，分销模式, uniapp 小程序端 h5端 pc端源码
已同步抖音新API
#### 项目工程分类

1.  后端代码 qingyun_xx(已开源)
2.  抖音H5 tiktokH5(已开源-加群获取)
3.  微信小程序 wxMini(已开源)
4.  系统管理后台PC manager(已开源)

#### 说明

防止走丢请给一个starred吧 :heart_eyes:  :smiley: 

1.  此项目可任意二开 永久免费 由于配置较多所以需要熟练掌握springboot并阅读代码
2.  交流qq群
![输入图片说明](db/a9eaa16d646d391996227390f0795d4.png)

#### 项目截图（服务器过期无体验地址）


1. 管理端系统

 <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170436.jpg" alt="img" style="zoom: 33%;" />

<img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170452.jpg" alt="输入图片说明" style="zoom: 33%;" />

<img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170455.jpg" alt="img" style="zoom:33%;" />
   
 <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170455.jpg" alt="img" style="zoom:33%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170455.jpg" alt="img" style="zoom:33%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170458.jpg" alt="输入图片说明" style="zoom:33%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170458.jpg" alt="输入图片说明" style="zoom:33%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170500.jpg" alt="输入图片说明" style="zoom:33%;" />

2. 企业微信



<img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170658.jpg" alt="输入图片说明" style="zoom:25%;" />

<img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170703.jpg" alt="输入图片说明" style="zoom:25%;" />

<img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170700.jpg" alt="输入图片说明" style="zoom:25%;" />

3. 抖音

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170708.jpg" alt="输入图片说明" style="zoom:25%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170711.jpg" alt="输入图片说明" style="zoom:25%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/db/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220905170706.jpg" alt="输入图片说明" style="zoom:25%;" />

   <img src="https://gitee.com/dangxiansheng/tiktok-local-pop-store-system/raw/master/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220409103310.jpg" alt="输入图片说明" style="zoom:25%;" />