package com.qingyun.tiktok.config;


import com.qingyun.common.core.redis.RedisCache;
import com.qingyun.tiktok.basis.service.DyBasisServiceImpl;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.common.bean.AccessToken;
import com.qingyun.tiktok.common.config.AppConfig;
import com.qingyun.tiktok.common.config.DyOpenApiConfig;
import com.qingyun.tiktok.common.config.HttpConfig;
import com.qingyun.tiktok.common.enums.HttpClientType;
import com.qingyun.tiktok.common.http.HttpExecutor;
import com.qingyun.tiktok.common.http.okhttp.OkHttpExecutorFactory;
import com.qingyun.tiktok.common.storage.DyStorageManager;
import com.qingyun.tiktok.common.storage.MemoryStorageManager;
import com.qingyun.tiktok.common.storage.RedisStorageManager;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 抖音open api手动配置
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/25 8:56
 * @modified mdmbct
 * @since 1.0
 */
@Configuration
@Data
public class DyConfig {
    @Value("${douyin.appId}")
    private String appId;
    @Value("${douyin.appSecret}")
    private String appSecret;
    @Autowired
    private RedisCache redisCache;
	@Value("${douyin.shopRedirectUri}")
    private String shopRedirectUri;
	@Value("${douyin.userRedirectUri}")
	private String userRedirectUri;
    /**
     * RedisStorageManager
     */
//    @Bean
//    public DyStorageManager dyStorageManager(StringRedisTemplate redisTemplate) {
//        DyRedisOps dyRedisOps = new RedisTemplateOps(redisTemplate);
//        return new RedisStorageManager(dyRedisOps, getDyOpenApiConfig());
//    }
    private DyOpenApiConfig getDyOpenApiConfig() {
        DyOpenApiConfig apiConfig = new DyOpenApiConfig();
        apiConfig.setApp(
                AppConfig.builder()
                        .key(appId)
                        .secret(appSecret)
                        .build()
        );
        apiConfig.setHttp(
                HttpConfig.builder()
                        .type(HttpClientType.OK_HTTP)
                        .build()
        );
        return apiConfig;
    }
    /**
     * JedisStorageManager
     */
   @Bean
   public DyStorageManager dyStorageManager() {

	   return new RedisStorageManager(redisCache, getDyOpenApiConfig());
   }

    @Bean
    public DyBasisService dyBasisService(DyStorageManager storageManager) {
        OkHttpExecutorFactory okHttpExecutorFactory = new OkHttpExecutorFactory();
        HttpExecutor httpExecutor = okHttpExecutorFactory.createHttpExecutor(storageManager.getOpenApiConfig().getHttp());
        return new DyBasisServiceImpl(storageManager, httpExecutor);
    }



}
