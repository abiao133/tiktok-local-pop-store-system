package com.qingyun.tiktok.basis.service.interfaces;


import com.qingyun.tiktok.basis.response.video.VideoCommentListRes;
import com.qingyun.tiktok.basis.response.video.VideoListRes;
import com.qingyun.tiktok.common.bean.PageCount;

/**
 * 搜索管理api实现接口
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/18 11:26
 * @modified mdmbct
 * @since 1.0
 */
public interface SearchManagerService {

	/**
	 * 视频搜索 <p>
	 *
	 * @param openId  open id
	 * @param cursor  分页游标, 第一页请求cursor是0, response中会返回下一页请求用到的cursor, 同时response还会返回has_more来表明是否有更多的数据。
	 * @param count   每页数量
	 * @param keyword 关键词
	 * @return 视频列表数据
	 * @see SearchManager#VIDEO_SEARCH
	 */
	public VideoListRes.VideoListResData videoSearch(String openId, long cursor,String keyword);




	/**
	 * 视频评论搜索 <p>
	 *
	 * @param clientToken
	 * @param cursor  分页游标, 第一页请求cursor是0, response中会返回下一页请求用到的cursor, 同时response还会返回has_more来表明是否有更多的数据。
	 * @param sec_item_id 视频ID
	 * @return 视频评论列表数据
	 * @see SearchManager#VIDEO_SEARCH
	 */
	public VideoCommentListRes.VideoCommentListResData videoCommentSearch(String clientToken, long cursor, String sec_item_id);
}
