package com.qingyun.tiktok.basis.response.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.qingyun.tiktok.common.response.DefaultResponseData;
import com.qingyun.tiktok.common.response.DyOpenApiResponse;
import com.qingyun.tiktok.common.response.ResponseExtra;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 热门视频榜
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/19 17:02
 * @modified mdmbct
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotVideoListRes implements DyOpenApiResponse {

    private static final long serialVersionUID = -2159600221255810950L;

    private ResponseExtra extra;

    private HotVideoListResData data;

    private String message;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class HotVideoListResData extends DefaultResponseData {

        private static final long serialVersionUID = -3584401751854804065L;
        /**
         * 由于置顶的原因, list长度可能比count指定的数量多一些或少一些。
         */
        private List<HotVideo> list;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class HotVideo {
        /**
         * 	排名
         */
        @JSONField(name = "rank")
        private Integer rank;

        /**
         * 视频播放页面。视频播放页可能会失效，请在观看视频前调用/video/data/获取最新的播放页
         */
        @JSONField(name = "share_url")
        private String shareUrl;

        /**
         * 	视频标题
         */
        @JSONField(name = "title")
        private String title;


        /**
         * 视频发布者	昵称
         */
        @JSONField(name = "author")
        private String author;

        /**
         * 播放数，离线数据（统计前一日数据）
         */
        @JSONField(name = "play_count")
        private Long playCount;

        /**
         * 点赞数，离线数据（统计前一日数据）
         */
        @JSONField(name = "digg_count")
        private Long diggCount;

        /**
         * 评论数，离线数据（统计前一日数据）
         */
        @JSONField(name = "comment_count")
        private Long commentCount;

        /**
         * 视频热词（以,隔开）
         */
        @JSONField(name = "hot_words")
        private String hotWords;

        /**
         * 热度指数
         */
        @JSONField(name = "hot_value")
        private Double hotValue;

        /**
         * 视频封面图
         */
        @JSONField(name = "item_cover")
        private String itemCover;
    }
}
