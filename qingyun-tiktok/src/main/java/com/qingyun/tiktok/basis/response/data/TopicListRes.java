package com.qingyun.tiktok.basis.response.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.qingyun.tiktok.common.response.DefaultResponseData;
import com.qingyun.tiktok.common.response.DyOpenApiResponse;
import com.qingyun.tiktok.common.response.ResponseExtra;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 回复视频评论响应
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/19 17:02
 * @modified mdmbct
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TopicListRes implements DyOpenApiResponse {

    private static final long serialVersionUID = -2159600221255810950L;

    private ResponseExtra extra;

    private TopicListResData data;

    private String message;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class TopicListResData extends DefaultResponseData {

        private static final long serialVersionUID = -3584401751854804065L;


        /**
         * 由于置顶的原因, list长度可能比count指定的数量多一些或少一些。
         */
        private List<Topic> list;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Topic {
        /**
         * 	排名
         */
        @JSONField(name = "rank")
        private Integer rank;

        /**
         * 排名变化
         */
        @JSONField(name = "rank_change")
        private String rankChange;

        /**
         * 	话题标题
         */
        @JSONField(name = "title")
        private String title;


        /**
         * 影响力指数
         */
        @JSONField(name = "effect_value")
        private Double effectValue;
    }
}
