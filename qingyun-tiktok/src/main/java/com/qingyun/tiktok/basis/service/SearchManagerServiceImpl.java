package com.qingyun.tiktok.basis.service;


import com.qingyun.tiktok.basis.api.SearchManager;
import com.qingyun.tiktok.basis.response.video.VideoCommentListRes;
import com.qingyun.tiktok.basis.response.video.VideoListRes;
import com.qingyun.tiktok.basis.service.interfaces.SearchManagerService;
import com.qingyun.tiktok.common.bean.PageCount;
import com.qingyun.tiktok.common.config.DyOpenApiConfig;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.http.HttpExecutor;
import com.qingyun.tiktok.common.service.BaseDyService;
import com.qingyun.tiktok.common.storage.DyStorageManager;

/**
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/18 11:28
 * @modified mdmbct
 * @since 1.0
 */
public class SearchManagerServiceImpl extends BaseDyService implements SearchManagerService {
	private final DyOpenApiConfig apiConfig;
	public SearchManagerServiceImpl(DyStorageManager storageManager, HttpExecutor httpExecutor) {
		super(storageManager, httpExecutor);
		apiConfig = storageManager.getOpenApiConfig();
	}



	@Override
	public VideoListRes.VideoListResData videoSearch(String openId, long cursor,  String keyword) {
		return simpleGetReqForDy(SearchManager.VIDEO_SEARCH,
			openId,
			VideoListRes.VideoListResData.class,
			openId, cursor, apiConfig.getApp().getPageSize(),keyword
		);
	}

	@Override
	public VideoCommentListRes.VideoCommentListResData videoCommentSearch(String clientToken, long cursor, String sec_item_id) {
		return simpleClientTokenGetReq(SearchManager.VIDEO_COMMENT_SEARCH,
			clientToken, ApiPlatform.DOU_YIN,
			VideoCommentListRes.VideoCommentListResData.class,
			cursor, apiConfig.getApp().getPageSize(),sec_item_id
		);
	}
}
