package com.qingyun.tiktok.basis.service;

import com.alipay.easysdk.kernel.util.Signer;
import com.alibaba.fastjson.JSONArray;
import com.qcloud.cos.utils.Md5Utils;
import com.qingyun.tiktok.basis.api.VideoManager;
import com.qingyun.tiktok.basis.body.video.*;
import com.qingyun.tiktok.basis.response.video.*;
import com.qingyun.tiktok.basis.service.interfaces.Auth2Service;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.basis.service.interfaces.VideoManagerService;
import com.qingyun.tiktok.common.bean.PageCount;
import com.qingyun.tiktok.common.bean.Ticket;
import com.qingyun.tiktok.common.config.DyOpenApiConfig;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.enums.ErrorCode;
import com.qingyun.tiktok.common.enums.RequestBodyType;
import com.qingyun.tiktok.common.exception.ApiCallException;
import com.qingyun.tiktok.common.exception.InvalidRequestParamException;
import com.qingyun.tiktok.common.http.ByteRequestBody;
import com.qingyun.tiktok.common.http.FileRequestBody;
import com.qingyun.tiktok.common.http.Head;
import com.qingyun.tiktok.common.http.HttpExecutor;
import com.qingyun.tiktok.common.response.DefaultResponse;
import com.qingyun.tiktok.common.response.DefaultResponseData;
import com.qingyun.tiktok.common.response.auth.TicketRes;
import com.qingyun.tiktok.common.service.AuthService;
import com.qingyun.tiktok.common.service.BaseDyService;
import com.qingyun.tiktok.common.storage.DyStorageManager;
import com.qingyun.tiktok.common.utils.StringUtils;
import com.qingyun.tiktok.common.utils.file.FileChunkHandler;
import com.qingyun.tiktok.common.utils.file.FileSplit;
import com.qingyun.tiktok.common.utils.file.FileUtils;
import com.qingyun.tiktok.common.utils.file.MultiThreadFileChunkGroupHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/9 11:31
 * @modified mdmbct
 * @since 1.0
 */
@Slf4j
public class VideoManagerServiceImpl extends BaseDyService implements VideoManagerService {
	private DyOpenApiConfig apiConfig;


    public VideoManagerServiceImpl(DyStorageManager storageManager, HttpExecutor httpExecutor) {
        super(storageManager, httpExecutor);
		apiConfig = storageManager.getOpenApiConfig();
    }

    @Override
    public VideoUploadRes.VideoUploadResponseData videoUpload(String openId, File videoFile, ApiPlatform platform) throws ApiCallException {

        if (videoFile.length() > MAX_SINGLE_VIDEO_FILE_SIZE) {
            throw new InvalidRequestParamException("大于128MB的视频,请使用分片上传!");
        }

        if (!videoFile.exists()) {
            throw new InvalidRequestParamException("视频文件不存在!");
        }

        String url = formatRequestUrl(VideoManager.VIDEO_UPLOAD, platform, openId);

        String fileName = videoFile.getName();
        String fileExtension = FileUtils.getFileExtension(fileName, false);
        FileRequestBody body = new FileRequestBody("video", fileName, "video/" + fileExtension, videoFile);

        String result = postRequest(url, body, openId, platform, RequestBodyType.FROM, Head.MULTIPART_FORM_DATA);
        return handleApiResponse(result, VideoUploadRes.class, VideoUploadRes.VideoUploadResponseData.class);
    }

    @Override
    public VideoPartInitRes.VideoPartInitResData videoPartInit(String openId, ApiPlatform platform) {
        return jsonPostReqForDy(VideoManager.VIDEO_PART_INIT,
                openId,
                null,
                VideoPartInitRes.VideoPartInitResData.class,
                openId
        );
    }

    @Override
    public DefaultResponseData videoPartUpload(String openId, String uploadId, int partNumber, byte[] part, File videoFile, ApiPlatform platform) {

        if (part.length < MIN_PART_VIDEO_FILE_SIZE || part.length > DEFAULT_PART_VIDEO_FILE_SIZE) {
            throw new InvalidRequestParamException("一个视频分片的大小在5MB至20MB之间");
        }

        log.info("part_number:{}, part_length:{}, upload_id:{}, fileName:{}", partNumber, part.length, uploadId, videoFile.getAbsolutePath());

        String url = formatRequestUrl(VideoManager.VIDEO_PART_UPLOAD, platform, openId, uploadId, partNumber);
        ByteRequestBody body = new ByteRequestBody("video", videoFile.getName(), part);

        String result = postRequest(url, body, openId, platform, RequestBodyType.FROM, Head.MULTIPART_FORM_DATA);
        return handleApiResponse(result, DefaultResponse.class, DefaultResponseData.class);
    }

    @Override
    public VideoUploadRes.VideoUploadResponseData videoPartComplete(String openId, String uploadId, ApiPlatform platform) {
        return jsonPostReqForDy(VideoManager.VIDEO_PART_COMPLETE,
                openId,
                null,
                VideoUploadRes.VideoUploadResponseData.class,
                openId, uploadId
        );
    }


    @Override
    public VideoCreateRes.VideoCreateResData videoCreate(String openId, VideoCreateReqBody body, ApiPlatform platform) {
        return jsonPostReqForDy(VideoManager.VIDEO_CREATE,
                openId,
                body,
                VideoCreateRes.VideoCreateResData.class,
                openId
        );
    }


    @Override
    public DefaultResponseData videoDelete(String openId, String itemId, ApiPlatform platform) throws ApiCallException {
        return jsonPostReqForDy(VideoManager.VIDEO_DELETE,
                openId,
                new VideoDeleteRequestBody(itemId),
                DefaultResponseData.class,
                openId
        );
    }


    @Override
    public VideoUploadRes.VideoUploadResponseData autoUploadVideo(String openId, File videoFile, ApiPlatform platform) throws FileNotFoundException {
        if (!videoFile.exists()) {
            throw new InvalidRequestParamException("视频文件不存在!");
        }

        long fileSize = videoFile.length();
        if (fileSize >= MAX_ALLOW_VIDEO_FILE_SIZE) {
            throw new InvalidRequestParamException("抖音允许上传的视频文件最大为4GB");
        }

        // 小于128MB 直接上传
        if (fileSize < MAX_SINGLE_VIDEO_FILE_SIZE) {
            return videoUpload(openId, videoFile, platform);
        }

        // 分片上传
        String uploadId = videoPartInit(openId, platform).getUploadId();

        int threadCount = storageManager.getOpenApiConfig().getUploadThreadNum();
        FileSplit fileSplit = new FileSplit(videoFile, DEFAULT_PART_VIDEO_FILE_SIZE, MIN_PART_VIDEO_FILE_SIZE, threadCount);
        // 防止线程数超过分块数
        threadCount = Math.min(threadCount, fileSplit.getChunkCount());

        if (threadCount == 1) {
            // 单线程上传文件
            fileSplit.split(chunkGroup -> Collections.singletonList(
                    videoPartUpload(openId, uploadId, chunkGroup.get(0).getChunkNum(), chunkGroup.get(0).getBytes(), videoFile, platform)
            ));

        } else {
            // 多线程上传文件
            FileChunkHandler<DefaultResponseData> fileChunkHandler = chunk -> videoPartUpload(openId, uploadId, chunk.getChunkNum(), chunk.getBytes(), videoFile, platform);
            MultiThreadFileChunkGroupHandler<DefaultResponseData> multiThreadFileChunkHandler = new MultiThreadFileChunkGroupHandler<>(threadCount, fileSplit.getChunkCount(), fileChunkHandler);
            fileSplit.split(multiThreadFileChunkHandler);
            // 通知线程池中线程结束
            multiThreadFileChunkHandler.finishUpload();
        }

        fileSplit.finishedSplit();
        return videoPartComplete(openId, uploadId, platform);
    }

    @Override
    public VideoCreateRes.VideoCreateResData autoCreateVideo(String openId, File videoFile, AutoCreateVideoParam param, ApiPlatform platform) throws ApiCallException, FileNotFoundException {

        long fileSize = videoFile.length();
        if (fileSize >= MAX_ALLOW_VIDEO_FILE_SIZE) {
            throw new InvalidRequestParamException("抖音允许上传的视频文件最大为4GB");
        }

        VideoUploadRes.VideoUploadResponseData videoUploadResponseData = autoUploadVideo(openId, videoFile, platform);
        String videoId = videoUploadResponseData.getVideo().getVideoId();

        VideoCreateReqBody body = VideoCreateReqBody.build(param);
        body.setVideoId(videoId);

        return videoCreate(openId, body, platform);
    }

    @Override
    public VideoListRes.VideoListResData getVideoList(String openId, long cursor, PageCount count, ApiPlatform platform) throws ApiCallException {
        return simpleGetReq(VideoManager.VIDEO_LIST,
                openId,
                platform,
                VideoListRes.VideoListResData.class,
                openId, cursor, count.getValue()
        );
    }

    @Override
    public VideoListRes.VideoListResData getVideoData(String openId, String[] videoIds, ApiPlatform platform) throws ApiCallException {
        VideoDataReqBody body = new VideoDataReqBody(videoIds);
        return jsonPostReqForDy(VideoManager.VIDEO_DATA,
                openId,
                body,
                VideoListRes.VideoListResData.class,
                openId
        );
    }

    @Override
    public ImageUploadRes.ImageUploadResData imageUpload(String openId, File imageFile) throws ApiCallException {

        if (imageFile.length() >= MAX_ALLOW_IMAGE_SIZE) {
            throw new InvalidRequestParamException("抖音允许上传的图片大小不能超过100MB!");
        }

        if (!imageFile.exists()) {
            throw new InvalidRequestParamException("图片文件不存在!");
        }

        String url = formatRequestUrl(VideoManager.IMAGE_UPLOAD, ApiPlatform.DOU_YIN, openId);

        String fileName = imageFile.getName();
        String extension = FileUtils.getFileExtension(fileName, false);
        FileRequestBody body = new FileRequestBody("image", fileName, "image/" + extension, imageFile);

        String result = postRequest(url, body, openId, ApiPlatform.DOU_YIN, RequestBodyType.FROM, Head.MULTIPART_FORM_DATA);
        return handleApiResponse(result, ImageUploadRes.class, ImageUploadRes.ImageUploadResData.class);
    }

    @Override
    public VideoCreateRes.VideoCreateResData imageCreate(String openId, ImageCreateBody body) throws ApiCallException {
        return jsonPostReqForDy(VideoManager.IMAGE_CREATE,
                openId,
                body,
                VideoCreateRes.VideoCreateResData.class,
                openId
        );
    }

    @Override
    public VideoCreateRes.VideoCreateResData autoCreateImage(String openId, File imageFile, AutoCreateImageParam autoCreateImageParam) throws ApiCallException {

        ImageUploadRes.ImageUploadResData imageUploadResData = imageUpload(openId, imageFile);

        ImageCreateBody body = ImageCreateBody.build(autoCreateImageParam);
        body.setImageId(imageUploadResData.getImage().getImageId());

        return imageCreate(openId, body);
    }

    @Override
    public VideoPoiRes.VideoPoiResData searchPOI(String clientToken,String keyword) throws ApiCallException {
        return simpleClientTokenGetReq(VideoManager.VIDEO_POI,clientToken,ApiPlatform.DOU_YIN,VideoPoiRes.VideoPoiResData.class,keyword );
    }

	@Override
	public String getH5Scheme(String videoPath,String title,String poiId,String shareId) {
		String nonce = UUID.randomUUID().toString();
		String timestamp = System.currentTimeMillis()/1000 +"";
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("nonce_str", nonce);


		Ticket ticket = storageManager.getTicket();
		paramsMap.put("ticket",ticket.getValue() );
		paramsMap.put("timestamp",timestamp );
		String signature = Md5Utils.md5Hex(Signer.getSignCheckContent(paramsMap));

		URIBuilder uri = null;
		try {
			uri = new URIBuilder("snssdk1128://openplatform/share");
			uri.addParameter("client_key", apiConfig.getApp().getKey());
			uri.addParameter("state", shareId);
			uri.addParameter("nonce_str", nonce);
			uri.addParameter("timestamp", timestamp);
			uri.addParameter("signature", signature);
			uri.addParameter("share_type", "h5");
			uri.addParameter("video_path", videoPath);
			uri.addParameter("need_download", "1");
			uri.addParameter("share_to_publish", "0");

			if(!StringUtils.isEmpty(title)){
				uri.addParameter("title", title);
			}
			if(!StringUtils.isEmpty(poiId)){
				uri.addParameter("poi_id", poiId);
			}

			JSONArray hashTags = new JSONArray();


			String url = uri.build().toString();
			return url;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getShareId(String clientToken) {
		VideoShareDataRes.VideoShareDataResData videoShareDataRes = simpleClientTokenGetReq(VideoManager.VIDEO_SHARE_ID,clientToken,ApiPlatform.DOU_YIN,VideoShareDataRes.VideoShareDataResData.class, true,"","","");
		if(videoShareDataRes.getErrorCode()== ErrorCode.SUCCESS.getValue()){
			return videoShareDataRes.getShareId();
		}
		return null;
	}


	/////////////////////////////////////////// private protected method ///////////////////////////////////////////

//    @Override
//    protected <T extends DyOpenApiResponse, D extends DefaultResponseData> D handleApiResponse(String responseResult, Class<T> responseClass, Class<D> responseDataClass) throws ApiResponseFailedException {
//
//        T t = JSONObject.parseObject(responseResult, responseClass);
//
//        if (t.getData().getErrorCode() == ErrorCode.SUCCESS.getValue()) {
//            return (D) t.getData();
//        }
//
//        throw new ApiResponseFailedException(t.getErrorMsg());
//    }




}
