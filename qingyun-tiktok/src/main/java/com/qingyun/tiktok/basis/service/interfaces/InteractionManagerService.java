package com.qingyun.tiktok.basis.service.interfaces;

import com.qingyun.tiktok.basis.body.interaction.ItemCommentReplyReqBody;
import com.qingyun.tiktok.basis.response.interaction.ItemCommentReplyListRes;
import com.qingyun.tiktok.basis.response.interaction.ItemCommentReplyRes;
import com.qingyun.tiktok.basis.response.user.UserOpenInfoRes;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.exception.ApiCallException;

/**
 * 互动管理服务接口
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/18 16:36
 * @modified mdmbct
 * @since 1.0
 */
public interface InteractionManagerService {


	/**
	 * 评论视频
	 *
	 * @param openId          open id
	 * @param apiPlatform 传{@link ApiPlatform#DOU_YIN} 或者 {@link ApiPlatform#SNS}
	 * @return 用户公开信息 {@link UserOpenInfoRes.GetUserOpenInfoResData}
	 * @throws ApiCallException 接口调用异常
	 */
	ItemCommentReplyRes.ItemCommentReplyResData sendComment(String openId, ItemCommentReplyReqBody commentReplyReqBody, ApiPlatform apiPlatform) throws ApiCallException;
}
