package com.qingyun.tiktok.basis.api;


import com.qingyun.tiktok.basis.response.data.HotVideoListRes;
import com.qingyun.tiktok.basis.response.data.MusicListRes;
import com.qingyun.tiktok.basis.response.data.TopicListRes;
import com.qingyun.tiktok.common.body.DyOpenApiRequestBody;
import com.qingyun.tiktok.common.body.VoidBody;
import com.qingyun.tiktok.common.enums.DyOpenApi;
import com.qingyun.tiktok.common.enums.RequestMethod;
import com.qingyun.tiktok.common.response.DyOpenApiResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 榜单数据api
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/18 11:18
 * @modified mdmbct
 * @since 1.0
 */
@Getter
@RequiredArgsConstructor
public enum DataManager implements DyOpenApi {

    /**
     * <a href="https://open.douyin.com/platform/doc/6908950744699815948">获取话题榜数据</a> <p>
     * Scope: `data.external.billboard_topic `需要申请权限 不需要用户授权<p>
     * <p>
     * 该接口用于获取话题榜数据。该接口适用于抖音。<p>
     * <p>
     * 该统计数据为离线数据；数据产出周期：每天10点前
     */
    TOPIC(
            RequestMethod.GET,
            VoidBody.class,
            TopicListRes.class,
            "/data/extern/billboard/topic/?"
    ),
    /**
     * <a href="https://open.douyin.com/platform/doc/6908910551393437707">获取热门视频数据</a> <p>
     * Scope: `data.external.billboard_topic `需要申请权限 不需要用户授权<p>
     * <p>
     * 该接口用于获取热门视频榜单数据。该接口适用于抖音。<p>
     * <p>
     * 该统计数据为离线数据，统计最近24小时；数据产出周期：每天10点前。
     */
    HOT_VIDEO(
            RequestMethod.GET,
            VoidBody.class,
            HotVideoListRes.class,
            "/data/extern/billboard/hot_video/?"
    ),
    MUSIC_HOT(
            RequestMethod.GET,
            VoidBody.class,
            MusicListRes.class,
            "/data/extern/billboard/music/hot/?"
    ),
    MUSIC_SOAR(
            RequestMethod.GET,
            VoidBody.class,
            MusicListRes.class,
            "/data/extern/billboard/music/soar/?"
    ),

    MUSIC_ORIGINAL(
            RequestMethod.GET,
            VoidBody.class,
            MusicListRes.class,
            "/data/extern/billboard/music/original/?"
    ),
    ;


    private final RequestMethod requestMethod;

    private final Class<? extends DyOpenApiRequestBody> requestBodyClass;

    private final Class<? extends DyOpenApiResponse> responseClass;

    private final String path;
}
