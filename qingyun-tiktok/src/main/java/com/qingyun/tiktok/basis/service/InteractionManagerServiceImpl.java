package com.qingyun.tiktok.basis.service;

import com.qingyun.tiktok.basis.api.InteractionManager;
import com.qingyun.tiktok.basis.api.VideoManager;
import com.qingyun.tiktok.basis.body.interaction.ItemCommentReplyReqBody;
import com.qingyun.tiktok.basis.response.interaction.ItemCommentReplyListRes;
import com.qingyun.tiktok.basis.response.interaction.ItemCommentReplyRes;
import com.qingyun.tiktok.basis.response.video.VideoUploadRes;
import com.qingyun.tiktok.basis.service.interfaces.*;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.exception.ApiCallException;
import com.qingyun.tiktok.common.http.HttpExecutor;
import com.qingyun.tiktok.common.service.BaseDyService;
import com.qingyun.tiktok.common.storage.DyStorageManager;

/**
 * @Classname InteractionManagerServiceImpl
 * @Author dyh
 * @Date 2021/10/23 11:10
 */
public class InteractionManagerServiceImpl extends BaseDyService implements InteractionManagerService {

	public InteractionManagerServiceImpl(DyStorageManager storageManager, HttpExecutor httpExecutor) {
		super(storageManager, httpExecutor);
	}
	@Override
	public ItemCommentReplyRes.ItemCommentReplyResData sendComment(String openId, ItemCommentReplyReqBody commentReplyReqBody, ApiPlatform apiPlatform) throws ApiCallException {
		return jsonPostReqForDy(InteractionManager.ITEM_COMMENT_REPLY,
			openId,
			commentReplyReqBody,
			ItemCommentReplyRes.ItemCommentReplyResData.class,
			openId
		);
	}
}
