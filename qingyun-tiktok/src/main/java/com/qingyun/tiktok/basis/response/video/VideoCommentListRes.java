package com.qingyun.tiktok.basis.response.video;

import com.alibaba.fastjson.annotation.JSONField;
import com.qingyun.tiktok.common.response.DyOpenApiResponse;
import com.qingyun.tiktok.common.response.PageResponse;
import com.qingyun.tiktok.common.response.ResponseExtra;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 查询授权账号视频数据返回数据
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/8 13:39
 * @modified mdmbct
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoCommentListRes implements DyOpenApiResponse {

    private ResponseExtra extra;

    private VideoCommentListResData data;


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class VideoCommentListResData extends PageResponse {

        /**
         * 由于置顶的原因, list长度可能比count指定的数量多一些或少一些。
         */
        private List<VideoComment> list;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class VideoComment {

        /**
         * 评论ID
         */
        @JSONField(name = "comment_id")
        private String commentId;

        /**
         * 评论用户ID
         */
        @JSONField(name = "comment_user_id")
        private String commentUserId;

        /**
         * 评论内容
         */
        private String content;

        /**
         * 评论创建时间戳
         */
        @JSONField(name = "create_time")
        private long createTime;

        /**
         * 点赞数
         */
        @JSONField(name = "digg_count")
        private int diggCount;

        /**
         * 回复数
         */
        @JSONField(name = "reply_comment_total")
        private int replyCommentTotal;

        /**
         * 是否置顶
         */
        private boolean top;
    }


}
