package com.qingyun.tiktok.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.PagePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.tiktok.domain.bo.TiktokUserBo;
import com.qingyun.tiktok.domain.bo.TiktokUserQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokUserVo;
import com.qingyun.tiktok.mapper.TiktokUserMapper;
import com.qingyun.tiktok.service.ITiktokUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokUserServiceImpl extends ServicePlusImpl<TiktokUserMapper, TiktokUser, TiktokUserVo> implements ITiktokUserService {

    @Override
    public TiktokUserVo queryById(Long id) {
        return getVoById(id);
    }

    @Override
    public TableDataInfo<TiktokUserVo> queryPageList(TiktokUserQueryBo bo) {
        PagePlus<TiktokUser, TiktokUserVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TiktokUserVo> queryList(TiktokUserQueryBo bo) {
        return listVo(buildQueryWrapper(bo));
    }

    private LambdaQueryWrapper<TiktokUser> buildQueryWrapper(TiktokUserQueryBo bo) {

        LambdaQueryWrapper<TiktokUser> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getNickName()), TiktokUser::getNickName, bo.getNickName());
        lqw.eq(StringUtils.isNotBlank(bo.getGender()), TiktokUser::getGender, bo.getGender());
        lqw.eq(StringUtils.isNotBlank(bo.getProvince()), TiktokUser::getProvince, bo.getProvince());
        lqw.eq(StringUtils.isNotBlank(bo.getCity()), TiktokUser::getCity, bo.getCity());
        lqw.eq(bo.getMemberLevel() != null, TiktokUser::getMemberLevel, bo.getMemberLevel());
        lqw.eq(bo.getState() != null, TiktokUser::getState, bo.getState());

        lqw.between(bo.getCreateTimeStart() != null && bo.getCreateTimeEnd() != null, TiktokUser::getCreateTime, bo.getCreateTimeStart(), bo.getCreateTimeEnd());
        lqw.orderByDesc(TiktokUser::getCreateTime);
        return lqw;
    }

    @Override
    public Boolean insertByBo(TiktokUserBo bo) {
        TiktokUser add = BeanUtil.toBean(bo, TiktokUser.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByBo(TiktokUserBo bo) {
        TiktokUser update = BeanUtil.toBean(bo, TiktokUser.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TiktokUser entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
