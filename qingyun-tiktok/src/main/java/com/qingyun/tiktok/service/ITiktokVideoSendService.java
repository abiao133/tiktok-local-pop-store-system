package com.qingyun.tiktok.service;


import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.tiktok.domain.TiktokVideoSend;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendBo;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokAppVideoCountsVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendListVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendVo;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 发送视频记录信息Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokVideoSendService extends IServicePlus<TiktokVideoSend, TiktokVideoSendVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokVideoSendVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokVideoSendListVo> queryPageList(TiktokVideoSendQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokVideoSendVo> queryList(TiktokVideoSendBo bo);

	/**
	 * 根据新增业务对象插入发送视频记录信息
	 * @param bo 发送视频记录信息新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokVideoSendBo bo);

	/**
	 * 根据编辑业务对象修改发送视频记录信息
	 * @param bo 发送视频记录信息编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokVideoSendBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


	/**
	 * 查询发送视频次数
	 * @param shopId
	 * @param date
	 * @return
	 */
	Integer selectSendVideoCountByDate(Long shopId,Date date);


	/**
	 * 查询limit条参与活动数据
	 * @param shopId
	 * @param limit
	 * @return
	 */
	List<String> selectSendTitleByCount(Long shopId,Integer limit);


	/**
	 * 查询视频播放数据
	 * @param shopId
	 * @return
	 */
	TiktokAppVideoCountsVo selectAppVideoCounts(Long shopId);


}
