package com.qingyun.tiktok.service;


import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.common.core.mybatisplus.core.IServicePlus;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.tiktok.domain.bo.TiktokUserBo;
import com.qingyun.tiktok.domain.bo.TiktokUserQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokUserVo;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface ITiktokUserService extends IServicePlus<TiktokUser, TiktokUserVo> {
	/**
	 * 查询单个
	 * @return
	 */
	TiktokUserVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<TiktokUserVo> queryPageList(TiktokUserQueryBo bo);

	/**
	 * 查询列表
	 */
	List<TiktokUserVo> queryList(TiktokUserQueryBo bo);

	/**
	 * 根据新增业务对象插入【请填写功能名称】
	 * @param bo 【请填写功能名称】新增业务对象
	 * @return
	 */
	Boolean insertByBo(TiktokUserBo bo);

	/**
	 * 根据编辑业务对象修改【请填写功能名称】
	 * @param bo 【请填写功能名称】编辑业务对象
	 * @return
	 */
	Boolean updateByBo(TiktokUserBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
