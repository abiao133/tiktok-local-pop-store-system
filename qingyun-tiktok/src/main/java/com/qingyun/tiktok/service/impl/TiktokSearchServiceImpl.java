package com.qingyun.tiktok.service.impl;

import com.qingyun.tiktok.service.ITiktokSearchService;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TiktokSearchServiceImpl implements ITiktokSearchService {

	private static final String SEARCH_URL = "https://www.douyin.com/aweme/v1/web/general/search/single/";
	@Override
	public void searchByUid(String uid) {

	}

	@Override
	public void searchByNickname(String nickname) {

	}

	@Override
	public void searchByKeywords(String keywords) {
		// 获取连接客户端工具
		CloseableHttpClient httpClient = HttpClients.createDefault();

		String entityStr = null;
		CloseableHttpResponse response = null;

		try {
			/*
			 * 由于GET请求的参数都是拼装在URL地址后方，所以我们要构建一个URL，带参数
			 */
			URIBuilder uriBuilder = new URIBuilder(SEARCH_URL);
			// 根据带参数的URI对象构建GET请求对象
			List<NameValuePair> pairs = new LinkedList<>();
			pairs.add(new BasicNameValuePair("aid", "6383" ));
			pairs.add(new BasicNameValuePair("sort_type", "1" ));
			pairs.add(new BasicNameValuePair("publish_time", "0" ));
			pairs.add(new BasicNameValuePair("query_correct_type", "1" ));
			pairs.add(new BasicNameValuePair("is_filter_search", "0" ));
			pairs.add(new BasicNameValuePair("from_group_id", "0" ));
			pairs.add(new BasicNameValuePair("offset", "0" ));
			pairs.add(new BasicNameValuePair("count", "30" ));
			pairs.add(new BasicNameValuePair("keyword", keywords ));

			uriBuilder.setParameters(pairs);

			HttpGet httpGet = new HttpGet(uriBuilder.build());
			/*
			 * 添加请求头信息
			 */
			// 浏览器表示
			httpGet.addHeader("authority","www.douyin.com");
			httpGet.addHeader("method","GET");
			httpGet.addHeader("path","/aweme/v1/web/general/search/single/?device_platform=webapp&aid=6383&channel=channel_pc_web&search_channel=aweme_general&sort_type=0&publish_time=0&keyword=%E8%A3%85%E4%BF%AE&search_source=normal_search&query_correct_type=1&is_filter_search=0&from_group_id=&offset=0&count=10&pc_client_type=1&version_code=170400&version_name=17.4.0&cookie_enabled=true&screen_width=1600&screen_height=900&browser_language=zh-CN&browser_platform=Win32&browser_name=Chrome&browser_version=96.0.4664.110&browser_online=true&engine_name=Blink&engine_version=96.0.4664.110&os_name=Windows&os_version=10&cpu_core_num=8&device_memory=8&platform=PC&downlink=10&effective_type=4g&round_trip_time=50&webid=7049183979321230884&msToken=npfbYNvFK_McM9JG-lr5O6Cz_euVmyPRH2x-N3HXOp0bKDpvRhDL4wyFlLve9w2aXJmYeKprXvYAZsN4DIf1Y_xj-TSUX2Yd6_sxT8vbPpAQUuSJ4aTun8jbLz8=&X-Bogus=DFSzswVLAkXANSJnSKxQ2KXAIQ5o");
			httpGet.addHeader("scheme","https");
			httpGet.addHeader("accept","application/json, text/plain, */*");
			httpGet.addHeader("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36");
			httpGet.addHeader("referer", "https://www.douyin.com/search/%E8%A3%85%E4%BF%AE?source=normal_search&aid=faf6f0b0-fb78-44a2-86a3-95facbcfd50d&enter_from=recommend&focus_method=&gid=7123529825167346975");
			// 执行请求
			response = httpClient.execute(httpGet);
			// 获得响应的实体对象
			HttpEntity entity = response.getEntity();
			// 使用Apache提供的工具类进行转换成字符串

			entityStr = EntityUtils.toString(entity, "UTF-8");
		} catch (ClientProtocolException e) {
			System.err.println("Http协议出现问题");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("解析错误");
			e.printStackTrace();
		} catch (URISyntaxException e) {
			System.err.println("URI解析异常");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO异常");
			e.printStackTrace();
		} finally {
			// 释放连接
			if (null != response) {
				try {
					response.close();
					httpClient.close();
				} catch (IOException e) {
					System.err.println("释放连接出错");
					e.printStackTrace();
				}
			}
		}

		// 打印响应内容
		System.out.println("解析结果："+entityStr);
	}


	public static void main(String[] args){
		new TiktokSearchServiceImpl().searchByKeywords("踩高跷为啥不会摔跤");
	}
}
