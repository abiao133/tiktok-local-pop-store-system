package com.qingyun.tiktok.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.ServicePlusImpl;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.utils.PageUtils;
import com.qingyun.security.utils.SecurityUtils;
import com.qingyun.tiktok.domain.TiktokVideoSend;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendBo;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokAppVideoCountsVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendListVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendVo;
import com.qingyun.tiktok.mapper.TiktokVideoSendMapper;
import com.qingyun.tiktok.service.ITiktokVideoSendService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 发送视频记录信息Service业务层处理
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Service
public class TiktokVideoSendServiceImpl extends ServicePlusImpl<TiktokVideoSendMapper, TiktokVideoSend, TiktokVideoSendVo> implements ITiktokVideoSendService {

	@Override
	public TiktokVideoSendVo queryById(Long id) {
		return getVoById(id);
	}

	@Override
	public TableDataInfo<TiktokVideoSendListVo> queryPageList(TiktokVideoSendQueryBo bo) {

		if(!SecurityUtils.isManager()){
			bo.setShopId(SecurityUtils.getShop().getId());
		}

		Page<TiktokVideoSendListVo> result = baseMapper.selectVideoSendList(PageUtils.buildPage(), bo);
		return PageUtils.buildDataInfo(result);
	}

	@Override
	public List<TiktokVideoSendVo> queryList(TiktokVideoSendBo bo) {
		return listVo(buildQueryWrapper(bo));
	}

	private LambdaQueryWrapper<TiktokVideoSend> buildQueryWrapper(TiktokVideoSendBo bo) {
		LambdaQueryWrapper<TiktokVideoSend> lqw = Wrappers.lambdaQuery();
		lqw.like(StringUtils.isNotBlank(bo.getTiktokNickName()), TiktokVideoSend::getTiktokNickName, bo.getTiktokNickName());
		lqw.eq(bo.getShopId() != null, TiktokVideoSend::getShopId, bo.getShopId());
		lqw.eq(bo.getActivityId() != null, TiktokVideoSend::getActivityId, bo.getActivityId());
		lqw.eq(bo.getStatus() != null, TiktokVideoSend::getStatus, bo.getStatus());
		return lqw;
	}

	@Override
	public Boolean insertByBo(TiktokVideoSendBo bo) {
		TiktokVideoSend add = BeanUtil.toBean(bo, TiktokVideoSend.class);
		validEntityBeforeSave(add);
		return save(add);
	}

	@Override
	public Boolean updateByBo(TiktokVideoSendBo bo) {
		TiktokVideoSend update = BeanUtil.toBean(bo, TiktokVideoSend.class);
		validEntityBeforeSave(update);
		return updateById(update);
	}

	/**
	 * 保存前的数据校验
	 *
	 * @param entity 实体类数据
	 */
	private void validEntityBeforeSave(TiktokVideoSend entity) {
		//TODO 做一些数据校验,如唯一约束
	}

	@Override
	public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
		if (isValid) {
			//TODO 做一些业务上的校验,判断是否需要校验
		}
		return removeByIds(ids);
	}


	@Override
	public Integer selectSendVideoCountByDate(Long shopId,Date date) {
		return baseMapper.selectSendVideoCountByDate(shopId,date);
	}

	@Override
	public List<String> selectSendTitleByCount(Long shopId,Integer limit) {
		return baseMapper.selectSendTitleByCount(shopId,limit);
	}

	@Override
	public TiktokAppVideoCountsVo selectAppVideoCounts(Long shopId) {
		return baseMapper.selectAppVideoCounts(shopId);
	}


}
