package com.qingyun.tiktok.service;

public interface ITiktokSearchService {

	void searchByUid(String uid);

	void searchByNickname(String nickname);


	void searchByKeywords(String keywords);
}
