package com.qingyun.tiktok.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("抖音用户业务查询对象")
public class TiktokUserQueryBo {

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;

    /**
     * 性别（0男 1女 2未知）
     */
    @ApiModelProperty(value = "性别（0男 1女 2未知）")
    private String gender;

    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    private String city;

    /**
     * 会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员
     */
    @ApiModelProperty(value = "会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员")
    private Integer memberLevel;

    /**
     * 0正常1禁用
     */
    @ApiModelProperty(value = "0正常1禁用")
    private Integer state;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间-开始")
    private Date createTimeStart;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间-开始")
    private Date createTimeEnd;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;
}
