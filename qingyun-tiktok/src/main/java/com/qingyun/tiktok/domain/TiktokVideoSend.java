package com.qingyun.tiktok.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 发送视频记录信息对象 tiktok_video_send
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tiktok_video_send")
public class TiktokVideoSend implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * $column.columnComment
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 抖音用户id
     */
    private Long tiktokUserId;

    /**
     * 抖音用户name
     */
    private String tiktokNickName;

    /**
     * 任务id 0为立刻发送的视频
     */
    private Long taskId;

    /**
     * 用户openid
     */
    private String openId;

    /**
     * 商家id
     */
    private Long shopId;

	/**合并后的视频ID*/
	private Long mergeId;

	/**
	 * 分享视频的ID
	 */
	private String shareId;

    /**
     * 视频唯一id
     */
    private String itemId;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 视频地址
     */
    private String videoPath;

    /**
     * 0未发布 1发布成功 2失败
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 点赞数量
     */
    private Long diggCount;

    /**
     * 用户下载数量
     */
    private Long downloadCount;

    /**
     * 用户转发数量
     */
    private Long forwardCount;

    /**
     * 用户播放数量
     */
    private Long playCount;

    /**
     * 用户分享数量
     */
    private Long shareCount;

    /**
     * 用户评论数量
     */
    private Long commentCount;

    /**
     * 迭代抓取次数 抓取10次
     */
    private Long iteration;

}
