package com.qingyun.tiktok.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("发送视频记录列表视图")
public class TiktokVideoSendListVo {

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "抖音用户name")
	private String tiktokNickName;

	@ApiModelProperty("商家名称")
	private String shopName;

	@ApiModelProperty("活动名称")
	private String activityName;

	@ApiModelProperty(value = "执行状态： 0未发布 1发布成功 2失败")
	private Integer status;

	@ApiModelProperty(value = "点赞数量")
	private Long diggCount;

	@ApiModelProperty(value = "用户下载数量")
	private Long downloadCount;

	@ApiModelProperty(value = "用户转发数量")
	private Long forwardCount;

	@ApiModelProperty(value = "用户播放数量")
	private Long playCount;

	@ApiModelProperty(value = "用户分享数量")
	private Long shareCount;

	@ApiModelProperty(value = "用户评论数量")
	private Long commentCount;

	@ApiModelProperty(value = "发送时间")
	private Date createTime;

	@ApiModelProperty("当前迭代抓取次数")
	private Long iteration;
}
