package com.qingyun.tiktok.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @Classname TiktokAppVideoCountsVo
 * @Author dyh
 * @Date 2021/9/25 11:54
 */
@Data
@ApiModel("视频数据对象")
public class TiktokAppVideoCountsVo {

	@ApiModelProperty("点赞量")
	private Integer diggCounts;

	@ApiModelProperty("下载量")
	private Integer downloadCounts;

	@ApiModelProperty("转发量")
	private Integer forwardCounts;

	@ApiModelProperty("播放量")
	private Integer playCounts;

	@ApiModelProperty("分享次数")
	private Integer shareCounts;

	@ApiModelProperty("评论条数")
	private Integer commentCounts;

}
