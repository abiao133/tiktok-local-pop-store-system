package com.qingyun.tiktok.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 【请填写功能名称】业务对象 tiktok_user
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("【请填写功能名称】业务对象")
public class TiktokUserBo extends BaseEntity {

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long id;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;

    /**
     * openid
     */
    @ApiModelProperty(value = "openid", required = true)
    @NotBlank(message = "openid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String openId;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /**
     * 性别（0男 1女 2未知）
     */
    @ApiModelProperty(value = "性别（0男 1女 2未知）")
    private String gender;

    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    private String city;

    /**
     * 国家
     */
    @ApiModelProperty(value = "国家")
    private String country;

    /**
     * 通用unionid
     */
    @ApiModelProperty(value = "通用unionid")
    private String unionId;

    /**
     * 会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员
     */
    @ApiModelProperty(value = "会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员", required = true)
    @NotNull(message = "会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer memberLevel;

    /**
     * 0正常1禁用
     */
    @ApiModelProperty(value = "0正常1禁用")
    private Integer state;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
