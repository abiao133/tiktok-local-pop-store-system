package com.qingyun.tiktok.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.qingyun.common.annotation.ExcelDictFormat;
import com.qingyun.common.convert.ExcelDictConvert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 【请填写功能名称】视图对象 tiktok_user
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("【请填写功能名称】视图对象")
@ExcelIgnoreUnannotated
public class TiktokUserVo {

	private static final long serialVersionUID = 1L;

	/**
     *  用户id
     */
	@ApiModelProperty("用户id")
	private Long id;

    /**
     * 昵称
     */
	@ExcelProperty(value = "昵称", index = 0)
	@ApiModelProperty("昵称")
	private String nickName;

    /**
     * openid
     */
	@ExcelProperty(value = "openid", index = 1)
	@ApiModelProperty("openid")
	private String openId;

    /**
     * 头像
     */
	@ExcelProperty(value = "头像", index = 2)
	@ApiModelProperty("头像")
	private String avatar;

    /**
     * 性别（0男 1女 2未知）
     */
	@ExcelProperty(value = "性别", converter = ExcelDictConvert.class, index = 3)
    @ExcelDictFormat(readConverterExp = "0=男,1=女,2=未知")
	@ApiModelProperty("性别（0男 1女 2未知）")
	private String gender;

    /**
     * 省
     */
	@ExcelProperty(value = "省", index = 4)
	@ApiModelProperty("省")
	private String province;

    /**
     * 市
     */
	@ExcelProperty(value = "市", index = 5)
	@ApiModelProperty("市")
	private String city;

    /**
     * 国家
     */
	@ExcelProperty(value = "国家", index = 6)
	@ApiModelProperty("国家")
	private String country;

    /**
     * 通用unionid
     */
	@ExcelProperty(value = "通用unionid", index = 7)
	@ApiModelProperty("通用unionid")
	private String unionId;

    /**
     * 会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员
     */
	@ExcelProperty(value = "会员等级", converter = ExcelDictConvert.class, index = 8)
	@ExcelDictFormat(readConverterExp = "1=普通会员,2=黄金会员,3=钻石会员,4=至尊会员")
	@ApiModelProperty("会员等级 1 普通会员 2黄金会员 3钻石会员 4至尊会员")
	private Integer memberLevel;

    /**
     * 0正常1禁用
     */
	@ExcelProperty(value = "0正常1禁用", converter = ExcelDictConvert.class, index = 9)
	@ExcelDictFormat(readConverterExp = "0=正常,1=禁用")
	@ApiModelProperty("0正常1禁用")
	private Integer state;
}
