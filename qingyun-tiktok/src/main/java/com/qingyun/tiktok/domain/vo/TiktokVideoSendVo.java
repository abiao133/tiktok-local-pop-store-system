package com.qingyun.tiktok.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 发送视频记录信息视图对象 tiktok_video_send
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Data
@ApiModel("发送视频记录信息视图对象")
@ExcelIgnoreUnannotated
public class TiktokVideoSendVo {

	private static final long serialVersionUID = 1L;

	/**
     *  $pkColumn.columnComment
     */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

    /**
     * 抖音用户id
     */
	@ExcelProperty(value = "抖音用户id")
	@ApiModelProperty("抖音用户id")
	private Long tiktokUserId;

    /**
     * 抖音用户name
     */
	@ExcelProperty(value = "抖音用户name")
	@ApiModelProperty("抖音用户name")
	private String tiktokNickName;

    /**
     * 任务id 0为立刻发送的视频
     */
	@ExcelProperty(value = "任务id 0为立刻发送的视频")
	@ApiModelProperty("任务id 0为立刻发送的视频")
	private Long taskId;

    /**
     * 用户openid
     */
	@ExcelProperty(value = "用户openid")
	@ApiModelProperty("用户openid")
	private String openId;

    /**
     * 商家id
     */
	@ExcelProperty(value = "商家id")
	@ApiModelProperty("商家id")
	private Long shopId;

    /**
     * 视频唯一id
     */
	@ExcelProperty(value = "视频唯一id")
	@ApiModelProperty("视频唯一id")
	private String itemId;

    /**
     * 活动id
     */
	@ExcelProperty(value = "活动id")
	@ApiModelProperty("活动id")
	private Long activityId;

    /**
     * 视频地址
     */
	@ExcelProperty(value = "视频地址")
	@ApiModelProperty("视频地址")
	private String videoPath;

    /**
     * 0未发布 1发布成功 2失败
     */
	@ExcelProperty(value = "0未发布 1发布成功 2失败")
	@ApiModelProperty("0未发布 1发布成功 2失败")
	private Integer status;

    /**
     * 点赞数量
     */
	@ExcelProperty(value = "点赞数量")
	@ApiModelProperty("点赞数量")
	private Long diggCount;

    /**
     * 用户下载数量
     */
	@ExcelProperty(value = "用户下载数量")
	@ApiModelProperty("用户下载数量")
	private Long downloadCount;

    /**
     * 用户转发数量
     */
	@ExcelProperty(value = "用户转发数量")
	@ApiModelProperty("用户转发数量")
	private Long forwardCount;

    /**
     * 用户播放数量
     */
	@ExcelProperty(value = "用户播放数量")
	@ApiModelProperty("用户播放数量")
	private Long playCount;

    /**
     * 用户分享数量
     */
	@ExcelProperty(value = "用户分享数量")
	@ApiModelProperty("用户分享数量")
	private Long shareCount;

    /**
     * 用户评论数量
     */
	@ExcelProperty(value = "用户评论数量")
	@ApiModelProperty("用户评论数量")
	private Long commentCount;

    /**
     * 迭代抓取次数 抓取10次
     */
	@ExcelProperty(value = "迭代抓取次数 抓取10次")
	@ApiModelProperty("迭代抓取次数 抓取10次")
	private Long Iteration;


}
