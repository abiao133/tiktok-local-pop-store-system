package com.qingyun.tiktok.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("发送视频记录信息查询对象")
public class TiktokVideoSendQueryBo {

	/**
	 * 商家id
	 */
	@ApiModelProperty(value = "商家id")
	private Long shopId;

	/**
	 * 活动id
	 */
	@ApiModelProperty(value = "活动id")
	private Long activityId;

	/**
	 * 0未发布 1发布成功 2失败
	 */
	@ApiModelProperty(value = "0未发布 1发布成功 2失败")
	private Integer status;

	/**
	 * 分页大小
	 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/**
	 * 当前页数
	 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
}
