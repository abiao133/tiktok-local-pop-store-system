package com.qingyun.tiktok.domain.bo;

import com.qingyun.common.core.domain.BaseEntity;
import com.qingyun.common.core.validate.AddGroup;
import com.qingyun.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 发送视频记录信息业务对象 tiktok_video_send
 *
 * @author qingyun
 * @date 2021-09-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("发送视频记录信息业务对象")
public class TiktokVideoSendBo extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 抖音用户id
     */
    @ApiModelProperty(value = "抖音用户id", required = true)
    @NotNull(message = "抖音用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long tiktokUserId;

    /**
     * 抖音用户name
     */
    @ApiModelProperty(value = "抖音用户name", required = true)
    @NotBlank(message = "抖音用户name不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tiktokNickName;

    /**
     * 任务id 0为立刻发送的视频
     */
    @ApiModelProperty(value = "任务id 0为立刻发送的视频")
    private Long taskId;

    /**
     * 用户openid
     */
    @ApiModelProperty(value = "用户openid", required = true)
    @NotBlank(message = "用户openid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String openId;

    /**
     * 商家id
     */
    @ApiModelProperty(value = "商家id")
    private Long shopId;

    /**
     * 视频唯一id
     */
    @ApiModelProperty(value = "视频唯一id", required = true)
    @NotBlank(message = "视频唯一id不能为空", groups = { AddGroup.class, EditGroup.class })
    private String itemId;

    /**
     * 活动id
     */
    @ApiModelProperty(value = "活动id")
    private Long activityId;

    /**
     * 视频地址
     */
    @ApiModelProperty(value = "视频地址")
    private String videoPath;

    /**
     * 0未发布 1发布成功 2失败
     */
    @ApiModelProperty(value = "0未发布 1发布成功 2失败", required = true)
    @NotNull(message = "0未发布 1发布成功 2失败不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 点赞数量
     */
    @ApiModelProperty(value = "点赞数量")
    private Long diggCount;

    /**
     * 用户下载数量
     */
    @ApiModelProperty(value = "用户下载数量")
    private Long downloadCount;

    /**
     * 用户转发数量
     */
    @ApiModelProperty(value = "用户转发数量")
    private Long forwardCount;

    /**
     * 用户播放数量
     */
    @ApiModelProperty(value = "用户播放数量")
    private Long playCount;

    /**
     * 用户分享数量
     */
    @ApiModelProperty(value = "用户分享数量")
    private Long shareCount;

    /**
     * 用户评论数量
     */
    @ApiModelProperty(value = "用户评论数量")
    private Long commentCount;

    /**
     * 迭代抓取次数 抓取10次
     */
    @ApiModelProperty(value = "迭代抓取次数 抓取10次")
    private Long Iteration;


    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
