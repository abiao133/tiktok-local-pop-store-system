package com.qingyun.tiktok.webhooks;

import lombok.Data;

/**
 * 抖音用户解除授权，推送事件给开发者APP
 */
@Data
public class WebhooksUnauthorize extends Webhooks {
	Content content ;

	public static class Content{
		String[] scopes;

		public String[] getScopes() {
			return scopes;
		}

		public void setScopes(String[] scopes) {
			this.scopes = scopes;
		}
	}
}
