package com.qingyun.tiktok.webhooks;

import lombok.Data;

@Data
public class Webhooks {
	private String event;
	private String client_key;
	private String from_user_id;
	private String to_user_id;

}
