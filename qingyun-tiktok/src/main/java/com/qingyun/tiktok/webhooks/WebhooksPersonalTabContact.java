package com.qingyun.tiktok.webhooks;

import lombok.Data;

/**
 * 抖音用户拨打当前抖音用户的个人主页智能电话
 */
@Data
public class WebhooksPersonalTabContact extends Webhooks {

}
