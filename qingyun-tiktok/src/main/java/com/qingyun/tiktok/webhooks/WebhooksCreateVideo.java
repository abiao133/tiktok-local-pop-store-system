package com.qingyun.tiktok.webhooks;

import lombok.Data;

/**
 * 用户使用开发者应用分享视频到抖音（携带分享id)
 */
@Data
public class WebhooksCreateVideo extends Webhooks {
	Content content ;

	public static class Content{
		private String item_id;//创建的视频id
		private String share_id;//标识分享的share_id

		public String getItem_id() {
			return item_id;
		}

		public void setItem_id(String item_id) {
			this.item_id = item_id;
		}

		public String getShare_id() {
			return share_id;
		}

		public void setShare_id(String share_id) {
			this.share_id = share_id;
		}
	}
}
