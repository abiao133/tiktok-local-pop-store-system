package com.qingyun.tiktok.webhooks;

import lombok.Data;

/**
 * 当前抖音用户收到的私信消息具体类型
 */
@Data
public class WebhooksReceiveMsg extends Webhooks {
	Content content ;

	public static class Content{
		String description;

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}
}
