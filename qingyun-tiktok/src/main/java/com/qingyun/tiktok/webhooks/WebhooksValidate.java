package com.qingyun.tiktok.webhooks;

import lombok.Data;

@Data
public class WebhooksValidate extends Webhooks {
	Content content ;

	public static class Content{
		String challenge;
		public String getChallenge(){
			return challenge;
		}
		public void setChallenge(String challenge){
			this.challenge = challenge;
		}
	}
}
