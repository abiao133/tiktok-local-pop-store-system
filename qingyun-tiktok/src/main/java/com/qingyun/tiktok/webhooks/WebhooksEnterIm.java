package com.qingyun.tiktok.webhooks;

import lombok.Data;

/**
 *
 * 抖音用户进入当前抖音用户的私信对话框
 */
@Data
public class WebhooksEnterIm extends Webhooks {
	Content content ;

	public static class Content{
		private String scene; //进入对话来源场景["video", "homepage"]
		private String object;//来源场景对应id（video对应视频id）
	}
}
