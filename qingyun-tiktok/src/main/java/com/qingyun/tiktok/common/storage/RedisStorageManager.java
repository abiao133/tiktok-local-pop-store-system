package com.qingyun.tiktok.common.storage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qingyun.common.core.redis.RedisCache;
import com.qingyun.common.exception.CustomException;
import com.qingyun.tiktok.common.bean.AccessToken;
import com.qingyun.tiktok.common.bean.ClientToken;
import com.qingyun.tiktok.common.bean.RefreshToken;
import com.qingyun.tiktok.common.bean.Ticket;
import com.qingyun.tiktok.common.config.DyOpenApiConfig;
import com.qingyun.tiktok.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;


import java.rmi.ServerException;
import java.util.concurrent.TimeUnit;

/**
 * redis配置存储管理器
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/9 14:02
 * @modified mdmbct
 * @since 1.0
 */
@Slf4j
public class RedisStorageManager extends BaseStorageManager {

	private final RedisCache redisCache;

	/**
	 * 构造函数 顺便将配置存储redis
	 */
	public RedisStorageManager(RedisCache redisCache, DyOpenApiConfig config) {
		super(config);
		this.redisCache = redisCache;
		saveOpenApiConfigToCache();
	}

	void saveOpenApiConfigToCache() {
		String s = JSONObject.toJSONString(openApiConfig);
		redisCache.setCacheObject(openApiConfig.getConfigCacheKey(), s);
	}

	/**
	 * 设置配置 同时保存配置至redis <p>
	 *
	 * @param config {@link DyOpenApiConfig}
	 */
	@Override
	public void setOpenApiConfig(DyOpenApiConfig config) {
		super.setOpenApiConfig(config);
		saveOpenApiConfigToCache();
	}

	public DyOpenApiConfig getOpenApiConfigFromCache() {
		return redisCache.getCacheObject(openApiConfig.getConfigCacheKey());
	}

	/**
	 * 保存AccessToken token过期后自动被redis删除
	 *
	 * @param openId open id
	 * @param token  AccessToken
	 */
	@Override
	public void saveAccessToken(String openId, AccessToken token) {
		redisCache.setCacheObject(accessTokenKey(openId), JSON.toJSONString(token));
		redisCache.expire(accessTokenKey(openId), token.getExpireIn(), TimeUnit.MILLISECONDS);

	}

	@Override
	public AccessToken getAccessToken(String openId) {
		String cacheObject = redisCache.getCacheObject(accessTokenKey(openId));
		return JSON.parseObject(cacheObject, AccessToken.class);
	}

	/**
	 * 保存RefreshToken token过期后自动被redis删除
	 *
	 * @param openId open id
	 * @param token  RefreshToken
	 */
	@Override
	public void saveRefreshToken(String openId, RefreshToken token) {
		String s = JSON.toJSONString(token);
		redisCache.setCacheObject(refreshTokenKey(openId), s);
		redisCache.expire(refreshTokenKey(openId), token.getExpireIn(), TimeUnit.MILLISECONDS);

	}

	@Override
	public RefreshToken getRefreshToken(String openId) throws CustomException {
		String cacheString = redisCache.getCacheObject(refreshTokenKey(openId));
		if (StringUtils.isEmpty(cacheString)) {
			throw new CustomException("请商家重新授权");
		}
		return JSON.parseObject(cacheString, RefreshToken.class);
	}

	@Override
	public void saveClientToken(ClientToken token) {
		redisCache.setCacheObject(clientTokenKey(), JSON.toJSONString(token));
		redisCache.expire(clientTokenKey(), token.getExpireIn(), TimeUnit.MILLISECONDS);
	}

	@Override
	public ClientToken getClientToken() {
		String cacheString = redisCache.getCacheObject(clientTokenKey());
		return JSON.parseObject(cacheString, ClientToken.class);
	}

	@Override
	public void saveTicket(Ticket ticket) {
		redisCache.setCacheObject(ticketKey(), JSON.toJSONString(ticket));
		redisCache.expire(ticketKey(),ticket.getExpireIn(), TimeUnit.MILLISECONDS);
	}
	@Override
	public Ticket getTicket() {
		String cacheString = redisCache.getCacheObject(ticketKey());
		return JSON.parseObject(cacheString, Ticket.class);
	}

}
