package com.qingyun.tiktok.common.http.apache;


import com.qingyun.tiktok.common.config.HttpConfig;
import com.qingyun.tiktok.common.http.HttpExecutor;
import com.qingyun.tiktok.common.http.HttpExecutorFactory;

/**
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/16 18:20
 * @modified mdmbct
 * @since 1.0
 */
public class ApacheHttpExecutorFactory implements HttpExecutorFactory {

    @Override
    public HttpExecutor createHttpExecutor(HttpConfig httpConfig) {
        return null;
    }
}
