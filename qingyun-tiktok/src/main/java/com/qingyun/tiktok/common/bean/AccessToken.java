package com.qingyun.tiktok.common.bean;


import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 接口调用凭证
 *
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/9 10:56
 * @modified mdmbct
 * @since 1.0
 */

public class AccessToken extends BaseToken{

    private static final long serialVersionUID = -8541238287096212480L;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public AccessToken(long takeEffect, long expireIn, String value) {
        super(takeEffect, expireIn, value);
    }

}
