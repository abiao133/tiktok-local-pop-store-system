package com.qingyun.tiktok.common.http.apache;



import com.qingyun.tiktok.common.enums.HttpClientType;
import com.qingyun.tiktok.common.enums.RequestBodyType;
import com.qingyun.tiktok.common.exception.ApiRequestFailedException;
import com.qingyun.tiktok.common.exception.InvalidRequestParamException;
import com.qingyun.tiktok.common.http.Head;
import com.qingyun.tiktok.common.http.HttpExecutor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/10 11:27
 * @modified mdmbct
 * @since 1.0
 */
@Slf4j
public class ApacheHttpExecutor implements HttpExecutor {


    @Override
    public HttpClientType getHttpType() {
        return HttpClientType.APACHE_HTTP;
    }


    @Override
    public <D> String executePost(Head[] heads, String url, D data, RequestBodyType requestBodyType) throws InvalidRequestParamException, ApiRequestFailedException {

        return null;
    }

    @Override
    public String executeGet(Head[] heads, String url) throws InvalidRequestParamException, ApiRequestFailedException {
        return null;
    }
}
