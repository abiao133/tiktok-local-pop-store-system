package com.qingyun.tiktok.common.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author mdmbct  mdmbct@outlook.com
 * @date 2021/3/9 10:57
 * @modified mdmbct
 * @since 1.0
 */

public abstract class BaseToken implements Serializable {

    private static final long serialVersionUID = 4870826807079258047L;

    /**
     * 生效时间 存入的ms
     */
    private final long takeEffect;

    /**
     * token过期时间 ms
     */
    protected final long expireIn;

    /**
     * token值
     */
    protected final String value;


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public long getTakeEffect() {
		return takeEffect;
	}

	public long getExpireIn() {
		return expireIn;
	}

	public String getValue() {
		return value;
	}

	public BaseToken(long takeEffect, long expireIn, String value) {
		this.takeEffect = takeEffect;
		this.expireIn = expireIn;
		this.value = value;
	}
}
