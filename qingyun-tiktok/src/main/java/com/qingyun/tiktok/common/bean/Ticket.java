package com.qingyun.tiktok.common.bean;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Getter
@RequiredArgsConstructor
public class Ticket implements Serializable {

		/**
		 * 过期时间 ms
		 */
		private final long expireIn;

		private final String value;
}
