package com.qingyun.tiktok.mapper;


import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokUserMapper extends BaseMapperPlus<TiktokUser> {

}
