package com.qingyun.tiktok.mapper;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingyun.common.core.mybatisplus.core.BaseMapperPlus;
import com.qingyun.tiktok.domain.TiktokVideoSend;
import com.qingyun.tiktok.domain.bo.TiktokVideoSendQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokAppVideoCountsVo;
import com.qingyun.tiktok.domain.vo.TiktokVideoSendListVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 发送视频记录信息Mapper接口
 *
 * @author qingyun
 * @date 2021-09-04
 */
public interface TiktokVideoSendMapper extends BaseMapperPlus<TiktokVideoSend> {

	/**
	 * 查询发送视频记录信息分页列表
	 */
    Page<TiktokVideoSendListVo> selectVideoSendList(@Param("page") Page<Object> page, @Param("query") TiktokVideoSendQueryBo query);

	/**
	 * 根据日期查询发送数量
	 * @param date
	 * @return
	 */
	Integer selectSendVideoCountByDate(@Param("shopId") Long shopId,@Param("date") Date date);




	/**
	 * 查询limit条发送人记录
	 * @param shopId
	 * @param limit
	 * @return
	 */
	List<String> selectSendTitleByCount(@Param("shopId") Long shopId,@Param("limit") Integer limit);


	/**
	 * 根据商户查看视频数据
	 * @param shopId
	 * @return
	 */
	TiktokAppVideoCountsVo selectAppVideoCounts(@Param("shopId") Long shopId);


}
