package com.qingyun.tiktok.util;

import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.tiktok.basis.response.user.UserOpenInfoRes;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.response.auth.AccessTokenRes;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Classname TikTokUserByCodeUtil
 * @Author dyh
 * @Date 2021/9/9 9:44
 */
@Component
public class TikTokUserByCodeUtil {

	@Autowired
	private DyBasisService basisService;

	@Autowired
	private MapperFacade mapperFacade;

	public  TiktokUser getTiktokUser(String code){
		AccessTokenRes.AccessTokenResData accessTokenResData = basisService.getAuth2Service().authAccessToken(code, ApiPlatform.DOU_YIN);
		UserOpenInfoRes.GetUserOpenInfoResData userOpenInfo = basisService.getUserManagerService().getUserOpenInfo(accessTokenResData.getOpenId(), ApiPlatform.DOU_YIN);
		TiktokUser tiktokUser = mapperFacade.map(userOpenInfo, TiktokUser.class);
		return tiktokUser;
	}

}
