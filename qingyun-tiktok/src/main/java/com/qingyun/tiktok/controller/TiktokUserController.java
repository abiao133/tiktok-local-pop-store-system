package com.qingyun.tiktok.controller;

import com.qingyun.common.annotation.Log;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.core.page.TableDataInfo;
import com.qingyun.common.enums.BusinessType;
import com.qingyun.common.utils.poi.ExcelUtil;
import com.qingyun.security.controller.BaseController;
import com.qingyun.tiktok.basis.response.video.VideoPoiRes;
import com.qingyun.tiktok.basis.service.interfaces.DyBasisService;
import com.qingyun.tiktok.common.enums.ApiPlatform;
import com.qingyun.tiktok.common.enums.ErrorCode;
import com.qingyun.tiktok.common.response.auth.ClientTokenRes;
import com.qingyun.tiktok.domain.bo.TiktokUserQueryBo;
import com.qingyun.tiktok.domain.vo.TiktokUserVo;
import com.qingyun.tiktok.service.ITiktokUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author qingyun
 * @date 2021-09-04
 */
@Validated
@Api( tags = {"^平台端 抖音用户管理"})
@AllArgsConstructor
@RestController
@RequestMapping("/system/tkUser")
public class TiktokUserController extends BaseController {

    private final ITiktokUserService iTiktokUserService;


	private final DyBasisService dyBasisService;

    /**
     * 查询抖音用户列表
     */
    @ApiOperation("^查询抖音用户列表 system:tiktokuser:selectList")
    @PreAuthorize("@ss.hasPermi('system:tiktokuser:selectList')")
    @GetMapping("/list")
    public TableDataInfo<TiktokUserVo> list(@Validated TiktokUserQueryBo bo) {
        return iTiktokUserService.queryPageList(bo);
    }


    @ApiOperation("导出抖音用户列表 system:tiktokuser:export")
    @PreAuthorize("@ss.hasPermi('system:tiktokuser:export')")
    @Log(title = "导出抖音用户列表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TiktokUserQueryBo bo, HttpServletResponse response) {
        List<TiktokUserVo> list = iTiktokUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "抖音用户数据", TiktokUserVo.class, response);
    }

	 /**
     * 获取【请填写功能名称】详细信息
     */
    @ApiOperation("获取抖音用户详细信息 system:tiktokuser:query")
    @PreAuthorize("@ss.hasPermi('system:tiktokuser:query')")
    @GetMapping("/{id}")
    public AjaxResult<TiktokUserVo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(iTiktokUserService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
//    @ApiOperation("新增抖音用户")
//    @PreAuthorize("@ss.hasPermi('system:tiktokuser:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @RepeatSubmit()
//    @PostMapping()
//    public AjaxResult<Void> add(@Validated(AddGroup.class) @RequestBody TiktokUserBo bo) {
//        return toAjax(iTiktokUserService.insertByBo(bo) ? 1 : 0);
//    }

//    /**
//     * 修改【请填写功能名称】
//     */
//    @ApiOperation("修改抖音用户 system:tiktokuser:edit")
//    @PreAuthorize("@ss.hasPermi('system:tiktokuser:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @RepeatSubmit()
//    @PutMapping()
//    public AjaxResult<Void> edit(@Validated(EditGroup.class) @RequestBody TiktokUserBo bo) {
//        return toAjax(iTiktokUserService.updateByBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @ApiOperation("删除抖音用户 system:tiktokuser:remove")
//    @PreAuthorize("@ss.hasPermi('system:tiktokuser:remove')")
//    @Log(title = "【请填写功能名称】" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iTiktokUserService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }


	@GetMapping("/getShareId")
    public AjaxResult<Void> getShareId() {
		ClientTokenRes.ClientTokenResData clientTokenResData = dyBasisService.getAuth2Service().authClientToken(ApiPlatform.DOU_YIN);
		if(clientTokenResData.getErrorCode()== ErrorCode.SUCCESS.getValue()){
			String clientToken = clientTokenResData.getAccessToken();
			String shareId = dyBasisService.getVideoService().getShareId(clientToken);
			return AjaxResult.success("获取shareId成功");
		}
        return AjaxResult.error("获取shareId失败");
    }

}
