package com.qingyun.security.service;


import com.qingyun.security.enums.App;
import com.qingyun.security.model.shop.AuthoAppConnect;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface MyUserDetailService  extends UserDetailsService {

    /**
     * 获取前端登陆的用户信息
     *
     * @param app 所登陆的应用
     * @param bizUserId openId
     * @return UserDetails
     * @throws
     */
    AuthoAppConnect loadUserByAppIdAndBizUserId(App app, String bizUserId);

    /**
     * 如果必要的话，插入新增用户
     * @param
     */
    void insertUserIfNecessary(AuthoAppConnect authoAppConnect);
}
