package com.qingyun.security.enums;


public enum App {
    MINIAPP(1),
    MP(2),
    QQ(3),
    WEIBO(4);
   private  Integer num;

    public Integer value() {
        return num;
    }

    App(Integer num){
        this.num = num;
    }

    public static App instance(Integer value) {
        App[] enums = values();
        for (App statusEnum : enums) {
            if (statusEnum.value().equals(value)) {
                return statusEnum;
            }
        }
        return null;
    }
}
