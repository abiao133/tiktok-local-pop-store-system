package com.qingyun.security.enums;

import java.util.Arrays;

public enum LoginType {

    PHONE(1),PASSWORD(2) ;
    private Integer type;

    LoginType(Integer type) {
        this.type = type;
    }

    public static LoginType selectLoginType(Integer type){
       return Arrays.stream(LoginType.values()).filter((k)->k.type.equals(type)).findFirst().orElse(LoginType.PHONE);
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
