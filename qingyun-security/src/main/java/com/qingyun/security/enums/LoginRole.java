package com.qingyun.security.enums;

import com.qingyun.common.core.domain.model.LoginUser;
import com.qingyun.security.model.api.wxmp.AppUser;
import com.qingyun.security.model.tenant.TenantLoginUser;
import com.qingyun.security.model.tiktok.TikTokLoginUser;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;

public enum LoginRole {

    SYS(1), //系统登录
    TENANT(2), //租户账号密码登陆
    TENANTWX(3), //租户微信登陆
    APP(4),//第三方登陆
	TIKTOK(5),//抖音登陆
	WXMALOGIN(6);

    private Integer type;



	public static LoginRole getLoginRoleType(Integer key){
        LoginRole [] cmdTypes = LoginRole .values();
        LoginRole  result = Arrays.asList(cmdTypes).stream()
                .filter(alarmGrade -> alarmGrade.getType().equals(key))
                .findFirst().orElse(null);
        return result;
    }
    LoginRole(Integer type) {
        this.type = type;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
