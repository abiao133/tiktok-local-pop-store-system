package com.qingyun.security.handle;




import com.alibaba.fastjson.JSON;
import com.qingyun.common.core.domain.AjaxResult;
import com.qingyun.common.utils.ServletUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 *
 * @author dangyonghang
 */
public abstract class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable
{
    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
    {
        ServletUtils.renderString(response, JSON.toJSONString(callback(request,e)));
    }

    public abstract AjaxResult callback(HttpServletRequest request, AuthenticationException e);
}
