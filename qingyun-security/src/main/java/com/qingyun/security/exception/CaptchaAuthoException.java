package com.qingyun.security.exception;

import org.springframework.security.core.AuthenticationException;

public class CaptchaAuthoException extends AuthenticationException {

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


    public CaptchaAuthoException(Integer code,String msg) {
        super(msg);
        this.msg=msg;
        this.code=code;
    }
}
