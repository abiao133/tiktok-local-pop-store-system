package com.qingyun.security.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthoException extends AuthenticationException {


    private String msg;


    public String getMsg() {
        return msg;
    }


    public AuthoException( String msg) {
        super(msg);
        this.msg=msg;
    }
}
