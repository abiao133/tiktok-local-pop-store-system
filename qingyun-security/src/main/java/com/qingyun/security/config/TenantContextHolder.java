package com.qingyun.security.config;


import com.qingyun.security.enums.LoginRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//登陆状态判断维持
public class TenantContextHolder
{
    public static final Logger log = LoggerFactory.getLogger(TenantContextHolder.class);

    /**
     * 使用ThreadLocal维护变量，ThreadLocal为每个使用该变量的线程提供独立的变量副本，
     *  所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本。
     */
    private static final ThreadLocal<Integer> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 设置登陆角色
     */
    public static void setTenantType(LoginRole loginRole)
    {
        log.info("切换到{}角色", loginRole.getType());
        CONTEXT_HOLDER.set(loginRole.getType());
    }

    /**
     * 获得数据源的变量
     */
    public static Integer getTenantType()
    {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清空数据源变量
     */
    public static void clearTenantType()
    {
        CONTEXT_HOLDER.remove();
    }
}
