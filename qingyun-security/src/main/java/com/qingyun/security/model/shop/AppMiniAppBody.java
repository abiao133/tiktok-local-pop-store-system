package com.qingyun.security.model.shop;

import lombok.Data;

/**
 * @Classname AppMiniAppBody
 * @Author dyh
 * @Date 2021/6/23 9:24
 */
@Data
public class AppMiniAppBody {

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 微信code
     */
    private String wxcode;

    /**
     * 验证码
     */
    private String code;

	/**
	 * 用户名称
	 */
	private String userName;

	/**
	 * 用户密码
	 */
	private String passWord;

    /**
     * 1 手机号登陆 2 微信code登陆
     */
    private Integer loginType;

}
