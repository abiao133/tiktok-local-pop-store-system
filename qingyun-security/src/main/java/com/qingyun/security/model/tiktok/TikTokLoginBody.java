package com.qingyun.security.model.tiktok;

import lombok.Data;

/**
 * @Classname TikTokLoginBody
 * @Author dyh
 * @Date 2021/9/8 15:43
 */
@Data
public class TikTokLoginBody {
	private String code;
	private Integer shopId;
}
