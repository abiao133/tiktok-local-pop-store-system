package com.qingyun.security.model.tenant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qingyun.common.core.domain.entity.TenantEnterprise;
import com.qingyun.common.core.domain.entity.TenantEntprisUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * 企业用户身份权限
 *
 * @author dangyonghang
 */
@Data
public class TenantLoginUser implements UserDetails
{
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间F
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录地点
     */
    private String loginLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 当前企业
     */
    private TenantEnterprise tenantEnterprise;

    /**
     * 当前企业id
     */
    private Long PdEnterpriseId;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    private String openid;

    /**
     * 用户信息
     */
    private TenantEntprisUser tenantEntprisUser;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }



    public TenantLoginUser()
    {
    }

    public TenantLoginUser(TenantEntprisUser tenantEntprisUser, Set<String> permissions)
    {
        this.tenantEntprisUser = tenantEntprisUser;
        this.permissions = permissions;
    }

    @JsonIgnore
    @Override
    public String getPassword()
    {
        return tenantEntprisUser.getPassWord();
    }

    @Override
    public String getUsername()
    {
        return tenantEntprisUser.getUserName();
    }

    /**
     * 账户是否未过期,过期无法验证
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    /**
     * 指定用户是否解锁,锁定的用户无法进行身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    /**
     * 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * 是否可用 ,禁用的用户不能身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isEnabled()
    {
        return true;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return null;
    }
}
