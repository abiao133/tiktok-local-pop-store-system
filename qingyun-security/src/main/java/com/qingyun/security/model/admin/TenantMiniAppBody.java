package com.qingyun.security.model.admin;

import lombok.Data;

/**
 * @Classname MiniappBody
 * @Author dyh
 * @Date 2021/6/2 15:26
 */
@Data
public class TenantMiniAppBody {
    private String code;
    private String phone;
    private String userName;
    private String password;
    private String checkCode;
    /**
     * 1 用户名手机号密码登陆 2 手机验证码登陆
     */
    private Integer loginType;
}
