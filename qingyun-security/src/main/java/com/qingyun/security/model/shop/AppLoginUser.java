package com.qingyun.security.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qingyun.common.core.domain.entity.SysUser;
import com.qingyun.common.core.domain.model.TiktokProxyVo;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Classname AppLoginUser
 * @Author dyh
 * @Date 2021/9/16 18:26
 */
@Data
@NoArgsConstructor
public class AppLoginUser  implements UserDetails {

	private static final long serialVersionUID = 1L;
	/**
	 * 用户唯一标识
	 */

	private String openId;

	/**
	 * 用户登录token
	 */

	private String token;

	/**
	 * 登录时间
	 */
	private Long loginTime;

	/**
	 * 过期时间
	 */

	private Long expireTime;

	private TiktokShopVo shop;

	private TiktokProxyVo proxy;

	/**
	 * 权限列表 前端动态渲染 和房东设置权限同步
	 */
	private Set<String> permissions;


	private SysUser sysUser;


	public AppLoginUser(SysUser sysUser,Set<String> permissions ) {
		this.permissions = permissions;
		this.sysUser = sysUser;
	}

	@JsonIgnore
	@Override
	public String getPassword()
	{
		return  sysUser.getPassword();
	}

	@Override
	public String getUsername()
	{
		return sysUser.getUserName();
	}

	/**
	 * 账户是否未过期,过期无法验证
	 */
	@JsonIgnore
	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	/**
	 * 指定用户是否解锁,锁定的用户无法进行身份验证
	 *
	 * @return
	 */
	@JsonIgnore
	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	/**
	 * 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
	 *
	 * @return
	 */
	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	/**
	 * 是否可用 ,禁用的用户不能身份验证
	 *
	 * @return
	 */
	@JsonIgnore
	@Override
	public boolean isEnabled()
	{
		return true;
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return null;

	}

}
