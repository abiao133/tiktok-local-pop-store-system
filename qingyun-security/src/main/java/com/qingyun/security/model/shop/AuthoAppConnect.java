package com.qingyun.security.model.shop;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 第三方用户数据对象 autho_app_connect
 *
 * @author dangyonghang
 * @date 2021-06-02
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("autho_app_connect")
public class AuthoAppConnect implements Serializable {

private static final long serialVersionUID=1L;


    /** 0管家 1普通用户 */
    @TableId(value = "id")
        private Long id;

    /** 用户表用户id */
        private Long userId;

    /** 用户名称（第三方） */
        private String nickName;

    /** 用户类型、1微信公众 2微信小程序，3qq登陆，4，微博等 */
        private Integer userType;

    /** 0管家 1普通用户 */
        private Integer entryType;

    /** 用户头像 */
        private String imageUrl;

    /** 第三方open_id */
        private String openId;

    /** 第三方union_id */
        private String unionId;

}
