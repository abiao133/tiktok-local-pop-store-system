package com.qingyun.security.model.api.wxmp;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户对象 autho_app_user
 *
 * @author dangyonghang
 * @date 2021-06-23
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("autho_app_user")
public class AuthoAppUser implements Serializable {

private static final long serialVersionUID=1L;


      /** ID */
        @TableId(value = "user_id")
        private Integer userId;

    /** 用户昵称 */
        private String nickName;

    /** 真实姓名 */
        private String realName;

    /** 用户邮箱 */
        private String userMail;

    /** 登录密码 */
        private String loginPassword;

    /** 支付密码 */
        private String payPassword;

    /** 手机号码 */
        private String userMobile;

    /** 修改时间 */

            @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifyTime;

    /** 注册时间 */

            @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userRegtime;

    /** 注册IP */
        private String userRegip;

    /** 最后登录时间 */

            @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userLasttime;

    /** 最后登录IP */
        private String userLastip;

    /** 备注 */
        private String userMemo;

    /** M(男) or F(女) */
        private String sex;

    /** 例如：2009-11-27 */
        @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    /** 头像图片路径 */
        private String pic;

    /** 状态 1 正常 0 无效 */
        private Long status;

    /** 用户积分 */
        private Long score;

}
