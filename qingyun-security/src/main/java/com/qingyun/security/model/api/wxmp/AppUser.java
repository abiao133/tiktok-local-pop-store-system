package com.qingyun.security.model.api.wxmp;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qingyun.security.model.api.wxmp.AuthoAppUser;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Data
public class AppUser implements UserDetails {

    private static final long serialVersionUID = 1L;


    /**
     * 用户唯一标识
     */

    private String openId;

    /**
     * 用户登录token
     */

    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */

    private Long expireTime;


    /**
     * 用户头像 第三方头像
     */

    private String imageUrl;

    /**
     * 权限列表 前端动态渲染 和房东设置权限同步
     */
    private Set<String> permissions;

    private AuthoAppUser authoAppUser;

    public AppUser(AuthoAppUser authoAppUser, Set<String> permissions) {
        this.permissions = permissions;
        this.authoAppUser = authoAppUser;
    }



    @JsonIgnore
    @Override
    public String getPassword()
    {
        return  null;
    }

    @Override
    public String getUsername()
    {
        return authoAppUser.getUserMobile();
    }

    /**
     * 账户是否未过期,过期无法验证
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    /**
     * 指定用户是否解锁,锁定的用户无法进行身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    /**
     * 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * 是否可用 ,禁用的用户不能身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isEnabled()
    {
        return true;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return null;
    }
}
