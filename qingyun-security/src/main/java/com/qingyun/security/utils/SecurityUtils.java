package com.qingyun.security.utils;


import cn.hutool.http.HttpStatus;
import com.qingyun.common.core.domain.entity.SysUser;
import com.qingyun.common.core.domain.model.LoginUser;
import com.qingyun.common.core.domain.model.TiktokProxyVo;
import com.qingyun.common.core.domain.model.TiktokShopVo;
import com.qingyun.common.core.domain.model.TiktokUser;
import com.qingyun.common.exception.CustomException;
import com.qingyun.security.model.api.wxmp.AppUser;
import com.qingyun.security.model.shop.AppLoginUser;
import com.qingyun.security.model.tenant.TenantLoginUser;
import com.qingyun.security.model.tiktok.TikTokLoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 安全服务工具类
 *
 * @author ruoyi
 */
public class SecurityUtils
{




    /**
     * 获取系统用户
     **/
    public static LoginUser getSysLoginUser()
    {
            Object principal = getAuthentication().getPrincipal();
            if(principal instanceof LoginUser) {
                return (LoginUser) getAuthentication().getPrincipal();
            }else{
                throw new CustomException("无法获取其他系统用户", HttpStatus.HTTP_UNAUTHORIZED);
            }
    }

    public static TikTokLoginUser getTikTokLoginUser(){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof TikTokLoginUser) {
			return (TikTokLoginUser) getAuthentication().getPrincipal();
		}else{
			throw new CustomException("无法获取其他系统用户", HttpStatus.HTTP_UNAUTHORIZED);
		}
	}

	public static AppLoginUser getAppLoginUser(){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof AppLoginUser) {
			return (AppLoginUser) getAuthentication().getPrincipal();
		}else{
			throw new CustomException("无法获取其他系统用户", HttpStatus.HTTP_UNAUTHORIZED);
		}
	}

	public static TiktokShopVo getShop(){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof LoginUser) {
			return ((LoginUser) getAuthentication().getPrincipal()).getShop();
		}else if(principal instanceof AppLoginUser){
			return ((AppLoginUser) getAuthentication().getPrincipal()).getShop();
		}
		return null;
	}

	public static TiktokProxyVo getProxy(){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof LoginUser) {
			return ((LoginUser) getAuthentication().getPrincipal()).getProxy();
		}else if(principal instanceof AppLoginUser){
			if (getLoginUser("01")) {
				return ((AppLoginUser) getAuthentication().getPrincipal()).getProxy();
			}else{
				throw new CustomException("你还不是代理", HttpStatus.HTTP_UNAUTHORIZED);
			}

		}
		return null;
	}

	public static SysUser getUser(){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof LoginUser) {
			return ((LoginUser) getAuthentication().getPrincipal()).getUser();
		}else if(principal instanceof AppLoginUser){
			return ((AppLoginUser) getAuthentication().getPrincipal()).getSysUser();
		}
		return null;
	}

	/**
     * 获取系统用户名称
     * @return
     */
    public static String getUsername()
    {
        try
        {
            return getSysLoginUser().getUsername();
        }
        catch (Exception e)
        {
            throw new CustomException("获取用户账户异常", HttpStatus.HTTP_UNAUTHORIZED);
        }
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication()
    {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword 真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

	/**
	 * 是否是系统管理员
	 * @return
	 */
	public static boolean isManager(){
    	return getLoginUser("00");
	}

	public static  boolean getLoginUser(String userType){
		Object principal = getAuthentication().getPrincipal();
		if(principal instanceof LoginUser) {
			return ((LoginUser) getAuthentication().getPrincipal()).getUser().getUserType().equals(userType);
		}else if(principal instanceof AppLoginUser){
			return ((AppLoginUser) getAuthentication().getPrincipal()).getSysUser().getUserType().equals(userType);
		}
		return false;
	}


	/**
	 * 是否是代理商
	 * @return
	 */
	public static boolean isProxy(){
		return getLoginUser("01");
	}

	public static boolean isShop(){
		return getLoginUser("01")||getLoginUser("02");
	}

}
